package app.cleaner.tw.cleanerapp.cleaner.cleancase;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;
import app.cleaner.tw.cleanerapp.backend.drfapi.builder.QueriesBuilder;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.PageNumberPagination;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.common.DividerItemDecoration;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivitySearchListBinding;
import app.cleaner.tw.cleanerapp.helper.GSONUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchResultActivity extends BaseActivity {

    private ActivitySearchListBinding binding;
    List<CleanService> caseDataList = new ArrayList<>();

    private QueriesBuilder queriesBuilder;
    private PageNumberPagination<CleanService> pagination;

    public static void start(Context context, QueriesBuilder queriesBuilder, PageNumberPagination<CleanService> pagination) {
        Intent starter = new Intent(context, SearchResultActivity.class);
        starter.putExtra("queriesBuilder", queriesBuilder);
        starter.putExtra("pagination", pagination);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBundle();
        EventBus.getDefault().register(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search_list);
        setUpToolBar();
        initViews();

        if (pagination != null)
            appendMockData(pagination);
    }

    private void getBundle() {
        if (getIntent().getExtras() != null) {
            queriesBuilder = (QueriesBuilder) getIntent().getSerializableExtra("queriesBuilder");
            pagination = (PageNumberPagination<CleanService>) getIntent().getSerializableExtra("pagination");
        }
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("案件列表");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private void initViews() {

        LinearLayoutManager lm = new LinearLayoutManager(this);
        lm.setOrientation(LinearLayoutManager.VERTICAL);

        SearchResultAdapter adapter = new SearchResultAdapter(caseDataList);
        adapter.setOnItemClickListener(new SearchResultAdapter.ItemClickListener() {
            @Override
            public void onItemClickListener(View view, int position) {
                CaseInfoActivity.start(activity, caseDataList.get(position), position);
            }
        });

        binding.recyclerView.addItemDecoration(new DividerItemDecoration(this));
        binding.recyclerView.setLayoutManager(lm);
        binding.recyclerView.setAdapter(adapter);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpdateSearchResult(UpdateSearchResultEvent event) {
        caseDataList.set(event.getPosition(), event.getCleanService());
        binding.recyclerView.getAdapter().notifyItemChanged(event.getPosition());
    }

    private void loadNextPage() {
        final KProgressHUD hud = KProgressHUD.create(activity).show();
        API.sharedService().getAllService(
                API.sharedAuthorizationToken(),
                queriesBuilder.pagination(pagination.nextPage()).create()
        ).enqueue(new Callback<PageNumberPagination<CleanService>>() {
            @Override
            public void onResponse(Call<PageNumberPagination<CleanService>> call, Response<PageNumberPagination<CleanService>> response) {
                hud.dismiss();
                if (response.isSuccessful()) {
                    pagination = response.body();
                    appendMockData(pagination);
                }
            }

            @Override
            public void onFailure(Call<PageNumberPagination<CleanService>> call, Throwable t) {
                hud.dismiss();
                Logger.e(t.getLocalizedMessage());
            }
        });
    }

    private void appendMockData(PageNumberPagination<CleanService> pagination) {
        for (CleanService cleanService : pagination.getResults()) {
            Logger.json(GSONUtils.toJson(cleanService));
        }
        caseDataList.addAll(pagination.getResults());

        binding.recyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
