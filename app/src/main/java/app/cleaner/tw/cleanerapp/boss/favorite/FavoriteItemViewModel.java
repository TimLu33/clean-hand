package app.cleaner.tw.cleanerapp.boss.favorite;

import android.databinding.ObservableField;

import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.CleanerProfile;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.SimpleUser;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.Rating;


public class FavoriteItemViewModel {

    public final ObservableField<String> headImage = new ObservableField<>();
    public final ObservableField<String> cleanerName = new ObservableField<>();
    public final ObservableField<String> hireCommend = new ObservableField<>();
    public final ObservableField<Integer> rating = new ObservableField<>();
    public final ObservableField<String> recommend = new ObservableField<>();


    public FavoriteItemViewModel(FavoriteActivity.FavoriteItem item) {
        Rating r = item.getRating();
        CleanerProfile cleanerProfile = item.getCleaner().getCleaner_profile();
        headImage.set(cleanerProfile.getHead_image() != null ? cleanerProfile.getHead_image().toString() : "");
        cleanerName.set(cleanerProfile.getDisplay_name() != null ? cleanerProfile.getDisplay_name() : "");

        // 我也不知道這星星是要顯示僱主的評價還是平均評價
        if (r != null) {
            hireCommend.set(r.getComment());
            rating.set(r.getRating());
        } else {
            hireCommend.set("您尚未評價此清潔手");
            rating.set(0);
        }

        recommend.set(cleanerProfile.getRecommendation() != null ? cleanerProfile.getRecommendation() : "");
    }
}
