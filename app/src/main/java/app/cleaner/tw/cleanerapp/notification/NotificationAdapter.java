package app.cleaner.tw.cleanerapp.notification;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.Notification;
import app.cleaner.tw.cleanerapp.boss.favorite.FavoriteAdapter;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> implements View.OnClickListener {

    private List<Notification> notificationList;

    public interface ItemClickListener {
        void onItemClickListener(View view, int position);
    }

    private ItemClickListener listener;


    public NotificationAdapter(List<Notification> notificationList) {
        this.notificationList = notificationList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_text_item, parent, false);
        view.setOnClickListener(this);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setTag(position);
        if (!notificationList.get(position).isRead()) {
            holder.itemView.setBackgroundColor(Color.parseColor("#FFF1EE"));
        } else {
            holder.itemView.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }
        holder.tvText.setText(notificationList.get(position).getMessage());
    }

    public void setOnItemClickListener(ItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }

    @Override
    public void onClick(View view) {
        if (listener != null) {
            listener.onItemClickListener(view, (int) view.getTag());
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvText;

        public ViewHolder(View itemView) {
            super(itemView);
            tvText = itemView.findViewById(R.id.tv_text);
        }
    }

}
