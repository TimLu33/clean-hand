package app.cleaner.tw.cleanerapp.cleaner.cleancase;

import android.databinding.BindingAdapter;
import android.databinding.ObservableField;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceQA;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChatItemViewModel {

    public final ObservableField<String> name = new ObservableField<>();
    public final ObservableField<String> picUrl = new ObservableField<>();
    public final ObservableField<String> content = new ObservableField<>();

    public ChatItemViewModel(ServiceQA model) {
        name.set(model.getSender_name());
        if (model.getSender_head_image() != null)
            picUrl.set(model.getSender_head_image().toExternalForm());
        content.set(model.getContent());
    }

    @BindingAdapter({"peopleSrc"})
    public static void loadPeopleImage(CircleImageView view, String url) {
        if (url != null && !url.isEmpty()) {
            Glide.with(view.getContext())
                    .load(url)
                    .apply(new RequestOptions().centerCrop().error(R.drawable.ic_people))
                    .into(view);
        }
    }
}
