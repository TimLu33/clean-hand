package app.cleaner.tw.cleanerapp.boss.search;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.view.View;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.CleanerProfile;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceType;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.boss.search.event.OnDealSuccessEvent;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityDealBinding;

public class DealSuccessActivity extends BaseActivity {

    private static final String CLEANER_PROFILE = "cleanerProfile";
    private ActivityDealBinding binding;
    private ViewModel viewModel;
    private CleanerProfile profile;

    public static void start(Context context, CleanerProfile profile) {
        Intent starter = new Intent(context, DealSuccessActivity.class);
        starter.putExtra(CLEANER_PROFILE, profile);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBundle();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_deal);
        setUpToolBar();
        viewModel = new ViewModel(profile);
        binding.setViewModel(viewModel);
        binding.setPresenter(new Presenter());

        //成交finish 堆疊的頁面
        EventBus.getDefault().post(new OnDealSuccessEvent());

    }

    private void getBundle() {
        if (getIntent().getExtras() != null) {
            profile = (CleanerProfile) getIntent().getSerializableExtra(CLEANER_PROFILE);
        }
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("成交");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private String getProvideServiceString(List<ServiceType> serviceList) {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < serviceList.size(); i++) {
            if (i != serviceList.size() -1) {
                s.append(serviceList.get(i).getName()).append("、");
            } else {
                s.append(serviceList.get(i).getName());
            }
        }
        return s.toString();
    }

    public class ViewModel {
        public final ObservableField<String> coverPhoto = new ObservableField<>();
        public final ObservableField<String> headImage = new ObservableField<>();
        public final ObservableField<String> cleanerName = new ObservableField<>();
        public final ObservableField<Integer> rating = new ObservableField<>();
        public final ObservableField<String> recommendation = new ObservableField<>();
        public final ObservableField<Integer> salary = new ObservableField<>();
        public final ObservableField<String> provideService = new ObservableField<>();

        public ViewModel(CleanerProfile profile) {
            if (profile.getCover_photos() != null && !profile.getCover_photos().isEmpty()) {
                coverPhoto.set(profile.getCover_photos().get(0).getImage().toString());
            }

            if (profile.getHead_image() != null ) {
                headImage.set(profile.getHead_image().toString());
            }

            if (profile.getDisplay_name() != null) {
                cleanerName.set(profile.getDisplay_name());
            }

            rating.set((int) profile.getAvg_rating());
            recommendation.set(profile.getRecommendation());
            salary.set(profile.getExpected_salary());
            provideService.set(getProvideServiceString(profile.getProvide_service_types()));
        }
    }

    public class Presenter implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.root_view) {
                finish();
            }
        }
    }
}
