package app.cleaner.tw.cleanerapp.forgot;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.AuthTokenBean;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.ApiError;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityForgetStep2Binding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ForgotPwdStep2Activity extends BaseActivity {

    private static final String REG_TYPE = "regType";
    private static final String PHONE = "phone";

    private ActivityForgetStep2Binding binding;

    private ViewModel viewModel;

    private Integer regType;
    private String phone;

    public static void start(Context context, int regType, String phone) {
        Intent starter = new Intent(context, ForgotPwdStep2Activity.class);
        starter.putExtra(REG_TYPE, regType);
        starter.putExtra(PHONE, phone);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBundle();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forget_step2);
        setUpToolBar();
        viewModel = new ViewModel(regType);
        binding.setViewModel(viewModel);
        binding.setPresenter(new Presenter());
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("忘記密碼");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private void getBundle() {
        if (getIntent().getExtras() != null) {
            regType = getIntent().getIntExtra(REG_TYPE, 0);
            phone = getIntent().getStringExtra(PHONE);
        }
    }

    public class ViewModel {

        public final ObservableField<Integer> regType = new ObservableField<>();

        public final ObservableField<String> verifyCode = new ObservableField<>();

        ViewModel(Integer regType) {
            this.regType.set(regType);
            verifyCode.set("");
        }

    }

    public class Presenter implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if (viewModel.verifyCode.get().isEmpty()) {
                Toast.makeText(activity, "請輸入驗證碼", Toast.LENGTH_SHORT).show();
            }  else {
                final KProgressHUD hud = KProgressHUD.create(activity).show();
                API.sharedService().validatePasswordResetCode(phone, viewModel.verifyCode.get()).enqueue(new Callback<AuthTokenBean>() {
                    @Override
                    public void onResponse(Call<AuthTokenBean> call, Response<AuthTokenBean> response) {
                        hud.dismiss();
                        if (response.isSuccessful()) {
                            API.shared().setAuthorizationToken(response.body().getToken());
                            ForgotPwdStep3Activity.start(activity, regType);
                        } else {
                            ApiError apiError = API.shared().parseError(response);
                            Logger.e(apiError.getHudMessage());
                            Toast.makeText(activity, apiError.getHudMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AuthTokenBean> call, Throwable t) {
                        hud.dismiss();
                        Logger.e(t.getLocalizedMessage());
                    }
                });
            }
        }
    }


}
