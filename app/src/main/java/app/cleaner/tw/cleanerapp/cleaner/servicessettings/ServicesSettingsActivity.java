package app.cleaner.tw.cleanerapp.cleaner.servicessettings;


import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.jzxiang.pickerview.TimePickerDialog;
import com.jzxiang.pickerview.data.DialogType;
import com.jzxiang.pickerview.listener.OnDateSetListener;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.geography.County;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.geography.Town;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.CleanerProfile;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.User;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceType;
import app.cleaner.tw.cleanerapp.backend.drfapi.builder.QueriesBuilder;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.PageNumberPagination;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.cleaner.event.ServiceSettingFinishEvent;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityCleanerServicesSettingsBinding;
import app.cleaner.tw.cleanerapp.databinding.ServicesTimePickerItemBinding;
import app.cleaner.tw.cleanerapp.databinding.SpinnerAreaItemBinding;
import app.cleaner.tw.cleanerapp.helper.GSONUtils;
import app.cleaner.tw.cleanerapp.helper.TimeUtils;
import app.cleaner.tw.cleanerapp.kaoui.LazyArrayAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServicesSettingsActivity extends BaseActivity implements OnDateSetListener {


    private ActivityCleanerServicesSettingsBinding binding;
    private ViewModel viewModel;

    private List<SpinnerAreaItemBinding> areaSpList = new ArrayList<>();
    private List<ServicesTimePickerItemBinding> serviceTimeList = new ArrayList<>();

    private long calendarTimeInMillis;
    private TimePickerDialog dateTimePicker;
    private TimePickerDialog.Builder operateTimeDialogBuilder;

    private ArrayList<ServiceType> serviceTypes = null;
    private ArrayList<County> cities = null;
    private User user = null;
    private CleanerProfile profile = null;

    //timePicker
    int startHour;
    int startMin;
    int endHour;
    int endMin;
    Time tempStart;
    Time tempEnd;
    private boolean isChildTimePicker = false;
    private int childTimePickerPosition = -1;

    public static void start(Context context) {
        Intent starter = new Intent(context, ServicesSettingsActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cleaner_services_settings);
        EventBus.getDefault().register(this);
        setUpToolBar();
        viewModel = new ViewModel();
        binding.setViewModel(viewModel);
        binding.setPresenter(new Presenter());
        initViews();
        operateTimeDialogBuilder = new TimePickerDialog.Builder().setType(DialogType.HOURS_MINS).setCallBack(this).setThemeColor(getColor(this, R.color.colorAccent));
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("服務設定");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private void initViews() {
        loadInfo();
        binding.areaLayout.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                binding.cityZoneLayout.setVisibility(i == binding.areaManuel.getId() ? View.VISIBLE : View.GONE);
            }
        });
        binding.timeLayout.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                binding.timePickerLayout.setVisibility(i == binding.timeSetting.getId() ? View.VISIBLE : View.GONE);
            }
        });
    }

    private void addAreaSp(final Town preferArea) {

//        final ArrayList<Town> towns = new ArrayList<>();

//        LazyArrayAdapter<County> cAdapter = new LazyArrayAdapter<County>(activity, R.layout.support_simple_spinner_dropdown_item, viewModel.counties) {
//            @Override
//            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
//                return getView(position, convertView, parent);
//            }
//
//            @NonNull
//            @Override
//            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
//                View view = viewFromResource(LayoutInflater.from(getContext()), convertView, parent, getResource());
//                TextView textView = view.findViewById(getFieldId());
//                textView.setText(getItem(position).getName());
//                return view;
//            }
//        };
//        final LazyArrayAdapter<Town> tAdapter = new LazyArrayAdapter<Town>(activity, R.layout.support_simple_spinner_dropdown_item, towns) {
//            @Override
//            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
//                return getView(position, convertView, parent);
//            }
//
//            @NonNull
//            @Override
//            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
//                View view = viewFromResource(LayoutInflater.from(getContext()), convertView, parent, getResource());
//                TextView textView = view.findViewById(getFieldId());
//                textView.setText(getItem(position).getName());
//                return view;
//            }
//        };
        if (preferArea != null) {
            final SpinnerAreaItemBinding spBinding = SpinnerAreaItemBinding.inflate(getLayoutInflater());

            spBinding.citySpinner.setText(preferArea.getCounty().getName());
            spBinding.zoneSpinner.setText(preferArea.getName());

//        spBinding.citySpinner.setAdapter(cAdapter);
//        spBinding.zoneSpinner.setAdapter(tAdapter);

//        ArrayList<County> cts = new ArrayList<>(viewModel.counties);
//        CollectionUtils.filter(cts, new Predicate<County>() {
//            @Override
//            public boolean evaluate(County object) {
//                if (preferArea == null)
//                    return false;
//                return object.getId() == preferArea.getCounty().getId();
//            }
//        });

//        if (!cts.isEmpty())
//            spBinding.citySpinner.setSelection(viewModel.counties.indexOf(cts.get(0)));


            final ArrayList<Town> targetTowns = new ArrayList<>();
            targetTowns.add(preferArea);


//        spBinding.citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                County city = viewModel.counties.get(i);
//                API.sharedService().getAllTowns(new QueriesBuilder()
//                        .filter("parent_area__county_code", city.getCounty_code())
//                        .create()
//                ).enqueue(new Callback<PageNumberPagination<Town>>() {
//                    @Override
//                    public void onResponse(Call<PageNumberPagination<Town>> call, Response<PageNumberPagination<Town>> response) {
//                        towns.removeAll(towns);
//                        if (response.isSuccessful())
//                            towns.addAll(response.body().getResults());
//                        else
//                            Logger.e(API.shared().parseError(response).getHudMessage());
//                        tAdapter.notifyDataSetChanged();
//
//                        if (!targetTowns.isEmpty()) {
//                            final Town t = targetTowns.get(0);
//                            ArrayList<Town> filterTowns = new ArrayList<>(towns);
//                            CollectionUtils.filter(filterTowns, new Predicate<Town>() {
//                                @Override
//                                public boolean evaluate(Town object) {
//                                    return object.getId() == t.getId();
//                                }
//                            });
//                            if (!filterTowns.isEmpty()) {
//                                spBinding.zoneSpinner.setSelection(towns.indexOf(filterTowns.get(0)));
//                                targetTowns.removeAll(targetTowns);
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<PageNumberPagination<Town>> call, Throwable t) {
//                        Logger.e(t.getLocalizedMessage());
//                    }
//                });
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });

//        spBinding.zoneSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                int index = getChildPosition(spBinding.getRoot());
//                Town town = tAdapter.getItem(position);
//                try {
//                    viewModel.selAreas.get(index-1);
//                    viewModel.selAreas.set(index-1, town);
//                } catch (IndexOutOfBoundsException e) {
//                    viewModel.selAreas.add(town);
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });

            spBinding.imgRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getChildPosition(spBinding.getRoot());
                    binding.cityZoneLayout.removeViewAt(position);
                    viewModel.selAreas.remove(position - 1);
                    if (binding.cityZoneLayout.getChildCount() != 1) {
                        areaSpList.remove(position - 1);
                    }
                }
            });

            binding.cityZoneLayout.addView(spBinding.getRoot());

            areaSpList.add(spBinding);
        }

    }

    private void addServiceTime(CleanerProfile.CustomServiceTime preferServiceTime) {
        final ServicesTimePickerItemBinding serviceTimeBinding = ServicesTimePickerItemBinding.inflate(getLayoutInflater());

        serviceTimeBinding.tvTime.setText(String.format(
                Locale.getDefault(),
                "%02d:%02d ~ %02d:%02d",
                preferServiceTime.getStart().getHours(), preferServiceTime.getStart().getMinutes(),
                preferServiceTime.getEnd().getHours(), preferServiceTime.getEnd().getMinutes()
        ));

//        serviceTimeBinding.tvTime.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                childTimePickerPosition = getChildPosition(serviceTimeBinding.getRoot()) -1;
//                showTimePicker(true);
//            }
//        });

        serviceTimeBinding.imgRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = getChildPosition(serviceTimeBinding.getRoot());
                binding.timePickerLayout.removeViewAt(position);
                viewModel.selServiceTimes.remove(position - 1);
                if (binding.timePickerLayout.getChildCount() != 1) {
                    serviceTimeList.remove(serviceTimeList.get(position - 1));
                }
            }
        });
        binding.timePickerLayout.addView(serviceTimeBinding.getRoot());
        serviceTimeList.add(serviceTimeBinding);
    }

    private int getChildPosition(View view) {
        return ((LinearLayout) view.getParent()).indexOfChild(view);
    }

    @Override
    public void onDateSet(TimePickerDialog view, long l) {
        switch (view.getTag()) {
            case "operateTimeStart":
                calendarTimeInMillis = TimeUtils.getDefaultTime(21, 0);
                String[] tempStart = TimeUtils.getTime(l, TimeUtils.HOUR_MINUTE_FORMAT).split(":");

                startHour = Integer.parseInt(tempStart[0]);
                startMin = Integer.parseInt(tempStart[1]);
                operateTimeDialogBuilder.setTitleStringId("工作時段-止")
                        .setCurrentMillseconds(calendarTimeInMillis)
                        .setMinMillseconds(l);
                dateTimePicker = operateTimeDialogBuilder.build();
                dateTimePicker.show(getSupportFragmentManager(), "operateTimeEnd");

                this.tempStart = new Time(startHour, startMin, 0);

                break;
            case "operateTimeEnd":
                String[] tempEnd = TimeUtils.getTime(l, TimeUtils.HOUR_MINUTE_FORMAT).split(":");
                endHour = Integer.parseInt(tempEnd[0]);
                endMin = Integer.parseInt(tempEnd[1]);

                this.tempEnd = new Time(endHour, endMin, 0);

                CleanerProfile.CustomServiceTime st = new CleanerProfile.CustomServiceTime();
                st.setStart(this.tempStart);
                st.setEnd(this.tempEnd);
                this.tempStart = null;
                this.tempEnd = null;

                addServiceTime(st);
                viewModel.selServiceTimes.add(st);

                break;
        }
    }

    private void initFirstAreaSpinner(ActivityCleanerServicesSettingsBinding binding) {

        final ArrayList<Town> towns = new ArrayList<>();

        LazyArrayAdapter<County> cAdapter = new LazyArrayAdapter<County>(activity, R.layout.support_simple_spinner_dropdown_item, viewModel.counties) {
            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                return getView(position, convertView, parent);
            }

            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = viewFromResource(LayoutInflater.from(getContext()), convertView, parent, getResource());
                TextView textView = view.findViewById(getFieldId());
                textView.setText(getItem(position).getName());
                return view;
            }
        };
        final LazyArrayAdapter<Town> tAdapter = new LazyArrayAdapter<Town>(activity, R.layout.support_simple_spinner_dropdown_item, towns) {
            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                return getView(position, convertView, parent);
            }

            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = viewFromResource(LayoutInflater.from(getContext()), convertView, parent, getResource());
                TextView textView = view.findViewById(getFieldId());
                textView.setText(getItem(position).getName());
                return view;
            }
        };

        binding.citySpinner.setAdapter(cAdapter);
        binding.zoneSpinner.setAdapter(tAdapter);

        binding.citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                County city = viewModel.counties.get(i);
                API.sharedService().getAllTowns(new QueriesBuilder()
                        .filter("parent_area__county_code", city.getCounty_code())
                        .pagination(1, QueriesBuilder.PAGE_SIZE_MAX)
                        .create()
                ).enqueue(new Callback<PageNumberPagination<Town>>() {
                    @Override
                    public void onResponse(Call<PageNumberPagination<Town>> call, Response<PageNumberPagination<Town>> response) {
                        towns.removeAll(towns);
                        if (response.isSuccessful())
                            towns.addAll(response.body().getResults());
                        else
                            Logger.e(API.shared().parseError(response).getHudMessage());
                        tAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onFailure(Call<PageNumberPagination<Town>> call, Throwable t) {
                        Logger.e(t.getLocalizedMessage());
                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void showTimePicker(boolean isChild) {
        isChildTimePicker = isChild;
        operateTimeDialogBuilder.setTitleStringId("服務時段-起").setCurrentMillseconds(TimeUtils.getDefaultTime(9)).setMinMillseconds(TimeUtils.getDefaultTime(0));
        dateTimePicker = operateTimeDialogBuilder.build();
        dateTimePicker.show(getSupportFragmentManager(), "operateTimeStart");
    }

    public class ViewModel {

        public final ObservableField<String> price = new ObservableField<>("0");
        public final ObservableField<String> servicesNumber = new ObservableField<>("");

        public final ObservableBoolean serveAllArea = new ObservableBoolean(true);
        public final ObservableBoolean serveAllTime = new ObservableBoolean();

        public final ObservableBoolean allowPet = new ObservableBoolean();
        public final ObservableBoolean equipTools = new ObservableBoolean();
        public final ObservableBoolean hasGoodCard = new ObservableBoolean();
        public final ObservableBoolean hasCleanerLicense = new ObservableBoolean();

        public final ArrayList<County> counties = new ArrayList<>();
        public final ArrayList<ServiceType> serviceTypes = new ArrayList<>();

        public final ArrayList<Town> selAreas = new ArrayList<>();
        public final ArrayList<ServiceType> selServiceTypes = new ArrayList<>();
        public final ArrayList<CleanerProfile.CustomServiceTime> selServiceTimes = new ArrayList<>();

        public ViewModel() {

        }
    }

    public class Presenter implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.img_add_area:
                    final Town town = (Town) binding.zoneSpinner.getSelectedItem();
                    if (town != null) {
                        ArrayList<Town> sameTowns = new ArrayList<>(viewModel.selAreas);
                        CollectionUtils.filter(sameTowns, new Predicate<Town>() {
                            @Override
                            public boolean evaluate(Town object) {
                                if (town == null)
                                    return false;
                                return object.getId() == town.getId();
                            }
                        });
                        if (!sameTowns.isEmpty()) {
                            Toast.makeText(activity, "你已選擇此區域", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        viewModel.selAreas.add(town);
                        addAreaSp(town);
                    }
                    break;
                case R.id.img_add_time:
                case R.id.tv_time:
                    showTimePicker(false);
                    break;
                case R.id.btn_enter:

                    // 防呆
                    if (!viewModel.serveAllArea.get() && viewModel.selAreas.isEmpty()) {
                        Toast.makeText(activity, "請選擇服務區域", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (!viewModel.serveAllTime.get() && viewModel.selServiceTimes.isEmpty()) {
                        Toast.makeText(activity, "請選擇服務時間", Toast.LENGTH_SHORT).show();
                        return;
                    }


                    // fill model

                    profile.setServe_all_areas(viewModel.serveAllArea.get());
                    profile.setServe_all_times(viewModel.serveAllTime.get());

                    profile.getCustom_service_areas().removeAll(profile.getCustom_service_areas());
                    profile.getCustom_service_areas().addAll(viewModel.selAreas);

                    profile.getCustom_service_times().removeAll(profile.getCustom_service_times());
                    profile.getCustom_service_times().addAll(viewModel.selServiceTimes);

                    profile.getProvide_service_types().removeAll(profile.getProvide_service_types());
                    profile.getProvide_service_types().addAll(viewModel.selServiceTypes);

                    profile.setExpected_salary(Integer.valueOf(viewModel.price.get()));
                    user.setCellphone(viewModel.servicesNumber.get());

                    profile.setAllow_pet(viewModel.allowPet.get());
                    profile.setEquip_tools(viewModel.equipTools.get());

                    ServicesSettingsConfirmActivity.start(activity, user, profile);
                    break;
            }
        }
    }

    private void loadInfo() {
        final KProgressHUD hud = KProgressHUD.create(this).show();
        final Runnable dismissHudR = new Runnable() {
            @Override
            public void run() {
                hud.dismiss();
            }
        };
        Runnable r = new Runnable() {
            @Override
            public void run() {
                try {
                    Response<List<ServiceType>> serviceTypeResponse = API.sharedService().getAllServiceTypes(API.sharedAuthorizationToken(), new QueriesBuilder().create()).execute();
                    Response<List<County>> citiesResponse = API.sharedService().getAllCounties(new QueriesBuilder().create()).execute();
                    Response<User> userResponse = API.sharedService().getUser(API.sharedAuthorizationToken()).execute();
                    Response<CleanerProfile> profileResponse = API.sharedService().cleanerGetProfile(API.sharedAuthorizationToken()).execute();
                    ;
                    if (serviceTypeResponse.isSuccessful()) {
                        serviceTypes = new ArrayList<>(serviceTypeResponse.body());
                    } else {
                        Logger.e(API.shared().parseError(citiesResponse).getHudMessage());
                    }

                    if (citiesResponse.isSuccessful()) {
                        cities = new ArrayList<>(citiesResponse.body());
                    } else {
                        Logger.e(API.shared().parseError(citiesResponse).getHudMessage());
                    }

                    if (userResponse.isSuccessful()) {
                        user = userResponse.body();
                        Logger.json(GSONUtils.toJson(user));
                    } else {
                        Logger.e(API.shared().parseError(citiesResponse).getHudMessage());
                    }

                    if (profileResponse.isSuccessful()) {
                        profile = profileResponse.body();
                        Logger.json(GSONUtils.toJson(profile));
                    } else {
                        Logger.e(API.shared().parseError(profileResponse).getHudMessage());
                    }

                    runOnUiThread(dismissHudR);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loadViewModel(user, profile);
                        }
                    });
                } catch (IOException e) {
                    runOnUiThread(dismissHudR);
                    Logger.e(e.getLocalizedMessage());
                }
            }
        };
        new Thread(r).start();
    }

    private void loadViewModel(User user, CleanerProfile profile) {
        viewModel.serveAllArea.set(profile.isServe_all_areas());
        viewModel.serveAllTime.set(profile.isServe_all_times());
        viewModel.price.set(String.valueOf(profile.getExpected_salary()));
        viewModel.servicesNumber.set(user.getCellphone());
        viewModel.allowPet.set(profile.isAllow_pet());
        viewModel.equipTools.set(profile.isEquip_tools());
        viewModel.hasGoodCard.set(profile.isHas_good_card());
        viewModel.hasCleanerLicense.set(profile.isHas_cleaner_license());

        viewModel.counties.removeAll(viewModel.counties);
        viewModel.counties.addAll(cities);
        viewModel.serviceTypes.removeAll(viewModel.serviceTypes);
        viewModel.serviceTypes.addAll(serviceTypes);

        viewModel.selAreas.removeAll(viewModel.selAreas);
        viewModel.selAreas.addAll(profile.getCustom_service_areas());
        viewModel.selServiceTypes.removeAll(viewModel.selServiceTypes);
        viewModel.selServiceTypes.addAll(profile.getProvide_service_types());
        viewModel.selServiceTimes.removeAll(viewModel.selServiceTimes);
        viewModel.selServiceTimes.addAll(profile.getCustom_service_times());

        if (profile.isHas_good_card()) {

        }

        if (profile.isHas_cleaner_license()) {

        }

        initFirstAreaSpinner(binding);
        for (Town area : viewModel.selAreas) {
            addAreaSp(area);
        }

        ArrayAdapter<ServiceType> adapter = new ArrayAdapter<ServiceType>(activity, R.layout.services_time_picker_item, viewModel.serviceTypes) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

                if (convertView == null) {
                    LayoutInflater inflater = LayoutInflater.from(activity);
                    convertView = inflater.inflate(R.layout.service_type_grid_item, parent, false);
                }

                final ServiceType serviceType = viewModel.serviceTypes.get(position);

                CheckBox checkBox = convertView.findViewById(R.id.checkbox);
                checkBox.setText(serviceType.getName());
                checkBox.setChecked(viewModel.selServiceTypes.contains(serviceType));
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            if (viewModel.selServiceTypes.contains(serviceType))
                                return;
                            viewModel.selServiceTypes.add(serviceType);
                        } else {
                            if (viewModel.selServiceTypes.contains(serviceType))
                                viewModel.selServiceTypes.remove(serviceType);
                        }
                    }
                });
                return convertView;
            }
        };
        binding.serviceTypeGrid.setAdapter(adapter);

        for (CleanerProfile.CustomServiceTime serviceTime : viewModel.selServiceTimes) {
            addServiceTime(serviceTime);
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFinishEvent(ServiceSettingFinishEvent event) {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
