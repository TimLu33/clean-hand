package app.cleaner.tw.cleanerapp.helper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexHelper {

    private static final String PHONE = "^09[0-9]{8}$";
    private static final String E_MAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String IDENTIFY_ID = "^[A-Z]{1}[1-2]{1}[0-9]{8}$";
    private static final String NATURE_PERSON_CODE = "^[A-Z]{2}+[0-9]{14}$";

    public static boolean isPhone(String text) {
        Pattern pattern = Pattern.compile(RegexHelper.PHONE);
        Matcher matcher = pattern.matcher(text);
        return matcher.matches();
    }

    public static boolean isMail(String text) {
        Pattern pattern = Pattern.compile(RegexHelper.E_MAIL);
        Matcher matcher = pattern.matcher(text);
        return matcher.matches();
    }

    public static boolean isNaturePersonCode(String text) {
        Pattern pattern = Pattern.compile(RegexHelper.NATURE_PERSON_CODE);
        Matcher matcher = pattern.matcher(text);
        return matcher.matches();
    }

    public static boolean isIdentiftId(String text) {
        Pattern pattern = Pattern.compile(RegexHelper.IDENTIFY_ID);
        Matcher matcher = pattern.matcher(text);
        return matcher.matches();
    }

}
