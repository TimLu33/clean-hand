package app.cleaner.tw.cleanerapp.boss.favorite;


import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.CleanerProfile;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.SimpleUser;
import app.cleaner.tw.cleanerapp.databinding.FavoriteItemBinding;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.BindingHolder> implements View.OnClickListener {


    public interface ItemClickListener {
        void onItemClickListener(View view, int position);
        void onDeleteClick(View view, int position);
    }

    private ItemClickListener listener;

    List<FavoriteActivity.FavoriteItem> favoriteList;

    public FavoriteAdapter(List<FavoriteActivity.FavoriteItem> favoriteList) {
        this.favoriteList = favoriteList;
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        FavoriteItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.favorite_item, parent, false);
        binding.getRoot().setOnClickListener(this);
        binding.imgDelete.setOnClickListener(this);
        return new BindingHolder(binding);
    }

    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {

        holder.binding.rootView.setTag(position);
        holder.binding.imgDelete.setTag(position);
        holder.bind(new FavoriteItemViewModel(favoriteList.get(position)));

    }

    @Override
    public int getItemCount() {
        return favoriteList.size();
    }

    public void setOnItemClickListener(ItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if (listener != null) {
            switch (view.getId()) {
                case R.id.root_view:
                    listener.onItemClickListener(view, (int) view.getTag());
                    break;
                case R.id.img_delete:
                    listener.onDeleteClick(view, (int) view.getTag());
                    break;
            }

        }

    }

    public class BindingHolder extends RecyclerView.ViewHolder {

        private FavoriteItemBinding binding;

        public BindingHolder(FavoriteItemBinding binding) {
            super(binding.rootView);
            this.binding = binding;
        }

        public void bind(FavoriteItemViewModel viewModel) {
            binding.setViewModel(viewModel);
            binding.executePendingBindings();
        }
    }
}
