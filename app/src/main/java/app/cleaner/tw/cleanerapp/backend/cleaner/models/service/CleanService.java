package app.cleaner.tw.cleanerapp.backend.cleaner.models.service;

import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;

import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import app.cleaner.tw.cleanerapp.backend.cleaner.models.geography.LocationBean;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.geography.Town;
import app.cleaner.tw.cleanerapp.common.Constants;

/**
 * Created by ARTHUR on 2017/10/18.
 */

public class CleanService implements Serializable {

    public enum Priority {
        @SerializedName("PE")
        Emergency,
        @SerializedName("PN")
        Normal
    }

    @Expose(serialize = false)
    private int id;

    @Expose(serialize = false)
    private int hirer;

    @Expose
    private Date time_range_start;

    @Expose
    private Date time_range_end;

    @Expose
    private String google_place_id;

    @Expose(serialize = false)
    private Town area;

    @Expose
    private String address;

    @Expose
    private String address_secured;

    @Expose
    private int service_type;

    @Expose
    private int estimate_cleaner_number;

    @Expose
    private int estimate_minute_per_cleaner;

    @Expose
    private Priority priority;

    @Expose(serialize = false)
    private LocationBean location;

    @Expose
    private boolean need_good_card;

    @Expose
    private boolean need_cleaner_license;

    @Expose
    private boolean limit_female;

    @Expose
    private boolean has_pet;

    @Expose
    private boolean provide_tools;

    @Expose
    private String remark;

    @Expose
    private HouseDetailBean house_detail;

    @Expose(serialize = false)
    private FeePaymentBean fee_payment;

    @Expose(serialize = false)
    private ServiceMatching cleaner_matching;

    @Expose(serialize = false)
    private List<ServicePhotoBean> clean_service_photos;

    @Expose(serialize = false)
    private List<ServiceMatching> hirer_matching_set;

    @Expose(serialize = false)
    private String hirer_name_read_only;

    @Expose(serialize = false)
    private float hirer_rating_read_only;

    @Expose(serialize = false)
    private ServiceType service_type_read_only;

    public String getOthers() {
        ArrayList<String> os = new ArrayList();
        if (isHas_pet())
            os.add("有寵物");
        if (isProvide_tools())
            os.add("有提供清潔用具");
        if (isNeed_cleaner_license())
            os.add("需要清潔證照");
        if (isNeed_good_card())
            os.add("需要良民證");
        if (isLimit_female())
            os.add("限定女性");
        if (os.isEmpty())
            return null;
        else
            return TextUtils.join(", ", os);
    }

    public int getId() {
        return id;
    }

    public int getHirer() {
        return hirer;
    }

    public Date getTime_range_start() {
        return time_range_start;
    }

    public Date getTime_range_end() {
        return time_range_end;
    }

    public String getGoogle_place_id() {
        return google_place_id;
    }

    public Town getArea() {
        return area;
    }

    public String getAddress() {
        return address;
    }

    public String getAddressSecured() {
        return address_secured;
    }

    public int getService_type() {
        return service_type;
    }

    public int getEstimate_cleaner_number() {
        return estimate_cleaner_number;
    }

    public int getEstimate_minute_per_cleaner() {
        return estimate_minute_per_cleaner;
    }

    public Priority getPriority() {
        return priority;
    }

    public LocationBean getLocation() {
        return location;
    }

    public boolean isNeed_good_card() {
        return need_good_card;
    }

    public boolean isNeed_cleaner_license() {
        return need_cleaner_license;
    }

    public boolean isLimit_female() {
        return limit_female;
    }

    public boolean isHas_pet() {
        return has_pet;
    }

    public boolean isProvide_tools() {
        return provide_tools;
    }

    public String getRemark() {
        return remark;
    }

    public HouseDetailBean getHouse_detail() {
        return house_detail;
    }

    public FeePaymentBean getFee_payment() {
        return fee_payment;
    }

    public ServiceMatching getCleaner_matching() {
        return cleaner_matching;
    }

    public List<ServicePhotoBean> getClean_service_photos() {
        return clean_service_photos;
    }

    public List<ServiceMatching> getHirer_matching_set() {
        return hirer_matching_set;
    }

    public String getHirer_name_read_only() {
        return hirer_name_read_only;
    }

    public float getHirer_rating_read_only() {
        return hirer_rating_read_only;
    }

    public ServiceType getService_type_read_only() {
        return service_type_read_only;
    }

    public void setTime_range_start(Date time_range_start) {
        this.time_range_start = time_range_start;
    }

    public void setTime_range_end(Date time_range_end) {
        this.time_range_end = time_range_end;
    }

    public void setGoogle_place_id(String google_place_id) {
        this.google_place_id = google_place_id;
    }

    public void setArea(Town area) {
        this.area = area;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setService_type(int service_type) {
        this.service_type = service_type;
    }

    public void setEstimate_cleaner_number(int estimate_cleaner_number) {
        this.estimate_cleaner_number = estimate_cleaner_number;
    }

    public void setEstimate_minute_per_cleaner(int estimate_minute_per_cleaner) {
        this.estimate_minute_per_cleaner = estimate_minute_per_cleaner;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public void setNeed_good_card(boolean need_good_card) {
        this.need_good_card = need_good_card;
    }

    public void setNeed_cleaner_license(boolean need_cleaner_license) {
        this.need_cleaner_license = need_cleaner_license;
    }

    public void setLimit_female(boolean limit_female) {
        this.limit_female = limit_female;
    }

    public void setHas_pet(boolean has_pet) {
        this.has_pet = has_pet;
    }

    public void setProvide_tools(boolean provide_tools) {
        this.provide_tools = provide_tools;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void setHouse_detail(HouseDetailBean house_detail) {
        this.house_detail = house_detail;
    }

    public void setService_type_read_only(ServiceType service_type_read_only) {
        this.service_type_read_only = service_type_read_only;
    }

    @Nullable
    public ServiceMatching.MatchingState getCurrentServiceMatchingState(int role) {

        if (role == Constants.BOSS) {
            ServiceMatching.MatchingState[] order = new ServiceMatching.MatchingState[] {
                    ServiceMatching.MatchingState.Invite,
                    ServiceMatching.MatchingState.Proposal,
                    ServiceMatching.MatchingState.Candidate,
                    ServiceMatching.MatchingState.Confirm,
                    ServiceMatching.MatchingState.Closed,
            };
            CollectionUtils.reverseArray(order);

            for (final ServiceMatching.MatchingState state: order) {
                if (CollectionUtils.find(getHirer_matching_set(), new Predicate<ServiceMatching>() {
                    @Override
                    public boolean evaluate(ServiceMatching object) {
                        return object.getMatching_state() == state;
                    }
                }) != null) {
                    return state;
                }
            }
        } else {
            if (cleaner_matching != null)
                return cleaner_matching.getMatching_state();
        }

        return null;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CleanService)
            return ((CleanService) obj).getId() == getId();
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        if (getId() != 0)
            return getId();
        return super.hashCode();
    }

    public static class HouseDetailBean implements Serializable {

        @Expose
        private int room_num;
        @Expose
        private int hall_num;
        @Expose
        private int bathroom_num;
        @Expose
        private int footage;

        public String getAsString() {
            return String.format(
                    Locale.getDefault(),
                    "%d房 %d廳 %d衛 %d坪",
                    getRoom_num(), getHall_num(), getBathroom_num(),
                    getFootage()
            );
        }

        public int getRoom_num() {
            return room_num;
        }

        public void setRoom_num(int room_num) {
            this.room_num = room_num;
        }

        public int getHall_num() {
            return hall_num;
        }

        public void setHall_num(int hall_num) {
            this.hall_num = hall_num;
        }

        public int getBathroom_num() {
            return bathroom_num;
        }

        public void setBathroom_num(int bathroom_num) {
            this.bathroom_num = bathroom_num;
        }

        public int getFootage() {
            return footage;
        }

        public void setFootage(int footage) {
            this.footage = footage;
        }
    }

    public static class FeePaymentBean implements Serializable {

        @Expose
        private String id;
        @Expose
        private int service;
        @Expose
        private int amount;

        @Expose
        private Date created_datetime;

        @Expose
        private boolean paid;

        @Expose
        private Date paid_datetime;

        @Expose
        private URL spg_url;

        public String getId() {
            return id;
        }

        public int getService() {
            return service;
        }

        public int getAmount() {
            return amount;
        }

        public Date getCreated_datetime() {
            return created_datetime;
        }

        public boolean isPaid() {
            return paid;
        }

        public Date getPaid_datetime() {
            return paid_datetime;
        }

        public URL getSpg_url() {
            return spg_url;
        }
    }

    public static class ServicePhotoBean implements Serializable {
        /**
         * id : e01b4440-31f9-4fc1-b8ca-567656675d16
         * image : https://cleanerbucket.s3.amazonaws.com/media/image/user/cover/e01b4440-31f9-4fc1-b8ca-567656675d16.jpg
         * description : my house
         */

        @Expose
        private String id;
        @Expose
        private URL image;
        @Expose
        private String description;

        public String getId() {
            return id;
        }

        public URL getImage() {
            return image;
        }

        public String getDescription() {
            return description;
        }
    }
}
