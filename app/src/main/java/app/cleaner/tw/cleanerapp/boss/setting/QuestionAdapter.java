package app.cleaner.tw.cleanerapp.boss.setting;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.databinding.QuestionItemBinding;

public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.BindingHolder> implements View.OnClickListener {

    public interface ItemClickListener {
        void onItemClickListener(View view, int position);
    }

    private ItemClickListener listener;

    private List<QuestionObject> faqList;

    public QuestionAdapter(List<QuestionObject> faqList) {
        this.faqList = faqList;
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        QuestionItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.question_item, parent, false);
        binding.getRoot().setOnClickListener(this);
        return new BindingHolder(binding);
    }

    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {
        holder.binding.getRoot().setTag(position);
        QuestionItemViewModel viewModel = new QuestionItemViewModel(faqList.get(position));
        holder.bind(viewModel);

    }

    @Override
    public int getItemCount() {
        return faqList.size();
    }

    public void setOnItemClickListener(ItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if (listener != null) {
            listener.onItemClickListener(view, (int) view.getTag());
        }
    }

    public class BindingHolder extends RecyclerView.ViewHolder {

        private QuestionItemBinding binding;

        public BindingHolder(QuestionItemBinding binding) {

            super(binding.rootView);
            this.binding = binding;
        }

        public void bind(QuestionItemViewModel viewModel) {
            binding.setViewModel(viewModel);
            binding.executePendingBindings();
        }
    }
}
