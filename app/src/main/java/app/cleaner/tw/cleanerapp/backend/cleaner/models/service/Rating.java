package app.cleaner.tw.cleanerapp.backend.cleaner.models.service;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.net.URL;

/**
 * Created by ARTHUR on 2017/10/18.
 */

public class Rating implements Serializable {

    @Expose
    private int rating;
    @Expose
    private int service_matching;
    @Expose
    private String comment;
    @Expose(serialize = false)
    private String writer_display_name;
    @Expose(serialize = false)
    private String receiver_display_name;
    @Expose(serialize = false)
    private URL writer_head_image;
    @Expose(serialize = false)
    private URL receiver_head_image;
    @Expose(serialize = false)
    private String clean_service_address;

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getService_matching() {
        return service_matching;
    }

    public void setService_matching(int service_matching) {
        this.service_matching = service_matching;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getWriter_display_name() {
        return writer_display_name;
    }

    public String getReceiver_display_name() {
        return receiver_display_name;
    }

    public URL getWriter_head_image() {
        return writer_head_image;
    }

    public URL getReceiver_head_image() {
        return receiver_head_image;
    }

    public String getClean_service_address() {
        return clean_service_address;
    }
}
