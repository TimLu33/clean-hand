package app.cleaner.tw.cleanerapp.backend.cleaner.models.service;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

public class Notification implements Serializable {

    /**
     * id : 77
     * title : 雇主回覆
     * message : t_hirer: hi
     * read : false
     * created_datetime : 2018-01-01 18:20:11
     * content_type : {"app_label":"service","model":"serviceqa"}
     * object_id : 966
     */
    @Expose
    private int id;
    @Expose
    private String title;
    @Expose
    private String message;
    @Expose
    private boolean read;
    @Expose
    private String created_datetime;
    @Expose(serialize = false)
    private ContentTypeBean content_type;
    @Expose
    private int object_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public String getCreated_datetime() {
        return created_datetime;
    }

    public void setCreated_datetime(String created_datetime) {
        this.created_datetime = created_datetime;
    }

    public ContentTypeBean getContent_type() {
        return content_type;
    }

    public void setContent_type(ContentTypeBean content_type) {
        this.content_type = content_type;
    }

    public int getObject_id() {
        return object_id;
    }

    public void setObject_id(int object_id) {
        this.object_id = object_id;
    }

    public static class ContentTypeBean implements Serializable {
        /**
         * app_label : service
         * model : serviceqa
         */
        @Expose
        private String app_label;
        @Expose
        private String model;

        public String getApp_label() {
            return app_label;
        }

        public void setApp_label(String app_label) {
            this.app_label = app_label;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }
    }
}
