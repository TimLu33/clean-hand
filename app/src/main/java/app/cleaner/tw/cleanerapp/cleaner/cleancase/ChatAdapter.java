package app.cleaner.tw.cleanerapp.cleaner.cleancase;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceQA;
import app.cleaner.tw.cleanerapp.common.Constants;
import app.cleaner.tw.cleanerapp.databinding.ChatLeftItemBinding;
import app.cleaner.tw.cleanerapp.databinding.ChatRightItemBinding;
import app.cleaner.tw.cleanerapp.mock.MockChatModel;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ServiceQA> chatList = new ArrayList<>();

    public ChatAdapter(List<ServiceQA> chatList) {
        this.chatList = chatList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == Constants.BOSS) {
            ChatLeftItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.chat_left_item, parent, false);
            return new BossBindingHolder(binding);
        } else {
            ChatRightItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.chat_right_item, parent, false);
            return new CleanerBindingHolder(binding);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof BossBindingHolder) {
            ((BossBindingHolder) holder).bind(new ChatItemViewModel(chatList.get(position)));
        } else if (holder instanceof CleanerBindingHolder) {
            ((CleanerBindingHolder) holder).bind(new ChatItemViewModel(chatList.get(position)));
        }
    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (chatList.get(position).getType() == ServiceQA.Type.A) {
            return Constants.BOSS;
        } else {
            return Constants.CLEANER;
        }
    }

    public class BossBindingHolder extends RecyclerView.ViewHolder {

        private ChatLeftItemBinding binding;

        public BossBindingHolder(ChatLeftItemBinding binding) {
            super(binding.rootView);
            this.binding = binding;
        }

        public void bind(ChatItemViewModel viewModel) {
            binding.setViewModel(viewModel);
            binding.executePendingBindings();
        }

    }

    public class CleanerBindingHolder extends RecyclerView.ViewHolder {

        private ChatRightItemBinding binding;

        public CleanerBindingHolder(ChatRightItemBinding binding) {
            super(binding.rootView);
            this.binding = binding;
        }

        public void bind(ChatItemViewModel viewModel) {
            binding.setViewModel(viewModel);
            binding.executePendingBindings();
        }

    }
}
