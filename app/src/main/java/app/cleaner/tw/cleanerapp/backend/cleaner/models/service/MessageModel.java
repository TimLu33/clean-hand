package app.cleaner.tw.cleanerapp.backend.cleaner.models.service;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

public class MessageModel implements Serializable {

    /**
     * id : 823
     * service : 164
     * cleaner : 290
     * hirer : 290
     * type : A
     * content : rr
     * image : null
     * created_datetime : 2017-12-11 22:41:18
     * sender_name : 3333
     * sender_head_image : https://cleanerbucket.s3.amazonaws.com/media/image/user/head/290/cb82875a-2e6e-4a42-8a52-53c62d742b54.jpg
     */
    @Expose
    private int id;
    @Expose
    private int service;
    @Expose
    private int cleaner;
    @Expose
    private int hirer;
    @Expose
    private String type;
    @Expose
    private String content;
    @Expose
    private Object image;
    @Expose
    private String created_datetime;
    @Expose
    private String sender_name;
    @Expose
    private String sender_head_image;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getService() {
        return service;
    }

    public void setService(int service) {
        this.service = service;
    }

    public int getCleaner() {
        return cleaner;
    }

    public void setCleaner(int cleaner) {
        this.cleaner = cleaner;
    }

    public int getHirer() {
        return hirer;
    }

    public void setHirer(int hirer) {
        this.hirer = hirer;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Object getImage() {
        return image;
    }

    public void setImage(Object image) {
        this.image = image;
    }

    public String getCreated_datetime() {
        return created_datetime;
    }

    public void setCreated_datetime(String created_datetime) {
        this.created_datetime = created_datetime;
    }

    public String getSender_name() {
        return sender_name;
    }

    public void setSender_name(String sender_name) {
        this.sender_name = sender_name;
    }

    public String getSender_head_image() {
        return sender_head_image;
    }

    public void setSender_head_image(String sender_head_image) {
        this.sender_head_image = sender_head_image;
    }
}
