package app.cleaner.tw.cleanerapp.boss.search;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import java.util.Locale;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.SimpleUser;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceMatching;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.common.Constants;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityCleanerClosedResumeDetailBinding;
import app.cleaner.tw.cleanerapp.helper.GSONUtils;
import app.cleaner.tw.cleanerapp.helper.TimeUtils;

public class ClosedResumeDetailActivity extends BaseActivity {

    private static final String CLEAN_SERVICE = "cleanService";
    private static final String CLEANER = "cleaner";

    private ActivityCleanerClosedResumeDetailBinding binding;
    private CleanService cleanService;
    private SimpleUser cleaner;

    public static void start(Context context, CleanService cleanService, SimpleUser cleaner) {
        Intent starter = new Intent(context, ClosedResumeDetailActivity.class);
        starter.putExtra(CLEAN_SERVICE, cleanService);
        starter.putExtra(CLEANER, cleaner);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBundle();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cleaner_closed_resume_detail);
        binding.setViewModel(new ViewModel(cleanService, cleaner));
        setUpToolBar();
    }

    private void getBundle() {
        if (getIntent().getExtras() != null) {
            cleanService = (CleanService) getIntent().getSerializableExtra(CLEAN_SERVICE);
            cleaner = (SimpleUser) getIntent().getSerializableExtra(CLEANER);
            GSONUtils.printJson(cleanService);
            GSONUtils.printJson(cleaner);
        }
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("案件資訊");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    public class ViewModel {

        public final ObservableField<String> houseUrl = new ObservableField<>();
        public final ObservableField<Integer> rating = new ObservableField<>(0);
        public final ObservableField<Integer> caseStatus = new ObservableField<>();
        public final ObservableField<String> address= new ObservableField<>();
        public final ObservableField<String> time = new ObservableField<>();
        public final ObservableField<String> houseStyle = new ObservableField<>();
        public final ObservableField<String> serviceType = new ObservableField<>();
        public final ObservableField<String> others = new ObservableField<>();
        public final ObservableField<String> cleanerName = new ObservableField<>();
        public final ObservableField<String> comment = new ObservableField<>("無");


        public ViewModel(CleanService cleanService, @NonNull SimpleUser cleaner) {

            if (!cleanService.getClean_service_photos().isEmpty()) {
                houseUrl.set(cleanService.getClean_service_photos().get(0).getImage().toExternalForm());
            }

            caseStatus.set(Constants.CASE_STATUS.CLOSED.ordinal());
            address.set(cleanService.getAddressSecured());
            time.set(String.format(
                    Locale.getDefault(),
                    "%s ~ %s",
                    TimeUtils.getTime(cleanService.getTime_range_start().getTime(), TimeUtils.DATE_FORMAT_MINS),
                    TimeUtils.getTime(cleanService.getTime_range_end().getTime(), TimeUtils.DATE_FORMAT_MINS)
            ));
            houseStyle.set(cleanService.getHouse_detail().getAsString());
            serviceType.set(cleanService.getService_type_read_only().getName());
            others.set(cleanService.getOthers() != null ? cleanService.getOthers() : "無");

            cleanerName.set(cleaner.getCleaner_profile().getDisplay_name());
            for (ServiceMatching serviceMatching : cleanService.getHirer_matching_set()) {
                if (serviceMatching.getCleaner() == cleaner.getId()) {
                    if (serviceMatching.getCleaner_rating() != null) {
                        if (serviceMatching.getCleaner_rating().getComment() != null && !serviceMatching.getCleaner_rating().getComment().isEmpty()) {
                            comment.set(serviceMatching.getCleaner_rating().getComment());
                        }
                        rating.set(serviceMatching.getCleaner_rating().getRating());
                    }
                    break;
                }
            }
        }
    }
}
