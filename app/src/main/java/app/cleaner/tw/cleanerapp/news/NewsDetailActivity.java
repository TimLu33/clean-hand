package app.cleaner.tw.cleanerapp.news;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.news.News;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityNewsDetailBinding;

public class NewsDetailActivity extends BaseActivity {

    private static final String NEWS_MODEL = "newsModel";
    private ActivityNewsDetailBinding binding;
    private NewsViewModel viewModel;
    private News model;

    public static void start(Context context, News model) {
        Intent starter = new Intent(context, NewsDetailActivity.class);
        starter.putExtra(NEWS_MODEL, model);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBundle();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_news_detail);
        setUpToolBar();
        viewModel = new NewsViewModel(model);
        binding.setViewModel(viewModel);
    }

    private void getBundle() {
        if (getIntent().getExtras() != null) {
            model = (News) getIntent().getSerializableExtra(NEWS_MODEL);
        }
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("最新消息");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }
}
