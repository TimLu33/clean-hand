package app.cleaner.tw.cleanerapp.cleaner.cleancase;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.orhanobut.logger.Logger;

import org.apache.commons.collections4.CollectionUtils;
import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceMatching;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceQA;
import app.cleaner.tw.cleanerapp.backend.drfapi.builder.QueriesBuilder;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.common.Constants;
import app.cleaner.tw.cleanerapp.common.DividerItemDecoration;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityCleanerChatBinding;
import app.cleaner.tw.cleanerapp.helper.GSONUtils;
import app.cleaner.tw.cleanerapp.mock.MockChatModel;
import app.cleaner.tw.cleanerapp.notification.RefreshNotificationEvent;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CleanerChatActivity extends BaseActivity {

    private static final String CASE_MODEL = "caseModel";
    private static final String HIRER_ID = "hirerId";
    public static final String POSITION = "position";

    private ActivityCleanerChatBinding binding;
    private ViewModel viewModel;

    // bundle
    private CleanService service;
    private int hirerId;
    private int position;

    // Timer
    Timer chatTimer = null;

    public static void start(Context context, CleanService service, int hirerId, int position) {
        Intent starter = new Intent(context, CleanerChatActivity.class);
        starter.putExtra(CASE_MODEL, service);
        starter.putExtra(HIRER_ID, hirerId);
        starter.putExtra(POSITION, position);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBundle();

        binding = DataBindingUtil.setContentView(this, R.layout.activity_cleaner_chat);
        setUpToolBar();
        viewModel = new ViewModel(service);
        viewModel.service.set(service);
        binding.setViewModel(viewModel);
        binding.setPresenter(new Presenter());
        initViews();

//        loadChats();
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("問與答");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private void startTimer() {
        chatTimer = new Timer("chatTimer");
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                loadChats();
            }
        };
        chatTimer.schedule(task, 0, 3000);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (chatTimer != null)
            chatTimer.cancel();
        startTimer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (chatTimer != null)
            chatTimer.cancel();
    }

    private void getBundle() {
        if (getIntent().getExtras() != null) {
            service = (CleanService) getIntent().getSerializableExtra(CASE_MODEL);
            hirerId = getIntent().getIntExtra(HIRER_ID, 0);
            position = getIntent().getIntExtra(POSITION, -1);
        }
    }

    private void initViews() {

        binding.btnProposal.setVisibility(hirerId != 0 ? View.GONE : View.VISIBLE);

        LinearLayoutManager lm = new LinearLayoutManager(this);
        lm.setOrientation(LinearLayoutManager.VERTICAL);
        lm.setStackFromEnd(true);

        ChatAdapter adapter = new ChatAdapter(viewModel.chats);
        binding.recyclerView.getItemAnimator().setAddDuration(0);
        binding.recyclerView.addItemDecoration(new DividerItemDecoration(this));
        binding.recyclerView.setLayoutManager(lm);
        binding.recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        binding.recyclerView.scrollToPosition(viewModel.chats.size() -1);

    }

    private void loadChats() {
        QueriesBuilder queriesBuilder = new QueriesBuilder()
                .order("created_datetime");

        if (service != null) {
            queriesBuilder.filter("service", viewModel.service.get().getId())
                    .filter("cleaner", API.sharedUserId());
        } else {
            queriesBuilder.filter("cleaner", API.sharedUserId())
                    .filter("hirer", hirerId);
        }

        API.sharedService().cleanerGetOwnQas(API.sharedAuthorizationToken(), queriesBuilder.create()).enqueue(new Callback<List<ServiceQA>>() {
            @Override
            public void onResponse(Call<List<ServiceQA>> call, Response<List<ServiceQA>> response) {
                if (response.isSuccessful()) {
                    viewModel.chats.clear();
                    viewModel.chats.addAll(response.body());
                    (binding.recyclerView.getAdapter()).notifyDataSetChanged();
                } else {
                    Logger.e(API.shared().parseError(response).getHudMessage());
                }
            }

            @Override
            public void onFailure(Call<List<ServiceQA>> call, Throwable t) {
                Logger.e(t.getLocalizedMessage());
            }
        });
    }

    private void showProposalConfirmDialog(DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setMessage("您確定要提案嗎");
        builder.setPositiveButton("是", listener);
        builder.setNegativeButton("否", null);
        builder.show();
    }

    private void showProposalSuccessDialog(DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setMessage("您的提案已經送出");
        builder.setPositiveButton("確定", listener);
        builder.show();

    }

    public class ViewModel {
        public final ObservableField<CleanService> service = new ObservableField<>();
        public final ArrayList<ServiceQA> chats = new ArrayList<>();
        public final ObservableField<String> textMsg = new ObservableField<>("");

        public boolean canProposal() {
            try {
                if (service.get().getCleaner_matching() == null)
                    return true;
                else
                    return service.get().getCleaner_matching().getMatching_state() == ServiceMatching.MatchingState.Invite;
            } catch (Exception e) {
                return false;
            }
        }

        public ViewModel(CleanService services) {
            this.service.set(services);
        }
    }

    public class Presenter implements View.OnClickListener {

        @Override
        public void onClick(final View view) {
            switch (view.getId()) {
                case R.id.btn_proposal:
                    showProposalConfirmDialog(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (i == -1) {
                                API.sharedService().cleanerProposalService(API.sharedAuthorizationToken(), viewModel.service.get().getId()).enqueue(new Callback<JsonObject>() {
                                    @Override
                                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                        if (response.isSuccessful()) {
                                            showProposalSuccessDialog(new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    API.sharedService().cleanerGetOwnService(API.sharedAuthorizationToken(), service.getId()).enqueue(new Callback<CleanService>() {
                                                        @Override
                                                        public void onResponse(Call<CleanService> call, Response<CleanService> response) {
                                                            Logger.json(GSONUtils.toJson(response.body()));
                                                            if (position != -1) {
                                                                EventBus.getDefault().post(new UpdateSearchResultEvent(response.body(), position));
                                                            }
                                                            finish();
                                                        }

                                                        @Override
                                                        public void onFailure(Call<CleanService> call, Throwable t) {
                                                            Logger.e(t.getLocalizedMessage());
                                                        }
                                                    });
                                                    finish();
                                                }
                                            });
                                        } else {
                                            Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<JsonObject> call, Throwable t) {
                                        Logger.e(t.getLocalizedMessage());
                                    }
                                });
                            }
                        }
                    });
                    break;
                case R.id.btn_send:
                    if (!viewModel.textMsg.get().isEmpty()) {
                        Map<String, Object> queries = new HashMap<>();
                        if (service != null) {
                            queries.put("service", viewModel.service.get().getId());
                            queries.put("hirer", viewModel.service.get().getHirer());
                        } else {
                            queries.put("hirer", hirerId);
                        }
                        queries.put("content", viewModel.textMsg.get());

                        API.sharedService().cleanerCreateQa(API.sharedAuthorizationToken(), queries).enqueue(new Callback<ServiceQA>() {
                            @Override
                            public void onResponse(Call<ServiceQA> call, Response<ServiceQA> response) {
                                if (response.isSuccessful()) {
                                    ServiceQA newQa = response.body();
                                    viewModel.chats.add(newQa);
                                    binding.recyclerView.getAdapter().notifyDataSetChanged();

                                    binding.recyclerView.scrollToPosition(viewModel.chats.size() -1);
                                    viewModel.textMsg.set("");
                                    EventBus.getDefault().post(new RefreshNotificationEvent());
                                } else {
                                    Logger.e(API.shared().parseError(response).getHudMessage());
                                }
                            }

                            @Override
                            public void onFailure(Call<ServiceQA> call, Throwable t) {
                                Logger.e(t.getLocalizedMessage());
                            }
                        });
                    }
                    break;
            }
        }
    }
}
