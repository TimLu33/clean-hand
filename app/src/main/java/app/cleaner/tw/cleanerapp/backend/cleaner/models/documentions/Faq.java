package app.cleaner.tw.cleanerapp.backend.cleaner.models.documentions;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by ARTHUR on 2017/10/18.
 */

public class Faq {

    enum Viewer {
        @SerializedName("CL")
        Cleaner,
        @SerializedName("HR")
        Hirer,
        @SerializedName("BT")
        Both
    }

    @Expose
    private int id;
    @Expose
    private int ordering;
    @Expose
    private String title;
    @Expose
    private String question;
    @Expose
    private String answer;
    @Expose
    private Viewer viewer;
    @Expose
    private Date created_datetime;
    @Expose
    private Date modified_datetime;

    public int getId() {
        return id;
    }

    public int getOrdering() {
        return ordering;
    }

    public String getTitle() {
        return title;
    }

    public String getQuestion() {
        return question;
    }

    public String getAnswer() {
        return answer;
    }

    public Viewer getViewer() {
        return viewer;
    }

    public Date getCreated_datetime() {
        return created_datetime;
    }

    public Date getModified_datetime() {
        return modified_datetime;
    }
}
