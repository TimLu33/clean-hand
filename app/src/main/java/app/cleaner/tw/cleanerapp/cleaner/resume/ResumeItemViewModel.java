package app.cleaner.tw.cleanerapp.cleaner.resume;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import java.util.Locale;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceMatching;
import app.cleaner.tw.cleanerapp.common.Constants;
import app.cleaner.tw.cleanerapp.helper.TimeUtils;

public class ResumeItemViewModel {

    public final ObservableField<String> address = new ObservableField<>();
    public final ObservableField<String> time = new ObservableField<>();
    public final ObservableField<String> boss = new ObservableField<>();
    public final ObservableField<Integer> caseStatus = new ObservableField<>();

    public ResumeItemViewModel(CleanService model, int role) {
        address.set(model.getAddressSecured());
        time.set(String.format(
                Locale.getDefault(),
                "%s ~ %s",
                TimeUtils.getTime(model.getTime_range_start().getTime(), TimeUtils.DATE_FORMAT_MINS),
                TimeUtils.getTime(model.getTime_range_end().getTime(), TimeUtils.DATE_FORMAT_MINS)
        ));
        boss.set(model.getHirer_name_read_only());


        ServiceMatching.MatchingState state = model.getCurrentServiceMatchingState(role);

        if (!model.getFee_payment().isPaid())
            caseStatus.set(Constants.CASE_STATUS.NOT_PAID.ordinal());
        else if (state != null)
            caseStatus.set(Constants.CASE_STATUS.fromBackendState(state).ordinal());
        else
            caseStatus.set(99);
    }
    @BindingAdapter({"caseStatus"})
    public static void setCaseStatusStyle(TextView textView, int status) {
        Context context = textView.getContext();
        if (status == Constants.CASE_STATUS.INVITE.ordinal()) {
            textView.setText("邀請中");
            textView.setBackgroundResource(R.drawable.ic_status_green1);
        } else if (status == Constants.CASE_STATUS.PROPOSAL.ordinal()) {
            textView.setText("已提案");
            textView.setBackgroundResource(R.drawable.ic_status_green1);
        } else if (status == Constants.CASE_STATUS.CANDIDATE.ordinal()) {
            textView.setText("選拔中");
            textView.setBackgroundResource(R.drawable.ic_status_green1);
        } else if (status == Constants.CASE_STATUS.CONFIRM.ordinal()) {
            textView.setText("已確認");
            textView.setBackgroundResource(R.drawable.ic_status_orange);
        } else if (status == Constants.CASE_STATUS.MISSED.ordinal()) {
            textView.setText("落選");
            textView.setBackgroundResource(R.drawable.ic_status_black);
        } else if (status == Constants.CASE_STATUS.CLOSED.ordinal()) {
            textView.setText("已結案");
            textView.setBackgroundResource(R.drawable.ic_status_black);
        } else if (status == Constants.CASE_STATUS.NOT_PAID.ordinal()) {
            textView.setText("未付款");
            textView.setBackgroundResource(R.drawable.ic_status_green1);
        } else {
            textView.setVisibility(View.GONE);
        }
    }
}
