package app.cleaner.tw.cleanerapp.common;


import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceMatching;

public class Constants {

    public static final int BOSS = 1;
    public static final int CLEANER = 2;

    public enum CASE_STATUS {
        INVITE, PROPOSAL, CANDIDATE, CONFIRM, MISSED, CLOSED, NOT_PAID;

        public int getValue() {
            return ordinal() +1;
        }

        public static CASE_STATUS fromBackendState(ServiceMatching.MatchingState matchingState) {
            switch (matchingState) {
                case Invite:
                    return CASE_STATUS.INVITE;
                case Proposal:
                    return CASE_STATUS.PROPOSAL;
                case Candidate:
                    return CASE_STATUS.CANDIDATE;
                case Confirm:
                    return CASE_STATUS.CONFIRM;
                case Missed:
                    return CASE_STATUS.MISSED;
                case Closed:
                    return CASE_STATUS.CLOSED;
            }
            return CASE_STATUS.INVITE;
        }
    }

}
