package app.cleaner.tw.cleanerapp.backend.drfapi.helper;

import android.util.Log;

import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Map;

import app.cleaner.tw.cleanerapp.backend.drfapi.models.ApiError;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by ARTHUR on 2017/10/17.
 */

public class DRFHelper {
    static final String TAG = "DRFHelper";

    public static ApiError parseError(Response<?> response, Retrofit retrofit) {
        Converter<ResponseBody, JsonObject> converter =
                retrofit.responseBodyConverter(JsonObject.class, new Annotation[0]);

        try {
            JsonObject jsonObject = converter.convert(response.errorBody());
            Log.d(TAG, jsonObject.toString());
            return new ApiError(response.code(), response.message(), jsonObject.entrySet());
        } catch (IOException e) {
            e.printStackTrace();
            return new ApiError(response.code(), response.message());
        }
    }

    // Http Media Type
    private static final MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json; charset=utf-8");
    private static final MediaType MEDIA_TYPE_IMAGE = MediaType.parse("image/*");

    public static MultipartBody.Part ImageRequestBodyPart(String name, File file) {
        RequestBody requestFile = RequestBody.create(
                MEDIA_TYPE_IMAGE,
                file
        );
        return MultipartBody.Part.createFormData(name, file.getName(), requestFile);
    }

//    public static Map<String, String> Filter(Pair<String, String> ... pairs) {
//        Map<String, String> map = new HashMap<>();
//        for (Pair<String, String> pair: pairs) {
//            map.put(pair.first, pair.second);
//        }
//        return map;
//    }

    public static RequestBody JsonRequestBody(Map<String, String> parameters) {
        JSONObject jsonObject = new JSONObject();
        try {
            for (String key: parameters.keySet()) {
                jsonObject.put(key, parameters.get(key));
            }
        } catch (JSONException e) {

        }
        return JsonRequestBody(jsonObject);
    }

    public static RequestBody JsonRequestBody(JSONObject jsonObject) {
        return RequestBody.create(MEDIA_TYPE_JSON, jsonObject.toString());
    }



    public static RequestBody ImagesRequestBody(Map<String, String> parameters, Map<String, File> files) {
        MultipartBody.Builder builder = new MultipartBody.Builder();
        for (String key: parameters.keySet()) {
            String value = parameters.get(key);
            builder.addFormDataPart(key, value);
        }
        for (String key: files.keySet()) {
            File file = files.get(key);
            builder.addFormDataPart(key, file.getName(), RequestBody.create(MEDIA_TYPE_IMAGE, file));
        }
        return builder.build();
    }

}
