package app.cleaner.tw.cleanerapp.backend.drfapi.builder;

import android.support.annotation.IntRange;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ARTHUR on 2017/10/17.
 */

public class QueriesBuilder implements Serializable {

    public static final int PAGE_SIZE_MAX = 1000;

    Map<String, String> filterMap = new HashMap<>();
    private Integer page, pageSize;
    private String orderingField;
    private String searchText;

    public QueriesBuilder pagination(@IntRange(from = 1) int page, int pageSize) {
        this.page = page;
        this.pageSize = pageSize;
        return this;
    }

    public QueriesBuilder pagination(@IntRange(from = 1) int page) {
        this.page = page;
        return this;
    }

    public QueriesBuilder filter(String field, String value) {
        filterMap.put(field, value);
        return this;
    }

    public QueriesBuilder filter(String field, boolean value) {
        filterMap.put(field, value ? "true": "false");
        return this;
    }

    public QueriesBuilder filter(String field, int value) {
        filterMap.put(field, String.valueOf(value));
        return this;
    }

    public QueriesBuilder search(String searchText) {
        this.searchText = searchText;
        return this;
    }

    public QueriesBuilder order(String orderingField) {
        this.orderingField = orderingField;
        return this;
    }

    public Map<String, String> create() {
        Map<String, String> qMap = new HashMap<>(filterMap);

        if (orderingField != null)
            qMap.put("ordering", orderingField);

        if (page != null)
            qMap.put("page", String.valueOf(page));

        if (pageSize != null)
            qMap.put("page_size", String.valueOf(pageSize));

        if (searchText != null)
            qMap.put("search", searchText);

        return qMap;
    }
}
