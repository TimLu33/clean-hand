package app.cleaner.tw.cleanerapp.backend.cleaner.models.member;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.net.URL;
import java.util.List;

/**
 * Created by ARTHUR on 2017/10/17.
 */

public class HirerProfile extends Profile implements Serializable {

    @Expose(serialize = false)
    private List<Integer> favorite_cleaners;

    public List<Integer> getFavorite_cleaners() {
        return favorite_cleaners;
    }

    public void setFavorite_cleaners(List<Integer> favorite_cleaners) {
        this.favorite_cleaners = favorite_cleaners;
    }
}
