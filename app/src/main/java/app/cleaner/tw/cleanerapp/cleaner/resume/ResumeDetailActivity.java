package app.cleaner.tw.cleanerapp.cleaner.resume;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.Locale;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.Profile;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.Rating;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.cleaner.cleancase.CleanerChatActivity;
import app.cleaner.tw.cleanerapp.cleaner.cleancase.UpdateSearchResultEvent;
import app.cleaner.tw.cleanerapp.cleaner.event.UpdateProposalEvent;
import app.cleaner.tw.cleanerapp.common.Constants;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityCleanerResumeDetailBinding;
import app.cleaner.tw.cleanerapp.helper.GSONUtils;
import app.cleaner.tw.cleanerapp.helper.TimeUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResumeDetailActivity extends BaseActivity {

    private static final String RESUME_MODEL = "resumeModel";
    private static final String POSITION = "position";
    private ActivityCleanerResumeDetailBinding binding;
    private ViewModel viewModel;

    private CleanService model;
    private int position;
    private Rating rating;
    private Profile profile;


    public static void start(Context context, CleanService model, int position) {
        Intent starter = new Intent(context, ResumeDetailActivity.class);
        starter.putExtra(RESUME_MODEL, model);
        starter.putExtra(POSITION, position);
        context.startActivity(starter);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBundle();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cleaner_resume_detail);
        setUpToolBar();
        binding.setPresenter(new Presenter());

        loadInfo();
    }

    private void getBundle() {
        if (getIntent().getExtras() != null) {
            model = (CleanService) getIntent().getExtras().getSerializable(RESUME_MODEL);
            Logger.json(GSONUtils.toJson(model));
            position = getIntent().getIntExtra(POSITION, -1);
        }
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("案件資訊");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private void loadInfo() {

        final KProgressHUD hud = KProgressHUD.create(activity).show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    profile = API.sharedService().cleanerGetProfile(API.sharedAuthorizationToken()).execute().body();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hud.dismiss();
                        viewModel = new ViewModel(model, profile);
                        binding.setViewModel(viewModel);
                    }
                });
            }
        }).start();
    }

    private void showProposalConfirmDialog(DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setMessage("您確定要提案嗎");
        builder.setPositiveButton("是", listener);
        builder.setNegativeButton("否", null);
        builder.show();
    }

    private void showProposalSuccessDialog(DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setMessage("您的提案已經送出");
        builder.setPositiveButton("確定", listener);
        builder.show();
    }

    public class ViewModel {

        public final ObservableField<String> houseUrl = new ObservableField<>();
        public final ObservableField<Integer> rating = new ObservableField<>();
        public final ObservableField<Integer> caseStatus = new ObservableField<>();
        public final ObservableField<String> address= new ObservableField<>();
        public final ObservableField<String> time = new ObservableField<>();
        public final ObservableField<String> houseStyle = new ObservableField<>();
        public final ObservableField<String> serviceType = new ObservableField<>();
        public final ObservableField<String> others = new ObservableField<>();
        public final ObservableField<String> cleanerName = new ObservableField<>();


        public ViewModel(CleanService cleanService, Profile profile) {

            if (!cleanService.getClean_service_photos().isEmpty()) {
                houseUrl.set(cleanService.getClean_service_photos().get(0).getImage().toExternalForm());
            }

            rating.set(Math.round(cleanService.getHirer_rating_read_only()));
            if (cleanService.getCleaner_matching() != null) {
                switch (cleanService.getCleaner_matching().getMatching_state()) {
                    case Invite:
                        caseStatus.set(Constants.CASE_STATUS.INVITE.ordinal());
                        break;
                    case Proposal:
                        caseStatus.set(Constants.CASE_STATUS.PROPOSAL.ordinal());
                        break;
                    case Candidate:
                        caseStatus.set(Constants.CASE_STATUS.CANDIDATE.ordinal());
                        break;
                    case Confirm:
                        caseStatus.set(Constants.CASE_STATUS.CONFIRM.ordinal());
                        break;
                    case Missed:
                        caseStatus.set(Constants.CASE_STATUS.MISSED.ordinal());
                        break;
                    case Closed:
                        caseStatus.set(Constants.CASE_STATUS.CLOSED.ordinal());
                        break;
                    default:
                        caseStatus.set(0);
                        break;
                }
            }

            address.set(cleanService.getAddressSecured());
            time.set(String.format(
                    Locale.getDefault(),
                    "%s ~ %s",
                    TimeUtils.getTime(model.getTime_range_start().getTime(), TimeUtils.DATE_FORMAT_MINS),
                    TimeUtils.getTime(model.getTime_range_end().getTime(), TimeUtils.DATE_FORMAT_MINS)
            ));
            houseStyle.set(cleanService.getHouse_detail().getAsString());
            serviceType.set(cleanService.getService_type_read_only().getName());
            others.set(cleanService.getOthers() != null ? cleanService.getOthers() : "無");

            if (profile != null) {
                Logger.d(profile);
                cleanerName.set(profile.getDisplay_name());
            } else {
//                Logger.e(profile.toString());
            }

        }
    }

    public class Presenter implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_confirm:
                    break;
                case R.id.btn_proposal:
                    showProposalConfirmDialog(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (i == -1) {
                                API.sharedService().cleanerProposalService(API.sharedAuthorizationToken(), model.getId()).enqueue(new Callback<JsonObject>() {
                                    @Override
                                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                        if (response.isSuccessful()) {
                                            showProposalSuccessDialog(new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    API.sharedService().cleanerGetOwnService(API.sharedAuthorizationToken(), model.getId()).enqueue(new Callback<CleanService>() {
                                                        @Override
                                                        public void onResponse(Call<CleanService> call, Response<CleanService> response) {
                                                            Logger.json(GSONUtils.toJson(response.body()));
                                                            if (position != -1) {
                                                                EventBus.getDefault().post(new UpdateProposalEvent(response.body(), position));
                                                            }
                                                            finish();
                                                        }

                                                        @Override
                                                        public void onFailure(Call<CleanService> call, Throwable t) {
                                                            Logger.e(t.getLocalizedMessage());
                                                        }
                                                    });
                                                    finish();
                                                }
                                            });
                                        } else {
                                            Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<JsonObject> call, Throwable t) {
                                        Logger.e(t.getLocalizedMessage());
                                    }
                                });
                            }
                        }
                    });
                    break;
                case R.id.btn_msg:
                    CleanerChatActivity.start(activity, model, 0, position);
                    break;
            }

        }
    }
}
