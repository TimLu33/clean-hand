package app.cleaner.tw.cleanerapp.backend.cleaner.api;

import android.support.annotation.NonNull;

import com.google.gson.JsonObject;

import java.util.List;
import java.util.Map;

import app.cleaner.tw.cleanerapp.backend.cleaner.models.documentions.Faq;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.geography.County;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.geography.Town;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.AuthTokenBean;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.CleanerProfile;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.HirerProfile;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.SimpleUser;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.User;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.news.News;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.MessageModel;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.Notification;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.Rating;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceMatching;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceQA;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceType;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.PageNumberPagination;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by ARTHUR on 2017/10/17.
 */

public interface CleanerWebService {

    // Auth

    @FormUrlEncoded
    @POST("api/phone_validate/")
    Call<JsonObject> sendPhoneValidate(@Field("phone") String phone);

    @FormUrlEncoded
    @POST("api/phone_validate/validate/")
    Call<JsonObject> validatePhone(@Field("phone") String phone, @Field("code") String code);

    @FormUrlEncoded
    @POST("rest-auth/registration/")
    Call <AuthTokenBean>register(@Field("email") String email, @Field("password1") String password1, @Field("password2") String password2);

//    @Headers({
//            "accept-language: zh-tw",
//            "cache-control: no-cache"
//    })
    @FormUrlEncoded
    @POST("rest-auth/login/")
    Call<AuthTokenBean> login(@Field("email") String email, @Field("password") String password);

    @POST("rest-auth/login_facebook/")
    Call<AuthTokenBean> login_facebook(@Body RequestBody body);

    @POST("rest-auth/login_google/")
    Call<AuthTokenBean> login_google(@Body RequestBody requestBody);

    @FormUrlEncoded
    @POST("/api/sms_password_reset/")
    Call<JsonObject> sendPasswordResetSms(@Field("phone") String phone);

    @FormUrlEncoded
    @POST("/api/sms_password_reset/reset/")
    Call<AuthTokenBean> validatePasswordResetCode(@Field("phone") String phone, @Field("code") String code);

    @FormUrlEncoded
    @POST("rest-auth/password/change/")
    Call<AuthTokenBean> changePassword(@Header("Authorization") String token, @Field("new_password1") String new_password1, @Field("new_password2") String new_password2);

    @GET("rest-auth/user/")
    Call<User> getUser(@Header("Authorization") String token);

    @PATCH("rest-auth/user/")
    Call<User> patchUser(@Header("Authorization") String token, @Body User user);


    // Cleaner

    @GET("api/cleaner_profile/")
    Call<CleanerProfile> cleanerGetProfile(@Header("Authorization") String token);

    @POST("api/cleaner_profile/")
    Call<CleanerProfile> cleanerCreateProfile(@Header("Authorization") String token, @Body CleanerProfile profile);

    @PATCH("api/cleaner_profile/")
    Call<CleanerProfile> cleanerPatchProfile(@Header("Authorization") String token, @Body CleanerProfile profile);

    @Multipart
    @PATCH("api/cleaner_profile/")
    Call<CleanerProfile> cleanerPatchProfileHeadImage(@Header("Authorization") String token, @Part MultipartBody.Part image);

    @FormUrlEncoded
    @PATCH("api/cleaner_profile/")
    Call<CleanerProfile> cleanerQuickPatchAllowInvite(@Header("Authorization") String token, @Field("allow_invite") boolean allowInvite);

    @DELETE(("api/cleaner_profile/"))
    Call<Object> cleanerDeleteProfile(@Header("Authorization") String token);


    @GET("api/cleaner_cover_photo/")
    Call<List<CleanerProfile.CoverPhoto>> cleanerGetCoverPhotos(@Header("Authorization") String token);

    @Multipart
    @POST("api/cleaner_cover_photo/")
    Call<CleanerProfile.CoverPhoto> cleanerCreateCoverPhoto(@Header("Authorization") String token, @Part MultipartBody.Part image);

    @DELETE("api/cleaner_cover_photo/{id}/")
    Call<Object> cleanerDeleteCoverPhoto(@Header("Authorization") String token, @Path("id") String id);

    @GET("api/cleaner_serve_time/")
    Call<List<CleanerProfile.CustomServiceTime>> cleanerGetCleanerServeTimes(@Header("Authorization") String token);

    @POST("api/cleaner_serve_time/")
    Call<CleanerProfile.CustomServiceTime> cleanerCreateCleanerServeTime(@Header("Authorization") String token, @Body CleanerProfile.CustomServiceTime customServiceTime);

    @DELETE("api/cleaner_serve_time/{id}/")
    Call<Object> cleanerDeleteCleanerServeTime(@Header("Authorization") String token, @Path("id") int id);

    @GET("api/cleaner_serve_area/")
    Call<List<Town>> cleanerGetOwnCleanerServeAreas(@Header("Authorization") String token, @NonNull @QueryMap Map<String, String> queries);

    @POST("api/cleaner_serve_area/{town_code}/collect/")
    Call<JsonObject> cleanerCollectCleanerServeArea(@Header("Authorization") String token, @Path("town_code") String town_code);

    @POST("api/cleaner_serve_area/{town_code}/remove/")
    Call<JsonObject> cleanerRemoveCleanerServeArea(@Header("Authorization") String token, @Path("town_code") String town_code);


    @GET("api/cleaner_serve_type/")
    Call<List<ServiceType>> cleanerGetOwnCleanerServiceTypes(@Header("Authorization") String token, @NonNull @QueryMap Map<String, String> queries);

    @POST("api/cleaner_serve_type/{id}/collect/")
    Call<JsonObject> cleanerCollectCleanerServeType(@Header("Authorization") String token, @Path("id") int id);

    @POST("api/cleaner_serve_type/{id}/remove/")
    Call<JsonObject> cleanerRemoveCleanerServeType(@Header("Authorization") String token, @Path("id") int id);


    @GET("api/cleaner_service/")
    Call<PageNumberPagination<CleanService>> cleanerGetOwnServices(@Header("Authorization") String token, @NonNull @QueryMap Map<String, String> queries);

    @GET("api/cleaner_service/{id}")
    Call<CleanService> cleanerGetOwnService(@Header("Authorization") String token, @Path("id") int id);

    @GET("api/cleaner_service_matching/")
    Call<List<ServiceMatching>> cleanerGetOwnMatchings(@Header("Authorization") String token, @NonNull @QueryMap Map<String, String> queries);

    @GET("api/cleaner_service_matching/{id}")
    Call<ServiceMatching> cleanerGetOwnMatching(@Header("Authorization") String token, @Path("id") int id);

    @FormUrlEncoded
    @POST("api/cleaner_service_matching/proposal/")
    Call<JsonObject> cleanerProposalService(@Header("Authorization") String token, @Field("service") int serviceId);

    @POST("api/cleaner_service_matching/{id}/arrive/")
    Call<JsonObject> cleanerArriveMatching(@Header("Authorization") String token, @Path("id") int matchingId);

    @POST("api/cleaner_service_matching/{id}/leave/")
    Call<JsonObject> cleanerLeaveMatching(@Header("Authorization") String token, @Path("id") int matchingId);


    @GET("api/cleaner_rating/")
    Call<PageNumberPagination<Rating>> getAllCleanerRatings(@Header("Authorization") String token, @NonNull @QueryMap Map<String, String> queries);

    @POST("api/cleaner_rating/")
    Call<Rating> hirerCreateCleanerRating(@Header("Authorization") String token, @Body Rating rating);

    // TODO: Create Raiting


    @GET("api/cleaner_find_hirer/")
    Call<PageNumberPagination<SimpleUser>> cleanerFindHirers(@Header("Authorization") String token, @NonNull @QueryMap Map<String, String> queries);

    @GET("api/cleaner_find_hirer/{id}")
    Call<SimpleUser> cleanerFindHirer(@Header("Authorization") String token, @Path("id") int hirerId);

    @GET("/api/cleaner_service_qa/")
    Call<List<ServiceQA>> cleanerGetOwnQas(@Header("Authorization") String token, @NonNull @QueryMap Map<String, String> queries);

    @FormUrlEncoded
    @POST("/api/cleaner_service_qa/")
    Call<ServiceQA> cleanerCreateQa(@Header("Authorization") String token, @FieldMap Map<String, Object> queries);

    @GET("/api/cleaner_service_qa/distinct_hirer/")
    Call<List<MessageModel>> getCleanerMessageList(@Header("Authorization") String token);

    // Hirer
    @GET("api/hirer_profile/")
    Call<HirerProfile> hirerGetProfile(@Header("Authorization") String token);

    @POST("api/hirer_profile/")
    Call<HirerProfile> hirerCreateProfile(@Header("Authorization") String token, @Body HirerProfile profile);

    @PATCH("api/hirer_profile/")
    Call<HirerProfile> hirerPatchProfile(@Header("Authorization") String token, @Body HirerProfile profile);

    @Multipart
    @PATCH("api/hirer_profile/")
    Call<CleanerProfile> hirerPatchProfileHeadImage(@Header("Authorization") String token, @Part MultipartBody.Part image);

    @DELETE("api/hirer_profile/")
    Call<Object> hirerDeleteProfile(@Header("Authorization") String token);


    @POST("api/hirer_service/")
    Call<CleanService> hirerCreateService(@Header("Authorization") String token, @Body CleanService cleanService);

    @GET("api/hirer_service/")
    Call<PageNumberPagination<CleanService>> hirerGetOwnServices(@Header("Authorization") String token, @NonNull @QueryMap Map<String, String> queries);

    @GET("api/hirer_service/{id}")
    Call<CleanService> hirerGetOwnService(@Header("Authorization") String token, @Path("id") int id);

    @Multipart
    @POST("api/hirer_service_photo/")
    Call<CleanService.ServicePhotoBean> hirerCreateServicePhoto(@Header("Authorization") String token, @Part("service") int serviceId, @Part MultipartBody.Part image);

    @GET("api/hirer_find_cleaner/")
    Call<PageNumberPagination<SimpleUser>> hirerFindCleaners(@Header("Authorization") String token, @NonNull @QueryMap Map<String, String> queries);

    @GET("api/hirer_find_cleaner/favorites/")
    Call<PageNumberPagination<SimpleUser>> hirerGetFavoriteCleaners(@Header("Authorization") String token, @NonNull @QueryMap Map<String, String> queries);

    @GET("api/hirer_find_cleaner/{id}")
    Call<SimpleUser> hirerFindCleaner(@Header("Authorization") String token, @Path("id") int cleanerId);

    @POST("api/hirer_find_cleaner/{id}/add_favorite/")
    Call<JsonObject> hirerAddFavoriteCleaner(@Header("Authorization") String token, @Path("id") int cleanerId);

    @DELETE("api/hirer_find_cleaner/{id}/remove_favorite/")
    Call<JsonObject> hirerRemoveFavoriteCleaner(@Header("Authorization") String token, @Path("id") int cleanerId);


    @GET("/api/hirer_service_qa/")
    Call<List<ServiceQA>> hirerGetOwnQas(@Header("Authorization") String token, @NonNull @QueryMap Map<String, String> queries);

    @GET("/api/hirer_service_qa/{id}")
    Call<ServiceQA> hirerGetOwnQa(@Header("Authorization") String token, @Path("id") int id);

    @GET("/api/cleaner_service_qa/{id}")
    Call<ServiceQA> CleanerGetOwnQa(@Header("Authorization") String token, @Path("id") int id);

    @FormUrlEncoded
    @POST("/api/hirer_service_qa/")
    Call<ServiceQA> hirerCreateQa(@Header("Authorization") String token, @FieldMap Map<String, Object> queries);

    @FormUrlEncoded
    @POST("api/hirer_service_matching/invite/")
    Call<JsonObject> hirerInviteService(@Header("Authorization") String token, @Field("service") int serviceId, @Field("cleaner") int cleanerId);

    @POST("api/hirer_service_matching/{id}/candidate/")
    Call<JsonObject> hirerCandidateMatching(@Header("Authorization") String token, @Path("id") int matchingId);

    @POST("api/hirer_service_matching/{id}/confirm/")
    Call<JsonObject> hirerConfirmMatching(@Header("Authorization") String token, @Path("id") int matchingId);

    @POST("api/hirer_service_matching/{id}/close/")
    Call<JsonObject> hirerCloseMatching(@Header("Authorization") String token, @Path("id") int matchingId);

    @GET("api/hirer_service_matching/{id}")
    Call<ServiceMatching> hirerGetOwnMatching(@Header("Authorization") String token, @Path("id") int matchingId);

    @GET("api/hirer_rating/")
    Call<PageNumberPagination<Rating>> getAllHirerRatings(@Header("Authorization") String token, @NonNull @QueryMap Map<String, String> queries);

    @GET("/api/hirer_service_qa/distinct_cleaner/")
    Call<List<MessageModel>> getHirerMessageList(@Header("Authorization") String token);

    // Service
    @GET("api/service_type/")
    Call<List<ServiceType>> getAllServiceTypes(@Header("Authorization") String token, @NonNull @QueryMap Map<String, String> queries);

    @GET("api/service/")
    Call<PageNumberPagination<CleanService>> getAllService(@Header("Authorization") String token, @NonNull @QueryMap Map<String, String> queries);

    @GET("api/service/{id}")
    Call<CleanService> getServiceById(@Header("Authorization") String token, @Path("id") int id);


    @GET("api/been_rated_service/")
    Call<PageNumberPagination<CleanService>> getAllRatedService(@Header("Authorization") String token, @NonNull @QueryMap Map<String, String> queries);


    //------

    // News
    @GET("api/news/")
    Call<PageNumberPagination<News>> getAllNews(@Header("Authorization") String token, @NonNull @QueryMap Map<String, String> queries);

    // FAQ
    @GET("api/faq/")
    Call<PageNumberPagination<Faq>> getAllFaq(@Header("Authorization") String token, @NonNull @QueryMap Map<String, String> queries);

    // Geography
    @GET("api/geo_county/")
    Call<List<County>> getAllCounties(@NonNull @QueryMap Map<String, String> queries);

    @GET("api/geo_town/")
    Call<PageNumberPagination<Town>> getAllTowns(@NonNull @QueryMap Map<String, String> queries);

    @FormUrlEncoded
    @POST("api/user_last_location/")
    Call<JsonObject> submitUserLastLocation(@Header("Authorization") String token, @Field("location") String formattedLocationString);

    @FormUrlEncoded
    @POST("api/fcm_device/")
    Call<JsonObject> registerFcmDevice(@Header("Authorization") String token, @Field("device_id") String device_id, @Field("registration_id") String registration_id, @Field("type") String type, @Field("active") boolean active);

    @FormUrlEncoded
    @PATCH("api/fcm_device/{registration_id}/")
    Call<JsonObject> patchFcmDevice(@Header("Authorization") String token, @Path("registration_id") String registration_id, @Field("active") boolean active);

    @DELETE("api/fcm_device/{registration_id}/")
    Call<JsonObject> unregisterFcmDevice(@Header("Authorization") String token, @Path("registration_id") String registration_id);

    @GET("api/notification")
    Call<PageNumberPagination<Notification>> getNotificationList(@Header("Authorization") String token, @NonNull @QueryMap Map<String, String> queries);

    @GET("api/notification/{id}")
    Call<Notification> getNotification(@Header("Authorization") String token, @Path("id") int id);

    @POST("api/notification/{id}/set_read/")
    Call<Notification> readNotification(@Header("Authorization") String token, @Path("id") int id);

}
