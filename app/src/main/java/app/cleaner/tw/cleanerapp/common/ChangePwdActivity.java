package app.cleaner.tw.cleanerapp.common;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Toast;

import com.orhanobut.logger.Logger;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.AuthTokenBean;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.databinding.ActivityChangePwdBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ChangePwdActivity extends BaseActivity {

    private ActivityChangePwdBinding binding;
    private ViewModel viewModel;

    public static void start(Context context) {
        Intent starter = new Intent(context, ChangePwdActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_pwd);
        setUpToolBar();
        viewModel = new ViewModel();
        binding.setViewModel(viewModel);
        binding.setPresenter(new Presenter());
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("修改密碼");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    public class ViewModel {
        public final ObservableField<String> pwd = new ObservableField<>("");
        public final ObservableField<String> confirmPwd = new ObservableField<>("");
    }

    public class Presenter implements View.OnClickListener {

        boolean isShowPwd = false;
        boolean isRepeatShowPwd = false;

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.img_show_pwd:
                    isShowPwd = !isShowPwd;
                    binding.inputPwd.setTransformationMethod(isShowPwd ? HideReturnsTransformationMethod.getInstance() : PasswordTransformationMethod.getInstance());
                    break;
                case R.id.img_show_confirm_pwd:
                    isRepeatShowPwd = !isRepeatShowPwd;
                    binding.inputConfirmPwd.setTransformationMethod(isRepeatShowPwd ? HideReturnsTransformationMethod.getInstance() : PasswordTransformationMethod.getInstance());
                    break;
                case R.id.btn_confirm:
                    if (viewModel.pwd.get().isEmpty() || viewModel.confirmPwd.get().isEmpty()) {
                        Toast.makeText(activity, "請輸入密碼", Toast.LENGTH_SHORT).show();
                    } else if (!viewModel.pwd.get().equals(viewModel.confirmPwd.get())) {
                        Toast.makeText(activity, "密碼與確認密碼不咦一致", Toast.LENGTH_SHORT).show();
                    } else {
                        //確認
                        changePassword();
                    }
                    break;
            }

        }
    }

    private void changePassword() {
        API.sharedService().changePassword(API.sharedAuthorizationToken(), viewModel.pwd.get(), viewModel.confirmPwd.get()).enqueue(new Callback<AuthTokenBean>() {
            @Override
            public void onResponse(Call<AuthTokenBean> call, Response<AuthTokenBean> response) {
                if (response.isSuccessful()) {
                    finish();
                    Toast.makeText(activity, "修改密碼成功", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AuthTokenBean> call, Throwable t) {
                Logger.e(t.getLocalizedMessage());
            }
        });
    }
}
