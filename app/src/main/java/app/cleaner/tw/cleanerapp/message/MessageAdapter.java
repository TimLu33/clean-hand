package app.cleaner.tw.cleanerapp.message;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.MessageModel;
import app.cleaner.tw.cleanerapp.databinding.MsgBoardItemBinding;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.BindingHolder> implements View.OnClickListener {

    public interface ItemClickListener {
        void onItemClickListener(View view, int position);
    }

    private ItemClickListener listener;
    List<MessageModel> messageList;

    public MessageAdapter(List<MessageModel> messageList) {
        this.messageList = messageList;
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MsgBoardItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.msg_board_item, parent, false);
        binding.getRoot().setOnClickListener(this);
        return new BindingHolder(binding);
    }

    @Override
    public void onBindViewHolder(MessageAdapter.BindingHolder holder, int position) {
        holder.binding.getRoot().setTag(position);
        MsgItemViewModel viewModel;
        viewModel = new MsgItemViewModel(messageList.get(position));
        holder.bind(viewModel);
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }

    public void setOnItemClickListener(ItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if (listener != null) {
            listener.onItemClickListener(view, (int) view.getTag());
        }
    }

    public class BindingHolder extends RecyclerView.ViewHolder {

        private MsgBoardItemBinding binding;

        public BindingHolder(MsgBoardItemBinding binding) {
            super(binding.rootView);
            this.binding = binding;
        }

        public void bind(MsgItemViewModel viewModel) {
            binding.setViewModel(viewModel);
            binding.executePendingBindings();
        }
    }
}
