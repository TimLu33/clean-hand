package app.cleaner.tw.cleanerapp.mock;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;
import java.util.Locale;

import app.cleaner.tw.cleanerapp.boss.addcase.AddCaseActivity;

public class MockAddCaseModel implements Parcelable {

    private String address;
    private Date workDate;
    private String workTime;
    private String houseStyle;
    private Boolean isEmergency;
    private String serviceType;
    private String people;
    private String hours;
    private Boolean isHasGoodCard;
    private Boolean isHasCleanLicense;
    private Boolean isHasPet;
    private Boolean isLimitFemale;
    private Boolean isOfferTools;

    public MockAddCaseModel getMockAddCaseModel(AddCaseActivity.ViewModel viewModel) {
        MockAddCaseModel model = new MockAddCaseModel();
        model.setAddress(viewModel.address.get());
        model.setWorkDate(viewModel.workDate.get());
        model.setWorkTime(viewModel.workTime.get());
        model.setIsEmergency(viewModel.isEmergency.get());
        model.setHouseStyle(String.format(Locale.getDefault(), "%s房 %s廳 %s衛 %s坪",
                viewModel.inputRoom.get(),
                viewModel.inputLivingRoom.get(),
                viewModel.inputBathRoom.get(),
                viewModel.pingNum.get()));
        model.setServiceType(viewModel.servicesType.get().getName());
        model.setPeople(viewModel.inputPeople.get());
        model.setHours(viewModel.inputHours.get());
        model.setHasGoodCard(viewModel.isHasGoodCard.get());
        model.setHasCleanLicense(viewModel.isHasCleanLicense.get());
        model.setHasPet(viewModel.isHasPet.get());
        model.setLimitFemale(viewModel.isLimitFemale.get());
        model.setOfferTools(viewModel.isOfferTools.get());
        return model;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getIsEmergency() {
        return isEmergency;
    }

    public void setIsEmergency(Boolean emergency) {
        isEmergency = emergency;
    }

    public String getHouseStyle() {
        return houseStyle;
    }

    public void setHouseStyle(String houseStyle) {
        this.houseStyle = houseStyle;
    }

    public Date getWorkDate() {
        return workDate;
    }

    public void setWorkDate(Date workDate) {
        this.workDate = workDate;
    }

    public String getWorkTime() {
        return workTime;
    }

    public void setWorkTime(String workTime) {
        this.workTime = workTime;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getPeople() {
        return people;
    }

    public void setPeople(String people) {
        this.people = people;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public Boolean getEmergency() {
        return isEmergency;
    }

    public void setEmergency(Boolean emergency) {
        isEmergency = emergency;
    }

    public Boolean getHasGoodCard() {
        return isHasGoodCard;
    }

    public void setHasGoodCard(Boolean hasGoodCard) {
        isHasGoodCard = hasGoodCard;
    }

    public Boolean getHasPet() {
        return isHasPet;
    }

    public void setHasPet(Boolean hasPet) {
        isHasPet = hasPet;
    }

    public Boolean getLimitFemale() {
        return isLimitFemale;
    }

    public void setLimitFemale(Boolean limitFemale) {
        isLimitFemale = limitFemale;
    }

    public Boolean getOfferTools() {
        return isOfferTools;
    }

    public void setOfferTools(Boolean offerTools) {
        isOfferTools = offerTools;
    }

    public Boolean getHasCleanLicense() {
        return isHasCleanLicense;
    }

    public void setHasCleanLicense(Boolean hasCleanLicense) {
        isHasCleanLicense = hasCleanLicense;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.address);
        dest.writeLong(this.workDate != null ? this.workDate.getTime() : -1);
        dest.writeString(this.workTime);
        dest.writeString(this.houseStyle);
        dest.writeValue(this.isEmergency);
        dest.writeString(this.serviceType);
        dest.writeString(this.people);
        dest.writeString(this.hours);
        dest.writeValue(this.isHasGoodCard);
        dest.writeValue(this.isHasCleanLicense);
        dest.writeValue(this.isHasPet);
        dest.writeValue(this.isLimitFemale);
        dest.writeValue(this.isOfferTools);
    }

    public MockAddCaseModel() {
    }

    protected MockAddCaseModel(Parcel in) {
        this.address = in.readString();
        long tmpWorkDate = in.readLong();
        this.workDate = tmpWorkDate == -1 ? null : new Date(tmpWorkDate);
        this.workTime = in.readString();
        this.houseStyle = in.readString();
        this.isEmergency = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.serviceType = in.readString();
        this.people = in.readString();
        this.hours = in.readString();
        this.isHasGoodCard = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.isHasCleanLicense = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.isHasPet = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.isLimitFemale = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.isOfferTools = (Boolean) in.readValue(Boolean.class.getClassLoader());
    }

    public static final Creator<MockAddCaseModel> CREATOR = new Creator<MockAddCaseModel>() {
        @Override
        public MockAddCaseModel createFromParcel(Parcel source) {
            return new MockAddCaseModel(source);
        }

        @Override
        public MockAddCaseModel[] newArray(int size) {
            return new MockAddCaseModel[size];
        }
    };
}
