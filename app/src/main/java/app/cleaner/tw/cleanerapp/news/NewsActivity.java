package app.cleaner.tw.cleanerapp.news;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.List;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.news.News;
import app.cleaner.tw.cleanerapp.backend.drfapi.builder.QueriesBuilder;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.PageNumberPagination;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.common.DividerItemDecoration;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.fcm.FCMInstanceIDService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsActivity extends BaseActivity {

    List<News> newsModelList = new ArrayList<>();
    NewsAdapter adapter;

    public static void start(Context context) {
        Intent starter = new Intent(context, NewsActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        setUpToolBar();
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        LinearLayoutManager lm = new LinearLayoutManager(this);
        adapter = new NewsAdapter(newsModelList);
        lm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(lm);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this));

        adapter.setOnItemClickListener(new NewsAdapter.ItemClickListener() {
            @Override
            public void onItemClickListener(View view, int position) {
                NewsDetailActivity.start(activity, newsModelList.get(position));
            }
        });
        loadNews();
        Logger.d(FirebaseInstanceId.getInstance().getToken());
    }

    private void setUpToolBar() {
        TextView tvTitle = (TextView) findViewById(R.id.title);
        tvTitle.setText("最新消息");
        ImageButton btnBack = (ImageButton) findViewById(R.id.btn_back);
        btnBack.setVisibility(View.VISIBLE);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void loadNews() {
        final KProgressHUD hud = KProgressHUD.create(activity).show();
        API.sharedService().getAllNews(API.sharedAuthorizationToken(), new QueriesBuilder().order("-created_datetime").pagination(1, 1000).create()).enqueue(new Callback<PageNumberPagination<News>>() {
            @Override
            public void onResponse(Call<PageNumberPagination<News>> call, Response<PageNumberPagination<News>> response) {
                hud.dismiss();
                if (response.isSuccessful()) {
                    newsModelList.addAll(response.body().getResults());
                    adapter.notifyDataSetChanged();

                    if (getIntent().getExtras() != null) {
                        String id = getIntent().getStringExtra("newsId");
                        for (News news : newsModelList) {
                            if (id.equals(String.valueOf(news.getId()))) {
                                NewsDetailActivity.start(activity, news);
                                break;
                            }
                        }
                    }

                } else {
                    Logger.e(API.shared().parseError(response).getHudMessage());
                }
            }

            @Override
            public void onFailure(Call<PageNumberPagination<News>> call, Throwable t) {
                Logger.e(t.getLocalizedMessage());
                hud.dismiss();
            }
        });
    }


}
