package app.cleaner.tw.cleanerapp.calendar;

import java.util.List;

import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;

public class CalenderObject {

    private String day;
    private List<CleanService> cleanServiceList;

    public CalenderObject(String day, List<CleanService> cleanServiceList) {
        this.day = day;
        this.cleanServiceList = cleanServiceList;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public List<CleanService> getCleanServiceList() {
        return cleanServiceList;
    }

    public void setCleanServiceList(List<CleanService> cleanServiceList) {
        this.cleanServiceList = cleanServiceList;
    }

}
