package app.cleaner.tw.cleanerapp.boss.setting;


public class QuestionObject {

    private String title;
    private String content;
    private boolean isShowContent;

    public QuestionObject(String title, String content, boolean isShowContent) {
        this.title = title;
        this.content = content;
        this.isShowContent = isShowContent;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isShowContent() {
        return isShowContent;
    }

    public void setShowContent(boolean showContent) {
        isShowContent = showContent;
    }
}
