package app.cleaner.tw.cleanerapp.cleaner.cleancase;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityCaseInfoBinding;
import app.cleaner.tw.cleanerapp.helper.GSONUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CaseInfoActivity extends BaseActivity {

    public static final String CASE_MODEL = "caseModel";
    public static final String POSITION = "position";

    private CleanService cleanService;
    private int position;

    private ActivityCaseInfoBinding binding;
    private SearchResultItemViewModel itemViewModel;

    public static void start(Context context, CleanService cleanService, int position) {
        Intent starter = new Intent(context, CaseInfoActivity.class);
        starter.putExtra(CASE_MODEL, cleanService);
        starter.putExtra(POSITION, position);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBundle();
        EventBus.getDefault().register(this);
        itemViewModel = new SearchResultItemViewModel(cleanService);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_case_info);
        setUpToolBar();
        binding.setItemViewModel(itemViewModel);
        binding.setPresenter(new Presenter());

    }

    private void setUpToolBar() {
        binding.appBar.title.setText("案件資訊");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private void getBundle() {
        if (getIntent().getExtras() != null) {
            cleanService = (CleanService) getIntent().getSerializableExtra(CASE_MODEL);
            position = getIntent().getIntExtra(POSITION, -1);
        }
    }

    private void showProposalConfirmDialog(DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setMessage("您確定要提案嗎");
        builder.setPositiveButton("是", listener);
        builder.setNegativeButton("否", null);
        builder.show();
    }

    private void showProposalSuccessDialog(DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setMessage("您的提案已經送出");
        builder.setPositiveButton("確定", listener);
        builder.show();
    }

    public class Presenter implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_message:
                    CleanerChatActivity.start(activity, itemViewModel.service.get(), 0, position);
                    break;
                case R.id.btn_proposal:
                    showProposalConfirmDialog(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (i == -1)
                                proposal();
                        }
                    });
                    break;
            }
        }
    }

    private void proposal() {
        final KProgressHUD hud = KProgressHUD.create(activity).show();
        API.sharedService().cleanerProposalService(API.sharedAuthorizationToken(), itemViewModel.service.get().getId()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                hud.dismiss();
                if (response.isSuccessful()) {
                    showProposalSuccessDialog(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            API.sharedService().cleanerGetOwnService(API.sharedAuthorizationToken(), cleanService.getId()).enqueue(new Callback<CleanService>() {
                                @Override
                                public void onResponse(Call<CleanService> call, Response<CleanService> response) {
                                    Logger.json(GSONUtils.toJson(response.body()));
                                    if (position != -1) {
                                        EventBus.getDefault().post(new UpdateSearchResultEvent(response.body(), position));
                                    }
                                    finish();
                                }

                                @Override
                                public void onFailure(Call<CleanService> call, Throwable t) {
                                    Logger.e(t.getLocalizedMessage());
                                }
                            });

                        }
                    });
                } else {
                    Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                    Logger.e(API.shared().parseError(response).getHudMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                hud.dismiss();
                Logger.e(t.getLocalizedMessage());
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpdateCleanService(UpdateSearchResultEvent event) {
        this.cleanService = event.getCleanService();
        itemViewModel = new SearchResultItemViewModel(cleanService);
        binding.setItemViewModel(itemViewModel);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
