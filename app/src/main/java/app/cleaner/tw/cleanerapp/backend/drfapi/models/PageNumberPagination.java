package app.cleaner.tw.cleanerapp.backend.drfapi.models;

import android.support.annotation.IntRange;
import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.net.URL;
import java.util.List;

/**
 * Created by ARTHUR on 2017/10/17.
 */

public class PageNumberPagination<T> implements Serializable {

    @IntRange(from = 0)
    @Expose(serialize = false)
    private int count;

    @Expose(serialize = false)
    private URL next;

    @Expose(serialize = false)
    private URL previous;

    @Expose(serialize = false)
    private List<T> results;

    public int getCount() {
        return count;
    }

    public URL getNext() {
        return next;
    }

    public URL getPrevious() {
        return previous;
    }

    public List<T> getResults() {
        return results;
    }

    @Nullable
    @IntRange(from = 1)
    public Integer nextPage() {
        return parsePage(next);
    }

    @Nullable
    @IntRange(from = 1)
    public Integer previousPage() {
        return parsePage(previous);
    }

    @Nullable
    @IntRange(from = 1)
    private Integer parsePage(URL url) {
        for (String q: url.getQuery().split("&")) {
            if (q.substring(0, 5).equals("page=")) {
                try {
                    return Integer.valueOf(q.substring(5));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
