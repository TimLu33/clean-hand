package app.cleaner.tw.cleanerapp.helper;


import com.google.gson.Gson;
import com.orhanobut.logger.Logger;

import java.lang.reflect.Type;

public class GSONUtils {

    private static final Gson GSON = new Gson();

    private GSONUtils() {
    }

    public static <T> T fromJson(String json, Class<T> clazz) {
        return GSON.fromJson(json, clazz);
    }

    public static <T> T fromJson(String json, Type typeOfT) {
        return GSON.fromJson(json, typeOfT);
    }

    public static String toJson(Object obj) {
        return GSON.toJson(obj);
    }

    public static void printJson(Object obj) {
        Logger.json(toJson(obj));
    }

}
