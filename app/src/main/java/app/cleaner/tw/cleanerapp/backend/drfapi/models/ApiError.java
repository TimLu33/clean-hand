package app.cleaner.tw.cleanerapp.backend.drfapi.models;

import android.text.TextUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * Created by ARTHUR on 2017/10/17.
 */

public class ApiError {
    private int statusCode;
    private String message;
    private Set<Map.Entry<String, JsonElement>> entrySet;

    public ApiError(int statusCode, String message) {
        super();

        this.statusCode = statusCode;
        this.message = message;
    }

    public ApiError(int statusCode, String message, Set<Map.Entry<String, JsonElement>> entrySet) {
        super();

        this.statusCode = statusCode;
        this.message = message;
        this.entrySet = entrySet;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getMessage() {
        return message;
    }

    public Set<Map.Entry<String, JsonElement>> getEntrySet() {
        return entrySet;
    }

    @Override
    public String toString() {
        return getHudMessage();
    }

    public String getHudMessage() {
        if (entrySet == null) {
            return "Unknown Error";
        }

        ArrayList<String> msgs = new ArrayList();
        for (Map.Entry<String, JsonElement> entry: entrySet) {
            if (entry.getKey().equals("detail")) {
                return entry.getValue().getAsString();
            }
            JsonArray jsonArray = entry.getValue().getAsJsonArray();
            ArrayList<String> reasons = new ArrayList();
            for (JsonElement element: jsonArray) {
                reasons.add(element.getAsString());
            }
            msgs.add(String.format(Locale.getDefault(), "%s", TextUtils.join(",", reasons)));
//            msgs.add(String.format(Locale.getDefault(), "%s: %s", entry.getKey(), TextUtils.join(",", reasons)));
        }
        return TextUtils.join("\n", msgs);
    }
}
