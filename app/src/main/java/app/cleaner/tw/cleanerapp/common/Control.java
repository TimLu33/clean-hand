package app.cleaner.tw.cleanerapp.common;

public class Control {

    private static volatile Control instance = null;

    private Control() {
    }

    public void execute(Command command) {
        command.execute();
    }

    public static Control getInstance() {

        if (instance == null) {

            synchronized (Control.class) {

                if (instance == null) {

                    instance = new Control();

                }

            }

        }

        return instance;

    }

}
