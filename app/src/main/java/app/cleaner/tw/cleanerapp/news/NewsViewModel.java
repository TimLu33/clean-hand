package app.cleaner.tw.cleanerapp.news;

import android.databinding.ObservableField;

import app.cleaner.tw.cleanerapp.backend.cleaner.models.news.News;

public class NewsViewModel {
    public final ObservableField<String> newsTitle = new ObservableField<>();
    public final ObservableField<String> newsContent = new ObservableField<>();

    public NewsViewModel(News model) {
        newsTitle.set(model.getSubject());
        newsContent.set(model.getContent());
    }
}
