package app.cleaner.tw.cleanerapp.mock;

import android.os.Parcel;
import android.os.Parcelable;

public class MockNewsModel implements Parcelable {

    private String newsTitle;
    private String newsContent;

    public MockNewsModel() {
    }

    protected MockNewsModel(Parcel in) {
        newsTitle = in.readString();
        newsContent = in.readString();
    }

    public static final Creator<MockNewsModel> CREATOR = new Creator<MockNewsModel>() {
        @Override
        public MockNewsModel createFromParcel(Parcel in) {
            return new MockNewsModel(in);
        }

        @Override
        public MockNewsModel[] newArray(int size) {
            return new MockNewsModel[size];
        }
    };

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getNewsContent() {
        return newsContent;
    }

    public void setNewsContent(String newsContent) {
        this.newsContent = newsContent;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(newsTitle);
        parcel.writeString(newsContent);
    }
}
