package app.cleaner.tw.cleanerapp.cleaner.resume;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;
import app.cleaner.tw.cleanerapp.backend.drfapi.builder.QueriesBuilder;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.PageNumberPagination;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.cleaner.cleancase.CaseInfoActivity;
import app.cleaner.tw.cleanerapp.cleaner.cleancase.UpdateSearchResultEvent;
import app.cleaner.tw.cleanerapp.cleaner.event.UpdateProposalEvent;
import app.cleaner.tw.cleanerapp.common.Constants;
import app.cleaner.tw.cleanerapp.common.DividerItemDecoration;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityCleanerResumeBinding;
import app.cleaner.tw.cleanerapp.mock.MockCleanerResumeModel;
import app.cleaner.tw.cleanerapp.news.NewsAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResumeActivity extends BaseActivity {

    private static final String IS_FROM_MSG_BOARD = "isFromMsgBoard";
    protected ActivityCleanerResumeBinding binding;
    protected List<CleanService> resumeList = new ArrayList<>();
    private boolean isFromMsgBoard;
    private boolean isLoadMore = false;


    public static void start(Context context, boolean isFromMsgBoard) {
        Intent starter = new Intent(context, ResumeActivity.class);
        starter.putExtra(IS_FROM_MSG_BOARD, isFromMsgBoard);
        context.startActivity(starter);
    }

    protected int getRole() {
        return Constants.CLEANER;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cleaner_resume);
        EventBus.getDefault().register(this);
        getBundle();
        setUpToolBar();
        initViews();

        if (getRole() == Constants.CLEANER) {
            loadResumes();
        }
    }

    private void initViews() {
        final LinearLayoutManager lm = new LinearLayoutManager(this);
        lm.setOrientation(LinearLayoutManager.VERTICAL);
        ResumeAdapter adapter = new ResumeAdapter(resumeList, getRole());
        adapter.setOnItemClickListener(new ResumeAdapter.ItemClickListener() {
            @Override
            public void onItemClickListener(View view, int position) {
                onListItemClick(resumeList.get(position), position);
            }
        });
        binding.recyclerView.setLayoutManager(lm);
        binding.recyclerView.addItemDecoration(new DividerItemDecoration(activity));
        binding.recyclerView.setAdapter(adapter);
        binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int totalCount = lm.getItemCount();
                int lastVisiblePos = lm.findLastVisibleItemPosition();
                if (!isLoadMore && totalCount < lastVisiblePos + 3) {
                    //TODO doing load more, set isLoadMore to false after load more is done
                    isLoadMore = true;
                    Logger.d("load more");
                }
            }
        });
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("案件列表");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private void getBundle() {
        if (getIntent().getExtras() != null) {
            isFromMsgBoard = getIntent().getBooleanExtra(IS_FROM_MSG_BOARD, false);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpdateResume(UpdateProposalEvent event) {
        resumeList.set(event.getPosition(), event.getCleanService());
        binding.recyclerView.getAdapter().notifyItemChanged(event.getPosition());
    }

    protected void onListItemClick(CleanService cleanService, int position) {
        if (isFromMsgBoard) {
            CaseInfoActivity.start(activity, cleanService, position);
        } else {
            ResumeDetailActivity.start(activity, cleanService, position);
        }
    }

    protected void loadResumes() {
        final KProgressHUD hud = KProgressHUD.create(activity).show();
        API.sharedService().cleanerGetOwnServices(API.sharedAuthorizationToken(), new QueriesBuilder().pagination(1, 1000).create()).enqueue(new Callback<PageNumberPagination<CleanService>>() {
            @Override
            public void onResponse(Call<PageNumberPagination<CleanService>> call, Response<PageNumberPagination<CleanService>> response) {
                hud.dismiss();
                if (response.isSuccessful()) {
                    resumeList.addAll(response.body().getResults());
                    binding.recyclerView.getAdapter().notifyDataSetChanged();
                } else {
                    Logger.e(API.shared().parseError(response).getHudMessage());
                }
            }

            @Override
            public void onFailure(Call<PageNumberPagination<CleanService>> call, Throwable t) {
                Logger.e(t.getLocalizedMessage());
                hud.dismiss();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
