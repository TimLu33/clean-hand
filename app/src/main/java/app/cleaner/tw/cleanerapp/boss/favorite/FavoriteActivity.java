package app.cleaner.tw.cleanerapp.boss.favorite;


import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.CleanerProfile;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.SimpleUser;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.Rating;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceMatching;
import app.cleaner.tw.cleanerapp.backend.drfapi.builder.QueriesBuilder;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.PageNumberPagination;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.boss.search.BossChatActivity;
import app.cleaner.tw.cleanerapp.boss.search.CleanerDetailActivity;
import app.cleaner.tw.cleanerapp.common.Constants;
import app.cleaner.tw.cleanerapp.common.DividerItemDecoration;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityFavoriteBinding;
import app.cleaner.tw.cleanerapp.helper.GSONUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavoriteActivity extends BaseActivity {

    public class FavoriteItem {
        private SimpleUser cleaner;
        private Rating rating;

        public FavoriteItem(SimpleUser cleaner, Rating rating) {
            this.cleaner = cleaner;
            this.rating = rating;
        }

        public SimpleUser getCleaner() {
            return cleaner;
        }

        public Rating getRating() {
            return rating;
        }
    }


    private ActivityFavoriteBinding binding;
    private List<FavoriteItem> cleanerList = new ArrayList<>();

    public static void start(Context context) {
        Intent starter = new Intent(context, FavoriteActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_favorite);
        setUpToolBar();
        initViews();

        loadFavorites();
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("我的最愛");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private void initViews() {
        LinearLayoutManager lm = new LinearLayoutManager(this);
        lm.setOrientation(LinearLayoutManager.VERTICAL);
        binding.recyclerView.setLayoutManager(lm);
        final FavoriteAdapter adapter = new FavoriteAdapter(cleanerList);
        adapter.setOnItemClickListener(new FavoriteAdapter.ItemClickListener() {
            @Override
            public void onItemClickListener(View view, final int position) {
                Logger.d("item click" + position);

                API.sharedService().hirerGetOwnServices(API.sharedAuthorizationToken(), new QueriesBuilder().pagination(1, 1000).create()).enqueue(new Callback<PageNumberPagination<CleanService>>() {
                    @Override
                    public void onResponse(Call<PageNumberPagination<CleanService>> call, Response<PageNumberPagination<CleanService>> response) {
                        if (response.isSuccessful()) {
                            CleanerDetailActivity.start(activity, cleanerList.get(position).getCleaner());
                        }
                    }

                    @Override
                    public void onFailure(Call<PageNumberPagination<CleanService>> call, Throwable t) {

                    }
                });

            }

            @Override
            public void onDeleteClick(View view, final int position) {
                final FavoriteItem item = cleanerList.get(position);
                // TODO 移除前或許該增加一個 Dialog
                API.sharedService().hirerRemoveFavoriteCleaner(API.sharedAuthorizationToken(), item.getCleaner().getId()).enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        if (response.isSuccessful()) {
                            cleanerList.remove(position);
                            adapter.notifyItemRemoved(position);
                            adapter.notifyItemRangeChanged(position, cleanerList.size());
                        } else {
                            Logger.e(API.shared().parseError(response).getHudMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        Logger.e(t.getLocalizedMessage());
                    }
                });
            }
        });
        binding.recyclerView.setAdapter(adapter);
        binding.recyclerView.addItemDecoration(new DividerItemDecoration(this));
        adapter.notifyDataSetChanged();
    }

    private void loadFavorites() {
        final KProgressHUD hud = KProgressHUD.create(activity).show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                final ArrayList<String> errs = new ArrayList<>();
                try {
                    Response<PageNumberPagination<SimpleUser>> response = API.sharedService().hirerGetFavoriteCleaners(API.sharedAuthorizationToken(), new QueriesBuilder().create()).execute();
                    if (response.isSuccessful()) {
                        cleanerList.clear();
                        // try to find comment from this user
                        for (SimpleUser cleaner: response.body().getResults()) {
                            Response<PageNumberPagination<Rating>> r = API.sharedService().getAllCleanerRatings(API.sharedAuthorizationToken(), new QueriesBuilder()
                                    .filter("service_matching__cleaner", cleaner.getId())
                                    .filter("service_matching__service__hirer", API.sharedUserId())
                                    .create()).execute();
                            if (r.isSuccessful() && r.body().getCount() > 0) {
                                cleanerList.add(new FavoriteItem(cleaner, r.body().getResults().get(0)));
                            } else {
                                cleanerList.add(new FavoriteItem(cleaner, null));
                            }
                        }
                    } else {
                        errs.add(API.shared().parseError(response).getHudMessage());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    errs.add(e.getLocalizedMessage());
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hud.dismiss();
                        if (!errs.isEmpty())
                            Toast.makeText(activity, TextUtils.join("\n", errs), Toast.LENGTH_SHORT).show();
                        else if (cleanerList.isEmpty())
                            Toast.makeText(activity, "您目前沒有最愛的清潔手", Toast.LENGTH_SHORT).show();
                        else
                            binding.recyclerView.getAdapter().notifyDataSetChanged();
                    }
                });
            }
        }).start();
    }

    private void showCaseListDialog(ArrayAdapter<String> arrayAdapter, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        builderSingle.setTitle("請選擇案件");
        builderSingle.setNegativeButton("取消", null);
        builderSingle.setAdapter(arrayAdapter, listener);
        builderSingle.show();
    }
}
