package app.cleaner.tw.cleanerapp.kaoui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

/**
 * Created by ARTHUR on 2017/10/24.
 */

public class LazyArrayAdapter<T> extends ArrayAdapter<T> {

    private Context mContext;
    private int fieldId = android.R.id.text1;
    private int resource;

    public int getFieldId() {
        return fieldId;
    }

    public int getResource() {
        return resource;
    }

    public Context getContext() {
        return mContext;
    }

    public LazyArrayAdapter(Context context, int textViewResourceId, ArrayList<T> items) {
        super(context, textViewResourceId, items);
        this.mContext = context;
        this.resource = textViewResourceId;
    }

    public  @NonNull
    View viewFromResource(@NonNull LayoutInflater inflater, @Nullable View convertView, @NonNull ViewGroup parent, int resource) {
        final View view;

        if (convertView == null) {
            view = inflater.inflate(resource, parent, false);
        } else {
            view = convertView;
        }

        return view;
    }
}
