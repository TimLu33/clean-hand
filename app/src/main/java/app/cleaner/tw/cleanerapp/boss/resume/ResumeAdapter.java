package app.cleaner.tw.cleanerapp.boss.resume;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;
import app.cleaner.tw.cleanerapp.databinding.BossResumeItemBinding;

public class ResumeAdapter extends RecyclerView.Adapter<ResumeAdapter.BindingHolder> implements View.OnClickListener {

    public interface ItemClickListener {
        void onItemClickListener(View view, int position);
    }

    private ItemClickListener listener;

    List<CleanService> resumeList;

    public ResumeAdapter(List<CleanService> resumeList) {
        this.resumeList = resumeList;
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        BossResumeItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.boss_resume_item, parent, false);
        binding.getRoot().setOnClickListener(this);
        return new BindingHolder(binding);
    }

    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {
        holder.binding.getRoot().setTag(position);
        ResumeItemViewModel viewModel = new ResumeItemViewModel(resumeList.get(position));
        holder.bind(viewModel);
    }

    @Override
    public int getItemCount() {
        return resumeList.size();
    }

    public void setOnItemClickListener(ItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if (listener != null) {
            listener.onItemClickListener(view, (int) view.getTag());
        }
    }

    public class BindingHolder extends RecyclerView.ViewHolder {

        private BossResumeItemBinding binding;

        public BindingHolder(BossResumeItemBinding binding) {
            super(binding.rootView);
            this.binding = binding;
        }

        public void bind(ResumeItemViewModel viewModel) {
            binding.setViewModel(viewModel);
            binding.executePendingBindings();
        }
    }
}
