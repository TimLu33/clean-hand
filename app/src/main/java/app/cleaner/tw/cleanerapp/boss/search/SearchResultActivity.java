package app.cleaner.tw.cleanerapp.boss.search;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.CleanerProfile;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.SimpleUser;
import app.cleaner.tw.cleanerapp.backend.drfapi.builder.QueriesBuilder;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.PageNumberPagination;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.boss.search.event.OnDealSuccessEvent;
import app.cleaner.tw.cleanerapp.common.DividerItemDecoration;
import app.cleaner.tw.cleanerapp.common.EndlessScrollListener;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityCleanerSearchResultBinding;
import app.cleaner.tw.cleanerapp.helper.GSONUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchResultActivity extends BaseActivity {

    private static final String QUERIES_BUILDER = "queriesBuilder";
    private static final String PAGINATION = "pagination";

    private ActivityCleanerSearchResultBinding binding;
    private QueriesBuilder queriesBuilder;
    private String next = null;
    private int page = 1;
    private List<SimpleUser> cleanerList = new ArrayList<>();

    public static void start(Context context, QueriesBuilder queriesBuilder, PageNumberPagination<SimpleUser> pagination) {
        Intent starter = new Intent(context, SearchResultActivity.class);
        starter.putExtra(QUERIES_BUILDER, queriesBuilder);
        starter.putExtra(PAGINATION, pagination);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBundle();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cleaner_search_result);
        EventBus.getDefault().register(this);
        setUpToolBar();
        LinearLayoutManager lm = new LinearLayoutManager(this);
        lm.setOrientation(LinearLayoutManager.VERTICAL);

        final SearchResultAdapter adapter = new SearchResultAdapter(cleanerList);
        adapter.setOnItemClickListener(new SearchResultAdapter.ItemClickListener() {
            @Override
            public void onItemClickListener(View view, int position) {
                CleanerDetailActivity.start(activity, cleanerList.get(position));
            }
        });
        binding.recyclerView.setLayoutManager(lm);
        binding.recyclerView.setAdapter(adapter);
        binding.recyclerView.addItemDecoration(new DividerItemDecoration(this));
        binding.recyclerView.addOnScrollListener(new EndlessScrollListener(EndlessScrollListener.SCROLL_DOWN_LOAD_MORE) {
            @Override
            public void onLoadMore() {
                QueriesBuilder loadMoreBuilder = queriesBuilder;
                page = page +1;
                loadMoreBuilder.pagination(page, 20);
                API.sharedService().hirerFindCleaners(API.sharedAuthorizationToken(), loadMoreBuilder.create()).enqueue(new Callback<PageNumberPagination<SimpleUser>>() {
                    @Override
                    public void onResponse(Call<PageNumberPagination<SimpleUser>> call, Response<PageNumberPagination<SimpleUser>> response) {
                        if (response.isSuccessful()) {
                            PageNumberPagination<SimpleUser> pagination = response.body();
//                    Toast.makeText(activity, String.format(Locale.getDefault(), "找到 %d 筆結果", pagination.getCount()), Toast.LENGTH_LONG).show();
                            if (pagination != null && pagination.getCount() > 0) {
                                cleanerList.addAll(pagination.getResults());
                                adapter.notifyDataSetChanged();
//                                SearchResultActivity.start(activity, queriesBuilder, pagination);
                            }

                        } else {
                            Logger.e(API.shared().parseError(response).getHudMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<PageNumberPagination<SimpleUser>> call, Throwable t) {
                        Logger.e(t.getLocalizedMessage());
                    }
                });

            }
        });
        adapter.notifyDataSetChanged();
    }

    private void getBundle() {
        if (getIntent().getExtras() != null) {
            queriesBuilder = (QueriesBuilder) getIntent().getSerializableExtra(QUERIES_BUILDER);
            PageNumberPagination<SimpleUser> pagination = (PageNumberPagination<SimpleUser>) getIntent().getSerializableExtra(PAGINATION);
            cleanerList.addAll(pagination.getResults());
            for (SimpleUser simpleUser : pagination.getResults()) {
                GSONUtils.printJson(simpleUser);
            }
        }
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("清潔手列表");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDealSuccessEvent(OnDealSuccessEvent event) {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
