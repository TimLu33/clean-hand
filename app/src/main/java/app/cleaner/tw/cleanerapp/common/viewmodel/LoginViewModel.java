package app.cleaner.tw.cleanerapp.common.viewmodel;

import android.databinding.ObservableField;

public class LoginViewModel {

    public final ObservableField<String> acc = new ObservableField<>();
    public final ObservableField<String> pwd = new ObservableField<>();

    public LoginViewModel() {
        acc.set("");
        pwd.set("");
    }
}
