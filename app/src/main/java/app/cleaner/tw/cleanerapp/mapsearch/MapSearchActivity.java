package app.cleaner.tw.cleanerapp.mapsearch;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;
import app.cleaner.tw.cleanerapp.backend.drfapi.builder.QueriesBuilder;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.PageNumberPagination;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.cleaner.cleancase.CaseInfoActivity;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityMapBinding;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapSearchActivity extends BaseActivity implements OnMapReadyCallback, EasyPermissions.PermissionCallbacks, GoogleMap.OnCameraMoveListener, GoogleMap.OnCameraIdleListener, GoogleMap.OnInfoWindowClickListener {

    // 連續移動時的查詢間隔
    private static final int QUERY_INTERVAL = 200;

    // 搜尋時鏡頭移動的最大包含數量(前 N 個)，畫面內包含太多會喪失辨識性
    private static final int SEARCH_LIMIT = 10;

    private static final int REQUEST_LOCATION_PERMISSION = 1001;

    private GoogleMap gMap;
    private SupportMapFragment mapFragment;
    private Geocoder geoCoder;
    private LocationManager lm;
    private ActivityMapBinding binding;
    private MapSearchViewModel mapSearchViewModel;

    private Handler handler = null;

    //使用者位置經緯度
    private LatLng myLatLng = null;
    //目前地圖位置經緯度
    private LatLng cameraLatLng = null;

    //
    private Map<CleanService, Marker> markerMap = new HashMap<>();
    private List<Marker> highlightedMarkers = new ArrayList<>();
    private boolean isCalling = false;
    private CameraPosition lastCameraPosition = null;

    public static void start(Context context) {
        Intent starter = new Intent(context, MapSearchActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_map);
        setUpToolBar();
        mapSearchViewModel = new MapSearchViewModel();
        binding.setViewModel(mapSearchViewModel);

        //按下鍵盤搜尋後的事件
        binding.inputSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {

                hideInputKeyBoard(binding.inputSearch);
                if (!mapSearchViewModel.textSearch.get().isEmpty()) {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        doSearch();
                        return true;
                    }
                } else {
                    releaseHighlightedMarkers();
                }
                return false;
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("地圖搜尋");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private void setUpMapIfNeeded() {
        if (gMap == null) {
            mapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        String[] perms = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
        if (!EasyPermissions.hasPermissions(activity, perms)) {
            EasyPermissions.requestPermissions(activity, "地圖搜尋需要權限", REQUEST_LOCATION_PERMISSION, perms);
        } else {
            setUpMap();
        }
    }

    private void setUpMap() {

        // 取得系統定位服務
        lm = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER) &&
                lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {

            if (ActivityCompat.checkSelfPermission(activity, android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    activity, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            gMap.setMyLocationEnabled(true);
            gMap.getUiSettings().setZoomControlsEnabled(true);
            gMap.setOnCameraMoveListener(this);
            gMap.setOnCameraIdleListener(this);
            gMap.setOnInfoWindowClickListener(this);

            if (mapFragment.getView() != null) {
                View locationButton =(mapFragment.getView().findViewWithTag("GoogleMapMyLocationButton"));
                View zoomInButton = mapFragment.getView().findViewWithTag("GoogleMapZoomInButton");
                View zoomLayout = (View) zoomInButton.getParent();

//                 adjust location button layout params above the zoom layout
                RelativeLayout.LayoutParams location_layout = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
                location_layout.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                location_layout.addRule(RelativeLayout.ABOVE, zoomLayout.getId());
            }

            // 建立位置的座標物件
            geoCoder = new Geocoder(activity, Locale.TRADITIONAL_CHINESE);
            moveUsersLocation();
        } else {
            Toast.makeText(activity, "請開啟定位服務", Toast.LENGTH_LONG).show();
            startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 1001); // 開啟設定頁面
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    private void moveUsersLocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location myLocation = lm.getLastKnownLocation("network");
        if (myLocation != null) {
            myLatLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
            cameraLatLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
            gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLatLng, 16));
        }

    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        setUpMap();
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }

    @Override
    public void onCameraIdle() {
        checkMarkers();
    }


    @Override
    public void onCameraMove() {

        if (gMap.getCameraPosition().equals(lastCameraPosition))
            return;

        if (isCalling)
            return;

        checkMarkers();
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        CleanService service = (CleanService) marker.getTag();
        Logger.d(service);
        CaseInfoActivity.start(activity, service, -1);
    }

    private BitmapDescriptor iconForService(CleanService service) {
        switch (service.getPriority()) {
            case Normal:
                return BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN);
            case Emergency:
                return BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);
        }
        return BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN);
    }

    // 新增旗標
    private Marker addMarker(CleanService service) {
        MarkerOptions options = new MarkerOptions()
                .position(new LatLng(service.getLocation().getCoordinates().get(1),service.getLocation().getCoordinates().get(0)))
                .icon(iconForService(service))
                .title(service.getHirer_name_read_only() + " (" + (service.getPriority() == CleanService.Priority.Emergency ? "急件": "可預約")+")")
                .snippet(String.format(
                        Locale.getDefault(),
                        "%d房 %d廳 %d衛  %d坪",
                        service.getHouse_detail().getRoom_num(),
                        service.getHouse_detail().getHall_num(),
                        service.getHouse_detail().getBathroom_num(),
                        service.getHouse_detail().getFootage()
                ));
        return gMap.addMarker(options);
    }

    // 還原被變色的搜尋旗標
    private void releaseHighlightedMarkers() {
        for (Marker marker: highlightedMarkers) {
            marker.setIcon(iconForService((CleanService) marker.getTag()));
        }
        highlightedMarkers.clear();
    }

    // 相機範圍內搜尋
    private void checkMarkers() {
        LatLngBounds latLngBounds = gMap.getProjection().getVisibleRegion().latLngBounds;
        QueriesBuilder queriesBuilder = new QueriesBuilder()
                .filter("in_bbox", String.format(
                        Locale.getDefault(),
                        "%f,%f,%f,%f",
                        latLngBounds.northeast.longitude,
                        latLngBounds.northeast.latitude,
                        latLngBounds.southwest.longitude,
                        latLngBounds.southwest.latitude))
                .pagination(1, 1000);
        API.sharedService().getAllService(API.sharedAuthorizationToken(), queriesBuilder.create()).enqueue(new Callback<PageNumberPagination<CleanService>>() {
            @Override
            public void onResponse(Call<PageNumberPagination<CleanService>> call, Response<PageNumberPagination<CleanService>> response) {

                // 拖台錢
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        isCalling = false;
                    }
                }, QUERY_INTERVAL);

                if (response.isSuccessful()) {
                    ArrayList<CleanService> newServices = new ArrayList<CleanService>(CollectionUtils.subtract(response.body().getResults(), markerMap.keySet()));
                    for (CleanService service: newServices) {
                        Marker marker = addMarker(service);
                        marker.setTag(service);
                        markerMap.put(service, marker);
                    }
                } else {
                    Logger.e(API.shared().parseError(response).getHudMessage());
                }
            }

            @Override
            public void onFailure(Call<PageNumberPagination<CleanService>> call, Throwable t) {
                isCalling = false;
                Logger.e(t.getLocalizedMessage());
            }
        });
        isCalling = true;
    }

    // 文字框搜尋
    private void doSearch() {
//        List<Address> addressList;
//        try {
//            addressList = geoCoder.getFromLocationName(viewModel.textSearch.get(), 1);
//            if (addressList.size() != 0) {
//                double lat = addressList.get(0).getLatitude();
//                double lng = addressList.get(0).getLongitude();
//                //搜尋文字轉換經緯度
//                LatLng latLng = new LatLng(lat, lng);
//                gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        releaseHighlightedMarkers();
        final KProgressHUD hud = KProgressHUD.create(activity).show();
        API.sharedService().getAllService(API.sharedAuthorizationToken(), new QueriesBuilder().search(mapSearchViewModel.textSearch.get()).pagination(1, SEARCH_LIMIT).create()).enqueue(new Callback<PageNumberPagination<CleanService>>() {
            @Override
            public void onResponse(Call<PageNumberPagination<CleanService>> call, Response<PageNumberPagination<CleanService>> response) {
                hud.dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getCount() == 0) {
                        Toast.makeText(activity, "此關鍵字沒有相關清潔服務", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    ArrayList<CleanService> newServices = new ArrayList<>(CollectionUtils.subtract(response.body().getResults(), markerMap.keySet()));
                    for (CleanService service: newServices) {
                        Marker marker = addMarker(service);
                        marker.setTag(service);
                        markerMap.put(service, marker);
                    }
                    // zoom
                    LatLngBounds.Builder builder = LatLngBounds.builder();
                    for (CleanService service: response.body().getResults()) {
                        Marker marker = markerMap.get(service);
                        marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
                        builder.include(new LatLng(service.getLocation().getCoordinates().get(1),service.getLocation().getCoordinates().get(0)));
                        highlightedMarkers.add(marker);
                    }
                    gMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 100));
                } else {
                    Logger.e(API.shared().parseError(response).getHudMessage());
                    Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PageNumberPagination<CleanService>> call, Throwable t) {
                hud.dismiss();
                Logger.e(t.getLocalizedMessage());
            }
        });
    }
}
