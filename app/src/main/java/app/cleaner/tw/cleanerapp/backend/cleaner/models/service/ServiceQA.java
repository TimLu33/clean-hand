package app.cleaner.tw.cleanerapp.backend.cleaner.models.service;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.net.URL;
import java.util.Date;

/**
 * Created by ARTHUR on 2017/10/28.
 */

public class ServiceQA {

    public enum Type {
        @SerializedName("Q")
        Q,
        @SerializedName("A")
        A
    }

    @Expose(serialize = false)
    private int id;
    @Expose
    private int service;
    @Expose
    private int cleaner;
    @Expose(serialize = false)
    private int hirer;
    @Expose(serialize = false)
    private Type type;
    @Expose
    private String content;
    @Expose(serialize = false)
    private URL image;
    @Expose(serialize = false)
    private Date created_datetime;
    @Expose(serialize = false)
    private URL sender_head_image;
    @Expose(serialize = false)
    public String sender_name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getService() {
        return service;
    }

    public void setService(int service) {
        this.service = service;
    }

    public int getCleaner() {
        return cleaner;
    }

    public void setCleaner(int cleaner) {
        this.cleaner = cleaner;
    }

    public int getHirer() {
        return hirer;
    }

    public void setHirer(int hirer) {
        this.hirer = hirer;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public URL getImage() {
        return image;
    }

    public void setImage(URL image) {
        this.image = image;
    }

    public Date getCreated_datetime() {
        return created_datetime;
    }

    public void setCreated_datetime(Date created_datetime) {
        this.created_datetime = created_datetime;
    }

    public URL getSender_head_image() {
        return sender_head_image;
    }

    public String getSender_name() {
        return sender_name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ServiceQA)
            return ((ServiceQA) obj).hashCode() == hashCode();
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        if (id != 0)
            return id;
        return super.hashCode();
    }
}
