package app.cleaner.tw.cleanerapp.boss.addcase;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

import com.jzxiang.pickerview.TimePickerDialog;
import com.jzxiang.pickerview.data.DialogType;
import com.jzxiang.pickerview.listener.OnDateSetListener;
import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.adapters.TextViewBindingAdapter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceType;
import app.cleaner.tw.cleanerapp.backend.drfapi.builder.QueriesBuilder;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.boss.PeopleAdapter;
import app.cleaner.tw.cleanerapp.common.InputFilterMinMax;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityBossAddCaseBinding;
import app.cleaner.tw.cleanerapp.helper.ImagePickerHelper;
import app.cleaner.tw.cleanerapp.helper.TimeUtils;
import app.cleaner.tw.cleanerapp.kaoui.LazyArrayAdapter;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AddCaseActivity extends BaseActivity implements OnDateSetListener, EasyPermissions.PermissionCallbacks {

    private final int REQUEST_PHOTO_CODE = 100;
    private final int REQUEST_PHOTO_CROP = 101;
    private final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1001;
    protected File tempFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "temp.png");

    private ActivityBossAddCaseBinding binding;

    private ViewModel viewModel;

    private int year, month, day;

    private ArrayList<ServiceType> serviceTypes = new ArrayList<>();

    private long calendarTimeInMillis;

    private TimePickerDialog dateTimePicker;

    private TimePickerDialog.Builder operateTimeDialogBuilder;

    StringBuilder tempSelectedTime;

    int startHour;

    int startMin;

    int endHour;

    int endMin;

    List<Bitmap> houseImageList = new ArrayList<>();

    public static void start(Context context) {
        Intent starter = new Intent(context, AddCaseActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_boss_add_case);
        EventBus.getDefault().register(this);
        setUpToolBar();
        viewModel = new ViewModel();
        binding.setViewModel(viewModel);
        binding.setPresenter(new Presenter());
        initViews();
        loadServiceTypes();
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("新增案件");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private void initViews() {
        operateTimeDialogBuilder = new TimePickerDialog.Builder().setType(DialogType.HOURS_MINS).setCallBack(this).setThemeColor(getColor(this, R.color.colorAccent));

        //限制EditText 輸入範圍
        setEditTextFilters();

        this.year = Calendar.getInstance().get(Calendar.YEAR);
        this.month = Calendar.getInstance().get(Calendar.MONTH);
        this.day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

        initAutoCompleteTextView();

        final LazyArrayAdapter<ServiceType> servicesTypeAdapter = new LazyArrayAdapter<ServiceType>(this, android.R.layout.simple_spinner_dropdown_item, serviceTypes) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = viewFromResource(LayoutInflater.from(activity), convertView, parent, getResource());
                TextView textView = view.findViewById(getFieldId());
                textView.setText(getItem(position).getName());
                return view;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                return getView(position, convertView, parent);
            }
        };
        binding.spinnerService.setAdapter(servicesTypeAdapter);
        binding.spinnerService.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                viewModel.servicesType.set(serviceTypes.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                viewModel.servicesType.set(null);
            }
        });
    }

    private void loadServiceTypes() {
        API.sharedService().getAllServiceTypes(API.sharedAuthorizationToken(), new QueriesBuilder().create()).enqueue(new Callback<List<ServiceType>>() {
            @Override
            public void onResponse(Call<List<ServiceType>> call, Response<List<ServiceType>> response) {
                if (response.isSuccessful()) {
                    serviceTypes.removeAll(serviceTypes);
                    serviceTypes.addAll(response.body());
                    if (!serviceTypes.isEmpty())
                        viewModel.servicesType.set(serviceTypes.get(0));
                    ((ArrayAdapter) binding.spinnerService.getAdapter()).notifyDataSetChanged();
                } else {
                    Logger.e(API.shared().parseError(response).getHudMessage());
                }
            }

            @Override
            public void onFailure(Call<List<ServiceType>> call, Throwable t) {
                Logger.e(t.getLocalizedMessage());
            }
        });
    }

    private void initAutoCompleteTextView() {
        binding.tvAuto.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    StartPlaceAutoComplete("");
                }

            }
        });
        binding.tvAuto.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                binding.tvAuto.setSelection(viewModel.address.get().length());
            }
        });
        binding.tvAuto.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                return true;
            }
        });
    }

    private void setEditTextFilters() {
        EditText[] editTexts_0_10 = new EditText[]{
                binding.inputRoom,
                binding.inputLivingRoom,
                binding.inputBathRoom,
        };
        for (EditText editText : editTexts_0_10) {
            editText.setFilters(new InputFilter[]{new InputFilterMinMax(0, 10)});
        }

        binding.inputPing.setFilters(new InputFilter[]{new InputFilterMinMax(1, 100)});

        binding.inputPeople.setFilters(new InputFilter[]{new InputFilterMinMax(1, 10)});
        binding.inputHours.setFilters(new InputFilter[]{new InputFilterMinMax(4, 48)});

    }

    private void showTimePicker() {
        operateTimeDialogBuilder.setTitleStringId("工作時段-起").setCurrentMillseconds(TimeUtils.getDefaultTime(9)).setMinMillseconds(TimeUtils.getDefaultTime(0));
        dateTimePicker = operateTimeDialogBuilder.build();
        dateTimePicker.show(getSupportFragmentManager(), "operateTimeStart");
    }

    public class ViewModel {

        public final ObservableField<Place> place = new ObservableField<>();

        public final ObservableField<String> address = new ObservableField<>("");

        public final ObservableField<String> floor = new ObservableField<>();

        public final ObservableField<String> room = new ObservableField<>();


        public final ObservableField<Date> workDate = new ObservableField<>();

        public final ObservableField<String> workTime = new ObservableField<>();


        public final ObservableField<Boolean> isEmergency = new ObservableField<>(false);

        public final ObservableField<String> pingNum = new ObservableField<>("");

        public final ObservableField<ServiceType> servicesType = new ObservableField<>();

        public final ObservableField<String> inputRoom = new ObservableField<>("");

        public final ObservableField<String> inputLivingRoom = new ObservableField<>("");

        public final ObservableField<String> inputBathRoom = new ObservableField<>("");

        public final ObservableField<String> inputPeople = new ObservableField<>("1");

        public final ObservableField<String> inputHours = new ObservableField<>("4");

        public final ObservableBoolean isHasGoodCard = new ObservableBoolean(false);

        public final ObservableBoolean isHasCleanLicense = new ObservableBoolean(false);

        public final ObservableBoolean isHasPet = new ObservableBoolean(false);

        public final ObservableBoolean isLimitFemale = new ObservableBoolean(false);

        public final ObservableBoolean isOfferTools = new ObservableBoolean(false);

    }

    private boolean checkForm() {

        if (viewModel.address.get() == null || viewModel.address.get().isEmpty()) {
            Toast.makeText(activity, "請輸入地址", Toast.LENGTH_SHORT).show();
            return false;
        } else if (viewModel.workDate.get() == null) {
            Toast.makeText(activity, "尚未設定日期", Toast.LENGTH_SHORT).show();
            return false;
        } else if (viewModel.workTime.get() == null) {
            Toast.makeText(activity, "尚未設定時段", Toast.LENGTH_SHORT).show();
            return false;
        } else if (viewModel.inputPeople.get() == null || viewModel.inputRoom.get().isEmpty() || viewModel.inputLivingRoom.get() == null || viewModel.inputLivingRoom.get().isEmpty() || viewModel.inputBathRoom.get() == null || viewModel.inputBathRoom.get().isEmpty()) {
            Toast.makeText(activity, "尚未設定格局", Toast.LENGTH_SHORT).show();
            return false;
        } else if (viewModel.pingNum.get() == null || viewModel.pingNum.get().isEmpty()) {
            Toast.makeText(activity, "尚未設定坪數", Toast.LENGTH_SHORT).show();
            return false;
        } else if (viewModel.inputPeople.get() == null || viewModel.inputPeople.get().isEmpty() || viewModel.inputHours.get() == null || viewModel.inputHours.get().isEmpty()) {
            Toast.makeText(activity, "尚未行情評估", Toast.LENGTH_SHORT).show();
            return false;
        } else if (viewModel.servicesType.get() == null) {
            Toast.makeText(activity, "尚未選擇清潔服務類型", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private void StartPlaceAutoComplete(String address) {
        try {
            AutocompleteFilter filter = new AutocompleteFilter.Builder()
                    .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS)
                    .setCountry("TW")
                    .build();

            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                            .setFilter(filter)
                            .zzih(address)
                            .build(activity);

            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                viewModel.place.set(PlaceAutocomplete.getPlace(activity, data));
                viewModel.address.set(viewModel.place.get().getAddress().toString());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                Logger.d(status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                Logger.d("canceled");
            }
        } else if (requestCode == REQUEST_PHOTO_CODE) {
            if (resultCode == RESULT_OK) {
                if (data.getData() != null) {
                    try {
                        ImagePickerHelper.getImageFromUri(activity, data, tempFile);
                        ImagePickerHelper.startCropImage(activity, tempFile, REQUEST_PHOTO_CROP);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else if (requestCode == REQUEST_PHOTO_CROP) {
            if (resultCode == RESULT_OK) {
                String path = tempFile.getPath();
                Bitmap bitmap = BitmapFactory.decodeFile(path);

                if (houseImageList.isEmpty()) {
                    binding.houseImage1.setImageBitmap(bitmap);
                } else if (houseImageList.size() == 1) {
                    binding.houseImage2.setImageBitmap(bitmap);
                } else {
                    binding.houseImage3.setImageBitmap(bitmap);
                }

                houseImageList.add(bitmap);

                if (tempFile.exists()) {
                    tempFile.delete();
                }
            }

        }
    }

    private void calculateHour() {
        try {
            if (viewModel.inputPeople.get() == null || Integer.valueOf(viewModel.inputPeople.get()) <= 0)
                viewModel.inputPeople.set("1");

            int cleanSpeed = 5;
            int minHour = 4;
            double ping = Double.valueOf(viewModel.pingNum.get());
            double people = Double.valueOf(viewModel.inputPeople.get());

            double ct = Math.ceil(ping / cleanSpeed / people);
            if (ct >= minHour) {
                viewModel.inputHours.set(String.valueOf((int) ct));
            } else {
                viewModel.inputHours.set(String.valueOf(minHour));
            }
            Logger.d(ct);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class Presenter implements View.OnClickListener, View.OnFocusChangeListener, TextViewBindingAdapter.AfterTextChanged {

        @Override
        public void afterTextChanged(Editable s) {
            calculateHour();
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                switch (v.getId()) {
                    case R.id.input_ping:
                    case R.id.input_people:
                        Logger.d("calculateHour");
                        calculateHour();
                        break;
                    default:
                        break;
                }
            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tv_auto:
                    StartPlaceAutoComplete(viewModel.address.get().isEmpty() ? "" : viewModel.address.get());
                    break;
                case R.id.btn_enter:
                    if (checkForm()) {
                        CleanService cleanService = new CleanService();
                        StringBuilder address = new StringBuilder(viewModel.address.get());
                        if (viewModel.floor.get() != null && !viewModel.floor.get().isEmpty())
                            address.append(viewModel.floor.get() + "樓");
                        if (viewModel.room.get() != null && !viewModel.room.get().isEmpty())
                            address.append(viewModel.room.get() + "室");
                        cleanService.setAddress(address.toString());
                        if (viewModel.place.get() != null)
                            cleanService.setGoogle_place_id(viewModel.place.get().getId());
                        Calendar dateCalender = Calendar.getInstance();
                        dateCalender.setTime(viewModel.workDate.get());
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(
                                dateCalender.get(Calendar.YEAR), dateCalender.get(Calendar.MONTH), dateCalender.get(Calendar.DAY_OF_MONTH),
                                Integer.valueOf(viewModel.workTime.get().split("~")[0].split(":")[0]),
                                Integer.valueOf(viewModel.workTime.get().split("~")[0].split(":")[1])
                        );
                        cleanService.setTime_range_start(calendar.getTime());

                        calendar.set(
                                dateCalender.get(Calendar.YEAR), dateCalender.get(Calendar.MONTH), dateCalender.get(Calendar.DAY_OF_MONTH),
                                Integer.valueOf(viewModel.workTime.get().split("~")[1].split(":")[0]),
                                Integer.valueOf(viewModel.workTime.get().split("~")[1].split(":")[1])
                        );
                        cleanService.setTime_range_end(calendar.getTime());

                        CleanService.HouseDetailBean houseDetail = new CleanService.HouseDetailBean();
                        houseDetail.setRoom_num(Integer.valueOf(viewModel.inputRoom.get()));
                        houseDetail.setHall_num(Integer.valueOf(viewModel.inputLivingRoom.get()));
                        houseDetail.setBathroom_num(Integer.valueOf(viewModel.inputBathRoom.get()));
                        houseDetail.setFootage(Integer.valueOf(viewModel.pingNum.get()));
                        cleanService.setHouse_detail(houseDetail);

                        cleanService.setPriority(viewModel.isEmergency.get() ? CleanService.Priority.Emergency : CleanService.Priority.Normal);

                        cleanService.setEstimate_cleaner_number(Integer.valueOf(viewModel.inputPeople.get()));
                        cleanService.setEstimate_minute_per_cleaner(60 * Integer.valueOf(viewModel.inputHours.get()));

                        cleanService.setNeed_good_card(viewModel.isHasGoodCard.get());
                        cleanService.setNeed_cleaner_license(viewModel.isHasCleanLicense.get());
                        cleanService.setHas_pet(viewModel.isHasPet.get());
                        cleanService.setLimit_female(viewModel.isLimitFemale.get());
                        cleanService.setProvide_tools(viewModel.isOfferTools.get());

                        cleanService.setService_type_read_only(viewModel.servicesType.get());
                        cleanService.setService_type(viewModel.servicesType.get().getId());

                        ArrayList<File> houseImages = new ArrayList<>();
                        for (Bitmap bitmap: houseImageList) {
                            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + UUID.randomUUID().toString() + ".png";
                            try {
                                bitmap.compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(path));
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                                continue;
                            }
                            houseImages.add(new File(path));
                        }

                        AddCaseConfirmActivity.start(activity, cleanService, viewModel.place.get(), houseImages);
                    }

                    break;
                case R.id.tv_work_date: {
                    new DatePickerDialog(activity, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                            Calendar calendar = Calendar.getInstance();
                            calendar.set(year, month, day);
                            viewModel.workDate.set(calendar.getTime());
                        }
                    }, AddCaseActivity.this.year, AddCaseActivity.this.month, AddCaseActivity.this.day).show();
                }
                break;
                case R.id.tv_work_time:
                    showTimePicker();
                    break;
                case R.id.root_view:
                    hideInputKeyBoard(v);
                    break;
                case R.id.btn_upload:
                    String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
                    if (!EasyPermissions.hasPermissions(activity, perms)) {
                        EasyPermissions.requestPermissions(activity, "上傳屋況需要權限", 1001, perms);
                    } else {
                        getImageFromAlbums(REQUEST_PHOTO_CODE);
                    }
                    break;
            }

        }
    }

    @Override
    public void onDateSet(TimePickerDialog view, long l) {
        switch (view.getTag()) {
            case "operateTimeStart":
                calendarTimeInMillis = TimeUtils.getDefaultTime(21, 0);
                String[] tempStart = TimeUtils.getTime(l, TimeUtils.HOUR_MINUTE_FORMAT).split(":");

                startHour = Integer.parseInt(tempStart[0]);
                startMin = Integer.parseInt(tempStart[1]);
                operateTimeDialogBuilder.setTitleStringId("服務時段-止")
                        .setCurrentMillseconds(calendarTimeInMillis)
                        .setMinMillseconds(l);
                dateTimePicker = operateTimeDialogBuilder.build();
                dateTimePicker.show(getSupportFragmentManager(), "operateTimeEnd");
                tempSelectedTime = new StringBuilder();
                tempSelectedTime.append(TimeUtils.getTime(l, TimeUtils.HOUR_MINUTE_FORMAT)).append("~");
                break;
            case "operateTimeEnd":
                String[] tempEnd = TimeUtils.getTime(l, TimeUtils.HOUR_MINUTE_FORMAT).split(":");
                endHour = Integer.parseInt(tempEnd[0]);
                endMin = Integer.parseInt(tempEnd[1]);
                tempSelectedTime.append(TimeUtils.getTime(l, TimeUtils.HOUR_MINUTE_FORMAT));
                viewModel.workTime.set(tempSelectedTime.toString());
//                binding.tvWorkTime.setText(tempSelectedTime);
                break;
        }
    }

    private void getImageFromAlbums(int requestCode) {
        try {
            if (!ImagePickerHelper.launchGooglePhotosPicker(activity, requestCode)) {
                Toast.makeText(activity, "請先安裝圖片選擇APP", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(activity, "請先安裝圖片選擇APP", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        getImageFromAlbums(REQUEST_PHOTO_CODE);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            hideInputKeyBoard(binding.getRoot());
        }
        return super.onTouchEvent(event);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAddCaseSuccess(AddCaseSuccessEvent event) {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
