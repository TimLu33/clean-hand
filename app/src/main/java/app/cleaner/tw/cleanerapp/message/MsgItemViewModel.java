package app.cleaner.tw.cleanerapp.message;

import android.databinding.ObservableField;

import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.MessageModel;

public class MsgItemViewModel {

    public final ObservableField<String> headImageUrl = new ObservableField<>();
    public final ObservableField<String> name = new ObservableField<>();
    public final ObservableField<String> createDate = new ObservableField<>();
    public final ObservableField<String> content = new ObservableField<>();

    public MsgItemViewModel(MessageModel model) {
        if (model.getSender_head_image() != null) {
            headImageUrl.set(model.getSender_head_image());
        }

        if (model.getSender_name() != null) {
            name.set(model.getSender_name());
        }

        if (model.getCreated_datetime() != null) {
            createDate.set(model.getCreated_datetime());
        }

        if (model.getContent() != null) {
            content.set(model.getContent());
        }
    }
}
