package app.cleaner.tw.cleanerapp;

import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.news.CleanServiceEvent;
import app.cleaner.tw.cleanerapp.news.NewsEvent;

public class LaunchActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (getIntent().getExtras() != null) {
            //從FCM進入
            String model = getIntent().getStringExtra("model");
            String object_id = getIntent().getStringExtra("object_id");

            if (model != null && object_id != null) {
                if (model.equals("serviceqa")) {

                } else if (model.equals("servicematching")) {

                } else if (model.equals("news")) {
                    EventBus.getDefault().postSticky(new NewsEvent(object_id));
                } else if (model.equals("cleanservice")) {
                    EventBus.getDefault().postSticky(new CleanServiceEvent(object_id));
                }
            }
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                HomeActivity.start(activity);
                finish();
            }
        }, 3000);
        TextView tvVersion = (TextView) findViewById(R.id.tv_version);
        try {
            tvVersion.setText(getString(R.string.version, getPackageManager().getPackageInfo(getPackageName(), 0).versionName));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }
}
