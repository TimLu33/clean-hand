package app.cleaner.tw.cleanerapp.boss.addcase;

import com.google.android.gms.location.places.Place;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.HirerProfile;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.Profile;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;
import app.cleaner.tw.cleanerapp.backend.drfapi.helper.DRFHelper;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityAddCaseConfirmBinding;
import app.cleaner.tw.cleanerapp.helper.GSONUtils;
import app.cleaner.tw.cleanerapp.mock.MockAddCaseModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AddCaseConfirmActivity extends BaseActivity {

    private static final String ADD_CASE_MODEL = "addCaseModel";
    private static final String PLACE = "place";
    private static final String HOUSE_IMAGES = "houseImages";
    private ActivityAddCaseConfirmBinding binding;
    private ViewModel viewModel;

    private Profile profile;

    // bundle
    private ArrayList<File> houseImages;
    private CleanService model;
    private Place place;

    public static void start(Context context, CleanService model, Place place, ArrayList<File> houseImages) {
        Intent starter = new Intent(context, AddCaseConfirmActivity.class);
        starter.putExtra(ADD_CASE_MODEL, model);
        starter.putExtra(PLACE, (Parcelable) place);
        starter.putExtra(HOUSE_IMAGES, houseImages);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBundle();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_case_confirm);
        setUpToolBar();
        viewModel = new ViewModel(model, houseImages);
        binding.setViewModel(viewModel);
        binding.setPresenter(new Presenter());

        loadInfo();
    }

    private void getBundle() {
        if (getIntent().getExtras() != null) {
            model = (CleanService) getIntent().getSerializableExtra(ADD_CASE_MODEL);
            place = getIntent().getParcelableExtra(PLACE);
            houseImages = (ArrayList<File>) getIntent().getSerializableExtra(HOUSE_IMAGES);
        }
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("新增案件");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private void loadInfo() {
        final KProgressHUD hud = new KProgressHUD(activity).show();

        API.sharedService().hirerGetProfile(API.sharedAuthorizationToken()).enqueue(new Callback<HirerProfile>() {
            @Override
            public void onResponse(Call<HirerProfile> call, Response<HirerProfile> response) {
                hud.dismiss();
                if (response.isSuccessful()) {
                    profile = response.body();
                    viewModel.bossName.set(profile.getDisplay_name());
                    viewModel.rating.set(Math.round(profile.getAvg_rating()));
                } else {
                    Logger.e(API.shared().parseError(response).getHudMessage());
                }
            }

            @Override
            public void onFailure(Call<HirerProfile> call, Throwable t) {
                Logger.e(t.getLocalizedMessage());
                hud.dismiss();
            }
        });

    }

    public class ViewModel {

        // From Profile
        public final ObservableField<String> bossName = new ObservableField<>();
        public final ObservableField<Integer> rating = new ObservableField<>();

        // From Service
        public final ObservableField<Boolean> isEmergency = new ObservableField<>();
        public final ObservableField<String> workArea = new ObservableField<>();
        public final ObservableField<String> workTime = new ObservableField<>();
        public final ObservableField<String> houseStyle = new ObservableField<>();
        public final ObservableField<String> serviceType = new ObservableField<>();
        public final ObservableField<String> others = new ObservableField<>();
        public final ObservableField<String> people = new ObservableField<>();
        public final ObservableField<String> hours = new ObservableField<>();

        public final ObservableField<String> firstHouseImage = new ObservableField<>();

        ViewModel(CleanService model, ArrayList<File> houseImages) {
            workArea.set(model.getAddress());
            workTime.set(
                    String.format(getString(R.string.common_date_format), model.getTime_range_start()) + " " +String.format(getString(R.string.common_time_format), model.getTime_range_start())+"~"+String.format(getString(R.string.common_time_format), model.getTime_range_end())
            );
            houseStyle.set(model.getHouse_detail().getAsString());
            isEmergency.set(model.getPriority() == CleanService.Priority.Emergency);
            serviceType.set(model.getService_type_read_only().getName());
            others.set(model.getOthers() == null ? "無" : model.getOthers());
            people.set(String.valueOf(model.getEstimate_cleaner_number()));
            hours.set(String.valueOf((model.getEstimate_minute_per_cleaner()/60)));

            if (!houseImages.isEmpty())
                firstHouseImage.set(houseImages.get(0).toString());
        }

    }

    private void showConfirmDialog(DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setMessage("預收服務費用100元\n如未媒合成功將退回");
        builder.setPositiveButton("是", listener);
        builder.setNegativeButton("否", null);
        builder.show();
    }

    private void createService() {
        final KProgressHUD hud = KProgressHUD.create(activity).show();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final Response<CleanService> createServiceResponse = API.sharedService().hirerCreateService(API.sharedAuthorizationToken(), model).execute();

                    if (!createServiceResponse.isSuccessful()) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hud.dismiss();
                                Toast.makeText(activity, API.shared().parseError(createServiceResponse).getHudMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                        return;
                    }

                    final CleanService remoteService = createServiceResponse.body();

                    for (File houseFile: houseImages) {
                        final Response<CleanService.ServicePhotoBean> createServicePhotoRespinse = API.sharedService().hirerCreateServicePhoto(API.sharedAuthorizationToken(), remoteService.getId(), DRFHelper.ImageRequestBodyPart("image", houseFile)).execute();
                        if (!createServicePhotoRespinse.isSuccessful()) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    hud.dismiss();
                                    Toast.makeText(activity, API.shared().parseError(createServicePhotoRespinse).getHudMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                            return;
                        }
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hud.dismiss();

                            URL payUrl = remoteService.getFee_payment().getSpg_url();
                            // TODO: 用瀏覽器帶他去付款, 金流平台說不要用「APP內WebView」, 我先隨便亂彈
                            Logger.d(payUrl);
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(payUrl.toString()));
                            startActivity(browserIntent);
                            EventBus.getDefault().post(new AddCaseSuccessEvent());
                            // TODO: back to home, finish ?
                            finish();
                        }
                    });

                    for (File houseFile: houseImages) {
                        houseFile.delete();
                    }

                } catch (IOException e) {
                    hud.dismiss();
                    e.printStackTrace();
                }
            }
        }).start();

//        API.sharedService().hirerCreateService(API.sharedAuthorizationToken(), model).enqueue(new Callback<CleanService>() {
//            @Override
//            public void onResponse(Call<CleanService> call, Response<CleanService> response) {
//                hud.dismiss();
//                if (response.isSuccessful()) {
//                    CleanService remoteService = response.body();
//                    URL payUrl = remoteService.getFee_payment().getSpg_url();
//                    // TODO: 用瀏覽器帶他去付款, 金流平台說不要用「APP內WebView」, 我先隨便亂彈
//                    Logger.d(payUrl);
//                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(payUrl.toString()));
//                    startActivity(browserIntent);
//                } else {
//                    Logger.e(API.shared().parseError(response).getHudMessage());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<CleanService> call, Throwable t) {
//                hud.dismiss();
//                Logger.e(t.getLocalizedMessage());
//            }
//        });
    }

    public class Presenter implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_confirm:
                    showConfirmDialog(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            createService();
                        }
                    });
                    break;
            }

        }
    }
}
