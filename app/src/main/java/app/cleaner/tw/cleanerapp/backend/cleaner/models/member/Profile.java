package app.cleaner.tw.cleanerapp.backend.cleaner.models.member;


import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.net.URL;

/**
 * Created by ARTHUR on 2017/10/22.
 */

public class Profile implements Serializable {

    @Expose
    protected String display_name;

    @Expose(serialize = false)
    protected URL head_image;

    @Expose(serialize = false)
    protected float avg_rating;

    @Expose(serialize = false)
    protected int min_rating;

    @Expose(serialize = false)
    protected int max_rating;

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public URL getHead_image() {
        return head_image;
    }

    public void setHead_image(URL head_image) {
        this.head_image = head_image;
    }

    public float getAvg_rating() {
        return avg_rating;
    }

    public void setAvg_rating(float avg_rating) {
        this.avg_rating = avg_rating;
    }

    public Object getMin_rating() {
        return min_rating;
    }

    public void setMin_rating(int min_rating) {
        this.min_rating = min_rating;
    }

    public Object getMax_rating() {
        return max_rating;
    }

    public void setMax_rating(int max_rating) {
        this.max_rating = max_rating;
    }

//    @Override
//    public void writeToParcel(Parcel parcel, int i) {
//        parcel.writeString(display_name);
//        parcel.writeFloat(avg_rating);
//        parcel.writeInt(min_rating);
//        parcel.writeInt(max_rating);
//    }
//
//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    public Profile() {
//
//    }
//
//    protected Profile(Parcel in) {
//        display_name = in.readString();
//        avg_rating = in.readFloat();
//        min_rating = in.readInt();
//        max_rating = in.readInt();
//    }
//
//    public static final Creator<Profile> CREATOR = new Creator<Profile>() {
//        @Override
//        public Profile createFromParcel(Parcel in) {
//            return new Profile(in);
//        }
//
//        @Override
//        public Profile[] newArray(int size) {
//            return new Profile[size];
//        }
//    };
}
