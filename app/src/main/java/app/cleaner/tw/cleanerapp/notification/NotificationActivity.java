package app.cleaner.tw.cleanerapp.notification;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.SimpleUser;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.Notification;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceMatching;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceQA;
import app.cleaner.tw.cleanerapp.backend.drfapi.builder.QueriesBuilder;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.PageNumberPagination;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.boss.CaseInfoActivity;
import app.cleaner.tw.cleanerapp.boss.UpdateUnReadNotificationEvent;
import app.cleaner.tw.cleanerapp.boss.search.BossChatActivity;
import app.cleaner.tw.cleanerapp.cleaner.cleancase.CleanerChatActivity;
import app.cleaner.tw.cleanerapp.cleaner.resume.ResumeDetailActivity;
import app.cleaner.tw.cleanerapp.common.Constants;
import app.cleaner.tw.cleanerapp.common.DividerItemDecoration;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityNotificationBinding;
import app.cleaner.tw.cleanerapp.helper.GSONUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends BaseActivity {

    private static final String ROLE = "role";
    private ActivityNotificationBinding binding;
    private List<Notification> notificationList = new ArrayList<>();
    private int role;

    public static void start(Context context, int role) {
        Intent starter = new Intent(context, NotificationActivity.class);
        starter.putExtra(ROLE, role);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_notification);
        EventBus.getDefault().register(this);
        getBundle();
        setUpToolBar();
        initViews();
        getNotification();
    }

    private void getBundle() {
        if (getIntent().getExtras() != null) {
            role = getIntent().getIntExtra(ROLE, 0);
        }
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("通知列表");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private void initViews() {
        NotificationAdapter adapter = new NotificationAdapter(notificationList);
        adapter.setOnItemClickListener(new NotificationAdapter.ItemClickListener() {
            @Override
            public void onItemClickListener(View view, final int position) {
                Logger.d(position);
                if (!notificationList.get(position).isRead()) {
                    API.sharedService().readNotification(API.sharedAuthorizationToken(), notificationList.get(position).getId()).enqueue(new Callback<Notification>() {
                        @Override
                        public void onResponse(Call<Notification> call, Response<Notification> response) {
                            if (response.isSuccessful()) {
                                notificationList.set(position, response.body());
                                binding.recyclerView.getAdapter().notifyItemChanged(position);
                                EventBus.getDefault().post(new UpdateUnReadNotificationEvent());
                                switch (notificationList.get(position).getContent_type().getModel()) {
                                    case "serviceqa":
                                        goToChat(notificationList.get(position).getObject_id());
                                        break;
                                    case "servicematching":
                                        if (role == Constants.CLEANER) {
                                            goCleanerResume(notificationList.get(position).getObject_id());
                                        } else {

                                        }
                                        break;
                                }
                            } else {
                                Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Notification> call, Throwable t) {
                            Toast.makeText(activity, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    switch (notificationList.get(position).getContent_type().getModel()) {
                        case "serviceqa":
                            goToChat(notificationList.get(position).getObject_id());
                            break;
                        case "servicematching":
                            if (role == Constants.CLEANER) {
                                goCleanerResume(notificationList.get(position).getObject_id());
                            } else {
                                goHirerResume(notificationList.get(position).getObject_id());
                            }
                            break;
                    }
                }
            }
        });
        LinearLayoutManager lm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        binding.recyclerView.setAdapter(adapter);
        binding.recyclerView.setLayoutManager(lm);
        binding.recyclerView.addItemDecoration(new DividerItemDecoration(activity));
        binding.recyclerView.getItemAnimator().setChangeDuration(0);
    }

    private void getNotification() {
        final KProgressHUD hud = KProgressHUD.create(activity).show();
        API.sharedService().getNotificationList(API.sharedAuthorizationToken(), new QueriesBuilder().pagination(1, 1000).create()).enqueue(new Callback<PageNumberPagination<Notification>>() {
            @Override
            public void onResponse(Call<PageNumberPagination<Notification>> call, Response<PageNumberPagination<Notification>> response) {
                hud.dismiss();
                if (response.isSuccessful()) {
                    notificationList.clear();
                    notificationList.addAll(response.body().getResults());
                    for (Notification notification : response.body().getResults()) {
                        GSONUtils.printJson(notification);
                    }
                    binding.recyclerView.getAdapter().notifyDataSetChanged();

                } else {
                    Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PageNumberPagination<Notification>> call, Throwable t) {
                hud.dismiss();
                Logger.e(t.getLocalizedMessage());
            }
        });

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpdateUnReadNotification(RefreshNotificationEvent event) {
        getNotification();
    }

    private void goToChat(int messageId) {
        final KProgressHUD hud = KProgressHUD.create(this).show();
        if (role == Constants.BOSS) {
            API.sharedService().hirerGetOwnQa(API.sharedAuthorizationToken(), messageId).enqueue(new Callback<ServiceQA>() {
                @Override
                public void onResponse(Call<ServiceQA> call, Response<ServiceQA> response) {
                    if (response.isSuccessful()) {
                        API.sharedService().hirerFindCleaner(API.sharedAuthorizationToken(), response.body().getCleaner()).enqueue(new Callback<SimpleUser>() {
                            @Override
                            public void onResponse(Call<SimpleUser> call, Response<SimpleUser> response) {
                                hud.dismiss();
                                if (response.isSuccessful()) {
                                    BossChatActivity.start(activity, response.body());
                                } else {
                                    Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<SimpleUser> call, Throwable t) {
                                hud.dismiss();
                                Logger.e(t.getLocalizedMessage());
                            }
                        });
                    } else {
                        Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ServiceQA> call, Throwable t) {
                    hud.dismiss();
                    Logger.e(t.getLocalizedMessage());
                }
            });

        } else {
            API.sharedService().CleanerGetOwnQa(API.sharedAuthorizationToken(), messageId).enqueue(new Callback<ServiceQA>() {
                @Override
                public void onResponse(Call<ServiceQA> call, Response<ServiceQA> response) {
                    API.sharedService().cleanerFindHirer(API.sharedAuthorizationToken(), response.body().getHirer()).enqueue(new Callback<SimpleUser>() {
                        @Override
                        public void onResponse(Call<SimpleUser> call, Response<SimpleUser> response) {
                            hud.dismiss();
                            if (response.isSuccessful()) {
                                CleanerChatActivity.start(activity, null, response.body().getId(), 0);
                            } else {
                                Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<SimpleUser> call, Throwable t) {
                            hud.dismiss();
                            Logger.e(t.getLocalizedMessage());
                        }
                    });
                }

                @Override
                public void onFailure(Call<ServiceQA> call, Throwable t) {
                    hud.dismiss();
                    Logger.e(t.getLocalizedMessage());
                }
            });

        }
    }

    private void goCleanerResume(int id) {
        final KProgressHUD hud = KProgressHUD.create(activity);
        hud.show();
        API.sharedService().cleanerGetOwnMatching(API.sharedAuthorizationToken(), id).enqueue(new Callback<ServiceMatching>() {
            @Override
            public void onResponse(Call<ServiceMatching> call, Response<ServiceMatching> response) {
                if (response.isSuccessful()) {
                    GSONUtils.printJson(response.body());
                    API.sharedService().cleanerGetOwnService(API.sharedAuthorizationToken(), response.body().getService()).enqueue(new Callback<CleanService>() {
                        @Override
                        public void onResponse(Call<CleanService> call, Response<CleanService> response) {
                            if (response.isSuccessful()) {
                                hud.dismiss();
                                ResumeDetailActivity.start(activity, response.body(), -1);
                            } else {
                                hud.dismiss();
                                Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<CleanService> call, Throwable t) {
                            hud.dismiss();
                            Toast.makeText(activity, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    hud.dismiss();
                    Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ServiceMatching> call, Throwable t) {
                hud.dismiss();
                Toast.makeText(activity, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void goHirerResume(int id) {
        final KProgressHUD hud = KProgressHUD.create(activity);
        hud.show();
        API.sharedService().hirerGetOwnMatching(API.sharedAuthorizationToken(), id).enqueue(new Callback<ServiceMatching>() {
            @Override
            public void onResponse(Call<ServiceMatching> call, Response<ServiceMatching> response) {
                if (response.isSuccessful()) {
                    API.sharedService().hirerGetOwnService(API.sharedAuthorizationToken(), response.body().getService()).enqueue(new Callback<CleanService>() {
                        @Override
                        public void onResponse(Call<CleanService> call, Response<CleanService> response) {
                            if (response.isSuccessful()) {
                                hud.dismiss();
                                CaseInfoActivity.start(activity, response.body());
                            } else {
                                hud.dismiss();
                                Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<CleanService> call, Throwable t) {
                            hud.dismiss();
                            Toast.makeText(activity, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    hud.dismiss();
                    Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ServiceMatching> call, Throwable t) {
                hud.dismiss();
                Toast.makeText(activity, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
