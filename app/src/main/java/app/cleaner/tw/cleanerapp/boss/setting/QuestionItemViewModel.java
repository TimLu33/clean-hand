package app.cleaner.tw.cleanerapp.boss.setting;


import android.databinding.ObservableField;

public class QuestionItemViewModel {

    public final ObservableField<String> faqTitle = new ObservableField<>();
    public final ObservableField<String> faqContent = new ObservableField<>();
    public final ObservableField<Boolean> isShowContent = new ObservableField<>(false);

    public QuestionItemViewModel(QuestionObject object) {
        faqTitle.set(object.getTitle());
        faqContent.set(object.getContent());
        isShowContent.set(object.isShowContent());
    }
}
