package app.cleaner.tw.cleanerapp.cleaner.home;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.GravityCompat;
import android.view.View;
import android.widget.Toast;

import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.Trigger;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import app.cleaner.tw.cleanerapp.HomeActivity;
import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.LocationJobService;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.CleanerProfile;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.SimpleUser;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.User;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.Notification;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceMatching;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceQA;
import app.cleaner.tw.cleanerapp.backend.drfapi.builder.QueriesBuilder;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.PageNumberPagination;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.calendar.CalendarActivity;
import app.cleaner.tw.cleanerapp.cleaner.cleancase.CaseInfoActivity;
import app.cleaner.tw.cleanerapp.cleaner.cleancase.CleanerChatActivity;
import app.cleaner.tw.cleanerapp.cleaner.cleancase.SearchCaseActivity;
import app.cleaner.tw.cleanerapp.cleaner.resume.ResumeActivity;
import app.cleaner.tw.cleanerapp.cleaner.resume.ResumeDetailActivity;
import app.cleaner.tw.cleanerapp.cleaner.setting.SettingActivity;
import app.cleaner.tw.cleanerapp.common.Constants;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.helper.GSONUtils;
import app.cleaner.tw.cleanerapp.mapsearch.MapSearchActivity;
import app.cleaner.tw.cleanerapp.message.MessageBoardActivity;
import app.cleaner.tw.cleanerapp.news.CleanServiceEvent;
import app.cleaner.tw.cleanerapp.news.NewsActivity;
import app.cleaner.tw.cleanerapp.cleaner.report.ReportSystemActivity;
import app.cleaner.tw.cleanerapp.cleaner.servicessettings.ServicesSettingsActivity;
import app.cleaner.tw.cleanerapp.databinding.ActivityCleanerHomeBinding;
import app.cleaner.tw.cleanerapp.news.NewsEvent;
import app.cleaner.tw.cleanerapp.notification.NotificationActivity;
import app.cleaner.tw.cleanerapp.register.RegisterPhoneVerifyActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CleanerHomeActivity extends BaseActivity {

    private ActivityCleanerHomeBinding binding;
    private ViewModel viewModel;

    public static void start(Context context) {
        Intent starter = new Intent(context, CleanerHomeActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cleaner_home);
        setUpToolBar();
        loadUserData();
        viewModel = new ViewModel();
        binding.setViewModel(viewModel);
        binding.setPresenter(new Presenter());

        // Schedule LocationJobService
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        Job job = dispatcher.newJobBuilder()
                .setService(LocationJobService.class)
                .setTag("LocationJobService")
                .setRecurring(true)
                .setTrigger(Trigger.executionWindow(0, 60))
                .setLifetime(Lifetime.FOREVER)
                .setReplaceCurrent(true)
                .setConstraints(Constraint.ON_ANY_NETWORK)
                .build();
        dispatcher.mustSchedule(job);
        QueriesBuilder queries = new QueriesBuilder();
        queries.pagination(1, 1000)
                .filter("read", false);

        API.sharedService().getNotificationList(API.sharedAuthorizationToken(), queries.create()).enqueue(new Callback<PageNumberPagination<Notification>>() {
            @Override
            public void onResponse(Call<PageNumberPagination<Notification>> call, Response<PageNumberPagination<Notification>> response) {
                if (response.isSuccessful()) {
                    Logger.d("未讀筆數" + response.body().getCount());
                    viewModel.pushBadgeNum.set(response.body().getCount());

                    if (viewModel.pushBadgeNum.get() != 0) {
                        binding.appBar.badge.setVisibility(View.VISIBLE);
                    } else {
                        binding.appBar.badge.setVisibility(View.GONE);
                    }

                } else {
                    Logger.e("error");
                }
            }

            @Override
            public void onFailure(Call<PageNumberPagination<Notification>> call, Throwable t) {
                Logger.e("error");
            }
        });

    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onNewsReceive(NewsEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        String id = event.getId();
        goNews(id);
    }

    @Override
    protected void onDestroy() {
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        dispatcher.cancel("LocationJobService");

        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("我是清潔手");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));

        binding.appBar.menuRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.drawerLayout.isDrawerOpen(GravityCompat.END)) {
                    binding.drawerLayout.closeDrawer(GravityCompat.END);
                } else {
                    binding.drawerLayout.openDrawer(GravityCompat.END);
                }
            }
        });
    }

    private void loadUserData() {
        final KProgressHUD hud = KProgressHUD.create(this).show();
        API.sharedService().getUser(API.sharedAuthorizationToken()).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                hud.dismiss();
                if (response.isSuccessful()) {
                    User user = response.body();
                    Logger.json(GSONUtils.toJson(user));
                    CleanerProfile profile = user.getCleaner_profile();
                    viewModel.user.set(user);
                    if (profile != null) {
                        viewModel.profile.set(profile);
                        viewModel.isBusinessOn.set(viewModel.profile.get().isAllow_invite());
                    }

                    if (viewModel.user.get().getCellphone() == null) {
                        RegisterPhoneVerifyActivity.start(activity, Constants.CLEANER, true);
                        finish();
                    }

                    if (getIntent().getExtras() != null && viewModel.user.get().getCellphone() != null) {
                        //從FCM進入
                        String model = getIntent().getStringExtra("model");
                        if (model.equals("serviceqa")) {
                            String id = getIntent().getStringExtra("messageId");
                            goChat(hud, id);
                        } else if (model.equals("servicematching")) {
                            String id = getIntent().getStringExtra("matchingId");
                            goResume(hud, id);
                        } else if (model.equals("news")) {
                            String id = getIntent().getStringExtra("newsId");
                            goNews(id);
                        } else if (model.equals("cleanservice")) {
                            String id = getIntent().getStringExtra("serviceId");
                            goCleanService(hud, Integer.valueOf(id));
                        }
                        return;
                    }

                    if (cleanServiceId != -1) {
                        goCleanService(hud, cleanServiceId);
                    }

                } else {
                    Logger.e(API.shared().parseError(response).getHudMessage());
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                hud.dismiss();
                Logger.e(t.getLocalizedMessage());
            }
        });
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onCleanServiceEvent(CleanServiceEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        cleanServiceId = Integer.valueOf(event.getId());
    }

    int cleanServiceId = -1;

    private void goCleanService(final KProgressHUD hud, int id) {
        hud.show();
        API.sharedService().getServiceById(API.sharedAuthorizationToken(), Integer.valueOf(id)).enqueue(new Callback<CleanService>() {
            @Override
            public void onResponse(Call<CleanService> call, Response<CleanService> response) {
                hud.dismiss();
                if (response.isSuccessful()) {
                    Logger.json(GSONUtils.toJson(response.body()));
                    if (response.body() != null) {
                        CaseInfoActivity.start(CleanerHomeActivity.this, response.body(), -1);
                    }
                } else {
                    Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CleanService> call, Throwable t) {
                hud.dismiss();
                Logger.e(t.getLocalizedMessage());
            }
        });
    }

    private void goChat(final KProgressHUD hud, String id) {
        API.sharedService().CleanerGetOwnQa(API.sharedAuthorizationToken(), Integer.valueOf(id)).enqueue(new Callback<ServiceQA>() {
            @Override
            public void onResponse(Call<ServiceQA> call, Response<ServiceQA> response) {
                API.sharedService().cleanerFindHirer(API.sharedAuthorizationToken(), response.body().getHirer()).enqueue(new Callback<SimpleUser>() {
                    @Override
                    public void onResponse(Call<SimpleUser> call, Response<SimpleUser> response) {
                        hud.dismiss();
                        if (response.isSuccessful()) {
                            CleanerChatActivity.start(activity, null, response.body().getId(), 0);
                        } else {
                            Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<SimpleUser> call, Throwable t) {
                        hud.dismiss();
                        Logger.e(t.getLocalizedMessage());
                    }
                });
            }

            @Override
            public void onFailure(Call<ServiceQA> call, Throwable t) {
                hud.dismiss();
                Logger.e(t.getLocalizedMessage());
            }
        });
    }

    private void goResume(final KProgressHUD hud, String id) {
        API.sharedService().cleanerGetOwnMatching(API.sharedAuthorizationToken(), Integer.parseInt(id)).enqueue(new Callback<ServiceMatching>() {
            @Override
            public void onResponse(Call<ServiceMatching> call, Response<ServiceMatching> response) {
                if (response.isSuccessful()) {
                    GSONUtils.printJson(response.body());
                    API.sharedService().cleanerGetOwnService(API.sharedAuthorizationToken(), response.body().getService()).enqueue(new Callback<CleanService>() {
                        @Override
                        public void onResponse(Call<CleanService> call, Response<CleanService> response) {
                            if (response.isSuccessful()) {
                                hud.dismiss();
                                ResumeDetailActivity.start(activity, response.body(), -1);
                            } else {
                                hud.dismiss();
                                Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<CleanService> call, Throwable t) {
                            hud.dismiss();
                            Toast.makeText(activity, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    hud.dismiss();
                    Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ServiceMatching> call, Throwable t) {
                hud.dismiss();
                Toast.makeText(activity, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void goNews(String id) {
        Intent i = new Intent();
        i.putExtra("newsId", id);
        startActivity(i.setClass(this, NewsActivity.class));
    }

    @Override
    public void onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.END)) {
            binding.drawerLayout.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    public class ViewModel {

        public final ObservableField<Boolean> isBusinessOn = new ObservableField<>(false);
        public final ObservableField<User> user = new ObservableField<>();
        public final ObservableField<CleanerProfile> profile = new ObservableField<>();

        //TODO 需要填通知與留言版的 badge number
        public final ObservableField<Integer> pushBadgeNum = new ObservableField<>(0);
        public final ObservableField<String> msgBoardBadgeNum = new ObservableField<>("11");

    }

    public class Presenter implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_services_settings:
                    if (viewModel.user.get() != null && viewModel.user.get().getCleaner_profile() != null) {
                        ServicesSettingsActivity.start(activity);
                    } else {
                        Toast.makeText(activity, "請先完成個人資訊設定", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.btn_case_search:
                case R.id.tv_search_case:
                    if (viewModel.user.get() != null && viewModel.user.get().getCleaner_profile() != null) {
                        SearchCaseActivity.start(activity);
                    } else {
                        Toast.makeText(activity, "請先完成個人資訊設定", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.btn_map_search:
                case R.id.tv_map_search:
                    if (viewModel.user.get() != null && viewModel.user.get().getCleaner_profile() != null) {
                        MapSearchActivity.start(activity);
                    } else {
                        Toast.makeText(activity, "請先完成個人資訊設定", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.btn_resume:
                    if (viewModel.user.get() != null && viewModel.user.get().getCleaner_profile() != null) {
                        ResumeActivity.start(activity, false);
                    } else {
                        Toast.makeText(activity, "請先完成個人資訊設定", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.message_notification_layout:
                    if (viewModel.user.get() != null && viewModel.user.get().getCleaner_profile() != null) {
                        NotificationActivity.start(activity, Constants.CLEANER);
                    } else {
                        Toast.makeText(activity, "請先完成個人資訊設定", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.btn_calendar:
                    if (viewModel.user.get() != null && viewModel.user.get().getCleaner_profile() != null) {
                        CalendarActivity.start(activity, Constants.CLEANER);
                    } else {
                        Toast.makeText(activity, "請先完成個人資訊設定", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.btn_business_status: {
                    if (viewModel.user.get() != null && viewModel.user.get().getCleaner_profile() != null) {
                        final boolean toChange = !viewModel.isBusinessOn.get();
                        final KProgressHUD hud = KProgressHUD.create(activity).show();
                        API.sharedService().cleanerQuickPatchAllowInvite(API.sharedAuthorizationToken(), toChange).enqueue(new Callback<CleanerProfile>() {
                            @Override
                            public void onResponse(Call<CleanerProfile> call, Response<CleanerProfile> response) {
                                hud.dismiss();
                                if (response.isSuccessful()) {
                                    viewModel.profile.set(response.body());
                                    viewModel.isBusinessOn.set(viewModel.profile.get().isAllow_invite());
                                } else {
                                    Logger.e(API.shared().parseError(response).getHudMessage());
                                }
                            }

                            @Override
                            public void onFailure(Call<CleanerProfile> call, Throwable t) {
                                hud.dismiss();
                                Logger.e(t.getLocalizedMessage());
                            }
                        });
                    } else {
                        Toast.makeText(activity, "請先完成個人資訊設定", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
                case R.id.btn_report:
                case R.id.tv_report_system:
                    if (viewModel.user.get() != null && viewModel.user.get().getCleaner_profile() != null) {
                        ReportSystemActivity.start(activity);
                    } else {
                        Toast.makeText(activity, "請先完成個人資訊設定", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.btn_news:
                case R.id.tv_news:
                    NewsActivity.start(activity);
                    break;
                case R.id.btn_settings:
                    SettingActivity.start(activity);
                    break;
                case R.id.tv_home:
                    Intent a = new Intent(activity, HomeActivity.class);
                    a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(a);
                    break;
                case R.id.tv_cleaner_home:
                    onBackPressed();
                    break;
                case R.id.message_board_layout:
                    if (viewModel.user.get() != null && viewModel.user.get().getCleaner_profile() != null) {
                        MessageBoardActivity.start(activity, Constants.CLEANER);
                    } else {
                        Toast.makeText(activity, "請先完成個人資訊設定", Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    }
}
