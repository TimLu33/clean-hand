package app.cleaner.tw.cleanerapp.boss.search;


import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.orhanobut.logger.Logger;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.CleanerProfile;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.SimpleUser;
import de.hdodenhof.circleimageview.CircleImageView;

public class SearchResultItemViewModel {

    public final ObservableField<String> coverPhoto = new ObservableField<>();
    public final ObservableField<String> headImage = new ObservableField<>();
    public final ObservableField<String> cleanerName = new ObservableField<>();
    public final ObservableField<Integer> rating = new ObservableField<>();
    public final ObservableField<String> recommendation = new ObservableField<>();

    public SearchResultItemViewModel(SimpleUser cleaner) {
        if (cleaner.getCleaner_profile().getCover_photos() != null && !cleaner.getCleaner_profile().getCover_photos().isEmpty()) {
            coverPhoto.set(cleaner.getCleaner_profile().getCover_photos().get(0).getImage().toString());
        }

        if (cleaner.getCleaner_profile().getHead_image() != null ) {
            headImage.set(cleaner.getCleaner_profile().getHead_image().toString());
        }

        if (cleaner.getCleaner_profile().getDisplay_name() != null) {
            cleanerName.set(cleaner.getCleaner_profile().getDisplay_name());
        }

        rating.set((int) cleaner.getCleaner_profile().getAvg_rating());
        recommendation.set(cleaner.getCleaner_profile().getRecommendation());
    }

    @BindingAdapter({"coverPhotoUrl"})
    public static void setCoverImage(ImageView imageView, String url) {
        if (url != null && !url.isEmpty()) {
            Glide.with(imageView.getContext())
                    .load(url)
                    .apply(new RequestOptions().centerCrop().error(R.color.white))
                    .into(imageView);
        } else {
            Glide.with(imageView.getContext())
                    .clear(imageView);
        }
    }

    @BindingAdapter({"headPhotoUrl"})
    public static void setHeadImage(CircleImageView imageView, String url) {
        if (url != null && !url.isEmpty()) {
            Glide.with(imageView.getContext())
                    .load(url)
                    .apply(new RequestOptions().centerCrop().error(R.drawable.ic_people))
                    .into(imageView);
        } else {
            Glide.with(imageView.getContext())
                    .clear(imageView);
        }
    }
}
