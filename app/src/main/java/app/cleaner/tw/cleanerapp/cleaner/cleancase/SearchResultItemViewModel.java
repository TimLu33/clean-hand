package app.cleaner.tw.cleanerapp.cleaner.cleancase;

import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.orhanobut.logger.Logger;

import java.util.Date;
import java.util.Locale;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceMatching;
import app.cleaner.tw.cleanerapp.helper.TimeUtils;

public class SearchResultItemViewModel {

    public final ObservableField<CleanService> service = new ObservableField<>();

    public final ObservableField<String> bossName = new ObservableField<>();
    public final ObservableField<String> housePicUrl = new ObservableField<>();
    public final ObservableField<Integer> rating = new ObservableField<>();
    public final ObservableField<CleanService.Priority> status = new ObservableField<>();
    public final ObservableField<String> address = new ObservableField<>();
    public final ObservableField<String> time = new ObservableField<>();
    public final ObservableField<String> roomStyle = new ObservableField<>();
    public final ObservableField<String> serviceType = new ObservableField<>();
    public final ObservableField<String> others = new ObservableField<>();
    public final ObservableField<String> priority = new ObservableField<>();
    public final ObservableField<Boolean> isEmergency = new ObservableField<>(false);


    public final ObservableField<String> housePicUrl0 = new ObservableField<>();
    public final ObservableField<String> housePicUrl1 = new ObservableField<>();
    public final ObservableField<String> housePicUrl2 = new ObservableField<>();

    public boolean canProposal() {
        try {
            if (service.get().getCleaner_matching() == null)
                return true;
            else
                return service.get().getCleaner_matching().getMatching_state() == ServiceMatching.MatchingState.Invite;
        } catch (Exception e) {
            return false;
        }
    }

    SearchResultItemViewModel(CleanService model) {
        service.set(model);

        bossName.set(model.getHirer_name_read_only());
        if (!model.getClean_service_photos().isEmpty())
            housePicUrl.set(model.getClean_service_photos().get(0).getImage().toExternalForm());
        rating.set(Math.round(model.getHirer_rating_read_only()));
        status.set(model.getPriority());
        address.set(model.getAddressSecured());
        time.set((TimeUtils.getTime(model.getTime_range_start().getTime(), TimeUtils.DATE_FORMAT_MINS) + " ~ " + TimeUtils.getTime(model.getTime_range_start().getTime(), TimeUtils.DATE_FORMAT_MINS)));
        roomStyle.set(String.format(Locale.getDefault(), "%d房 %d廳 %d衛 %d坪", model.getHouse_detail().getRoom_num(), model.getHouse_detail().getHall_num(), model.getHouse_detail().getBathroom_num(), model.getHouse_detail().getFootage()));
        serviceType.set(model.getService_type_read_only().getName());

        others.set(model.getOthers());
        Date d = new Date();
        Logger.d(d);
        Logger.d(model.getTime_range_end());
        Logger.d(d.compareTo(model.getTime_range_end()));

        if (d.compareTo(model.getTime_range_end()) > 0) {
            priority.set("已過期");
        } else {
            priority.set(model.getPriority() == CleanService.Priority.Normal ? "可預約" : "急件");
            isEmergency.set(model.getPriority() == CleanService.Priority.Emergency);
        }


        ObservableField<String>[] fields = new ObservableField[]{housePicUrl0, housePicUrl1, housePicUrl2};
        for (int i = 0; i < model.getClean_service_photos().size(); i++) {
            fields[i].set(model.getClean_service_photos().get(i).getImage().toExternalForm());
        }
    }

    @BindingAdapter({"houseSrc"})
    public static void loadHouseImage(ImageView view, String url) {
        Glide.with(view.getContext())
                .load(url)
                .apply(new RequestOptions().centerCrop().error(R.drawable.ic_house))
                .into(view);
    }



}
