package app.cleaner.tw.cleanerapp.backend.cleaner.models.member;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ARTHUR on 2017/10/17.
 */

public class User implements Serializable {

    public enum Gender {
        @SerializedName("Male")
        Male,
        @SerializedName("Female")
        Female
    }

    @Expose(serialize = false) private int id;

    @Expose(serialize = false) private String username;

    @Expose(serialize = false) private String email;

    @Expose private String first_name;

    @Expose private String last_name;

    @Expose private Gender gender;

    @Expose private String birthday;

    @Expose private String identity_id;

    @Expose private String cellphone;

    @Expose private String telephone;

    @Expose private Integer area2;

    @Expose private String address;

    @Expose(deserialize = false) boolean is_cellphone_validated;

    @Expose(serialize = false) private CleanerProfile cleaner_profile;

    @Expose(serialize = false) private HirerProfile hirer_profile;

    @Expose(serialize = false) private boolean is_profile_complete;

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public Gender getGender() {
        return gender;
    }

    public Date getBirthday() {
        if (birthday == null)
            return null;
        try {
            return getFormatter().parse(birthday);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getIdentity_id() {
        return identity_id;
    }

    public String getCellphone() {
        return cellphone;
    }

    public String getTelephone() {
        return telephone;
    }

    public boolean is_cellphone_validated() {
        return is_cellphone_validated;
    }

    public Integer getArea2() {
        return area2;
    }

    public String getAddress() {
        return address;
    }

    public CleanerProfile getCleaner_profile() {
        return cleaner_profile;
    }

    public HirerProfile getHirer_profile() {
        return hirer_profile;
    }

    public boolean is_profile_complete() {
        return is_profile_complete;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void setBirthday(Date birthday) {
        if (birthday == null)
            this.birthday = null;
        else
            this.birthday = getFormatter().format(birthday);
    }

    public void setIdentity_id(String identity_id) {
        this.identity_id = identity_id;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public void setArea2(Integer area2) {
        this.area2 = area2;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    private SimpleDateFormat getFormatter() {
        return new SimpleDateFormat("yyyy-MM-dd");
    }
}
