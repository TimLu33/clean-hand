package app.cleaner.tw.cleanerapp.news;

public class CleanServiceEvent {

    String id;

    public CleanServiceEvent(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

}
