package app.cleaner.tw.cleanerapp.boss.resume;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;
import app.cleaner.tw.cleanerapp.backend.drfapi.builder.QueriesBuilder;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.PageNumberPagination;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.boss.CaseInfoActivity;
import app.cleaner.tw.cleanerapp.cleaner.resume.ResumeActivity;
import app.cleaner.tw.cleanerapp.cleaner.resume.ResumeDetailActivity;
import app.cleaner.tw.cleanerapp.common.Constants;
import app.cleaner.tw.cleanerapp.common.DividerItemDecoration;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityCleanerResumeBinding;
import app.cleaner.tw.cleanerapp.helper.GSONUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BossOwnResumeActivity extends ResumeActivity {

    public static void start(Context context) {
        Intent starter = new Intent(context, BossOwnResumeActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected int getRole() {
        return Constants.BOSS;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!resumeList.isEmpty()) {
            resumeList.clear();
        }
        loadResumes();
    }

    @Override
    protected void loadResumes() {
        final KProgressHUD hud = KProgressHUD.create(activity).show();
        API.sharedService().hirerGetOwnServices(API.sharedAuthorizationToken(), new QueriesBuilder().order("-created_datetime").pagination(1, 1000).create())
                .enqueue(new Callback<PageNumberPagination<CleanService>>() {
                    @Override
                    public void onResponse(Call<PageNumberPagination<CleanService>> call, Response<PageNumberPagination<CleanService>> response) {
                        hud.dismiss();
                        if (response.isSuccessful()) {
                            Logger.d(response.body().getResults().size() + "QQQ");
                            for (CleanService cleanService : response.body().getResults()) {
                                Logger.json(GSONUtils.toJson(cleanService));
                            }
                            resumeList.addAll(response.body().getResults());
                            binding.recyclerView.getAdapter().notifyDataSetChanged();
                        } else {
                            Logger.e(API.shared().parseError(response).getHudMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<PageNumberPagination<CleanService>> call, Throwable t) {
                        Logger.e(t.getLocalizedMessage());
                        hud.dismiss();
                    }
                });
    }

    @Override
    protected void onListItemClick(final CleanService cleanService, int position) {
        if (!cleanService.getFee_payment().isPaid()) {
            showConfirmDialog(new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    URL payUrl = cleanService.getFee_payment().getSpg_url();
                    Logger.d(payUrl);
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(payUrl.toString()));
                    startActivity(browserIntent);
                }
            });
        } else {
            CaseInfoActivity.start(activity, cleanService);
        }

    }

    private void showConfirmDialog(DialogInterface.OnClickListener listener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setMessage("此案件尚未付款，將導至付款頁面");
        builder.setPositiveButton("確定", listener);
        builder.setNegativeButton("取消", null);
        builder.show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRefreshResume(onRefreshResumeEvent event) {
        resumeList.clear();
        loadResumes();
    }

}
