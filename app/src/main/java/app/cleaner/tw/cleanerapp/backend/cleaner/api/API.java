package app.cleaner.tw.cleanerapp.backend.cleaner.api;

import android.support.annotation.Nullable;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.orhanobut.logger.Logger;

import java.io.IOException;
import java.sql.Date;
import java.sql.Time;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.User;
import app.cleaner.tw.cleanerapp.backend.drfapi.deserializers.DateDeserializer;
import app.cleaner.tw.cleanerapp.backend.drfapi.deserializers.TimeDeserializer;
import app.cleaner.tw.cleanerapp.backend.drfapi.deserializers.TimeSerializer;
import app.cleaner.tw.cleanerapp.backend.drfapi.helper.DRFHelper;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.ApiError;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ARTHUR on 2017/10/17.
 */

public class API {

    private String API_HOST = "http://www.cleanhand.cc/";

    // singleton
    private static API mInstance;
    public static synchronized API shared() {
        if (mInstance == null) {
            mInstance = new API();
        }
        return mInstance;
    }

    public static synchronized CleanerWebService sharedService() {
        API.shared();
        return mInstance.service;
    }

    @Nullable
    public static synchronized Integer sharedUserId() {
        API.shared();
        return mInstance.userId;
    }

    @Nullable
    public static synchronized String sharedAuthorizationToken() {
        API.shared();
        if (mInstance.authorizationToken == null)
            return null;
        return String.format(Locale.getDefault(), "Token %s", mInstance.authorizationToken);
    }

    // 建構
    public API() {
        super();
        gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .registerTypeAdapter(Time.class, new TimeSerializer())
                .registerTypeAdapter(Time.class, new TimeDeserializer())
                .excludeFieldsWithoutExposeAnnotation()
                .serializeNulls()
                .create();
        OkHttpClient client = new OkHttpClient.Builder()
                .addNetworkInterceptor(new StethoInterceptor())
                .connectTimeout(40, TimeUnit.SECONDS)
                .readTimeout(40, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();

                        // zh-tw(zh-hant), en
//                        String langCode = Locale.getDefault().getDisplayLanguage();

                        Request request = original
                                .newBuilder()
                                .addHeader("Accept-Language", "zh-tw")
//                                .method(original.method(), original.body())
                                .build();
//                        Request request = builder.build();
                        okhttp3.Response response = chain.proceed(request);

                        return response;
                    }
                })
                .build();
        retrofit = new Retrofit.Builder()
                .baseUrl(API_HOST)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        service = retrofit.create(CleanerWebService.class);
    }

    private final Gson gson;
    private final CleanerWebService service;
    private final Retrofit retrofit;

    // 身份認證
    private String authorizationToken = null;
    public void setAuthorizationToken(String authorizationToken) {
        this.authorizationToken = authorizationToken;

        // get user async
        API.sharedService().getUser(API.sharedAuthorizationToken()).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    userId = response.body().getId();
                } else {
                    Logger.e(API.shared().parseError(response).getHudMessage());
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Logger.e(t.getLocalizedMessage());
            }
        });
    }

    private Integer userId = null;

//    public Map<String, String> AuthorizationHeader() {
//        Map<String, String> header = new HashMap<>();
//        if (authorizationToken != null)
//            header.put("Authorization", "Token "+authorizationToken);
//        return header;
//    }

    // Error Handle
    public ApiError parseError(Response<?> response) {
        return DRFHelper.parseError(response, retrofit);
    }

    public Gson getGson() {
        return gson;
    }




}
