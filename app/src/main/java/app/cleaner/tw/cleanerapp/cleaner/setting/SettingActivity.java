package app.cleaner.tw.cleanerapp.cleaner.setting;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import app.cleaner.tw.cleanerapp.HomeActivity;
import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.geography.County;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.Profile;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.User;
import app.cleaner.tw.cleanerapp.backend.drfapi.builder.QueriesBuilder;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.ApiError;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.boss.setting.AboutActivity;
import app.cleaner.tw.cleanerapp.boss.setting.CommonQuestionActivity;
import app.cleaner.tw.cleanerapp.common.ChangePwdActivity;
import app.cleaner.tw.cleanerapp.common.Preference;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityCleanerSettingBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingActivity extends BaseActivity {

    private ActivityCleanerSettingBinding binding;
    private ViewModel viewModel;
    private User user = null;
    private Profile profile = null;


    public static void start(Context context) {
        Intent starter = new Intent(context, SettingActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cleaner_setting);
        EventBus.getDefault().register(this);
        setUpToolBar();
        viewModel = new ViewModel();
        binding.setViewModel(viewModel);
        binding.setPresenter(new Presenter());
        getProfile();
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("設定");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private void getProfile() {
        final KProgressHUD hud = KProgressHUD.create(this).show();
        final Runnable dismissHudR = new Runnable() {
            @Override
            public void run() {
                hud.dismiss();
            }
        };

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Response<User> userResponse = API.sharedService().getUser(API.sharedAuthorizationToken()).execute();
                    Response<List<County>> citiesResponse = API.sharedService().getAllCounties(new QueriesBuilder().pagination(1, 1000).create()).execute();
                    Response[] rs = new Response[]{userResponse, citiesResponse};

                    if (userResponse.isSuccessful() && citiesResponse.isSuccessful()) {
                        user = userResponse.body();
                        if (user != null) {
                            profile = user.getCleaner_profile();
                            if (profile != null) {
                                if (profile.getHead_image() != null) {
                                    viewModel.headImageUrl.set(profile.getHead_image().toString());
                                }
                                if (profile.getDisplay_name() != null) {
                                    viewModel.nickName.set(profile.getDisplay_name());
                                }
                            }
                        }
                    } else {
                        final ArrayList<String> msgs = new ArrayList<String>();
                        for (Response response : rs) {
                            if (!response.isSuccessful()) {
                                ApiError apiError = API.shared().parseError(response);
                                Logger.e(apiError.getHudMessage());
                                msgs.add(apiError.getHudMessage());
                            }
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(activity, TextUtils.join("\n", msgs), Toast.LENGTH_SHORT).show();
                            }
                        });
                        runOnUiThread(dismissHudR);
                        return;
                    }
                    runOnUiThread(dismissHudR);

                } catch (IOException e) {
                    e.printStackTrace();
                    runOnUiThread(dismissHudR);
                }
            }
        }).start();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEditSuccessEvent(EditSuccessEvent event) {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void unRegisterToken() {
        String android_id = Settings.Secure.getString(activity.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        API.sharedService().unregisterFcmDevice(API.sharedAuthorizationToken(), android_id).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    public class ViewModel {
        public final ObservableField<String> headImageUrl = new ObservableField<>("");
        public final ObservableField<String> nickName = new ObservableField<>("");
    }

    public class Presenter implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.profile_layout:
                    ProfileSettingActivity.start(activity, user);
                    break;
                case R.id.push_notification_layout:
                    break;
                case R.id.about_layout:
                    AboutActivity.start(activity);
                    break;
                case R.id.common_question_layout:
                    CommonQuestionActivity.start(activity);
                    break;
                case R.id.edit_pwd_layout:
                    ChangePwdActivity.start(activity);
                    break;
                case R.id.tv_logout:
                    unRegisterToken();
                    Preference.setCleanerToken(activity, "");
                    Intent i = new Intent(activity, HomeActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    break;
            }

        }
    }
}
