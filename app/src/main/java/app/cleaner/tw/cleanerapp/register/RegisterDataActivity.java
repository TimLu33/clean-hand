package app.cleaner.tw.cleanerapp.register;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableField;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.Transformer;
import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.geography.County;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.geography.Town;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.CleanerProfile;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.HirerProfile;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.Profile;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.User;
import app.cleaner.tw.cleanerapp.backend.drfapi.builder.QueriesBuilder;
import app.cleaner.tw.cleanerapp.backend.drfapi.helper.DRFHelper;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.ApiError;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.PageNumberPagination;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.boss.BossHomeActivity;
import app.cleaner.tw.cleanerapp.cleaner.home.CleanerHomeActivity;
import app.cleaner.tw.cleanerapp.common.Constants;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityRegDataBinding;
import app.cleaner.tw.cleanerapp.helper.GSONUtils;
import app.cleaner.tw.cleanerapp.helper.ImagePickerHelper;
import app.cleaner.tw.cleanerapp.helper.TimeUtils;
import app.cleaner.tw.cleanerapp.kaoui.LazyArrayAdapter;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterDataActivity extends BaseActivity implements EasyPermissions.PermissionCallbacks {

    private final static String PHONE = "phone";

    private final int REQUEST_PHOTO_CODE = 100;
    private final int REQUEST_PHOTO_CROP = 101;
    protected File newHeadFile;
    private static final String REG_TYPE = "regType";
    private ActivityRegDataBinding binding;
    private ViewModel viewModel;
    private Integer regType;
    private String phone;

    private Profile profile;
    private User user;

    private int year, month, day;
    private String[] gender = {"男", "女"};

    private final ArrayList<County> cities = new ArrayList<>();
    private final ArrayList<Town> areas = new ArrayList<>();

    public static void start(Context context, int regType, @Nullable String phone) {
        Intent starter = new Intent(context, RegisterDataActivity.class);
        starter.putExtra(REG_TYPE, regType);
        if (phone != null)
            starter.putExtra(PHONE, phone);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBundle();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_reg_data);
        setUpToolBar();
        viewModel = new ViewModel(regType);
        binding.setViewModel(viewModel);
        binding.setPresenter(new Presenter());
        initViews();

        getAreaProfile();
    }

    private void getBundle() {
        if (getIntent().getExtras() != null) {
            regType = getIntent().getIntExtra(REG_TYPE, Constants.CLEANER);
            phone = getIntent().getStringExtra(PHONE);
        }
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("註冊資料");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private void initViews() {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_dropdown_item, gender);
        binding.gender.setAdapter(adapter);
        binding.gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                viewModel.gender.set(gender[i]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        this.year = Calendar.getInstance().get(Calendar.YEAR);
        this.month = Calendar.getInstance().get(Calendar.MONTH);
        this.day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
    }

    private void getAreaProfile() {
        final KProgressHUD hud = KProgressHUD.create(this).show();
        final Runnable dismissHudR = new Runnable() {
            @Override
            public void run() {
                hud.dismiss();
            }
        };

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Response<User> userResponse = API.sharedService().getUser(API.sharedAuthorizationToken()).execute();
                    Response<List<County>> citiesResponse = API.sharedService().getAllCounties(new QueriesBuilder().pagination(1, 1000).create()).execute();
                    Response[] rs = new Response[]{userResponse, citiesResponse};

                    if (userResponse.isSuccessful() && citiesResponse.isSuccessful()) {
                        user = userResponse.body();
                        profile = (regType == Constants.CLEANER) ? user.getCleaner_profile() : user.getHirer_profile();
                        cities.clear();
                        cities.addAll(citiesResponse.body());
                    } else {
                        final ArrayList<String> msgs = new ArrayList<String>();
                        for (Response response : rs) {
                            if (!response.isSuccessful()) {
                                ApiError apiError = API.shared().parseError(response);
                                Logger.e(apiError.getHudMessage());
                                msgs.add(apiError.getHudMessage());
                            }
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(activity, TextUtils.join("\n", msgs), Toast.LENGTH_SHORT).show();
                            }
                        });
                        runOnUiThread(dismissHudR);
                        return;
                    }
                    runOnUiThread(dismissHudR);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setUpAreaSpinner();
                            setUpInfomation();
                            ;
                        }
                    });

                } catch (IOException e) {
                    e.printStackTrace();
                    runOnUiThread(dismissHudR);
                }
            }
        }).start();
    }

    private void setUpInfomation() {
        if (profile != null) {
            if (profile.getDisplay_name() != null)
                viewModel.nickName.set(profile.getDisplay_name());
//            if (profile.getClass() == CleanerProfile.class)
//                if (((CleanerProfile) profile).getIntroduction() != null)
//                    viewModel.houseDescription.set(((CleanerProfile) profile).getIntroduction());
        }
        if (user != null) {
            if (user.getLast_name() != null)
                viewModel.lastName.set(user.getLast_name());
            if (user.getFirst_name() != null)
                viewModel.firstName.set(user.getFirst_name());
            if (user.getGender() != null)
                viewModel.gender.set(user.getGender() == User.Gender.Male ? gender[0] : gender[1]);
            if (user.getIdentity_id() != null)
                viewModel.identify.set(user.getIdentity_id());
            if (user.getBirthday() != null)
                viewModel.birthDay.set(user.getBirthday());
            if (user.getEmail() != null)
                viewModel.email.set(user.getEmail());
            if (user.getCellphone() != null)
                viewModel.cellPhone.set(user.getCellphone());
            if (user.getTelephone() != null)
                viewModel.phone.set(user.getTelephone());
            if (user.getArea2() != null)
                viewModel.area2.set(user.getArea2());
            if (user.getAddress() != null)
                viewModel.address.set(user.getAddress());
        }

        if (phone != null)
            viewModel.cellPhone.set(phone);

        checkCountySelection();
    }

    private void checkCountySelection() {
        if (viewModel.area2.get() != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        final List<County> cs = API.sharedService().getAllCounties(new QueriesBuilder().filter("sub_areas", viewModel.area2.get()).create()).execute().body();
                        if (!cs.isEmpty()) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    binding.citySpinner.setSelection(cities.indexOf(cs.get(0)) + 1);
                                }
                            });
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }

    private void checkTownSelection() {
        if (viewModel.area2.get() != null) {
            Town t = CollectionUtils.find(areas, new Predicate<Town>() {
                @Override
                public boolean evaluate(Town object) {
                    return object.getId() == viewModel.area2.get();
                }
            });
            if (t != null)
                binding.zoneSpinner.setSelection(areas.indexOf(t) + 1);
        }
    }

    private void setUpAreaSpinner() {
        final LazyArrayAdapter cAdapter = new LazyArrayAdapter<County>(activity, android.R.layout.simple_spinner_dropdown_item, cities) {
            @Override
            public int getCount() {
                return super.getCount() + 1;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                return getView(position, convertView, parent);
            }

            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = viewFromResource(LayoutInflater.from(activity), convertView, parent, getResource());
                TextView textView = view.findViewById(getFieldId());
                if (position == 0) {
                    textView.setText("請選擇");
                } else {
                    textView.setText(getItem(position - 1).getName());
                }
                return view;
            }
        };
        binding.citySpinner.setAdapter(cAdapter);

        final LazyArrayAdapter tAdapter = new LazyArrayAdapter<Town>(activity, android.R.layout.simple_spinner_dropdown_item, areas) {
            @Override
            public int getCount() {
                return super.getCount() + 1;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                return getView(position, convertView, parent);
            }

            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = viewFromResource(LayoutInflater.from(activity), convertView, parent, getResource());
                TextView textView = view.findViewById(getFieldId());
                if (position == 0) {
                    textView.setText("請選擇");
                } else {
                    textView.setText(getItem(position - 1).getName());
                }
                return view;
            }
        };
        binding.zoneSpinner.setAdapter(tAdapter);

        binding.citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, final View view, int position, long id) {
                if (position == 0) {
                    areas.clear();
                    ((ArrayAdapter) binding.zoneSpinner.getAdapter()).notifyDataSetChanged();
                    viewModel.area2.set(null);
                } else {
                    API.sharedService().getAllTowns(new QueriesBuilder()
                            .filter("parent_area__county_code", ((County) cAdapter.getItem(position - 1)).getCounty_code())
                            .pagination(1, QueriesBuilder.PAGE_SIZE_MAX)
                            .create()
                    ).enqueue(new Callback<PageNumberPagination<Town>>() {
                        @Override
                        public void onResponse(Call<PageNumberPagination<Town>> call, Response<PageNumberPagination<Town>> response) {
                            areas.clear();
                            if (response.isSuccessful()) {
                                areas.addAll(response.body().getResults());
                            } else {
                                Logger.e(API.shared().parseError(response).getHudMessage());
                            }
                            tAdapter.notifyDataSetChanged();

                            checkTownSelection();
                        }

                        @Override
                        public void onFailure(Call<PageNumberPagination<Town>> call, Throwable t) {
                            Logger.e(t.getLocalizedMessage());
                        }
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                areas.clear();
                tAdapter.notifyDataSetChanged();
                viewModel.area2.set(null);
            }
        });

        binding.zoneSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    viewModel.area2.set(null);
                } else {
                    viewModel.area2.set(((Town) tAdapter.getItem(position - 1)).getId());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                viewModel.area2.set(null);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_PHOTO_CODE) {
                if (data.getData() != null) {
                    try {
                        newHeadFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + UUID.randomUUID().toString() + ".png");

                        ImagePickerHelper.getImageFromUri(activity, data, newHeadFile);
                        ImagePickerHelper.startCropImage(activity, newHeadFile, REQUEST_PHOTO_CROP);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else if (requestCode == REQUEST_PHOTO_CROP) {

                String path = newHeadFile.getPath();
                Bitmap bitmap = BitmapFactory.decodeFile(path);
                binding.image.setImageBitmap(bitmap);

//                if (tempFile.exists()) {
//                    tempFile.delete();
//                }
            }
        }
    }

    private void getImageFromAlbums(int requestCode) {
        try {
            if (!ImagePickerHelper.launchGooglePhotosPicker(activity, requestCode)) {
                Toast.makeText(activity, "請先安裝圖片選擇APP", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(activity, "請先安裝圖片選擇APP", Toast.LENGTH_SHORT).show();
        }
    }

    private void showConfirmDialog(DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setMessage("註冊完成");
        builder.setPositiveButton("確定", listener);
        builder.show();
    }

    public class ViewModel {
        public final ObservableField<Integer> regType = new ObservableField<>();
        public final ObservableField<String> nickName = new ObservableField<>("");
        public final ObservableField<String> firstName = new ObservableField<>("");
        public final ObservableField<String> lastName = new ObservableField<>("");
        public final ObservableField<String> identify = new ObservableField<>("");
        public final ObservableField<String> gender = new ObservableField<>("");
        public final ObservableField<Date> birthDay = new ObservableField<>();
        public final ObservableField<String> email = new ObservableField<>("");
        public final ObservableField<String> phone = new ObservableField<>("");
        public final ObservableField<String> cellPhone = new ObservableField<>("");
        public final ObservableField<Integer> area2 = new ObservableField<>();
        public final ObservableField<String> address = new ObservableField<>("");
        public final ObservableField<String> houseDescription = new ObservableField<>("");

        public ViewModel(int regType) {
            this.regType.set(regType);
        }
    }

    public class Presenter implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_enter:
                    Logger.json(GSONUtils.toJson(viewModel));

                    if (viewModel.nickName.get().isEmpty()) {
                        Toast.makeText(activity, "請輸入暱稱", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    profile.setDisplay_name(viewModel.nickName.get());

                    user.setLast_name(viewModel.lastName.get());
                    user.setFirst_name(viewModel.firstName.get());
                    user.setGender(viewModel.gender.get().equals(gender[0]) ? User.Gender.Male : User.Gender.Female);
                    user.setIdentity_id(viewModel.identify.get());
                    user.setTelephone(viewModel.phone.get());
                    user.setCellphone(viewModel.cellPhone.get());
                    user.setArea2(viewModel.area2.get());
                    user.setAddress(viewModel.address.get());
                    user.setBirthday(viewModel.birthDay.get());

                    final KProgressHUD hud = KProgressHUD.create(activity).show();
                    final Runnable dismissHudR = new Runnable() {
                        @Override
                        public void run() {
                            hud.dismiss();
                        }
                    };

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {

                                final ArrayList<String> errMsgs = new ArrayList<>();
                                if (newHeadFile != null) {
                                    Response r = (regType == Constants.CLEANER) ? API.sharedService().cleanerPatchProfileHeadImage(API.sharedAuthorizationToken(), DRFHelper.ImageRequestBodyPart("head_image", newHeadFile)).execute()
                                            : API.sharedService().hirerPatchProfileHeadImage(API.sharedAuthorizationToken(), DRFHelper.ImageRequestBodyPart("head_image", newHeadFile)).execute();
                                    if (!r.isSuccessful()) {
                                        errMsgs.add(API.shared().parseError(r).getHudMessage());
                                    }
                                }
                                Response response = API.sharedService().patchUser(API.sharedAuthorizationToken(), user).execute();
                                Response response2 = regType == Constants.CLEANER ? API.sharedService().cleanerPatchProfile(API.sharedAuthorizationToken(), (CleanerProfile) profile).execute() : API.sharedService().hirerPatchProfile(API.sharedAuthorizationToken(), (HirerProfile) profile).execute();
                                Response[] rs = new Response[]{response, response2};

                                for (Response r : rs) {
                                    if (!r.isSuccessful()) {
                                        ApiError apiError = API.shared().parseError(r);
                                        errMsgs.add(apiError.getHudMessage());
                                        Logger.e(apiError.getHudMessage());
                                    }
                                }
                                if (regType == Constants.CLEANER) {
                                    API.sharedService().cleanerQuickPatchAllowInvite(API.sharedAuthorizationToken(), true).enqueue(new Callback<CleanerProfile>() {
                                        @Override
                                        public void onResponse(Call<CleanerProfile> call, Response<CleanerProfile> response) {
                                            runOnUiThread(dismissHudR);
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    if (errMsgs.isEmpty()) {
                                                        showConfirmDialog(new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                                EventBus.getDefault().post(new onRegisterSuccessEvent());
                                                                CleanerHomeActivity.start(activity);
                                                                finish();
                                                            }
                                                        });
                                                    } else {
                                                        Toast.makeText(activity, TextUtils.join("\n", errMsgs), Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            });
                                        }

                                        @Override
                                        public void onFailure(Call<CleanerProfile> call, Throwable t) {
                                            Logger.e(t.getLocalizedMessage());
                                        }
                                    });
                                } else {
                                    runOnUiThread(dismissHudR);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (errMsgs.isEmpty()) {
                                                showConfirmDialog(new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        EventBus.getDefault().post(new onRegisterSuccessEvent());
                                                        BossHomeActivity.start(activity);
                                                        finish();
                                                    }
                                                });
                                            } else {
                                                Toast.makeText(activity, TextUtils.join("\n", errMsgs), Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                                Logger.e(e.getLocalizedMessage());
                                runOnUiThread(dismissHudR);
                            }
                        }
                    }).start();

                    break;
                case R.id.btn_set_stickers:
                    String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
                    if (!EasyPermissions.hasPermissions(activity, perms)) {
                        EasyPermissions.requestPermissions(activity, "上傳大頭貼需要權限", 1001, perms);
                    } else {
                        getImageFromAlbums(REQUEST_PHOTO_CODE);
                    }

                    break;
                case R.id.tv_birthday:
                    new DatePickerDialog(activity, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                            Calendar calendar = Calendar.getInstance();
                            calendar.set(i, i1, i2);
                            viewModel.birthDay.set(calendar.getTime());
                        }
                    }, year, month, day).show();
                    break;
            }

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        getImageFromAlbums(REQUEST_PHOTO_CODE);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }
}
