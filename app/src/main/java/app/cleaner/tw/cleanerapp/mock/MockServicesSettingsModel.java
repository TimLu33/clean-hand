package app.cleaner.tw.cleanerapp.mock;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class MockServicesSettingsModel implements Parcelable {

    private List<String> serviceAreaList;
    private List<String> serviceTimeList;
    private List<String> serviceTypeList;
    private int salary;
    private String servicePhone;
    private boolean hasGoodCard;
    private boolean hasCleanerLicense;
    private boolean allowPet;
    private boolean equipTools;

    //提供UI顯示
    private List<String> othersList;

    public List<String> getServiceAreaList() {
        return serviceAreaList;
    }

    public void setServiceAreaList(List<String> serviceAreaList) {
        this.serviceAreaList = serviceAreaList;
    }

    public List<String> getServiceTimeList() {
        return serviceTimeList;
    }

    public void setServiceTimeList(List<String> serviceTimeList) {
        this.serviceTimeList = serviceTimeList;
    }

    public List<String> getServiceTypeList() {
        return serviceTypeList;
    }

    public void setServiceTypeList(List<String> serviceTypeList) {
        this.serviceTypeList = serviceTypeList;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getServicePhone() {
        return servicePhone;
    }

    public void setServicePhone(String servicePhone) {
        this.servicePhone = servicePhone;
    }

    public boolean isHasGoodCard() {
        return hasGoodCard;
    }

    public void setHasGoodCard(boolean hasGoodCard) {
        this.hasGoodCard = hasGoodCard;
    }

    public boolean isHasCleanerLicense() {
        return hasCleanerLicense;
    }

    public void setHasCleanerLicense(boolean hasCleanerLicense) {
        this.hasCleanerLicense = hasCleanerLicense;
    }

    public boolean isAllowPet() {
        return allowPet;
    }

    public void setAllowPet(boolean allowPet) {
        this.allowPet = allowPet;
    }

    public boolean isEquipTools() {
        return equipTools;
    }

    public void setEquipTools(boolean equipTools) {
        this.equipTools = equipTools;
    }

    public List<String> getOthersList() {
        return othersList;
    }

    public void setOthersList(List<String> othersList) {
        this.othersList = othersList;
    }

    public MockServicesSettingsModel() {

    }

    protected MockServicesSettingsModel(Parcel in) {
        serviceAreaList = in.createStringArrayList();
        serviceTimeList = in.createStringArrayList();
        serviceTypeList = in.createStringArrayList();
        salary = in.readInt();
        servicePhone = in.readString();
        hasGoodCard = in.readByte() != 0;
        hasCleanerLicense = in.readByte() != 0;
        allowPet = in.readByte() != 0;
        equipTools = in.readByte() != 0;
        othersList = in.createStringArrayList();
    }

    public static final Creator<MockServicesSettingsModel> CREATOR = new Creator<MockServicesSettingsModel>() {
        @Override
        public MockServicesSettingsModel createFromParcel(Parcel in) {
            return new MockServicesSettingsModel(in);
        }

        @Override
        public MockServicesSettingsModel[] newArray(int size) {
            return new MockServicesSettingsModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeStringList(serviceAreaList);
        parcel.writeStringList(serviceTimeList);
        parcel.writeStringList(serviceTypeList);
        parcel.writeInt(salary);
        parcel.writeString(servicePhone);
        parcel.writeByte((byte) (hasGoodCard ? 1 : 0));
        parcel.writeByte((byte) (hasCleanerLicense ? 1 : 0));
        parcel.writeByte((byte) (allowPet ? 1 : 0));
        parcel.writeByte((byte) (equipTools ? 1 : 0));
        parcel.writeStringList(othersList);
    }
}
