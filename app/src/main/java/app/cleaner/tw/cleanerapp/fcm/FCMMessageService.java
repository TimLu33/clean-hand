package app.cleaner.tw.cleanerapp.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.orhanobut.logger.Logger;

import java.util.Map;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.boss.BossHomeActivity;
import app.cleaner.tw.cleanerapp.cleaner.home.CleanerHomeActivity;
import app.cleaner.tw.cleanerapp.common.Preference;
import app.cleaner.tw.cleanerapp.helper.GSONUtils;

public class FCMMessageService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Logger.d("QQQ");
        if (remoteMessage != null && remoteMessage.getNotification() != null) {
            Logger.d("www");
            sendNotification(remoteMessage);
        }

    }

    private void sendNotification(RemoteMessage remoteMessage) {

        int notifyId = (int) (Math.random() * 100);
        String title = "";
        if (remoteMessage != null && remoteMessage.getNotification() != null && remoteMessage.getNotification().getTitle() != null) {
            title = remoteMessage.getNotification().getTitle();
        }
        String id;
        String model;
        Intent intent = new Intent();
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
        notificationBuilder.setContentTitle(title)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setContentText(remoteMessage.getNotification().getBody())
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setAutoCancel(true);

        Map<String, String> data = remoteMessage.getData();

        if (data != null && data.size() > 0) {
            id = data.get("object_id");
            model = data.get("model");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            if (model.equals("serviceqa")) {
                intent.putExtra("messageId", id);
                intent.putExtra("model", model);
                intent.setClass(this, title.equals("雇主回覆") ? CleanerHomeActivity.class : BossHomeActivity.class);
            } else if (model.equals("servicematching")) {
                if (!title.isEmpty()) {
                    intent.putExtra("matchingId", id);
                    intent.putExtra("model", model);
                    if (title.equals("清潔手到點通知") || title.equals("清潔手離點通知") || title.equals("提案通知")) {
                        intent.setClass(this, BossHomeActivity.class);
                    } else {
                        intent.setClass(this, CleanerHomeActivity.class);
                    }
                }
            } else if (model.equals("news")) {
                intent.putExtra("model", model);
                intent.putExtra("newsId", id);
                String hireToken = Preference.getHirerToken(this);
                intent.setClass(this, hireToken.isEmpty() ? CleanerHomeActivity.class : BossHomeActivity.class);
            } else if (model.equals("cleanservice")) {
                intent.putExtra("model", model);
                intent.putExtra("serviceId", id);
                intent.setClass(this, CleanerHomeActivity.class);
            }

            PendingIntent pendingIntent = PendingIntent.getActivities(this, notifyId, new Intent[]{intent}, PendingIntent.FLAG_ONE_SHOT);
            notificationBuilder.setContentIntent(pendingIntent);
        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = null;
            if (android.os.Build.VERSION.SDK_INT >= 26) {
                channel = new NotificationChannel("normal", "normal", NotificationManager.IMPORTANCE_HIGH);
                notificationManager.createNotificationChannel(channel);
            }
            notificationBuilder.setChannelId("normal");
        }
        notificationManager.notify(notifyId, notificationBuilder.build());

    }
}
