package app.cleaner.tw.cleanerapp.boss.search;

import com.jzxiang.pickerview.TimePickerDialog;
import com.jzxiang.pickerview.data.DialogType;
import com.jzxiang.pickerview.listener.OnDateSetListener;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.geography.County;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.geography.Town;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.SimpleUser;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceType;
import app.cleaner.tw.cleanerapp.backend.drfapi.builder.QueriesBuilder;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.PageNumberPagination;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.boss.search.event.OnDealSuccessEvent;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityBossSearchBinding;
import app.cleaner.tw.cleanerapp.helper.GSONUtils;
import app.cleaner.tw.cleanerapp.helper.TimeUtils;
import app.cleaner.tw.cleanerapp.kaoui.LazyArrayAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SearchActivity extends BaseActivity implements OnDateSetListener {

    private ActivityBossSearchBinding binding;
    private ViewModel viewModel;
    private ArrayList<County> cities = null;
    private ArrayList<ServiceType> serviceTypes = null;
    private int year, month, day;

    private long calendarTimeInMillis;
    private TimePickerDialog dateTimePicker;
    private TimePickerDialog.Builder operateTimeDialogBuilder;
    StringBuilder tempSelectedTime;
    int startHour, startMin, endHour, endMin;

    public static void start(Context context) {
        Intent starter = new Intent(context, SearchActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_boss_search);
        EventBus.getDefault().register(this);
        viewModel = new ViewModel();
        binding.setViewModel(viewModel);
        binding.setPresenter(new Presenter());
        setUpToolBar();
        initViews();
        getAreaType();
    }


    private void setUpToolBar() {
        binding.appBar.title.setText("條件搜尋");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private void initViews() {
        operateTimeDialogBuilder = new TimePickerDialog.Builder().setType(DialogType.HOURS_MINS).setCallBack(this).setThemeColor(getColor(this, R.color.colorAccent));

        this.year = Calendar.getInstance().get(Calendar.YEAR);
        this.month = Calendar.getInstance().get(Calendar.MONTH);
        this.day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
    }

    private void showTimePicker() {
        operateTimeDialogBuilder.setTitleStringId("工作時段-起").setCurrentMillseconds(TimeUtils.getDefaultTime(9)).setMinMillseconds(TimeUtils.getDefaultTime(0));
        dateTimePicker = operateTimeDialogBuilder.build();
        dateTimePicker.show(getSupportFragmentManager(), "operateTimeStart");
    }

    private void getAreaType() {
        final KProgressHUD hud = KProgressHUD.create(this).show();
        final Runnable dismissHudR = new Runnable() {
            @Override
            public void run() {
                hud.dismiss();
            }
        };
        Runnable r = new Runnable() {
            @Override
            public void run() {
                try {
                    Response<List<County>> citiesResponse = API.sharedService().getAllCounties(new QueriesBuilder().create()).execute();
                    Response<List<ServiceType>> serviceTypeResponse = API.sharedService().getAllServiceTypes(API.sharedAuthorizationToken(), new QueriesBuilder().create()).execute();

                    if (citiesResponse.isSuccessful()) {
                        cities = new ArrayList<>(citiesResponse.body());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                viewModel.counties.addAll(cities);
                                initFirstAreaSpinner(binding);
                            }
                        });
                    } else {
                        Logger.e(API.shared().parseError(citiesResponse).getHudMessage());
                    }

                    if (serviceTypeResponse.isSuccessful()) {
                        serviceTypes = new ArrayList<>(serviceTypeResponse.body());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                viewModel.serviceTypes.addAll(serviceTypes);
                                initServiceTypeSpinner(binding);
                            }
                        });
                    } else {
                        Logger.e(API.shared().parseError(serviceTypeResponse).getHudMessage());
                    }

                    runOnUiThread(dismissHudR);

                } catch (IOException e) {
                    runOnUiThread(dismissHudR);
                    Logger.e(e.getLocalizedMessage());
                }
            }
        };
        new Thread(r).start();
    }

    private void initServiceTypeSpinner(ActivityBossSearchBinding binding) {
        LazyArrayAdapter<ServiceType> adapter = new LazyArrayAdapter<ServiceType>(activity, R.layout.support_simple_spinner_dropdown_item, viewModel.serviceTypes) {
            @Override
            public int getCount() {
                return super.getCount()+1;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                return getView(position, convertView, parent);
            }

            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = viewFromResource(LayoutInflater.from(getContext()), convertView, parent, getResource());
                TextView textView = view.findViewById(getFieldId());
                if (position == 0)
                    textView.setText("不限");
                else
                    textView.setText(getItem(position-1).getName());
                return view;
            }
        };
        binding.spinnerService.setAdapter(adapter);
        binding.spinnerService.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0)
                    viewModel.serviceType.set(null);
                else
                    viewModel.serviceType.set(viewModel.serviceTypes.get(position-1));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                viewModel.serviceType.set(null);
            }
        });
    }

    private void initFirstAreaSpinner(final ActivityBossSearchBinding binding) {

        final ArrayList<Town> towns = new ArrayList<>();

        final LazyArrayAdapter<County> cAdapter = new LazyArrayAdapter<County>(activity, R.layout.support_simple_spinner_dropdown_item, viewModel.counties) {

            @Override
            public int getCount() {
                return super.getCount()+1;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                return getView(position, convertView, parent);
            }

            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = viewFromResource(LayoutInflater.from(getContext()), convertView, parent, getResource());
                TextView textView = view.findViewById(getFieldId());
                if (position == 0)
                    textView.setText("不限");
                else
                    textView.setText(getItem(position-1).getName());
                return view;
            }
        };

        final LazyArrayAdapter<Town> tAdapter = new LazyArrayAdapter<Town>(activity, R.layout.support_simple_spinner_dropdown_item, towns) {

            @Override
            public int getCount() {
                return super.getCount()+1;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                return getView(position, convertView, parent);
            }

            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = viewFromResource(LayoutInflater.from(getContext()), convertView, parent, getResource());
                TextView textView = view.findViewById(getFieldId());
                if (position == 0)
                    textView.setText("不限");
                else
                    textView.setText(getItem(position-1).getName());
                return view;
            }
        };

        binding.citySpinner.setAdapter(cAdapter);
        binding.zoneSpinner.setAdapter(tAdapter);

        binding.citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    towns.removeAll(towns);
                    tAdapter.notifyDataSetChanged();
                    viewModel.county.set(null);
                    viewModel.town.set(null);
                    return;
                }

                County city = viewModel.counties.get(i-1);
                viewModel.county.set(city);
                API.sharedService().getAllTowns(new QueriesBuilder()
                        .filter("parent_area__county_code", city.getCounty_code())
                        .pagination(1, QueriesBuilder.PAGE_SIZE_MAX)
                        .create()
                ).enqueue(new Callback<PageNumberPagination<Town>>() {
                    @Override
                    public void onResponse(Call<PageNumberPagination<Town>> call, Response<PageNumberPagination<Town>> response) {
                        towns.removeAll(towns);
                        if (response.isSuccessful())
                            towns.addAll(response.body().getResults());
                        else
                            Logger.e(API.shared().parseError(response).getHudMessage());
                        tAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onFailure(Call<PageNumberPagination<Town>> call, Throwable t) {
                        Logger.e(t.getLocalizedMessage());
                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                viewModel.county.set(null);
            }
        });

        binding.zoneSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    viewModel.town.set(null);
                    return;
                }

                viewModel.town.set(towns.get(position-1));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                viewModel.town.set(null);
            }
        });
        
    }

    private void doSearch() {

        final QueriesBuilder queriesBuilder = new QueriesBuilder();

        queriesBuilder.pagination(1, 20);

        if (viewModel.county.get() != null)
            queriesBuilder.filter("serve_areas__parent_area", viewModel.county.get().getId());

        if (viewModel.town.get() != null)
            queriesBuilder.filter("serve_areas", viewModel.town.get().getId());

        if (viewModel.serviceType.get() != null)
            queriesBuilder.filter("cleaner_profile__provide_service_types", viewModel.serviceType.get().getId());

        if (viewModel.aveRatingStart.get() != null && viewModel.aveRatingEnd.get() != null) {
            queriesBuilder.filter("cleaner_average_rating_range_0", viewModel.aveRatingStart.get());
            queriesBuilder.filter("cleaner_average_rating_range_1", viewModel.aveRatingEnd.get());
        }

        if (viewModel.serveTimesRangeStart.get() != null && viewModel.serveTimesRangeEnd.get() != null) {
            queriesBuilder.filter("service_times_range_0", viewModel.serveTimesRangeStart.get().toString());
            queriesBuilder.filter("service_times_range_1", viewModel.serveTimesRangeEnd.get().toString());
        }

        if (viewModel.allowPet.get() != null)
            queriesBuilder.filter("cleaner_profile__allow_pet", viewModel.allowPet.get());

        if (viewModel.equipTools.get() != null)
            queriesBuilder.filter("cleaner_profile__equip_tools", viewModel.equipTools.get());

        if (viewModel.hasGoodCard.get() != null)
            queriesBuilder.filter("cleaner_profile__has_good_card", viewModel.hasGoodCard.get());

        if (viewModel.hasCleanerLicense.get() != null)
            queriesBuilder.filter("cleaner_profile__has_cleaner_license", viewModel.hasCleanerLicense.get());

        if (viewModel.limitFemale.get() != null)
            queriesBuilder.filter("gender", viewModel.limitFemale.get() ? "Female": "Male");

        final KProgressHUD hud = KProgressHUD.create(activity).show();

        API.sharedService().hirerFindCleaners(API.sharedAuthorizationToken(), queriesBuilder.create()).enqueue(new Callback<PageNumberPagination<SimpleUser>>() {
            @Override
            public void onResponse(Call<PageNumberPagination<SimpleUser>> call, Response<PageNumberPagination<SimpleUser>> response) {
                hud.dismiss();
                if (response.isSuccessful()) {
                    PageNumberPagination<SimpleUser> pagination = response.body();
//                    Toast.makeText(activity, String.format(Locale.getDefault(), "找到 %d 筆結果", pagination.getCount()), Toast.LENGTH_LONG).show();
                    if (pagination.getCount() > 0) {
                        SearchResultActivity.start(activity, queriesBuilder, pagination);
                    }

                } else {
                    Logger.e(API.shared().parseError(response).getHudMessage());
                }
            }

            @Override
            public void onFailure(Call<PageNumberPagination<SimpleUser>> call, Throwable t) {
                Logger.e(t.getLocalizedMessage());
                hud.dismiss();
            }
        });

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDealSuccessEvent(OnDealSuccessEvent event) {
        finish();
    }

    @Override
    public void onDateSet(TimePickerDialog view, long l) {
        switch (view.getTag()) {
            case "operateTimeStart":
                calendarTimeInMillis = TimeUtils.getDefaultTime(21, 0);
                String[] tempStart = TimeUtils.getTime(l, TimeUtils.HOUR_MINUTE_FORMAT).split(":");

                startHour = Integer.parseInt(tempStart[0]);
                startMin = Integer.parseInt(tempStart[1]);
                operateTimeDialogBuilder.setTitleStringId("服務時段-止")
                        .setCurrentMillseconds(calendarTimeInMillis)
                        .setMinMillseconds(l);
                dateTimePicker = operateTimeDialogBuilder.build();
                dateTimePicker.show(getSupportFragmentManager(), "operateTimeEnd");
                tempSelectedTime = new StringBuilder();
                viewModel.serveTimesRangeStart.set(Time.valueOf(getTimeFormat(l)));
                tempSelectedTime.append(TimeUtils.getTime(l, TimeUtils.HOUR_MINUTE_FORMAT)).append("~");
                break;
            case "operateTimeEnd":
                String[] tempEnd = TimeUtils.getTime(l, TimeUtils.HOUR_MINUTE_FORMAT).split(":");
                endHour = Integer.parseInt(tempEnd[0]);
                endMin = Integer.parseInt(tempEnd[1]);
                tempSelectedTime.append(TimeUtils.getTime(l, TimeUtils.HOUR_MINUTE_FORMAT));
                viewModel.serveTimesRangeEnd.set(Time.valueOf(getTimeFormat(l)));
                viewModel.searchTimeRange.set(tempSelectedTime.toString());
                break;
        }
    }

    public String getTimeFormat(long millis) {
        return String.format(Locale.getDefault(), "%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), // The change is in this line
                TimeUnit.MILLISECONDS.toSeconds(millis) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
    }

    public class ViewModel {
        public final ArrayList<County> counties = new ArrayList<>();
        public final ArrayList<ServiceType> serviceTypes = new ArrayList<>();

        public final ObservableField<County> county = new ObservableField<>();
        public final ObservableField<Town> town = new ObservableField<>();
        public final ObservableField<ServiceType> serviceType = new ObservableField<>();

        public final ObservableField<Date> searchDate = new ObservableField<>();
        public final ObservableField<String> searchTimeRange = new ObservableField<>();

        public final ObservableField<Boolean> allowPet = new ObservableField<>();
        public final ObservableField<Boolean> equipTools = new ObservableField<>();
        public final ObservableField<Boolean> hasGoodCard = new ObservableField<>();
        public final ObservableField<Boolean> hasCleanerLicense = new ObservableField<>();
        public final ObservableField<Boolean> limitFemale = new ObservableField<>();

        public final ObservableField<String> aveRatingStart = new ObservableField<>();
        public final ObservableField<String> aveRatingEnd = new ObservableField<>();

        public final ObservableField<Time> serveTimesRangeStart = new ObservableField<>();
        public final ObservableField<Time> serveTimesRangeEnd = new ObservableField<>();

    }

    public class Presenter implements View.OnClickListener {

        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.tv_work_date:
                    new DatePickerDialog(activity, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                            Calendar calendar = Calendar.getInstance();
                            calendar.set(year, month, day);
                            viewModel.searchDate.set(calendar.getTime());
                        }
                    }, SearchActivity.this.year, SearchActivity.this.month, SearchActivity.this.day).show();
                    break;
                case R.id.tv_work_time:
                    showTimePicker();
                    break;
                case R.id.btn_confirm:
                    doSearch();
                    break;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
