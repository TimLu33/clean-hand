package app.cleaner.tw.cleanerapp.backend.cleaner.models.news;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.net.URL;
import java.util.Date;
import java.util.List;

/**
 * Created by ARTHUR on 2017/10/18.
 */

public class News implements Serializable {

    enum Viewer {
        @SerializedName("CL")
        Cleaner,
        @SerializedName("HR")
        Hirer,
        @SerializedName("BT")
        Both
    }
    @Expose
    private int id;
    @Expose
    private String subject;
    @Expose
    private String content;
    @Expose
    private Viewer viewer;
    @Expose
    private Date created_datetime;
    @Expose
    private Date modified_datetime;
    @Expose
    private List<NewsPhotosBean> news_photos;

    public int getId() {
        return id;
    }

    public String getSubject() {
        return subject;
    }

    public String getContent() {
        return content;
    }

    public Viewer getViewer() {
        return viewer;
    }

    public Date getCreated_datetime() {
        return created_datetime;
    }

    public Date getModified_datetime() {
        return modified_datetime;
    }

    public List<NewsPhotosBean> getNews_photos() {
        return news_photos;
    }

    public static class NewsPhotosBean implements Serializable {

        @Expose
        private String title;
        @Expose
        private URL image;
        @Expose
        private Date created_datetime;

        public String getTitle() {
            return title;
        }

        public URL getImage() {
            return image;
        }

        public Date getCreated_datetime() {
            return created_datetime;
        }
    }
}
