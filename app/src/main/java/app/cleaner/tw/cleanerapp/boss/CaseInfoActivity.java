package app.cleaner.tw.cleanerapp.boss;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.RatingBar;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.Transformer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.SimpleUser;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.Rating;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceMatching;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.boss.search.CleanerDetailActivity;
import app.cleaner.tw.cleanerapp.common.Constants;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityBossCaseInfoBinding;
import app.cleaner.tw.cleanerapp.databinding.DialogEndCaseBinding;
import app.cleaner.tw.cleanerapp.helper.GSONUtils;
import app.cleaner.tw.cleanerapp.helper.TimeUtils;
import app.cleaner.tw.cleanerapp.mock.MockPeopleModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CaseInfoActivity extends BaseActivity {

    private final static String CASE_MODEL = "caseModel";

    private CleanService cleanService;

    private ActivityBossCaseInfoBinding binding;
    private ViewModel viewModel;
    private EndCaseDialogViewModel dialogViewModel = null;

    private ArrayList<ServiceMatching> currentMachings = new ArrayList<>();

    public static void start(Context context, CleanService cleanService) {
        Intent starter = new Intent(context, CaseInfoActivity.class);
        starter.putExtra(CASE_MODEL, cleanService);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBundle();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_boss_case_info);
        setUpToolBar();
        viewModel = new ViewModel(cleanService);
        binding.setViewModel(viewModel);
        binding.setPresenter(new Presenter());

        initViews();
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("案件資訊");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private void getBundle() {
        if (getIntent().getExtras() != null) {
            cleanService = (CleanService) getIntent().getSerializableExtra(CASE_MODEL);
        }
    }

    private void initViews() {

        final PeopleAdapter adapter = new PeopleAdapter(currentMachings);

        currentMachings.addAll(viewModel.inviteList);

        adapter.setOnItemLongClicklistener(new PeopleAdapter.ItemLongClickListener() {
            @Override
            public void onItemLongClickListener(View view, int position) {
                ServiceMatching matching = currentMachings.get(position);

                Logger.d(matching.getCleaner());
                final KProgressHUD hud = KProgressHUD.create(activity).show();
                API.sharedService().hirerFindCleaner(API.sharedAuthorizationToken(), matching.getCleaner()).enqueue(new Callback<SimpleUser>() {
                    @Override
                    public void onResponse(Call<SimpleUser> call, Response<SimpleUser> response) {
                        hud.dismiss();
                        if (response.isSuccessful()) {
                            CleanerDetailActivity.start(activity, response.body());
                        } else {
                            Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<SimpleUser> call, Throwable t) {
                        Logger.e(t.getLocalizedMessage());
                    }
                });


            }
        });

        adapter.setOnItemClickListener(new PeopleAdapter.ItemClickListener() {
            @Override
            public void onItemClickListener(View view, final int position) {
                final ServiceMatching matching = currentMachings.get(position);
                if (viewModel.isInviteClick.get()) {
                    //點擊邀請下的清潔手

                } else if (viewModel.isProposalClick.get()) {
                    //點擊提案下的清潔手
                    showAlertDialog("是否要設為候選？", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            API.sharedService().hirerCandidateMatching(API.sharedAuthorizationToken(), matching.getId()).enqueue(new Callback<JsonObject>() {
                                @Override
                                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                    if (response.isSuccessful()) {
                                        matching.setMatching_state(ServiceMatching.MatchingState.Candidate);
                                        viewModel.caseStatus.set(Constants.CASE_STATUS.fromBackendState(cleanService.getCurrentServiceMatchingState(Constants.BOSS)).ordinal());
                                        currentMachings.remove(matching);
                                        viewModel.proposalList.remove(matching);
                                        viewModel.candidateList.add(matching);
                                        adapter.notifyDataSetChanged();
                                    } else {
                                        Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<JsonObject> call, Throwable t) {
                                    Logger.e(t.getLocalizedMessage());
                                }
                            });
                        }
                    });

                } else if (viewModel.isCandidateClick.get()) {
                    //點擊候選下的清潔手
                    showAlertDialog("是否要設為確認？", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            API.sharedService().hirerConfirmMatching(API.sharedAuthorizationToken(), matching.getId()).enqueue(new Callback<JsonObject>() {
                                @Override
                                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                    if (response.isSuccessful()) {
                                        matching.setMatching_state(ServiceMatching.MatchingState.Confirm);
                                        viewModel.caseStatus.set(Constants.CASE_STATUS.fromBackendState(cleanService.getCurrentServiceMatchingState(Constants.BOSS)).ordinal());
                                        currentMachings.remove(matching);
                                        viewModel.candidateList.remove(matching);
                                        viewModel.confirmList.add(matching);
                                        adapter.notifyDataSetChanged();
                                    } else {
                                        Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<JsonObject> call, Throwable t) {
                                    Logger.e(t.getLocalizedMessage());
                                }
                            });
                        }
                    });
                } else if (viewModel.isConfirmClick.get()) {
                    //點擊確認下的清潔手
                    showAlertDialog("是否要設為結案？", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            API.sharedService().hirerCloseMatching(API.sharedAuthorizationToken(), matching.getId()).enqueue(new Callback<JsonObject>() {
                                @Override
                                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                    if (response.isSuccessful()) {
                                        matching.setMatching_state(ServiceMatching.MatchingState.Closed);
                                        viewModel.caseStatus.set(Constants.CASE_STATUS.fromBackendState(cleanService.getCurrentServiceMatchingState(Constants.BOSS)).ordinal());
                                        currentMachings.remove(matching);
                                        viewModel.confirmList.remove(matching);
                                        viewModel.closedList.add(matching);
                                        adapter.notifyDataSetChanged();
                                    } else {
                                        Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<JsonObject> call, Throwable t) {
                                    Logger.e(t.getLocalizedMessage());
                                }
                            });
                        }
                    });
                } else if (viewModel.isClosedClick.get()) {
                    Logger.json(GSONUtils.toJson(viewModel.closedList.get(position)));
                    if (viewModel.closedList.get(position).getCleaner_rating() != null) {
                        showRdyCommentDialog();
                    } else {
                        //點擊結案下的清潔手
                        showEndCaseDialog(new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //TODO save favorite, comment, rating
                                final KProgressHUD hud = KProgressHUD.create(activity).show();
                                final ArrayList<String> erross = new ArrayList<String>();
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            if (dialogViewModel.isAddFavorite.get()) {
                                                Response r = API.sharedService().hirerAddFavoriteCleaner(API.sharedAuthorizationToken(), matching.getCleaner()).execute();
                                                if (!r.isSuccessful())
                                                    erross.add(API.shared().parseError(r).getHudMessage());
                                            }

                                            if (dialogViewModel.rating.get() != null) {
                                                int ratingValue = Math.round(dialogViewModel.rating.get());
                                                Rating rating = new Rating();
                                                rating.setRating(ratingValue);
                                                rating.setComment(dialogViewModel.comment.get());
                                                rating.setService_matching(matching.getId());
                                                Response r = API.sharedService().hirerCreateCleanerRating(API.sharedAuthorizationToken(), rating).execute();
                                                if (!r.isSuccessful())
                                                    erross.add(API.shared().parseError(r).getHudMessage());
                                            }

                                        } catch (IOException e) {
                                            e.printStackTrace();
                                            erross.add(e.getLocalizedMessage());
                                        }

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                hud.dismiss();
                                                Toast.makeText(activity, "評價清潔手成功", Toast.LENGTH_SHORT).show();
                                                finish();
                                            }
                                        });
                                    }
                                }).start();

                            }
                        }, cleanService.getCleaner_matching());
                    }
                }
            }
        });

        LinearLayoutManager lm = new LinearLayoutManager(this);
        lm.setOrientation(LinearLayoutManager.HORIZONTAL);
        binding.recyclerView.setLayoutManager(lm);
        binding.recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }

    private void showAlertDialog(String msg, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setMessage(msg);
        builder.setPositiveButton("確定", listener);
        builder.setNegativeButton("取消", null);
        builder.show();
    }

    private void showEndCaseDialog(DialogInterface.OnClickListener listener, ServiceMatching serviceMatching) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        DialogEndCaseBinding binding = DialogEndCaseBinding.inflate(getLayoutInflater());
        Logger.json(GSONUtils.toJson(serviceMatching));
        dialogViewModel = new EndCaseDialogViewModel(serviceMatching);
        binding.setViewModel(dialogViewModel);
        builder.setCancelable(false);
        builder.setPositiveButton("確認", listener);
        builder.setNegativeButton("取消", null);
        builder.setView(binding.getRoot());
        builder.show();
    }

    private void showRdyCommentDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage("您已評價過該清潔手");
        builder.setPositiveButton("確認", null);
        builder.show();
    }

    public class ViewModel {

        public final List<ServiceMatching> inviteList = new ArrayList<>();
        public final List<ServiceMatching> proposalList = new ArrayList<>();
        public final List<ServiceMatching> candidateList = new ArrayList<>();
        public final List<ServiceMatching> confirmList = new ArrayList<>();
        public final List<ServiceMatching> closedList = new ArrayList<>();

        public final ObservableField<String> houseUrl = new ObservableField<>("213");
        public final ObservableField<String> bossName = new ObservableField<>("123");
        public final ObservableField<Integer> rating = new ObservableField<>(2);
        public final ObservableField<Integer> caseStatus = new ObservableField<>(0);
        public final ObservableField<String> address = new ObservableField<>("12345");
        public final ObservableField<String> time = new ObservableField<>("321");
        public final ObservableField<String> houseStyle = new ObservableField<>("123");
        public final ObservableField<String> serviceType = new ObservableField<>("22");
        public final ObservableField<String> others = new ObservableField<>("1222");

        public final ObservableField<Boolean> isInviteClick = new ObservableField<>(true);
        public final ObservableField<Boolean> isProposalClick = new ObservableField<>(false);
        public final ObservableField<Boolean> isCandidateClick = new ObservableField<>(false);
        public final ObservableField<Boolean> isConfirmClick = new ObservableField<>(false);
        public final ObservableField<Boolean> isClosedClick = new ObservableField<>(false);

        public ViewModel(CleanService cleanService) {
            if (!cleanService.getClean_service_photos().isEmpty()) {
                houseUrl.set(cleanService.getClean_service_photos().get(0).getImage().toExternalForm());
            }

            bossName.set(cleanService.getHirer_name_read_only());
            rating.set(Math.round(cleanService.getHirer_rating_read_only()));

            inviteList.addAll(cleanService.getHirer_matching_set());
            proposalList.addAll(cleanService.getHirer_matching_set());
            candidateList.addAll(cleanService.getHirer_matching_set());
            confirmList.addAll(cleanService.getHirer_matching_set());
            closedList.addAll(cleanService.getHirer_matching_set());

            CollectionUtils.filter(inviteList, new Predicate<ServiceMatching>() {
                @Override
                public boolean evaluate(ServiceMatching object) {
                    return object.getMatching_state() == ServiceMatching.MatchingState.Invite;
                }
            });

            CollectionUtils.filter(proposalList, new Predicate<ServiceMatching>() {
                @Override
                public boolean evaluate(ServiceMatching object) {
                    return object.getMatching_state() == ServiceMatching.MatchingState.Proposal;
                }
            });

            CollectionUtils.filter(candidateList, new Predicate<ServiceMatching>() {
                @Override
                public boolean evaluate(ServiceMatching object) {
                    return object.getMatching_state() == ServiceMatching.MatchingState.Candidate;
                }
            });

            CollectionUtils.filter(confirmList, new Predicate<ServiceMatching>() {
                @Override
                public boolean evaluate(ServiceMatching object) {
                    return object.getMatching_state() == ServiceMatching.MatchingState.Confirm;
                }
            });

            CollectionUtils.filter(closedList, new Predicate<ServiceMatching>() {
                @Override
                public boolean evaluate(ServiceMatching object) {
                    return object.getMatching_state() == ServiceMatching.MatchingState.Closed;
                }
            });

            ServiceMatching.MatchingState state = cleanService.getCurrentServiceMatchingState(Constants.BOSS);

            if (state != null)
                caseStatus.set(Constants.CASE_STATUS.fromBackendState(state).ordinal());
            else
                caseStatus.set(99);

            address.set(cleanService.getAddress());

            time.set(String.format(
                    Locale.getDefault(),
                    "%s %s ~ %s",
                    TimeUtils.getTime(cleanService.getTime_range_start().getTime(), TimeUtils.DATE_FORMAT_DATE),
                    TimeUtils.getTime(cleanService.getTime_range_start().getTime(), TimeUtils.HOUR_MINUTE_FORMAT),
                    TimeUtils.getTime(cleanService.getTime_range_end().getTime(), TimeUtils.HOUR_MINUTE_FORMAT)
            ));

            houseStyle.set(cleanService.getHouse_detail().getAsString());

            serviceType.set(cleanService.getService_type_read_only().getName());

            others.set(cleanService.getOthers());

        }
    }

    public class EndCaseDialogViewModel {
        public final ObservableField<Date> arriveTime = new ObservableField<>(new Date());
        public final ObservableField<Date> leaveTime = new ObservableField<>(new Date());
        public final ObservableField<Boolean> isAddFavorite = new ObservableField<>(false);
        public final ObservableField<String> comment = new ObservableField<>("");
        public final ObservableField<Float> rating = new ObservableField<>(0.0f);

        public EndCaseDialogViewModel(ServiceMatching serviceMatching) {
            if (serviceMatching != null) {
                if (serviceMatching.getArrive_datetime() != null) {
                    arriveTime.set(serviceMatching.getArrive_datetime());
                }

                if (serviceMatching.getLeave_datetime() != null) {
                    leaveTime.set(serviceMatching.getLeave_datetime());
                }

                if (serviceMatching.getCleaner_rating() != null) {
                    if (serviceMatching.getCleaner_rating().getComment() != null) {
                        comment.set(serviceMatching.getCleaner_rating().getComment());
                    }

                    if (serviceMatching.getCleaner_rating().getRating() != 0) {
                        rating.set((float) serviceMatching.getCleaner_rating().getRating());
                    }

                }

            }
        }
    }

    private void changeList(List<ServiceMatching> list) {
        currentMachings.clear();
        currentMachings.addAll(list);
        binding.recyclerView.getAdapter().notifyDataSetChanged();
    }

    public class Presenter implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_invite:
                    if (!viewModel.isInviteClick.get()) {
                        viewModel.isInviteClick.set(true);
                        viewModel.isProposalClick.set(false);
                        viewModel.isCandidateClick.set(false);
                        viewModel.isConfirmClick.set(false);
                        viewModel.isClosedClick.set(false);
                        changeList(viewModel.inviteList);
                    }

                    break;
                case R.id.btn_proposal:
                    if (!viewModel.isProposalClick.get()) {
                        viewModel.isInviteClick.set(false);
                        viewModel.isProposalClick.set(true);
                        viewModel.isCandidateClick.set(false);
                        viewModel.isConfirmClick.set(false);
                        viewModel.isClosedClick.set(false);
                        changeList(viewModel.proposalList);
                    }
                    break;
                case R.id.btn_candidate:
                    if (!viewModel.isCandidateClick.get()) {
                        viewModel.isInviteClick.set(false);
                        viewModel.isProposalClick.set(false);
                        viewModel.isCandidateClick.set(true);
                        viewModel.isConfirmClick.set(false);
                        viewModel.isClosedClick.set(false);
                        changeList(viewModel.candidateList);
                    }
                    break;
                case R.id.btn_confirm:
                    if (!viewModel.isConfirmClick.get()) {
                        viewModel.isInviteClick.set(false);
                        viewModel.isProposalClick.set(false);
                        viewModel.isCandidateClick.set(false);
                        viewModel.isConfirmClick.set(true);
                        viewModel.isClosedClick.set(false);
                        changeList(viewModel.confirmList);
                    }
                    break;
                case R.id.btn_closed:
                    if (!viewModel.isClosedClick.get()) {
                        viewModel.isInviteClick.set(false);
                        viewModel.isProposalClick.set(false);
                        viewModel.isCandidateClick.set(false);
                        viewModel.isConfirmClick.set(false);
                        viewModel.isClosedClick.set(true);
                        changeList(viewModel.closedList);
                    }
                    break;
            }

        }
    }
}
