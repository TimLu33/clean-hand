package app.cleaner.tw.cleanerapp.backend.cleaner.models.service;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.net.URL;
import java.util.Date;

/**
 * Created by ARTHUR on 2017/10/18.
 */

public class ServiceMatching implements Serializable {

    public enum MatchingState {
        @SerializedName("IV")
        Invite,
        @SerializedName("PR")
        Proposal,
        @SerializedName("CD")
        Candidate,
        @SerializedName("CF")
        Confirm,
        @SerializedName("MS")
        Missed,
        @SerializedName("CL")
        Closed,
    }

    @Expose
    private int id;

    @Expose
    private int service;

    @Expose
    private MatchingState matching_state;

    @Expose
    private int cleaner;

    @Expose
    private Date created_datetime;

    @Expose
    private Date modified_datetime;

    @Expose
    private boolean success;

    @Expose
    private Date arrive_datetime;

    @Expose
    private Date leave_datetime;

    @Expose(serialize = false)
    private Rating cleaner_rating;

    @Expose(serialize = false)
    private Rating hirer_rating;

    @Expose(serialize = false)
    private String cleaner_name_read_only;

    @Expose(serialize = false)
    private URL cleaner_head_image_read_only;

    public int getId() {
        return id;
    }

    public int getService() {
        return service;
    }

    public MatchingState getMatching_state() {
        return matching_state;
    }

    public int getCleaner() {
        return cleaner;
    }

    public Date getCreated_datetime() {
        return created_datetime;
    }

    public Date getModified_datetime() {
        return modified_datetime;
    }

    public boolean isSuccess() {
        return success;
    }

    public Date getArrive_datetime() {
        return arrive_datetime;
    }

    public Date getLeave_datetime() {
        return leave_datetime;
    }

    public Rating getCleaner_rating() {
        return cleaner_rating;
    }

    public Rating getHirer_rating() {
        return hirer_rating;
    }

    public String getCleaner_name_read_only() {
        return cleaner_name_read_only;
    }

    public URL getCleaner_head_image_read_only() {
        return cleaner_head_image_read_only;
    }

    public void setMatching_state(MatchingState matching_state) {
        this.matching_state = matching_state;
    }
}
