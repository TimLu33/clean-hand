package app.cleaner.tw.cleanerapp.cleaner.cleancase;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.jzxiang.pickerview.TimePickerDialog;
import com.jzxiang.pickerview.data.DialogType;
import com.jzxiang.pickerview.listener.OnDateSetListener;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.geography.County;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.geography.Town;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceType;
import app.cleaner.tw.cleanerapp.backend.drfapi.builder.QueriesBuilder;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.ApiError;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.PageNumberPagination;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.common.InputFilterMinMax;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivitySearchCaseBinding;
import app.cleaner.tw.cleanerapp.helper.GSONUtils;
import app.cleaner.tw.cleanerapp.helper.TimeUtils;
import app.cleaner.tw.cleanerapp.kaoui.LazyArrayAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchCaseActivity extends BaseActivity implements OnDateSetListener {

    private ActivitySearchCaseBinding binding;
    private ViewModel viewModel;

    ArrayList<County> cities = new ArrayList<>();
    ArrayList<Town> areas = new ArrayList<>();
    ArrayList<ServiceType> serviceTypes = new ArrayList<>();

    LazyArrayAdapter<County> cityAdapter;
    LazyArrayAdapter<Town> zoneAdapter;
    LazyArrayAdapter<ServiceType> serviceTypeAdapter;


    private int year, month, day;

    private long calendarTimeInMillis;
    private TimePickerDialog dateTimePicker;
    private TimePickerDialog.Builder operateTimeDialogBuilder;
    int startHour;
    int startMin;
    int endHour;
    int endMin;
    StringBuilder tempSelectedTime;

    public static void start(Context context) {
        Intent starter = new Intent(context, SearchCaseActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search_case);
        setUpToolBar();
        viewModel = new ViewModel();
        binding.setViewModel(viewModel);
        binding.setPresenter(new Presenter());

        initViews();
        getData();
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("案件搜尋");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private void initViews() {
        setupAdapters();

        operateTimeDialogBuilder = new TimePickerDialog.Builder().setType(DialogType.HOURS_MINS).setCallBack(this).setThemeColor(getColor(this, R.color.colorAccent));
        this.year = Calendar.getInstance().get(Calendar.YEAR);
        this.month = Calendar.getInstance().get(Calendar.MONTH);
        this.day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

        //限制輸入1 ~ 10
        setEditTextFilters();

    }

    private void getData() {
        API.sharedService().getAllCounties(new QueriesBuilder().create()).enqueue(new Callback<List<County>>() {
            @Override
            public void onResponse(Call<List<County>> call, Response<List<County>> response) {
                cities.removeAll(cities);
                cities.addAll(response.body());
                ((ArrayAdapter)binding.citySpinner.getAdapter()).notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<County>> call, Throwable t) {

            }
        });

        API.sharedService().getAllServiceTypes(API.sharedAuthorizationToken(), new QueriesBuilder().create()).enqueue(new Callback<List<ServiceType>>() {
            @Override
            public void onResponse(Call<List<ServiceType>> call, Response<List<ServiceType>> response) {
                serviceTypes.removeAll(serviceTypes);
                serviceTypes.addAll(response.body());
                ((ArrayAdapter)binding.spServiceType.getAdapter()).notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<ServiceType>> call, Throwable t) {

            }
        });
    }

    private void setEditTextFilters() {

        EditText[] editTexts_0_10 = new EditText[] {
                binding.inputRoom0, binding.inputRoom1,
                binding.inputLivingRoom0, binding.inputLivingRoom1,
                binding.inputBathRoom0, binding.inputBathRoom1
        };
        for (EditText editText : editTexts_0_10) {
            editText.setFilters(new InputFilter[]{new InputFilterMinMax(0, 10)});
        }

        EditText[] editTexts_0_100 = new EditText[] {
                binding.inputSquareFeet0, binding.inputSquareFeet1
        };
        for (EditText editText : editTexts_0_100) {
            editText.setFilters(new InputFilter[]{new InputFilterMinMax(0, 100)});
        }





    }

    private void search() {
        final KProgressHUD hud = KProgressHUD.create(activity).show();
        final QueriesBuilder queriesBuilder = new QueriesBuilder();

        if (binding.areaManuel.isChecked() && viewModel.zone.get() != null) {
            queriesBuilder.filter("area", viewModel.zone.get().getId());
        }

        if (!viewModel.searchAllTime.get() && viewModel.workTimeRange.get() != null) {
            String s = viewModel.workTimeRange.get().split("~")[0];
            String e = viewModel.workTimeRange.get().split("~")[1];
            queriesBuilder.filter("time_range__startswith__time__gte", s+":00");
            queriesBuilder.filter("time_range__endswith__time__lte", e+":00");
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        if (viewModel.searchStartTime.get() != null) {
            queriesBuilder.filter("time_range_date_0", dateFormat.format(viewModel.searchStartTime.get()));
        }
        if (viewModel.searchEndTime.get() != null) {
            queriesBuilder.filter("time_range_date_1", dateFormat.format(viewModel.searchEndTime.get()));
        }
        if (viewModel.serviceType.get() != null) {
            queriesBuilder.filter("service_type", viewModel.serviceType.get().getId());
        }

        Logger.d("sq:"+viewModel.inputSquare0.get());

        queriesBuilder.filter("house_detail__room_num__range", String.format(Locale.getDefault(), "%s,%s", viewModel.inputLivingRoom0.get(), viewModel.inputLivingRoom1.get()));
        queriesBuilder.filter("house_detail__hall_num__range", String.format(Locale.getDefault(), "%s,%s", viewModel.inputRoom0.get(), viewModel.inputRoom1.get()));
        queriesBuilder.filter("house_detail__bathroom_num__range", String.format(Locale.getDefault(), "%s,%s", viewModel.inputBathRoom0.get(), viewModel.inputBathRoom1.get()));
        queriesBuilder.filter("house_detail__footage__range", String.format(Locale.getDefault(), "%s,%s", viewModel.inputSquare0.get(), viewModel.inputSquare1.get()));

        Logger.d(queriesBuilder.create());

        API.sharedService().getAllService(API.sharedAuthorizationToken(), queriesBuilder.pagination(1, QueriesBuilder.PAGE_SIZE_MAX).create()).enqueue(new Callback<PageNumberPagination<CleanService>>() {
            @Override
            public void onResponse(Call<PageNumberPagination<CleanService>> call, Response<PageNumberPagination<CleanService>> response) {
                Logger.d("count:"+response.body().getResults().size());
                hud.dismiss();
                if (response.isSuccessful()) {
                    for (CleanService cleanService : response.body().getResults()) {
                        Logger.json(GSONUtils.toJson(cleanService));
                    }

                    Toast.makeText(activity, String.format(Locale.getDefault(), "找到 %d 結果", response.body().getCount()), Toast.LENGTH_LONG).show();
                    if (response.body().getCount() != 0) {
                        SearchResultActivity.start(activity, queriesBuilder, response.body());
                    }
                } else {
                    ApiError apiError = API.shared().parseError(response);
                    Logger.e(apiError.getHudMessage());
                    Toast.makeText(activity, apiError.getHudMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PageNumberPagination<CleanService>> call, Throwable t) {
                hud.dismiss();
                Logger.e(t.getLocalizedMessage());
            }
        });
    }


    private void setupAdapters() {

        cityAdapter = new LazyArrayAdapter<County>(activity, R.layout.support_simple_spinner_dropdown_item, cities){
            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                return getView(position, convertView, parent);
            }

            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = viewFromResource(LayoutInflater.from(getContext()), convertView, parent, getResource());
                TextView textView = view.findViewById(getFieldId());
                textView.setText(getItem(position).getName());
                Logger.d(getItem(position).getCounty_code()
                );
                return view;
            }
        };
        zoneAdapter = new LazyArrayAdapter<Town>(activity, R.layout.support_simple_spinner_dropdown_item, areas) {
            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                return getView(position, convertView, parent);
            }

            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = viewFromResource(LayoutInflater.from(getContext()), convertView, parent, getResource());
                TextView textView = view.findViewById(getFieldId());
                textView.setText(getItem(position).getName());
                return view;
            }
        };
        serviceTypeAdapter = new LazyArrayAdapter<ServiceType>(activity, R.layout.support_simple_spinner_dropdown_item, serviceTypes) {

            @Override
            public int getCount() {
                return super.getCount()+1;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                return getView(position, convertView, parent);
            }

            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = viewFromResource(LayoutInflater.from(getContext()), convertView, parent, getResource());
                TextView textView = view.findViewById(getFieldId());
                if (position == 0)
                    textView.setText("全部");
                else
                    textView.setText(getItem(position-1).getName());
                return view;
            }
        };

        binding.citySpinner.setAdapter(cityAdapter);
        binding.zoneSpinner.setAdapter(zoneAdapter);
        binding.spServiceType.setAdapter(serviceTypeAdapter);

        binding.citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                County city = cities.get(i);
                viewModel.city.set(city);


                API.sharedService().getAllTowns(new QueriesBuilder()
                        .filter("parent_area__county_code", city.getCounty_code())
                        .pagination(1, QueriesBuilder.PAGE_SIZE_MAX)
                        .create()
                ).enqueue(new Callback<PageNumberPagination<Town>>() {
                    @Override
                    public void onResponse(Call<PageNumberPagination<Town>> call, Response<PageNumberPagination<Town>> response) {
                        areas.removeAll(areas);
                        if (response.isSuccessful()) {
                            areas.addAll(response.body().getResults());
                        }
                        zoneAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onFailure(Call<PageNumberPagination<Town>> call, Throwable t) {
                        Logger.e(t.getLocalizedMessage());
                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                viewModel.city.set(null);
            }
        });

        binding.zoneSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                viewModel.zone.set(areas.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                viewModel.zone.set(null);
            }
        });

        binding.spServiceType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0)
                    viewModel.serviceType.set(null);
                else
                    viewModel.serviceType.set(serviceTypes.get(position-1));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                viewModel.serviceType.set(null);
            }
        });
    }

    private void showTimePicker() {
        operateTimeDialogBuilder.setTitleStringId("工作時段-起").setCurrentMillseconds(TimeUtils.getDefaultTime(9)).setMinMillseconds(TimeUtils.getDefaultTime(0));
        dateTimePicker = operateTimeDialogBuilder.build();
        dateTimePicker.show(getSupportFragmentManager(), "operateTimeStart");
    }

    @Override
    public void onDateSet(TimePickerDialog view, long l) {
        switch (view.getTag()) {
            case "operateTimeStart":
                calendarTimeInMillis = TimeUtils.getDefaultTime(21, 0);
                String[] tempStart = TimeUtils.getTime(l, TimeUtils.HOUR_MINUTE_FORMAT).split(":");

                startHour = Integer.parseInt(tempStart[0]);
                startMin = Integer.parseInt(tempStart[1]);
                operateTimeDialogBuilder.setTitleStringId("服務時段-止")
                        .setCurrentMillseconds(calendarTimeInMillis)
                        .setMinMillseconds(l);
                dateTimePicker = operateTimeDialogBuilder.build();
                dateTimePicker.show(getSupportFragmentManager(), "operateTimeEnd");
                tempSelectedTime = new StringBuilder();
                tempSelectedTime.append(TimeUtils.getTime(l, TimeUtils.HOUR_MINUTE_FORMAT)).append("~");
                break;
            case "operateTimeEnd":
                String[] tempEnd = TimeUtils.getTime(l, TimeUtils.HOUR_MINUTE_FORMAT).split(":");
                endHour = Integer.parseInt(tempEnd[0]);
                endMin = Integer.parseInt(tempEnd[1]);
                tempSelectedTime.append(TimeUtils.getTime(l, TimeUtils.HOUR_MINUTE_FORMAT));
                viewModel.workTimeRange.set(tempSelectedTime.toString());
//                binding.tvWorkTime.setText(tempSelectedTime);
                break;
        }
    }

    public class ViewModel {

        public final ObservableBoolean searchAllArea = new ObservableBoolean(true);
        public final ObservableBoolean searchAllTime = new ObservableBoolean(true);

        public final ObservableField<County> city = new ObservableField<>();
        public final ObservableField<Town> zone = new ObservableField<>();
        public final ObservableField<ServiceType> serviceType = new ObservableField<>();

        public final ObservableField<Date> searchStartTime = new ObservableField<>();
        public final ObservableField<Date> searchEndTime = new ObservableField<>();

        public final ObservableField<String> workTimeRange = new ObservableField<>();

        public final ObservableField<String> inputRoom0 = new ObservableField<>("0");
        public final ObservableField<String> inputRoom1 = new ObservableField<>("10");

        public final ObservableField<String> inputLivingRoom0 = new ObservableField<>("0");
        public final ObservableField<String> inputLivingRoom1 = new ObservableField<>("10");

        public final ObservableField<String> inputBathRoom0 = new ObservableField<>("0");
        public final ObservableField<String> inputBathRoom1 = new ObservableField<>("4");

        public final ObservableField<String> inputSquare0 = new ObservableField<>("0");
        public final ObservableField<String> inputSquare1 = new ObservableField<>("100");

        public ViewModel() {
        }

    }

    public class Presenter implements View.OnClickListener {

        public void onSearchAreaRadioChanged(RadioGroup radioGroup, int id) {
            viewModel.searchAllArea.set(id == R.id.all_area);
            if (id == R.id.all_area) {
            }
        }

        public void onSearchTimeRadioChanged(RadioGroup radioGroup, int id) {
            viewModel.searchAllTime.set(id == R.id.all_time);
            if (id == R.id.all_time) {

            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_confirm:
                    search();
                    break;
                case R.id.tv_time_start:
                    {
                        new DatePickerDialog(activity, new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                Calendar calendar = Calendar.getInstance();
                                calendar.set(year, month, day);
                                viewModel.searchStartTime.set(calendar.getTime());
                            }
                        }, SearchCaseActivity.this.year, SearchCaseActivity.this.month, SearchCaseActivity.this.day).show();
                    }
                    break;
                case R.id.tv_time_end:
                    {
                        new DatePickerDialog(activity, new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                Calendar calendar = Calendar.getInstance();
                                calendar.set(year, month, day);
                                viewModel.searchEndTime.set(calendar.getTime());
                            }
                        }, SearchCaseActivity.this.year, SearchCaseActivity.this.month, SearchCaseActivity.this.day).show();
                    }
                    break;
                case R.id.tv_work_time:
                case R.id.time_setting:
                    showTimePicker();
                    break;
            }

        }
    }

}
