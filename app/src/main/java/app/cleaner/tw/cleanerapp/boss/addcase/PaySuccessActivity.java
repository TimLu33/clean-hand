package app.cleaner.tw.cleanerapp.boss.addcase;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.boss.BossHomeActivity;


public class PaySuccessActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_success);

        Toast.makeText(this, "感謝您支持我們的服務！", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, BossHomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
