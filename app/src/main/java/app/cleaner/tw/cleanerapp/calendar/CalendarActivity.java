package app.cleaner.tw.cleanerapp.calendar;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableField;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.ArrayAdapter;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;
import app.cleaner.tw.cleanerapp.backend.drfapi.builder.QueriesBuilder;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.PageNumberPagination;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.boss.CaseInfoActivity;
import app.cleaner.tw.cleanerapp.cleaner.event.UpdateProposalEvent;
import app.cleaner.tw.cleanerapp.cleaner.resume.ResumeDetailActivity;
import app.cleaner.tw.cleanerapp.common.Constants;
import app.cleaner.tw.cleanerapp.common.DividerItemDecoration;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityCalendarBinding;
import app.cleaner.tw.cleanerapp.helper.TimeUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CalendarActivity extends BaseActivity {

    private static final String ROLE = "role";

    private ActivityCalendarBinding binding;

    private ViewModel viewModel;

    private int role;

    Calendar calendar;

    CalendarAdapter adapter;

    List<CleanService> resumeList = new ArrayList<>();

    List<CalenderObject> calenderObjList = new ArrayList<>();

    public static void start(Context context, int role) {
        Intent starter = new Intent(context, CalendarActivity.class);
        starter.putExtra(ROLE, role);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_calendar);
        EventBus.getDefault().register(this);
        getBundle();
        setUpToolBar();
        viewModel = new ViewModel();
        calendar = Calendar.getInstance();
        String calendarTitle = DateFormat.format("yyyy年MM月", calendar.getTime()).toString();
        viewModel.calendarTitle.set(calendarTitle);
        binding.setViewModel(viewModel);
        binding.setPresenter(new Presenter());
        if (role == Constants.CLEANER) {
            loadCleanerResumes();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (role == Constants.BOSS) {
            loadHireResumes();
        }
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("行事曆");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private void getBundle() {
        if (getIntent().getExtras() != null) {
            role = getIntent().getIntExtra(ROLE, 0);
        }
    }

    private void loadCleanerResumes() {
        final KProgressHUD hud = KProgressHUD.create(activity).show();
        API.sharedService().cleanerGetOwnServices(API.sharedAuthorizationToken(), new QueriesBuilder().pagination(1, 1000).create()).enqueue(new Callback<PageNumberPagination<CleanService>>() {
            @Override
            public void onResponse(Call<PageNumberPagination<CleanService>> call, Response<PageNumberPagination<CleanService>> response) {
                hud.dismiss();
                if (response.isSuccessful()) {
                    resumeList.addAll(response.body().getResults());
                    initViews();
                } else {
                    Logger.e(API.shared().parseError(response).getHudMessage());
                }
            }

            @Override
            public void onFailure(Call<PageNumberPagination<CleanService>> call, Throwable t) {
                Logger.e(t.getLocalizedMessage());
                hud.dismiss();
            }
        });
    }

    private void loadHireResumes() {
        resumeList.clear();
        final KProgressHUD hud = KProgressHUD.create(activity).show();
        API.sharedService().hirerGetOwnServices(API.sharedAuthorizationToken(), new QueriesBuilder().order("-created_datetime").pagination(1, 1000).create())
                .enqueue(new Callback<PageNumberPagination<CleanService>>() {
                    @Override
                    public void onResponse(Call<PageNumberPagination<CleanService>> call, Response<PageNumberPagination<CleanService>> response) {
                        hud.dismiss();
                        if (response.isSuccessful()) {
                            resumeList.addAll(response.body().getResults());
                            initViews();
                        } else {
                            Logger.e(API.shared().parseError(response).getHudMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<PageNumberPagination<CleanService>> call, Throwable t) {
                        Logger.e(t.getLocalizedMessage());
                        hud.dismiss();
                    }
                });
    }

    private void initViews() {
        GridLayoutManager lm = new GridLayoutManager(this, 7);
        adapter = new CalendarAdapter(calenderObjList, role);
        adapter.setOnItemClickListener(new CalendarAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {
                if (calenderObjList.get(position).getCleanServiceList() != null && calenderObjList.get(position).getCleanServiceList().size() > 1) {
                    //有兩個以上的案件
                    AlertDialog.Builder builderSingle = new AlertDialog.Builder(CalendarActivity.this);
                    builderSingle.setTitle("請選擇案件");
                    final ArrayList<CleanService> services = new ArrayList<>(calenderObjList.get(position).getCleanServiceList());
                    final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(CalendarActivity.this, android.R.layout.select_dialog_singlechoice);
                    for (CleanService cleanService : services) {
                        arrayAdapter.add(String.format(
                                Locale.getDefault(),
                                "%s %s",
                                TimeUtils.getTime(cleanService.getTime_range_start().getTime(), TimeUtils.DATE_FORMAT_DATE),
                                cleanService.getAddress()
                        ));
                    }

                    builderSingle.setNegativeButton("取消", null);

                    builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (role == Constants.BOSS) {
                                if (!calenderObjList.get(position).getCleanServiceList().get(0).getFee_payment().isPaid()) {
                                    //未付款
                                    showConfirmDialog(new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            goPayment(position);
                                        }
                                    });
                                } else {
                                    CaseInfoActivity.start(activity, services.get(which));
                                }
                            } else {
                                ResumeDetailActivity.start(activity, services.get(which), position);
                            }
                        }
                    });
                    builderSingle.show();
                } else if (calenderObjList.get(position).getCleanServiceList() != null && calenderObjList.get(position).getCleanServiceList().size() == 1) {

                    if (role == Constants.BOSS) {
                        //僅有一個案件
                        if (!calenderObjList.get(position).getCleanServiceList().get(0).getFee_payment().isPaid()) {
                            //未付款
                            showConfirmDialog(new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    goPayment(position);
                                }
                            });
                        } else {
                            CaseInfoActivity.start(activity, calenderObjList.get(position).getCleanServiceList().get(0));
                        }
                    } else {
                        ResumeDetailActivity.start(activity, calenderObjList.get(position).getCleanServiceList().get(0), position);
                    }
                }
            }
        });
        binding.recyclerView.setLayoutManager(lm);
        binding.recyclerView.setAdapter(adapter);
        binding.recyclerView.addItemDecoration(new DividerItemDecoration(this));

        getFirstMonthDay(calendar);
        setupCalendar();
    }

    private void goPayment(int position) {
        URL payUrl = calenderObjList.get(position).getCleanServiceList().get(0).getFee_payment().getSpg_url();
        Logger.d(payUrl);
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(payUrl.toString()));
        startActivity(browserIntent);
    }

    private void showConfirmDialog(DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setMessage("此案件尚未付款，將導至付款頁面");
        builder.setPositiveButton("確定", listener);
        builder.setNegativeButton("取消", null);
        builder.show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpdateResume(UpdateProposalEvent event) {
        resumeList.set(event.getPosition(), event.getCleanService());
        binding.recyclerView.getAdapter().notifyItemChanged(event.getPosition());
    }

    public class ViewModel {

        public final ObservableField<String> calendarTitle = new ObservableField<>("");
    }

    public class Presenter implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.img_down:
                    Logger.d(getFirstMonthDay(calendar));
                    calendar.add(Calendar.MONTH, -1);
                    viewModel.calendarTitle.set(DateFormat.format("yyyy年MM月", calendar.getTime()).toString());
                    setupCalendar();
                    break;
                case R.id.img_up:
                    Logger.d(getFirstMonthDay(calendar));
                    calendar.add(Calendar.MONTH, +1);
                    viewModel.calendarTitle.set(DateFormat.format("yyyy年MM月", calendar.getTime()).toString());
                    setupCalendar();
                    break;
            }

        }
    }

    //每個月的第一天日期
    public static Date getFirstMonthDay(Calendar calendar) {
        calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DATE));
        return calendar.getTime();
    }

    private void setupCalendar() {
        calenderObjList.clear();

        //塞空的天數
        int empty = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        if (empty > 0) {
            for (int i = 0; i < empty; i++) {
                CalenderObject obj = new CalenderObject("", null);
                calenderObjList.add(obj);
            }
        }

        //取得當月天數
        int day = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        //新增該月行事曆項目
        for (int i = 1; i < (day + 1); i++) {
            CalenderObject obj = new CalenderObject(String.valueOf(i), null);
            calenderObjList.add(obj);
        }

        String year = String.valueOf(calendar.get(Calendar.YEAR));
        String month = String.valueOf(calendar.get(Calendar.MONTH) + 1);
        if (Integer.valueOf(month) < 10) {
            month = "0" + month;
        }
        Logger.d("目前選到的年份：" + year + "月份：" + month);

        for (CleanService cleanService : resumeList) {
            //履歷資料的年份
            String rYear = DateFormat.format("yyyy", cleanService.getTime_range_start()).toString();
            //履歷資料的月份
            String rMonth = DateFormat.format("MM", cleanService.getTime_range_start()).toString();

            //履歷資料日期與所選日期相符
            if (year.equals(rYear) && month.equals(rMonth)) {
                String d = DateFormat.format("dd", cleanService.getTime_range_start()).toString();

                if (calenderObjList.get(empty + Integer.valueOf(d) - 1).getCleanServiceList() == null) {
                    List<CleanService> cleanServicesList = new ArrayList<>();
                    cleanServicesList.add(cleanService);
                    calenderObjList.get(empty + Integer.valueOf(d) - 1).setCleanServiceList(cleanServicesList);
                } else {
                    calenderObjList.get(empty + Integer.valueOf(d) - 1).getCleanServiceList().add(cleanService);
                }

            }
        }

        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
