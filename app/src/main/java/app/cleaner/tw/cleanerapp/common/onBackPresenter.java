package app.cleaner.tw.cleanerapp.common;

import android.view.View;

import app.cleaner.tw.cleanerapp.base.BaseActivity;

public class onBackPresenter implements View.OnClickListener {

    private BaseActivity activity;

    public onBackPresenter(BaseActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onClick(View view) {
        activity.onBackPressed();
    }
}
