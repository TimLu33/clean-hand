package app.cleaner.tw.cleanerapp.backend.drfapi.deserializers;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.sql.Date;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by ARTHUR on 2017/11/9.
 */

public class TimeSerializer implements JsonSerializer<Time> {
    private static final String TIME_FORMAT = "HH:mm:ss";

    @Override
    public JsonElement serialize(Time src, Type typeOfSrc, JsonSerializationContext context) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(TIME_FORMAT, Locale.getDefault());
            String s = sdf.format(new Date(src.getTime()));
            return new JsonPrimitive(s);
        } catch (Exception e) {
        }
        throw new JsonParseException("Unparseable time: \"" + src.toString()
                + "\". Supported formats: " + TIME_FORMAT);
    }

}
