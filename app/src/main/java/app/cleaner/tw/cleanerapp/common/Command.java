package app.cleaner.tw.cleanerapp.common;

public interface Command {
    public void execute();
}
