package app.cleaner.tw.cleanerapp.forgot;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.ApiError;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityForgetStep1Binding;
import app.cleaner.tw.cleanerapp.helper.RegexHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ForgotPwdStep1Activity extends BaseActivity {

    private static final String REG_TYPE = "regType";
    private static final String PHONE = "phone";

    private ActivityForgetStep1Binding binding;

    private ViewModel viewModel;

    private Integer regType;

    public static void start(Context context, int regType) {
        Intent starter = new Intent(context, ForgotPwdStep1Activity.class);
        starter.putExtra(REG_TYPE, regType);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBundle();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forget_step1);
        setUpToolBar();
        viewModel = new ViewModel(regType);
        binding.setViewModel(viewModel);
        binding.setPresenter(new Presenter());
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("忘記密碼");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private void getBundle() {
        if (getIntent().getExtras() != null) {
            regType = getIntent().getIntExtra(REG_TYPE, 0);
        }
    }

    public class ViewModel {

        public final ObservableField<Integer> regType = new ObservableField<>();

        public final ObservableField<String> phone = new ObservableField<>();

        ViewModel(Integer regType) {
            this.regType.set(regType);
            phone.set("");
        }

    }

    public class Presenter implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if (viewModel.phone.get().isEmpty()) {
                Toast.makeText(activity, "請輸入手機號碼", Toast.LENGTH_SHORT).show();
            } else if (!RegexHelper.isPhone(viewModel.phone.get())){
                Toast.makeText(activity, "手機號碼格式有誤", Toast.LENGTH_SHORT).show();
            } else {
                // API Sent Password Reset SMS
                final KProgressHUD hud = KProgressHUD.create(activity).show();
                API.sharedService().sendPasswordResetSms(viewModel.phone.get()).enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        hud.dismiss();
                        if (response.isSuccessful()) {
                            ForgotPwdStep2Activity.start(activity, regType, viewModel.phone.get());
                        } else {
                            ApiError apiError = API.shared().parseError(response);
                            Logger.e(apiError.getHudMessage());
                            Toast.makeText(activity, apiError.getHudMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        hud.dismiss();
                        Logger.e(t.getLocalizedMessage());
                    }
                });
            }

        }
    }


}
