package app.cleaner.tw.cleanerapp;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import app.cleaner.tw.cleanerapp.base.BaseActivity;

public class HomeActivity extends BaseActivity {

    public static void start(Context context) {
        Intent starter = new Intent(context, HomeActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        final Button btn = (Button) findViewById(R.id.btn_logo);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginMainActivity.start(activity);
            }
        });
        btn.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                int w = btn.getMeasuredWidth();
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(w, w);
                btn.setLayoutParams(params);
                btn.getViewTreeObserver().removeOnPreDrawListener(this);
                return false;
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isGooglePlayServicesAvailable(this)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setCancelable(false);
            builder.setMessage("GooglePlayServices版本過低\n請更新GooglePlayServices服務");
            builder.setPositiveButton("確定", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
            builder.show();
        }
    }

    private boolean isGooglePlayServicesAvailable(Activity context) {
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context);
        return code == ConnectionResult.SUCCESS;
    }
}
