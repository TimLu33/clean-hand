package app.cleaner.tw.cleanerapp.boss.setting;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Transformer;

import java.util.ArrayList;
import java.util.List;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.documentions.Faq;
import app.cleaner.tw.cleanerapp.backend.drfapi.builder.QueriesBuilder;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.PageNumberPagination;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.common.DividerItemDecoration;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityCommonQuestionBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommonQuestionActivity extends BaseActivity {

    private ActivityCommonQuestionBinding binding;
    private List<QuestionObject> faqList = new ArrayList<>();

    public static void start(Context context) {
        Intent starter = new Intent(context, CommonQuestionActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_common_question);
        setUpToolBar();
        LinearLayoutManager lm = new LinearLayoutManager(this);
        lm.setOrientation(LinearLayoutManager.VERTICAL);

        final QuestionAdapter adapter = new QuestionAdapter(faqList);
        adapter.setOnItemClickListener(new QuestionAdapter.ItemClickListener() {
            @Override
            public void onItemClickListener(View view, int position) {
                faqList.get(position).setShowContent(!faqList.get(position).isShowContent());
                adapter.notifyItemChanged(position);
            }
        });
        binding.recyclerView.addItemDecoration(new DividerItemDecoration(this));
        binding.recyclerView.setLayoutManager(lm);
        binding.recyclerView.setAdapter(adapter);
        binding.recyclerView.getItemAnimator().setChangeDuration(0);


        loadFaq();
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("常見問題");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private void loadFaq() {
        final KProgressHUD hud = KProgressHUD.create(activity).show();
        API.sharedService().getAllFaq(API.sharedAuthorizationToken(), new QueriesBuilder().pagination(1, 1000).create()).enqueue(new Callback<PageNumberPagination<Faq>>() {
            @Override
            public void onResponse(Call<PageNumberPagination<Faq>> call, Response<PageNumberPagination<Faq>> response) {
                hud.dismiss();
                if (response.isSuccessful()) {
                    ArrayList<QuestionObject> qs = new ArrayList<>(CollectionUtils.collect(response.body().getResults(), new Transformer<Faq, QuestionObject>() {
                        @Override
                        public QuestionObject transform(Faq input) {
                            return new QuestionObject(input.getQuestion(), input.getAnswer(), false);
                        }
                    }));
                    faqList.addAll(qs);
                    binding.recyclerView.getAdapter().notifyDataSetChanged();
                } else {
                    Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PageNumberPagination<Faq>> call, Throwable t) {
                hud.dismiss();
                Logger.e(t.getLocalizedMessage());
            }
        });
    }

    private void setMockData() {
        for (int i = 0; i < 5; i++) {
            QuestionObject obj = new QuestionObject("title", "content", false);
            faqList.add(obj);
        }
        binding.recyclerView.getAdapter().notifyDataSetChanged();
    }

}
