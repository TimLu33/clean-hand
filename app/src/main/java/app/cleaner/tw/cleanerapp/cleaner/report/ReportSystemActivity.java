package app.cleaner.tw.cleanerapp.cleaner.report;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceMatching;
import app.cleaner.tw.cleanerapp.backend.drfapi.builder.QueriesBuilder;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.PageNumberPagination;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.boss.search.CleanerDetailActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportSystemActivity extends BaseActivity {

    enum Action {
        Arrive,
        Leave
    }

    KProgressHUD hud = null;

    public static void start(Context context) {
        Intent starter = new Intent(context, ReportSystemActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cleaner_report_system);
        setUpToolBar();
        initViews();
    }

    private void setUpToolBar() {
        TextView tvTitle = (TextView) findViewById(R.id.title);
        tvTitle.setText("回報系統");
        ImageButton btnBack = (ImageButton) findViewById(R.id.btn_back);
        btnBack.setVisibility(View.VISIBLE);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void initViews() {
        hud = KProgressHUD.create(activity);
        Button btnArrive = (Button) findViewById(R.id.btn_arrive);
        Button btnLeave = (Button) findViewById(R.id.btn_leave);
        btnArrive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findMatchings(Action.Arrive);
            }
        });
        btnLeave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findMatchings(Action.Leave);
            }
        });
    }

    private void showReportDialog(DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setMessage("您確定要回報嗎");
        builder.setPositiveButton("是", listener);
        builder.setNegativeButton("否", null);
        builder.show();
    }

    private void showReportSuccessDialog(DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setMessage("您的回報已經送出");
        builder.setPositiveButton("確定", listener);
        builder.show();
    }

    private void findMatchings(final Action action) {
        hud.show();
        QueriesBuilder queriesBuilder = new QueriesBuilder()
                .filter("service_matching_set__matching_state", "CF")
                .filter("service_matching_set__cleaner", API.sharedUserId())
                .pagination(1, 1000)
                .order("-created_datetime");
        switch (action) {
            case Arrive:
                queriesBuilder.filter("service_matching_arrived", false);
                break;
            case Leave:
                queriesBuilder.filter("service_matching_left", false);
                break;
        }
        API.sharedService().cleanerGetOwnServices(API.sharedAuthorizationToken(), queriesBuilder.create()).enqueue(new Callback<PageNumberPagination<CleanService>>() {
            @Override
            public void onResponse(Call<PageNumberPagination<CleanService>> call, Response<PageNumberPagination<CleanService>> response) {
                hud.dismiss();
                if (response.isSuccessful()) {
                    List<CleanService> services = response.body().getResults();
                    showPickMatchingsDialog(action, services);
                } else {
                    Logger.e(API.shared().parseError(response).getHudMessage());
                }
            }

            @Override
            public void onFailure(Call<PageNumberPagination<CleanService>> call, Throwable t) {
                hud.dismiss();
                Logger.e(t.getLocalizedMessage());
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (hud.isShowing()) {
            hud.dismiss();
        }
    }

    private void showPickMatchingsDialog(final Action action, final List<CleanService> services) {

        if (services.isEmpty()) {
            Toast.makeText(activity, "您目前沒有可到離點服務", Toast.LENGTH_SHORT).show();
            return;
        }
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(activity, android.R.layout.select_dialog_singlechoice);

        for (CleanService service: services) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(service.getTime_range_start());
            String exampleListTitle = String.format(
                    Locale.getDefault(),
                    "%s(%s/%s, %s)",
                    service.getHirer_name_read_only(),
                    calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH),
                    service.getAddress()
            );
            arrayAdapter.add(exampleListTitle);
        }

        AlertDialog.Builder builderSingle = new AlertDialog.Builder(activity);
        builderSingle.setTitle("請選擇案件");
        builderSingle.setNegativeButton("取消", null);
        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                final CleanService selectedService = services.get(i);
                switch (action) {
                    case Arrive:
                        showReportDialog(new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (which == -1) {
                                    arrive(selectedService);
                                }
                            }
                        });
                        break;
                    case Leave:
                        showReportDialog(new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (which == -1) {
                                    leave(selectedService);
                                }
                            }
                        });
                        break;
                }
            }
        });
        builderSingle.show();

    }

    private void arrive(CleanService service) {
        hud.show();
        API.sharedService().cleanerArriveMatching(API.sharedAuthorizationToken(), service.getCleaner_matching().getId()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                hud.dismiss();
                if (response.isSuccessful()) {
                    showReportSuccessDialog(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                } else {
                    Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                    Logger.e(API.shared().parseError(response).getHudMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                hud.dismiss();
                Logger.e(t.getLocalizedMessage());
            }
        });
    }

    private void leave(CleanService service) {
        hud.show();
        API.sharedService().cleanerLeaveMatching(API.sharedAuthorizationToken(), service.getCleaner_matching().getId()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                hud.dismiss();
                if (response.isSuccessful()) {
                    showReportSuccessDialog(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                } else {
                    Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                    Logger.e(API.shared().parseError(response).getHudMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                hud.dismiss();
                Logger.e(t.getLocalizedMessage());
            }
        });
    }


}
