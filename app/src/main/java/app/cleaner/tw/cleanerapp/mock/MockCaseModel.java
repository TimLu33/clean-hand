package app.cleaner.tw.cleanerapp.mock;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.orhanobut.logger.Logger;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;

public class MockCaseModel implements Serializable {

    private CleanService cleanService;

    private String bossName;
    private String housePicUrl;
    private Integer rating;
    private CleanService.Priority status;
    private String address;
    private String time;
    private String roomStyle;
    private String serviceType;
    private String others;

    public MockCaseModel(CleanService cleanService) {
        super();
        setCleanService(cleanService);
    }

    public CleanService getCleanService() {
        return cleanService;
    }

    public void setCleanService(CleanService cleanService) {
        this.cleanService = cleanService;
        setup();
    }

    private void setup() {
        setBossName(cleanService.getHirer_name_read_only());
        setRating(Math.round(cleanService.getHirer_rating_read_only()));

        if (!cleanService.getClean_service_photos().isEmpty())
            setHousePicUrl(cleanService.getClean_service_photos().get(0).getImage().toExternalForm());

        setAddress(cleanService.getAddress());

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd", Locale.getDefault());
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());

        setTime(String.format(
                Locale.getDefault(),
                "%s %s ~ %s",
                dateFormat.format(cleanService.getTime_range_start()),
                timeFormat.format(cleanService.getTime_range_start()),
                timeFormat.format(cleanService.getTime_range_end())
        ));

        setStatus(cleanService.getPriority()
        );

        setRoomStyle(String.format(
                Locale.getDefault(),
                "%d房 %d廳 %d衛 %d坪",
                cleanService.getHouse_detail().getRoom_num(),
                cleanService.getHouse_detail().getHall_num(),
                cleanService.getHouse_detail().getBathroom_num(),
                cleanService.getHouse_detail().getFootage()
        ));

        setServiceType(cleanService.getService_type_read_only().getName());

        ArrayList<String> others = new ArrayList<>();
        if (cleanService.isHas_pet())
            others.add("有寵物");
        if (cleanService.isLimit_female())
            others.add("限女");
        if (cleanService.isNeed_cleaner_license())
            others.add("需要清潔證照");
        if (cleanService.isNeed_good_card())
            others.add("需要良民證");
        if (cleanService.isProvide_tools())
            others.add("自備工具");
        if (!others.isEmpty())
            setOthers(TextUtils.join(", ", others));
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getBossName() {
        return bossName;
    }

    public void setBossName(String bossName) {
        this.bossName = bossName;
    }

    public String getHousePicUrl() {
        return housePicUrl;
    }

    public void setHousePicUrl(String housePicUrl) {
        this.housePicUrl = housePicUrl;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public CleanService.Priority getStatus() {
        return status;
    }

    public void setStatus(CleanService.Priority status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRoomStyle() {
        return roomStyle;
    }

    public void setRoomStyle(String roomStyle) {
        this.roomStyle = roomStyle;
    }

    public String getOthers() {
        return others;
    }

    public void setOthers(String others) {
        this.others = others;
    }


//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeString(this.bossName);
//        dest.writeString(this.housePicUrl);
//        dest.writeValue(this.rating);
//        dest.writeValue(this.status);
//        dest.writeString(this.address);
//        dest.writeString(this.time);
//        dest.writeString(this.roomStyle);
//        dest.writeString(this.serviceType);
//        dest.writeString(this.others);
//    }

    public MockCaseModel() {
    }

    protected MockCaseModel(Parcel in) {
        this.bossName = in.readString();
        this.housePicUrl = in.readString();
        this.rating = (Integer) in.readValue(Integer.class.getClassLoader());
        this.status = (CleanService.Priority) in.readValue(Integer.class.getClassLoader());
        this.address = in.readString();
        this.time = in.readString();
        this.roomStyle = in.readString();
        this.serviceType = in.readString();
        this.others = in.readString();
    }

//    public static final Creator<MockCaseModel> CREATOR = new Creator<MockCaseModel>() {
//        @Override
//        public MockCaseModel createFromParcel(Parcel source) {
//            return new MockCaseModel(source);
//        }
//
//        @Override
//        public MockCaseModel[] newArray(int size) {
//            return new MockCaseModel[size];
//        }
//    };
}