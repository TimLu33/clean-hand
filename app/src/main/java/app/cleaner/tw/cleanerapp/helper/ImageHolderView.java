package app.cleaner.tw.cleanerapp.helper;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.bigkoo.convenientbanner.holder.Holder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import app.cleaner.tw.cleanerapp.common.ConvenientBannerModel;

public class ImageHolderView<T> implements Holder<T> {
    private ImageView imageView;

    @Override
    public View createView(Context context) {
        imageView = new ImageView(context);
        return imageView;
    }

    @Override
    public void UpdateUI(Context context, int position, T data) {
        if (data instanceof ConvenientBannerModel) {
            if (((ConvenientBannerModel) data).getImageUrl() != null) {
                Glide.with(context)
                        .load(((ConvenientBannerModel) data).getImageUrl())
                        .apply(new RequestOptions().centerCrop())
                        .into(imageView);
            } else {
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                imageView.setImageBitmap(((ConvenientBannerModel) data).getBitmap());
            }
        }
    }
}
