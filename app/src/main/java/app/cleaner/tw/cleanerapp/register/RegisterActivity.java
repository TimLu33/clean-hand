package app.cleaner.tw.cleanerapp.register;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.AuthTokenBean;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.CleanerProfile;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.HirerProfile;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.ApiError;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.common.Constants;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityRegisterAccountBinding;
import app.cleaner.tw.cleanerapp.helper.RegexHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends BaseActivity {

    private static final String PHONE = "phone";
    private static final String REG_TYPE = "regType";
    private ActivityRegisterAccountBinding binding;
    private RegViewModel viewModel;
    private Integer regType;
    private String phone;

    public static void start(Context context, int regType, String phone) {
        Intent starter = new Intent(context, RegisterActivity.class);
        starter.putExtra(REG_TYPE, regType);
        starter.putExtra(PHONE, phone);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBundle();
        EventBus.getDefault().register(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register_account);
        setUpToolBar();
        viewModel = new RegViewModel(regType);
        binding.setViewModel(viewModel);
        binding.setPresenter(new Presenter());
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("註冊帳號");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private void getBundle() {
        if (getIntent().getExtras() != null) {
            regType = getIntent().getIntExtra(REG_TYPE, 0);
            phone = getIntent().getStringExtra(PHONE);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRegSuccessEvent(onRegisterSuccessEvent event) {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public class Presenter implements View.OnClickListener {

        boolean isShowPwd = false;
        boolean isRepeatShowPwd = false;

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_reg:
                    if (viewModel.acc.get().isEmpty()) {
                        Toast.makeText(activity, "請輸入帳號", Toast.LENGTH_SHORT).show();
                    } else if (!RegexHelper.isMail(viewModel.acc.get())) {
                        Toast.makeText(activity, "帳號格式輸入錯誤", Toast.LENGTH_SHORT).show();
                    } else if (viewModel.pwd.get().isEmpty() || viewModel.confirmPwd.get().isEmpty()) {
                        Toast.makeText(activity, "請輸入密碼", Toast.LENGTH_SHORT).show();
                    } else if (!viewModel.pwd.get().equals(viewModel.confirmPwd.get())) {
                        Toast.makeText(activity, "密碼與確認密碼不一致", Toast.LENGTH_SHORT).show();
                    } else {
                        // API Register
                        final KProgressHUD hud = KProgressHUD.create(activity).show();

                        final Runnable dismissHudR = new Runnable() {
                            @Override
                            public void run() {
                                hud.dismiss();
                            }
                        };

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Response<AuthTokenBean> registerResponse = API.sharedService().register(viewModel.acc.get(), viewModel.pwd.get(), viewModel.confirmPwd.get()).execute();
                                    if (registerResponse.isSuccessful()) {
                                        AuthTokenBean tokenBean = registerResponse.body();
                                        API.shared().setAuthorizationToken(tokenBean.getToken());
                                        if (regType == Constants.CLEANER) {
                                            CleanerProfile cleanerProfile = new CleanerProfile();
                                            Response<CleanerProfile> createProfileResponse = API.sharedService().cleanerCreateProfile(API.sharedAuthorizationToken(), cleanerProfile).execute();
                                            if (!createProfileResponse.isSuccessful()) {
                                                ApiError apiError = API.shared().parseError(createProfileResponse);
                                                Logger.e(apiError.getHudMessage());
                                            }
                                        } else {
                                            HirerProfile hirerProfile = new HirerProfile();
                                            Response<HirerProfile> createProfileResponse = API.sharedService().hirerCreateProfile(API.sharedAuthorizationToken(), hirerProfile).execute();
                                            if (!createProfileResponse.isSuccessful()) {
                                                ApiError apiError = API.shared().parseError(createProfileResponse);
                                                Logger.e(apiError.getHudMessage());
                                            }
                                        }
                                        runOnUiThread(dismissHudR);
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                RegisterDataActivity.start(activity, regType, phone);
                                            }
                                        });
                                    } else {
                                        final ApiError apiError = API.shared().parseError(registerResponse);
                                        Logger.e(apiError.getHudMessage());
                                        runOnUiThread(dismissHudR);
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(activity, apiError.getHudMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    Logger.e(e.getLocalizedMessage());
                                    runOnUiThread(dismissHudR);
                                }
                            }
                        }).start();


//                        API.sharedService().register(viewModel.acc.get(), viewModel.pwd.get(), viewModel.confirmPwd.get()).enqueue(new Callback<AuthTokenBean>() {
//                            @Override
//                            public void onResponse(Call<AuthTokenBean> call, Response<AuthTokenBean> response) {
//                                hud.dismiss();
//                                if (response.isSuccessful()) {
//                                    AuthTokenBean tokenBean = response.body();
//                                    API.shared().setAuthorizationToken(tokenBean.getToken());
//                                    RegisterDataActivity.start(activity, regType);
//                                } else {
//                                    ApiError apiError = API.shared().parseError(response);
//                                    Toast.makeText(activity, apiError.getHudMessage(), Toast.LENGTH_SHORT).show();
//                                    Logger.e(apiError.getHudMessage());
//                                }
//                            }
//
//                            @Override
//                            public void onFailure(Call<AuthTokenBean> call, Throwable t) {
//                                hud.dismiss();
//                                Logger.e(t.getLocalizedMessage());
//                            }
//                        });
                    }
                    break;
                case R.id.show_pwd:
                    isShowPwd = !isShowPwd;
                    binding.inputPwd.setTransformationMethod(isShowPwd ? HideReturnsTransformationMethod.getInstance() : PasswordTransformationMethod.getInstance());
                    break;
                case R.id.show_confirm_pwd:
                    isRepeatShowPwd = !isRepeatShowPwd;
                    binding.inputConfirmPwd.setTransformationMethod(isShowPwd ? HideReturnsTransformationMethod.getInstance() : PasswordTransformationMethod.getInstance());
                    break;
            }

        }
    }
}
