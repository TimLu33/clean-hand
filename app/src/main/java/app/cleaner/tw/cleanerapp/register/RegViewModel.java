package app.cleaner.tw.cleanerapp.register;

import android.databinding.ObservableField;

/**
 * Created by juntinglu on 2017/10/20.
 */

public class RegViewModel {

    public final ObservableField<Integer> regType = new ObservableField<>();
    public final ObservableField<String> acc = new ObservableField<>();
    public final ObservableField<String> pwd = new ObservableField<>();
    public final ObservableField<String> confirmPwd = new ObservableField<>();


    public RegViewModel(Integer regType) {
        this.regType.set(regType);
        acc.set("");
        pwd.set("");
        confirmPwd.set("");
    }

}
