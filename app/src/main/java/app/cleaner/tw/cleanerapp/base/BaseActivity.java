package app.cleaner.tw.cleanerapp.base;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import app.cleaner.tw.cleanerapp.common.Command;
import app.cleaner.tw.cleanerapp.common.Control;

public class BaseActivity extends AppCompatActivity {

    public BaseActivity activity;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
    }

    public void hideInputKeyBoard(View v) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);
        }
    }

    public Drawable getDrawable(Context context, int drawableId) {
        return ContextCompat.getDrawable(context, drawableId);
    }

    public int getColor(Context context, int colorId) {
        return ContextCompat.getColor(context, colorId);
    }

    public void execute(Command command) {
        Control.getInstance().execute(command);
    }
}
