package app.cleaner.tw.cleanerapp.boss.search;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.CleanerProfile;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.SimpleUser;
import app.cleaner.tw.cleanerapp.databinding.SearchCleanerResultItemBinding;

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.BindingHolder> implements View.OnClickListener {

    public interface ItemClickListener {
        void onItemClickListener(View view, int position);
    }

    private ItemClickListener listener;
    private List<SimpleUser> cleanerList = new ArrayList<>();

    public SearchResultAdapter(List<SimpleUser> cleanerList) {
        this.cleanerList = cleanerList;
    }

    @Override
    public SearchResultAdapter.BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        SearchCleanerResultItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.search_cleaner_result_item, parent, false);
        binding.getRoot().setOnClickListener(this);
        return new BindingHolder(binding);
    }

    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {
        holder.binding.getRoot().setTag(position);
        SearchResultItemViewModel viewModel = new SearchResultItemViewModel(cleanerList.get(position));
        holder.bind(viewModel);
    }

    @Override
    public int getItemCount() {
        return cleanerList.size();
    }

    public void setOnItemClickListener(ItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if (listener != null) {
            listener.onItemClickListener(view, (int) view.getTag());
        }
    }

    public class BindingHolder extends RecyclerView.ViewHolder {

        private SearchCleanerResultItemBinding binding;

        public BindingHolder(SearchCleanerResultItemBinding binding) {
            super(binding.rootView);
            this.binding = binding;
        }

        public void bind(SearchResultItemViewModel viewModel) {
            binding.setViewModel(viewModel);
            binding.executePendingBindings();
        }

    }
}
