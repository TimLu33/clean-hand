package app.cleaner.tw.cleanerapp.backend.cleaner.models.geography;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by ARTHUR on 2017/10/17.
 */

public class County implements Serializable {

    /**
     * id : 32
     * county_code : 10008
     * name : 南投縣
     * name_eng : Nantou County
     */

    @Expose
    private int id;

    @Expose
    private String county_code;

    @Expose
    private String name;

    @Expose
    private String name_eng;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCounty_code() {
        return county_code;
    }

    public String getName() {
        return name;
    }

    public String getName_eng() {
        return name_eng;
    }
}
