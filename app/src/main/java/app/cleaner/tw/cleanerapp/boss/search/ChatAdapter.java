package app.cleaner.tw.cleanerapp.boss.search;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceQA;
import app.cleaner.tw.cleanerapp.cleaner.cleancase.ChatItemViewModel;
import app.cleaner.tw.cleanerapp.common.Constants;
import app.cleaner.tw.cleanerapp.databinding.ChatLeftItemBinding;
import app.cleaner.tw.cleanerapp.databinding.ChatRightItemBinding;
import app.cleaner.tw.cleanerapp.mock.MockChatModel;

public class ChatAdapter extends app.cleaner.tw.cleanerapp.cleaner.cleancase.ChatAdapter {

    public ChatAdapter(List<ServiceQA> chatList) {
        super(chatList);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == Constants.CLEANER) {
            ChatLeftItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.chat_left_item, parent, false);
            return new BossBindingHolder(binding);
        } else {
            ChatRightItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.chat_right_item, parent, false);
            return new CleanerBindingHolder(binding);
        }
    }
}
