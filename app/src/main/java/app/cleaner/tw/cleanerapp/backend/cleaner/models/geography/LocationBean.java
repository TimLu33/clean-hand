package app.cleaner.tw.cleanerapp.backend.cleaner.models.geography;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ARTHUR on 2017/11/8.
 */

public class LocationBean implements Serializable {
    enum LocationType {
        @SerializedName("Point")
        Point
    }

    @Expose
    private LocationType type;
    @Expose
    private List<Double> coordinates;

    public LocationType getType() {
        return type;
    }

    public List<Double> getCoordinates() {
        return coordinates;
    }
}
