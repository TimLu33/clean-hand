package app.cleaner.tw.cleanerapp.cleaner.cleancase;

import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;
import app.cleaner.tw.cleanerapp.backend.drfapi.builder.QueriesBuilder;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.PageNumberPagination;

public class UpdateSearchResultEvent {

    private CleanService cleanService;
    private int position;

    public UpdateSearchResultEvent(CleanService cleanService, int position) {
        this.cleanService = cleanService;
        this.position = position;
    }

    public CleanService getCleanService() {
        return cleanService;
    }

    public void setCleanService(CleanService cleanService) {
        this.cleanService = cleanService;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
