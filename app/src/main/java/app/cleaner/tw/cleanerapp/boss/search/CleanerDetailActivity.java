package app.cleaner.tw.cleanerapp.boss.search;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bigkoo.convenientbanner.ConvenientBanner;
import com.bigkoo.convenientbanner.holder.CBViewHolderCreator;
import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.CleanerProfile;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.SimpleUser;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceType;
import app.cleaner.tw.cleanerapp.backend.drfapi.builder.QueriesBuilder;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.PageNumberPagination;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.boss.search.event.OnDealSuccessEvent;
import app.cleaner.tw.cleanerapp.common.ConvenientBannerModel;
import app.cleaner.tw.cleanerapp.common.ImageConfig;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityCleanerDetailBinding;
import app.cleaner.tw.cleanerapp.helper.ImageHolderView;
import app.cleaner.tw.cleanerapp.helper.ScreenHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CleanerDetailActivity extends BaseActivity {

    enum Action {
        SentQA,
        Invite
    }

    private static final String CLEANER_USER = "cleaner";
    private ActivityCleanerDetailBinding binding;
    private ViewModel viewModel;

    private SimpleUser cleaner;
    private List<ConvenientBannerModel> imageList = new ArrayList<>();

    public static void start(Context context, SimpleUser cleaner) {
        Intent starter = new Intent(context, CleanerDetailActivity.class);
        starter.putExtra(CLEANER_USER, cleaner);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBundle();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cleaner_detail);
        EventBus.getDefault().register(this);
        setUpToolBar();
        initViews();
        viewModel = new ViewModel(cleaner.getCleaner_profile());
        binding.setViewModel(viewModel);
        binding.setPresenter(new Presenter());
    }

    private void getBundle() {
        if (getIntent().getExtras() != null) {
            cleaner = (SimpleUser) getIntent().getSerializableExtra(CLEANER_USER);
        }
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("個人主頁");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private void initViews() {
        float height = (float) ScreenHelper.getWidth(activity) / (float) ImageConfig.coverWidth * (float) ImageConfig.coverHeight;
        binding.coverPhoto.setLayoutParams(new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) height));

        for (CleanerProfile.CoverPhoto photo : cleaner.getCleaner_profile().getCover_photos()) {
            ConvenientBannerModel image = new ConvenientBannerModel(photo.getImage().toString());
            imageList.add(image);
        }
        binding.coverPhoto.setPages(new CBViewHolderCreator<ImageHolderView>() {
            @Override
            public ImageHolderView createHolder() {
                return new ImageHolderView();
            }
        }, imageList);
        if (imageList.size() > 1) {
            binding.coverPhoto.setPageIndicator(new int[]{R.drawable.banner_dian_blur, R.drawable.banner_dian_focus})
                    .setPageIndicatorAlign(ConvenientBanner.PageIndicatorAlign.CENTER_HORIZONTAL);
        } else {
            binding.coverPhoto.setCanLoop(false);
        }
    }

    private String getProvideServiceString(List<ServiceType> serviceList) {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < serviceList.size(); i++) {
            if (i != serviceList.size() - 1) {
                s.append(serviceList.get(i).getName()).append("、");
            } else {
                s.append(serviceList.get(i).getName());
            }
        }
        return s.toString();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDealSuccessEvent(OnDealSuccessEvent event) {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void showInviteSuccessDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(CleanerDetailActivity.this);
        builder.setMessage("邀請提案成功");
        builder.setCancelable(false);
        builder.setPositiveButton("確定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                binding.btnInvite.setEnabled(false);
            }
        });
        builder.show();
    }


    public class ViewModel {

        public final ObservableField<String> coverPhoto = new ObservableField<>();
        public final ObservableField<String> headImage = new ObservableField<>();
        public final ObservableField<String> cleanerName = new ObservableField<>();
        public final ObservableField<Integer> rating = new ObservableField<>();
        public final ObservableField<String> recommendation = new ObservableField<>();
        public final ObservableField<String> intro = new ObservableField<>();
        public final ObservableField<Integer> salary = new ObservableField<>();
        public final ObservableField<String> provideService = new ObservableField<>();

        public ViewModel(CleanerProfile profile) {
            if (profile.getCover_photos() != null && !profile.getCover_photos().isEmpty()) {
                coverPhoto.set(profile.getCover_photos().get(0).getImage().toString());
            }

            if (profile.getHead_image() != null) {
                headImage.set(profile.getHead_image().toString());
            }

            if (profile.getDisplay_name() != null) {
                cleanerName.set(profile.getDisplay_name());
            }

            rating.set((int) profile.getAvg_rating());
            recommendation.set(profile.getRecommendation());
            intro.set(profile.getIntroduction() != null ? profile.getIntroduction() : "");
            salary.set(profile.getExpected_salary());
            provideService.set(getProvideServiceString(profile.getProvide_service_types()));
        }
    }

    public class Presenter implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_check_out:
                    //查看評價
                    CleanerRatedListActivity.start(activity, cleaner);
                    break;
                case R.id.btn_invite:
                    //邀請提案
                    showPaidServicesToPick(Action.Invite);
                    break;
                case R.id.btn_message:
                    //即時通訊
                    sentQA();
                    break;
            }

        }
    }

    private void showPaidServicesToPick(final Action action) {
        final KProgressHUD hud = KProgressHUD.create(activity).show();
        QueriesBuilder queriesBuilder = new QueriesBuilder()
                .filter("fee_payment__paid", true)
                .order("-created_datetime")
                .pagination(1, 1000);
        API.sharedService().hirerGetOwnServices(API.sharedAuthorizationToken(), queriesBuilder.create()).enqueue(new Callback<PageNumberPagination<CleanService>>() {
            @Override
            public void onResponse(Call<PageNumberPagination<CleanService>> call, Response<PageNumberPagination<CleanService>> response) {
                hud.dismiss();
                if (response.isSuccessful()) {
                    final List<CleanService> availableServices = response.body().getResults();

                    if (availableServices.isEmpty()) {
                        Toast.makeText(activity, "您沒有可問與答的服務", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    //Dialog picker
                    final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(CleanerDetailActivity.this, android.R.layout.select_dialog_singlechoice);
                    for (CleanService availableService : availableServices) {
                        arrayAdapter.add(availableService.getAddress());
                    }
                    showCaseListDialog(arrayAdapter, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            switch (action) {
                                case SentQA:
//                                    sentQA(availableServices.get(i));
                                    break;
                                case Invite:
                                    invite(availableServices.get(i));
                                    break;
                            }
                        }
                    });

                } else {
                    Logger.e(API.shared().parseError(response).getHudMessage());
                }
            }

            @Override
            public void onFailure(Call<PageNumberPagination<CleanService>> call, Throwable t) {
                hud.dismiss();
                Logger.e(t.getLocalizedMessage());
            }
        });
    }

    private void showCaseListDialog(ArrayAdapter<String> arrayAdapter, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(CleanerDetailActivity.this);
        builderSingle.setTitle("請選擇案件");
        builderSingle.setNegativeButton("取消", null);
        builderSingle.setAdapter(arrayAdapter, listener);
        builderSingle.show();
    }

    private void sentQA() {
        BossChatActivity.start(activity, cleaner);
    }

    private void invite(CleanService service) {
        final KProgressHUD hud = KProgressHUD.create(activity).show();
        API.sharedService().hirerInviteService(API.sharedAuthorizationToken(), service.getId(), cleaner.getId()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                hud.dismiss();
                if (response.isSuccessful())
                    showInviteSuccessDialog();
                else
                    Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                hud.dismiss();
                Logger.e(t.getLocalizedMessage());
            }
        });
    }


}



