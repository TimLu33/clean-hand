package app.cleaner.tw.cleanerapp.boss.profile;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableField;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.geography.County;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.geography.Town;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.HirerProfile;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.User;
import app.cleaner.tw.cleanerapp.backend.drfapi.builder.QueriesBuilder;
import app.cleaner.tw.cleanerapp.backend.drfapi.helper.DRFHelper;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.ApiError;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.PageNumberPagination;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.common.Preference;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityBossProfileSettingBinding;
import app.cleaner.tw.cleanerapp.helper.GSONUtils;
import app.cleaner.tw.cleanerapp.helper.ImagePickerHelper;
import app.cleaner.tw.cleanerapp.helper.RegexHelper;
import app.cleaner.tw.cleanerapp.kaoui.LazyArrayAdapter;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileSettingActivity extends BaseActivity implements EasyPermissions.PermissionCallbacks {

    private final int REQUEST_PHOTO_CODE = 100;

    private final int REQUEST_PHOTO_CROP = 101;

    protected File newHeadFile;
    private static final String USER = "user";

    private static final String IS_NEED_CREATE_PROFILE = "isNeedCreateProfile";

    private ActivityBossProfileSettingBinding binding;

    private ViewModel viewModel;

    private String[] gender = {"男", "女"};

    ArrayList<County> cities = new ArrayList<>();

    ArrayList<Town> areas = new ArrayList<>();

    LazyArrayAdapter<County> cityAdapter;

    LazyArrayAdapter<Town> zoneAdapter;

    KProgressHUD hud;

    private int year, month, day;

    private User user;

    private HirerProfile profile;

    private boolean isNeedCreateProfile;

    public static void start(Context context, User user, boolean isNeedCreateProfile) {
        Intent starter = new Intent(context, ProfileSettingActivity.class);
        starter.putExtra(USER, user);
        starter.putExtra(IS_NEED_CREATE_PROFILE, isNeedCreateProfile);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModel();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_boss_profile_setting);
        initViews();
        setUpToolBar();
        getArea();

        binding.setViewModel(viewModel);
        binding.setPresenter(new Presenter());
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("個人資料");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private void getBundle() {
        if (getIntent().getExtras() != null) {
            user = (User) getIntent().getSerializableExtra(USER);
            isNeedCreateProfile = getIntent().getBooleanExtra(IS_NEED_CREATE_PROFILE, false);
            setupAdapters();
            if (user != null) {
                onUserDataLoaded(user);
            }

        }
    }

    private void initViews() {
        hud = KProgressHUD.create(this);
        this.year = Calendar.getInstance().get(Calendar.YEAR);
        this.month = Calendar.getInstance().get(Calendar.MONTH);
        this.day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_dropdown_item, gender);
        binding.gender.setAdapter(adapter);
        binding.gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                viewModel.gender.set(gender[i]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void getArea() {
        API.sharedService().getAllCounties(new QueriesBuilder().create()).enqueue(new Callback<List<County>>() {
            @Override
            public void onResponse(Call<List<County>> call, Response<List<County>> response) {
                cities.removeAll(cities);
                cities.addAll(response.body());
                getBundle();
            }

            @Override
            public void onFailure(Call<List<County>> call, Throwable t) {

            }
        });
    }

    private void setupAdapters() {

        cityAdapter = new LazyArrayAdapter<County>(activity, R.layout.support_simple_spinner_dropdown_item, cities) {

            @Override
            public int getCount() {
                return super.getCount() + 1;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                return getView(position, convertView, parent);
            }

            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = viewFromResource(LayoutInflater.from(getContext()), convertView, parent, getResource());
                TextView textView = view.findViewById(getFieldId());
                if (position == 0) {
                    textView.setText("請選擇");
                } else {
                    textView.setText(getItem(position - 1).getName());
                }
                return view;
            }
        };
        zoneAdapter = new LazyArrayAdapter<Town>(activity, R.layout.support_simple_spinner_dropdown_item, areas) {
            @Override
            public int getCount() {
                return super.getCount() + 1;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                return getView(position, convertView, parent);
            }

            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = viewFromResource(LayoutInflater.from(getContext()), convertView, parent, getResource());
                TextView textView = view.findViewById(getFieldId());
                if (position == 0) {
                    textView.setText("請選擇");
                } else {
                    textView.setText(getItem(position - 1).getName());
                }
                return view;
            }
        };

        binding.citySpinner.setAdapter(cityAdapter);
        binding.zoneSpinner.setAdapter(zoneAdapter);

        binding.citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position == 0) {
                    areas.clear();
                    ((ArrayAdapter) binding.zoneSpinner.getAdapter()).notifyDataSetChanged();
                    viewModel.area2.set(null);
                } else {

                    API.sharedService().getAllTowns(new QueriesBuilder()
                            .filter("parent_area__county_code", ((County) cityAdapter.getItem(position-1)).getCounty_code())
                            .pagination(1, QueriesBuilder.PAGE_SIZE_MAX)
                            .create()
                    ).enqueue(new Callback<PageNumberPagination<Town>>() {
                        @Override
                        public void onResponse(Call<PageNumberPagination<Town>> call, Response<PageNumberPagination<Town>> response) {
                            areas.removeAll(areas);
                            if (response.isSuccessful()) {
                                areas.addAll(response.body().getResults());
                            }
                            zoneAdapter.notifyDataSetChanged();
                            checkTownSelection();
                        }

                        @Override
                        public void onFailure(Call<PageNumberPagination<Town>> call, Throwable t) {
                            Logger.e(t.getLocalizedMessage());
                        }
                    });
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        binding.zoneSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (position == 0) {
                    viewModel.area2.set(null);
                } else {
                    viewModel.area2.set((zoneAdapter.getItem(position - 1)).getId());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

    }

    private void getImageFromAlbums(int requestCode) {
        try {
            if (!ImagePickerHelper.launchGooglePhotosPicker(activity, requestCode)) {
                Toast.makeText(activity, "請先安裝圖片選擇APP", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(activity, "請先安裝圖片選擇APP", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        getImageFromAlbums(requestCode);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }

    private boolean checkForm() {
        if (viewModel.nickName.get().isEmpty()) {
            Toast.makeText(activity, "請輸入暱稱", Toast.LENGTH_SHORT).show();
            return false;
        } else if (viewModel.firstName.get().isEmpty() || viewModel.lastName.get().isEmpty()) {
            Toast.makeText(activity, "請輸入真實姓名", Toast.LENGTH_SHORT).show();
            return false;
        } else if (viewModel.identify.get().isEmpty()) {
            Toast.makeText(activity, "請輸入身份證字號", Toast.LENGTH_SHORT).show();
            return false;
        } else if (viewModel.email.get().isEmpty()) {
            Toast.makeText(activity, "請輸入電子郵件", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!RegexHelper.isMail(viewModel.email.get())) {
            Toast.makeText(activity, "電子郵件格式輸入錯誤", Toast.LENGTH_SHORT).show();
            return false;
        } else if (viewModel.cellPhone.get().isEmpty()) {
            Toast.makeText(activity, "請輸入手機", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!RegexHelper.isPhone(viewModel.cellPhone.get())) {
            Toast.makeText(activity, "手機格式輸入錯誤", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_PHOTO_CODE) {
                if (data.getData() != null) {
                    try {
                        newHeadFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + UUID.randomUUID().toString()+".png");
                        ImagePickerHelper.getImageFromUri(activity, data, newHeadFile);
                        ImagePickerHelper.startCropImage(activity, newHeadFile, REQUEST_PHOTO_CROP);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else if (requestCode == REQUEST_PHOTO_CROP) {

                String path = newHeadFile.getPath();
                Bitmap bitmap = BitmapFactory.decodeFile(path);
                binding.image.setImageBitmap(bitmap);

            }
        }
    }

    private void onUserDataLoaded(User user) {

        if (user.getHirer_profile() != null && user.getHirer_profile().getHead_image() != null) {
            viewModel.headImage.set(user.getHirer_profile().getHead_image().toString());
        }

        viewModel.nickName.set((user.getHirer_profile() != null && user.getHirer_profile().getDisplay_name() != null) ? user.getHirer_profile().getDisplay_name() : "");
        viewModel.identify.set(user.getIdentity_id() != null ? user.getIdentity_id() : "");
        viewModel.email.set(user.getEmail() != null ? user.getEmail() : "");
        viewModel.phone.set(user.getTelephone() != null ? user.getTelephone() : "");
        viewModel.cellPhone.set(user.getCellphone() != null ? user.getCellphone() : "");
        viewModel.birthDay.set(user.getBirthday() != null ? user.getBirthday() : null);
        viewModel.gender.set(user.getGender() != null ? user.getGender().toString() : "");
        viewModel.firstName.set(user.getFirst_name() != null ? user.getFirst_name() : "");
        viewModel.lastName.set(user.getLast_name() != null ? user.getLast_name() : "");

        binding.gender.setSelection(viewModel.gender.get().equals("Male") ? 0 : 1);
        checkCountySelection();
        viewModel.address.set(user.getAddress() != null ? user.getAddress() : "");
    }

    public class ViewModel {

        public final ObservableField<String> headImage = new ObservableField<>("");

        public final ObservableField<String> nickName = new ObservableField<>("");

        public final ObservableField<String> firstName = new ObservableField<>("");

        public final ObservableField<String> lastName = new ObservableField<>("");

        public final ObservableField<String> gender = new ObservableField<>();

        public final ObservableField<String> identify = new ObservableField<>("");

        public final ObservableField<Date> birthDay = new ObservableField<>();

        public final ObservableField<String> email = new ObservableField<>("");

        public final ObservableField<String> phone = new ObservableField<>("");

        public final ObservableField<String> cellPhone = new ObservableField<>("");

        public final ObservableField<Integer> area2 = new ObservableField<>();

        public final ObservableField<String> address = new ObservableField<>("");
    }

    public class Presenter implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_set_stickers:
                    String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
                    if (!EasyPermissions.hasPermissions(activity, perms)) {
                        EasyPermissions.requestPermissions(activity, "上傳大頭貼需要權限", 1001, perms);
                    } else {
                        getImageFromAlbums(REQUEST_PHOTO_CODE);
                    }
                    break;
                case R.id.tv_birthday:
                    new DatePickerDialog(activity, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                            Calendar calendar = Calendar.getInstance();
                            calendar.set(year, month, day);
                            viewModel.birthDay.set(calendar.getTime());
                        }
                    }, ProfileSettingActivity.this.year, ProfileSettingActivity.this.month, ProfileSettingActivity.this.day).show();
                    break;
                case R.id.btn_confirm:
                    if (checkForm()) {
                        submit();
                    }
                    break;
            }

        }
    }

    private void checkCountySelection() {
        if (user.getArea2() != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        final List<County> cs = API.sharedService().getAllCounties(new QueriesBuilder().filter("sub_areas", user.getArea2()).create()).execute().body();
                        if (!cs.isEmpty()) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    for (int i = 0; i < cities.size(); i++) {
                                        if (cities.get(i).getId() == cs.get(0).getId()) {
                                            binding.citySpinner.setSelection(i + 1);
                                            break;
                                        }
                                    }
                                }
                            });
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }

    private void checkTownSelection() {
        if (user.getArea2() != null) {
            Town t = CollectionUtils.find(areas, new Predicate<Town>() {
                @Override
                public boolean evaluate(Town object) {
                    return object.getId() == user.getArea2();
                }
            });
            if (t != null) {
                for (int i = 0; i < areas.size(); i++) {
                    if (areas.get(i).getId() == t.getId()) {
                        binding.zoneSpinner.setSelection(i + 1);
                    }
                }
            }

        }
    }

    private void showSettingSuccessDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage("個人資料設定成功");
        builder.setCancelable(false);
        builder.setPositiveButton("確定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (Preference.getHirerToken(activity).isEmpty()) {
                    Preference.setHirerToken(activity, Preference.getTempToken(activity));
                }
                finish();
            }
        });
        builder.show();
    }

    private void submit() {

        profile = user.getHirer_profile();
        final User user = this.user;

        if (profile == null) {
            profile = new HirerProfile();
        }

        if (viewModel.nickName.get().isEmpty()) {
            Toast.makeText(activity, "請輸入暱稱", Toast.LENGTH_SHORT).show();
            return;
        }

        profile.setDisplay_name(viewModel.nickName.get());
        user.setLast_name(viewModel.lastName.get());
        user.setFirst_name(viewModel.firstName.get());
        user.setGender(viewModel.gender.get().equals(gender[0]) ? User.Gender.Male : User.Gender.Female);
        user.setIdentity_id(viewModel.identify.get());
        user.setTelephone(viewModel.phone.get());
        user.setCellphone(viewModel.cellPhone.get());
        user.setArea2(viewModel.area2.get());
        user.setAddress(viewModel.address.get());
        user.setBirthday(viewModel.birthDay.get());

        hud.show();

        Logger.d(API.shared().getGson().toJson(user));
        Logger.d(API.shared().getGson().toJson(profile));

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Response patchUser = API.sharedService().patchUser(API.sharedAuthorizationToken(), user).execute();
                    Response patchProfile = isNeedCreateProfile ?
                            API.sharedService().hirerCreateProfile(API.sharedAuthorizationToken(), profile).execute() :
                            API.sharedService().hirerPatchProfile(API.sharedAuthorizationToken(), profile).execute();
                    Response[] rs = {patchUser, patchProfile};

                    if (newHeadFile != null) {
                        final ArrayList<String> errMsg = new ArrayList<>();
                        Response r = API.sharedService().hirerPatchProfileHeadImage(API.sharedAuthorizationToken(), DRFHelper.ImageRequestBodyPart("head_image", newHeadFile)).execute();
                        if (!r.isSuccessful()) {
                            errMsg.add(API.shared().parseError(r).getHudMessage());
                        }
                    }

                    if (patchUser.isSuccessful() && patchProfile.isSuccessful()) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                EventBus.getDefault().post(new EditHirerProfileSuccessEvent());
                                showSettingSuccessDialog();
                                hud.dismiss();
                            }
                        });
                    } else {
                        final ArrayList<String> msgs = new ArrayList<>();
                        for (Response response : rs) {
                            if (!response.isSuccessful()) {
                                ApiError apiError = API.shared().parseError(response);
                                Logger.e(apiError.getHudMessage());
                                msgs.add(apiError.getHudMessage());
                            }
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hud.dismiss();
                                Toast.makeText(activity, TextUtils.join("\n", msgs), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                } catch (IOException e) {

                    e.printStackTrace();
                }

            }
        }).start();

    }
}
