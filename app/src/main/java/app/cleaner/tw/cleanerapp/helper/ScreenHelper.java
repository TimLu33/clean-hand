package app.cleaner.tw.cleanerapp.helper;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;

public class ScreenHelper {

    public static DisplayMetrics metrics = new DisplayMetrics();

    public static int getWidth(Activity activity) {

        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        return metrics.widthPixels;

    }

        public static int getHeight(Activity activity) {

        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        return metrics.heightPixels;

    }

    public static int Dp2Px(Context context, float dp) {

        float scale = context.getResources().getDisplayMetrics().density;

        return (int) (dp * scale + 0.5f);

    }

}
