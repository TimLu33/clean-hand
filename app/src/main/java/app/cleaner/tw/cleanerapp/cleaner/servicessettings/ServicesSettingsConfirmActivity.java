package app.cleaner.tw.cleanerapp.cleaner.servicessettings;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Transformer;
import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.geography.Town;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.CleanerProfile;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.User;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceType;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.cleaner.event.ServiceSettingFinishEvent;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityCleanerServicesConfirmBinding;
import app.cleaner.tw.cleanerapp.mock.MockServicesSettingsModel;
import retrofit2.Call;
import retrofit2.Response;

public class ServicesSettingsConfirmActivity extends BaseActivity {

    private static final String USER = "user";
    private static final String CLEANER_PROFILE = "cleanerProfile";

    private ActivityCleanerServicesConfirmBinding binding;

    private User user;
    private CleanerProfile profile;
    private ViewModel viewModel;

    public static void start(Context context, User user, CleanerProfile profile) {
        Intent starter = new Intent(context, ServicesSettingsConfirmActivity.class);
        starter.putExtra(USER, user);
        starter.putExtra(CLEANER_PROFILE, profile);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBundle();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cleaner_services_confirm);
        setUpToolBar();
        viewModel = new ViewModel(user, profile);
        binding.setViewModel(viewModel);
        binding.setPresenter(new Presenter());

        if (profile.getHead_image() != null) {
            Glide.with(this)
                    .load(profile.getHead_image())
                    .apply(new RequestOptions().centerCrop())
                    .into(binding.imgCleaner);
        }

    }

    private void getBundle() {
        user = (User) getIntent().getSerializableExtra(USER);
        profile = (CleanerProfile) getIntent().getSerializableExtra(CLEANER_PROFILE);
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("服務設定");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    public class ViewModel {

        public final ObservableField<Integer> rating = new ObservableField<>();
        public final ObservableField<String> nickName = new ObservableField<>();

        public final ObservableField<String> workArea = new ObservableField<>();
        public final ObservableField<String> workTime = new ObservableField<>();
        public final ObservableField<String> serviceType = new ObservableField<>();
        public final ObservableField<String> workSalary = new ObservableField<>();
        public final ObservableField<String> servicesNumber = new ObservableField<>();
        public final ObservableField<String> others = new ObservableField<>();

        public ViewModel(User user, CleanerProfile profile) {
            nickName.set(profile.getDisplay_name() != null ? profile.getDisplay_name() : "清潔手暱稱");
            rating.set(Math.round(profile.getAvg_rating()));

            Collection<String> areas = CollectionUtils.collect(profile.getCustom_service_areas(), new Transformer<Town, String>() {
                @Override
                public String transform(Town input) {
                    return String.format(Locale.getDefault(), "%s-%s", input.getCounty().getName(), input.getName());
                }
            });

            if (profile.isServe_all_areas()) {
                workArea.set("全區");
            } else {
                workArea.set(areas.isEmpty() ? "" : TextUtils.join(", ", areas));
            }

            if (profile.isServe_all_times()) {
                workTime.set("全時段");
            } else {
                Collection<String> times = CollectionUtils.collect(profile.getCustom_service_times(), new Transformer<CleanerProfile.CustomServiceTime, String>() {
                    @Override
                    public String transform(CleanerProfile.CustomServiceTime input) {
                        return String.format(
                                Locale.getDefault(),
                                "%02d:%02d ~ %02d:%02d",
                                input.getStart().getHours(), input.getStart().getMinutes(),
                                input.getEnd().getHours(), input.getEnd().getMinutes()
                        );
                    }
                });
                workTime.set(times.isEmpty() ? "" : TextUtils.join(", ", times));
            }

            Collection<String> serviceTypes = CollectionUtils.collect(profile.getProvide_service_types(), new Transformer<ServiceType, String>() {
                @Override
                public String transform(ServiceType input) {
                    return input.getName();
                }
            });
            serviceType.set(serviceTypes.isEmpty() ? "" : TextUtils.join(", ", serviceTypes));

            workSalary.set(String.valueOf(profile.getExpected_salary()));
            servicesNumber.set(user.getCellphone());

            others.set(profile.getOthers());
        }
    }

    public class Presenter implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_confirm:
                    submit();
                    break;
            }
        }
    }

    private void submit() {
        final KProgressHUD hud = KProgressHUD.create(activity).show();
        new Thread(new Runnable() {
            @Override
            public void run() {

                final ArrayList<String> errs = new ArrayList();

                try {
                    API.sharedService().patchUser(API.sharedAuthorizationToken(), user).execute();
                    Response<CleanerProfile> pr = API.sharedService().cleanerPatchProfile(API.sharedAuthorizationToken(), profile).execute();

                    if (pr.isSuccessful()) {
                        final CleanerProfile p = pr.body();
                        // compare serviceType, times, areas
                        Collection<Town> aIntersection = CollectionUtils.intersection(p.getCustom_service_areas(), profile.getCustom_service_areas());
                        Collection<Town> a2Add = CollectionUtils.subtract(profile.getCustom_service_areas(), aIntersection);
                        Collection<Town> a2Remove = CollectionUtils.subtract(p.getCustom_service_areas(), aIntersection);

                        Collection<ServiceType> sIntersection = CollectionUtils.intersection(p.getProvide_service_types(), profile.getProvide_service_types());
                        Collection<ServiceType> s2Add = CollectionUtils.subtract(profile.getProvide_service_types(), sIntersection);
                        Collection<ServiceType> s2Remove = CollectionUtils.subtract(p.getProvide_service_types(), sIntersection);

                        Collection<CleanerProfile.CustomServiceTime> tIntersection = CollectionUtils.intersection(p.getCustom_service_times(), profile.getCustom_service_times());
                        Collection<CleanerProfile.CustomServiceTime> t2Add = CollectionUtils.subtract(profile.getCustom_service_times(), tIntersection);
                        Collection<CleanerProfile.CustomServiceTime> t2Remove = CollectionUtils.subtract(p.getCustom_service_times(), tIntersection);

                        ArrayList<Call<?>> calls = new ArrayList<Call<?>>();

                        for (Town t: a2Add) {
                            calls.add(API.sharedService().cleanerCollectCleanerServeArea(API.sharedAuthorizationToken(), t.getTown_code()));
                        }
                        for (Town t: a2Remove) {
                            calls.add(API.sharedService().cleanerRemoveCleanerServeArea(API.sharedAuthorizationToken(), t.getTown_code()));
                        }

                        for (ServiceType t: s2Add) {
                            calls.add(API.sharedService().cleanerCollectCleanerServeType(API.sharedAuthorizationToken(), t.getId()));
                        }
                        for (ServiceType t: s2Remove) {
                            calls.add(API.sharedService().cleanerRemoveCleanerServeType(API.sharedAuthorizationToken(), t.getId()));
                        }

                        for (CleanerProfile.CustomServiceTime t: t2Add) {
                            calls.add(API.sharedService().cleanerCreateCleanerServeTime(API.sharedAuthorizationToken(), t));
                        }
                        for (CleanerProfile.CustomServiceTime t: t2Remove) {
                            calls.add(API.sharedService().cleanerDeleteCleanerServeTime(API.sharedAuthorizationToken(), t.getId()));
                        }

                        for (Call<?> call: calls) {
                            Response<?> response = call.execute();
                            if (response.isSuccessful()) {
                                Logger.d(response.message());
                            } else {
                                errs.add(API.shared().parseError(response).getHudMessage());
                            }
                        }
                    } else {
                        errs.add(API.shared().parseError(pr).getHudMessage());
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    errs.add(e.getLocalizedMessage());
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hud.dismiss();
                        if (errs.isEmpty()) {
                            showSettingSuccessDialog(new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                    EventBus.getDefault().post(new ServiceSettingFinishEvent());
                                }
                            });
                        } else {
                            Logger.e(TextUtils.join("\n ",errs));
                            Toast.makeText(activity, TextUtils.join("\n ",errs), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }).start();
    }

    private void showSettingSuccessDialog(DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setMessage("服務設定成功");
        builder.setPositiveButton("確定", listener);
        builder.show();
    }
}
