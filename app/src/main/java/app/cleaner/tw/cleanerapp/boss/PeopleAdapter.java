package app.cleaner.tw.cleanerapp.boss;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceMatching;
import app.cleaner.tw.cleanerapp.mock.MockPeopleModel;
import de.hdodenhof.circleimageview.CircleImageView;

public class PeopleAdapter extends RecyclerView.Adapter<PeopleAdapter.ViewHolder> implements View.OnClickListener, View.OnLongClickListener {

    public interface ItemClickListener {
        void onItemClickListener(View view, int position);
    }

    public interface ItemLongClickListener {
        void onItemLongClickListener(View view, int position);
    }

    private ItemClickListener listener;
    private ItemLongClickListener longClicklistener;
    private List<ServiceMatching> peopleList;

    public PeopleAdapter(List<ServiceMatching> peopleList) {
        this.peopleList = peopleList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.people_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        holder.itemView.setOnClickListener(this);
        holder.itemView.setOnLongClickListener(this);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setTag(position);
        Glide.with(holder.imgPeople.getContext())
                .load(peopleList.get(position).getCleaner_head_image_read_only() != null ? peopleList.get(position).getCleaner_head_image_read_only(): null)
                .apply(new RequestOptions().centerCrop().error(R.drawable.ic_people))
                .into(holder.imgPeople);
        holder.name.setText(peopleList.get(position).getCleaner_name_read_only());
    }

    @Override
    public int getItemCount() {
        return peopleList.size();
    }

    public void setOnItemClickListener(ItemClickListener listener) {
        this.listener = listener;
    }

    public void setOnItemLongClicklistener(ItemLongClickListener listener) {
        this.longClicklistener = listener;
    }

    @Override
    public void onClick(View view) {
        if (listener != null) {
            listener.onItemClickListener(view, (int) view.getTag());
        }
    }

    @Override
    public boolean onLongClick(View view) {
        if (longClicklistener != null) {
            longClicklistener.onItemLongClickListener(view, (int) view.getTag());
        }
        return true;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView imgPeople;
        TextView name;

        public ViewHolder(View itemView) {
            super(itemView);
            imgPeople = itemView.findViewById(R.id.img_people);
            name = itemView.findViewById(R.id.tv_name);
        }
    }
}
