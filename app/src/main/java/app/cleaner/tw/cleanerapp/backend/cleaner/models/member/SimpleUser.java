package app.cleaner.tw.cleanerapp.backend.cleaner.models.member;

import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;

import org.joda.time.LocalDate;
import org.joda.time.Years;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import app.cleaner.tw.cleanerapp.backend.cleaner.models.geography.LocationBean;

/**
 * Created by ARTHUR on 2017/10/18.
 */

public class SimpleUser implements Serializable {

    @Expose
    private int id;

    @Expose
    private String username;

    @Expose
    private String email;

    @Expose
    private String first_name;

    @Expose
    private String last_name;

    @Expose
    private User.Gender gender;

    @Expose
    private Date birthday;

    @Expose
    private String cellphone;

    @Expose
    private String telephone;

    @Expose
    private boolean is_cellphone_validated;

    @Expose
    private int area2;

    @Expose
    private String address;

    @Expose
    private CleanerProfile cleaner_profile;

    @Expose
    private HirerProfile hirer_profile;

    @Expose (serialize = false)
    private LastLocationBean last_location;

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public User.Gender getGender() {
        return gender;
    }

    public Date getBirthday() {
        return birthday;
    }

    public String getCellphone() {
        return cellphone;
    }

    public String getTelephone() {
        return telephone;
    }

    public boolean is_cellphone_validated() {
        return is_cellphone_validated;
    }

    public int getArea2() {
        return area2;
    }

    public String getAddress() {
        return address;
    }

    public CleanerProfile getCleaner_profile() {
        return cleaner_profile;
    }

    public HirerProfile getHirer_profile() {
        return hirer_profile;
    }

    public LastLocationBean getLast_location() {
        return last_location;
    }

    public static class LastLocationBean implements Serializable {
        @Expose
        private LocationBean location;

        public LocationBean getLocation() {
            return location;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SimpleUser)
            return obj.hashCode() == hashCode();
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        if (getId() != 0)
            return getId();
        return super.hashCode();
    }

    @Nullable
    public Integer getAge() {
        if (getBirthday() == null)
            return null;
        LocalDate birthdate = LocalDate.fromDateFields(getBirthday());
        LocalDate now = new LocalDate();
        Years years = Years.yearsBetween(birthdate, now);
        return years.getYears();
    }
}
