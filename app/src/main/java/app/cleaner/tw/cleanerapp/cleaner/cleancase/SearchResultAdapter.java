package app.cleaner.tw.cleanerapp.cleaner.cleancase;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;
import app.cleaner.tw.cleanerapp.databinding.SearchResultItemBinding;
import app.cleaner.tw.cleanerapp.mock.MockCaseModel;
import app.cleaner.tw.cleanerapp.mock.MockNewsModel;

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.BindingHolder> implements View.OnClickListener {

    public interface ItemClickListener {
        void onItemClickListener(View view, int position);
    }

    private ItemClickListener listener;
    private List<CleanService> caseList = new ArrayList<>();

    public SearchResultAdapter(List<CleanService> newsList) {
        this.caseList = newsList;
    }

    @Override
    public SearchResultAdapter.BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        SearchResultItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.search_result_item, parent, false);
        binding.getRoot().setOnClickListener(this);
        return new BindingHolder(binding);
    }

    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {
        holder.binding.getRoot().setTag(position);
        SearchResultItemViewModel viewModel = new SearchResultItemViewModel(caseList.get(position));
        holder.bind(viewModel);
    }

    @Override
    public int getItemCount() {
        return caseList.size();
    }

    public void setOnItemClickListener(ItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if (listener != null) {
            listener.onItemClickListener(view, (int) view.getTag());
        }
    }

    public class BindingHolder extends RecyclerView.ViewHolder {

        private SearchResultItemBinding binding;

        public BindingHolder(SearchResultItemBinding binding) {
            super(binding.rootView);
            this.binding = binding;
        }

        public void bind(SearchResultItemViewModel viewModel) {
            binding.setViewModel(viewModel);
            binding.executePendingBindings();
        }

    }
}
