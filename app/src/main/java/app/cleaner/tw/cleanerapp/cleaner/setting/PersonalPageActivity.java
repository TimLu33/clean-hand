package app.cleaner.tw.cleanerapp.cleaner.setting;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableField;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bigkoo.convenientbanner.ConvenientBanner;
import com.bigkoo.convenientbanner.holder.CBViewHolderCreator;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.CleanerProfile;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.User;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceType;
import app.cleaner.tw.cleanerapp.backend.drfapi.helper.DRFHelper;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.ApiError;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.common.ConvenientBannerModel;
import app.cleaner.tw.cleanerapp.common.ImageConfig;
import app.cleaner.tw.cleanerapp.common.Preference;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityCleanerPersonalPageBinding;
import app.cleaner.tw.cleanerapp.helper.ImagePickerHelper;
import app.cleaner.tw.cleanerapp.helper.ImageHolderView;
import app.cleaner.tw.cleanerapp.helper.ScreenHelper;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;

public class PersonalPageActivity extends BaseActivity implements EasyPermissions.PermissionCallbacks {

    private static final String USER = "user";
    private static final String PROFILE = "profile";
    private static final String HEAD_IMAGE = "newHeadImage";
    private static final String IS_CREATE_PROFILE = "isCreateProfile";

    private final int REQUEST_PHOTO_CODE = 100;
    private final int REQUEST_PHOTO_CROP = 101;
    //    protected File tempFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "temp.png");
    private ActivityCleanerPersonalPageBinding binding;
    private ViewModel viewModel;

    // TODO: fill me
    protected File coverPhoto = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + UUID.randomUUID().toString() + ".png");
    List<ConvenientBannerModel> coverImages = new ArrayList<>();
    private ArrayList<File> newCoverPhotos = new ArrayList<>();

    // bundle
    private User user;
    private CleanerProfile profile;
    private File newHeadFile;
    private boolean isCreateProfile;


    public static void start(Context context, User user, CleanerProfile profile, @Nullable File newHeadFile, boolean isCreateProfile) {
        Intent starter = new Intent(context, PersonalPageActivity.class);
        starter.putExtra(USER, user);
        starter.putExtra(PROFILE, profile);
        if (newHeadFile != null)
            starter.putExtra(HEAD_IMAGE, newHeadFile);
        starter.putExtra(IS_CREATE_PROFILE, isCreateProfile);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBundle();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cleaner_personal_page);
        setUpToolBar();
        initViews();
        viewModel = new ViewModel(profile);
        if (newHeadFile != null)
            viewModel.headImage.set(newHeadFile.getPath());
        binding.setViewModel(viewModel);
        binding.setPresenter(new Presenter());
    }

    private void getBundle() {
        if (getIntent().getExtras() != null) {
            user = (User) getIntent().getSerializableExtra(USER);
            profile = (CleanerProfile) getIntent().getSerializableExtra(PROFILE);
            newHeadFile = (File) getIntent().getSerializableExtra(HEAD_IMAGE);
            isCreateProfile = getIntent().getBooleanExtra(IS_CREATE_PROFILE, false);
        }
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("個人主頁");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private void initViews() {
        float height = (float) ScreenHelper.getWidth(activity) / (float) ImageConfig.coverWidth * (float) ImageConfig.coverHeight;
        binding.coverImages.setLayoutParams(new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) height));
        if (profile != null && profile.getCover_photos() != null) {
            for (CleanerProfile.CoverPhoto photo : profile.getCover_photos()) {
                ConvenientBannerModel image = new ConvenientBannerModel(photo.getImage().toString());
                coverImages.add(image);
            }
            binding.coverImages.setPages(new CBViewHolderCreator<ImageHolderView>() {
                @Override
                public ImageHolderView createHolder() {
                    return new ImageHolderView();
                }
            }, coverImages);
            if (coverImages.size() > 1) {
                binding.coverImages.setPageIndicator(new int[]{R.drawable.banner_dian_blur, R.drawable.banner_dian_focus})
                        .setPageIndicatorAlign(ConvenientBanner.PageIndicatorAlign.CENTER_HORIZONTAL);
            } else {
                binding.coverImages.setCanLoop(false);
            }
        }
    }

    private void submit() {
        profile.setRecommendation(viewModel.recommend.get());
        profile.setIntroduction(viewModel.intro.get());

        final KProgressHUD hud = KProgressHUD.create(activity).show();

        new Thread(new Runnable() {
            @Override
            public void run() {
                final ArrayList<String> errMsgs = new ArrayList<String>();
                try {
                    Response response = API.sharedService().patchUser(API.sharedAuthorizationToken(), user).execute();
                    Response response2 = isCreateProfile ? API.sharedService().cleanerCreateProfile(API.sharedAuthorizationToken(), profile).execute() : API.sharedService().cleanerPatchProfile(API.sharedAuthorizationToken(), (CleanerProfile) profile).execute();
                    Logger.d(isCreateProfile);
                    Response[] rs = new Response[]{response, response2};

                    for (Response r : rs) {
                        if (!r.isSuccessful()) {
                            ApiError apiError = API.shared().parseError(r);
                            errMsgs.add(apiError.getHudMessage());
                            Logger.e(apiError.getHudMessage());
                        }
                    }

                    if (newHeadFile != null) {
                        Response r = API.sharedService().cleanerPatchProfileHeadImage(API.sharedAuthorizationToken(), DRFHelper.ImageRequestBodyPart("head_image", newHeadFile)).execute();
                        if (!r.isSuccessful()) {
                            errMsgs.add(API.shared().parseError(r).getHudMessage());
                        }
                    }

                    if (!newCoverPhotos.isEmpty()) {
                        List<CleanerProfile.CoverPhoto> coverPhotos = API.sharedService().cleanerGetCoverPhotos(API.sharedAuthorizationToken()).execute().body();
                        if (coverPhotos.size() + newCoverPhotos.size() > 3) {
                            errMsgs.add(String.format(Locale.getDefault(), "您已經上傳了 %d 張生活照，僅能再上傳 %d 張", coverPhotos.size(), 3 - coverPhotos.size()));
                        } else {
                            for (File f : newCoverPhotos) {
                                Response r = API.sharedService().cleanerCreateCoverPhoto(API.sharedAuthorizationToken(), DRFHelper.ImageRequestBodyPart("image", f)).execute();
                                if (!r.isSuccessful()) {
                                    errMsgs.add(API.shared().parseError(r).getHudMessage());
                                }
                            }
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    Logger.e(e.getLocalizedMessage());
                    errMsgs.add(e.getLocalizedMessage());
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hud.dismiss();
                        if (errMsgs.isEmpty()) {
                            showConfirmDialog(new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    if (Preference.getCleanerToken(activity).isEmpty()) {
                                        Preference.setCleanerToken(activity, Preference.getTempToken(activity));
                                    }
                                    EventBus.getDefault().post(new EditSuccessEvent());
                                    finish();
                                }
                            });
                        } else {
                            Toast.makeText(activity, TextUtils.join("\n", errMsgs), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }).start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_PHOTO_CODE) {
                if (data.getData() != null) {
                    try {
                        ImagePickerHelper.getImageFromUri(activity, data, coverPhoto);
                        ImagePickerHelper.startCropImage(activity, coverPhoto, REQUEST_PHOTO_CROP, (float) 1.77777778);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else if (requestCode == REQUEST_PHOTO_CROP) {

                String path = coverPhoto.getPath();
                Bitmap bitmap = BitmapFactory.decodeFile(path);
                if (coverImages.size() == 3) {
                    Toast.makeText(activity, "生活照數量已達上限", Toast.LENGTH_SHORT).show();
                    return;
                }
                ConvenientBannerModel image = new ConvenientBannerModel(bitmap);
                coverImages.add(image);
                binding.coverImages.setPages(new CBViewHolderCreator<ImageHolderView>() {
                    @Override
                    public ImageHolderView createHolder() {
                        return new ImageHolderView();
                    }
                }, coverImages);
                newCoverPhotos.add(coverPhoto);
            }
        }
    }

    private void getImageFromAlbums(int requestCode) {
        try {
            if (!ImagePickerHelper.launchGooglePhotosPicker(activity, requestCode)) {
                Toast.makeText(activity, "請先安裝圖片選擇APP", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(activity, "請先安裝圖片選擇APP", Toast.LENGTH_SHORT).show();
        }
    }

    private void showConfirmDialog(DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setMessage("編輯成功");
        builder.setPositiveButton("確定", listener);
        builder.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        getImageFromAlbums(REQUEST_PHOTO_CODE);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }

    public class ViewModel {
        public final ObservableField<String> coverPhoto = new ObservableField<>("");
        public final ObservableField<String> headImage = new ObservableField<>("");
        public final ObservableField<String> recommend = new ObservableField<>("");
        public final ObservableField<String> intro = new ObservableField<>("");
        public final ObservableField<Integer> rating = new ObservableField<>(0);
        public final ObservableField<Integer> salary = new ObservableField<>();
        public final ObservableField<String> serviceType = new ObservableField<>("");


        public ViewModel(CleanerProfile profile) {
            if (profile != null) {
                if (profile.getCover_photos() != null && !profile.getCover_photos().isEmpty()) {
                    coverPhoto.set(profile.getCover_photos().get(0).getImage().toString());
                }

                if (profile.getHead_image() != null) {
                    headImage.set(profile.getHead_image().toString());
                }

                rating.set((int) profile.getAvg_rating());
                salary.set(profile.getExpected_salary());

                recommend.set(profile.getRecommendation() != null ? profile.getRecommendation() : "");
                intro.set(profile.getIntroduction() != null ? profile.getIntroduction() : "");

                if (profile.getProvide_service_types() != null) {
                    StringBuilder s = new StringBuilder();
                    for (ServiceType type : profile.getProvide_service_types()) {
                        s.append(type.getName()).append("、");
                    }
                    serviceType.set(s.toString());
                }
            }
        }
    }

    public class Presenter implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_upload_cover_photo:
                    String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
                    if (!EasyPermissions.hasPermissions(activity, perms)) {
                        EasyPermissions.requestPermissions(activity, "上傳生活照需要權限", 1001, perms);
                    } else {
                        getImageFromAlbums(REQUEST_PHOTO_CODE);
                    }
                    break;
                case R.id.btn_confirm:
                    submit();
                    break;
            }

        }
    }
}
