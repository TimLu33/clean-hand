package app.cleaner.tw.cleanerapp.backend.cleaner.models.member;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ARTHUR on 2017/10/19.
 */

public class AuthTokenBean {
    @Expose
    @SerializedName("key")
    private String token;

    public String getToken() {
        return token;
    }
}
