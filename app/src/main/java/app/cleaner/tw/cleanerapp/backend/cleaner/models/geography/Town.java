package app.cleaner.tw.cleanerapp.backend.cleaner.models.geography;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by ARTHUR on 2017/10/17.
 */

public class Town implements Serializable {
    /**
     * id : 198
     * town_code : 10017020
     * name : 七堵區
     * name_eng : Qidu District
     * county : {"id":38,"county_code":"10017","name":"基隆市","name_eng":"Keelung City"}
     */

    @Expose
    private int id;

    @Expose
    private String town_code;

    @Expose
    private String name;

    @Expose
    private String name_eng;

    @Expose
    private County county;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTown_code() {
        return town_code;
    }

    public void setTown_code(String town_code) {
        this.town_code = town_code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName_eng() {
        return name_eng;
    }

    public void setName_eng(String name_eng) {
        this.name_eng = name_eng;
    }

    public County getCounty() {
        return county;
    }

    public void setCounty(County county) {
        this.county = county;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Town)
            return ((Town) obj).getId() == getId();
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        if (getId() != 0)
            return getId();
        return super.hashCode();
    }

}
