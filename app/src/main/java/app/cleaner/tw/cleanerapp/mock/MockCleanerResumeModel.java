package app.cleaner.tw.cleanerapp.mock;

import android.os.Parcel;
import android.os.Parcelable;

public class MockCleanerResumeModel implements Parcelable {

    private String address;
    private String time;
    private String boss;
    private int caseStatus;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getBoss() {
        return boss;
    }

    public void setBoss(String boss) {
        this.boss = boss;
    }

    public int getCaseStatus() {
        return caseStatus;
    }

    public void setCaseStatus(int caseStatus) {
        this.caseStatus = caseStatus;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.address);
        dest.writeString(this.time);
        dest.writeString(this.boss);
        dest.writeInt(this.caseStatus);
    }

    public MockCleanerResumeModel() {
    }

    protected MockCleanerResumeModel(Parcel in) {
        this.address = in.readString();
        this.time = in.readString();
        this.boss = in.readString();
        this.caseStatus = in.readInt();
    }

    public static final Parcelable.Creator<MockCleanerResumeModel> CREATOR = new Parcelable.Creator<MockCleanerResumeModel>() {
        @Override
        public MockCleanerResumeModel createFromParcel(Parcel source) {
            return new MockCleanerResumeModel(source);
        }

        @Override
        public MockCleanerResumeModel[] newArray(int size) {
            return new MockCleanerResumeModel[size];
        }
    };
}
