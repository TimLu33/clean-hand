package app.cleaner.tw.cleanerapp.boss.search;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.SimpleUser;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceMatching;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceQA;
import app.cleaner.tw.cleanerapp.backend.drfapi.builder.QueriesBuilder;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.PageNumberPagination;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.boss.CaseInfoActivity;
import app.cleaner.tw.cleanerapp.boss.search.event.OnDealSuccessEvent;
import app.cleaner.tw.cleanerapp.common.DividerItemDecoration;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityBossChatBinding;
import app.cleaner.tw.cleanerapp.helper.GSONUtils;
import app.cleaner.tw.cleanerapp.helper.TimeUtils;
import app.cleaner.tw.cleanerapp.notification.RefreshNotificationEvent;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BossChatActivity extends BaseActivity {

    enum Action {
        Invite,
        Candidate
    }

    private static final String CLEANER_USER = "cleaner";

    // Bundle
    private SimpleUser cleaner;

    private ActivityBossChatBinding binding;
    private ViewModel viewModel;

    // Timer
    Timer chatTimer = null;

    public static void start(Context context, SimpleUser cleaner) {
        Intent starter = new Intent(context, BossChatActivity.class);
        starter.putExtra(CLEANER_USER, cleaner);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBundle();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_boss_chat);
        EventBus.getDefault().register(this);
        setUpToolBar();
        viewModel = new ViewModel(cleaner);
        binding.setViewModel(viewModel);
        binding.setPresenter(new Presenter());
        initViews();
    }

    private void startTimer() {
        chatTimer = new Timer("chatTimer");
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                loadChats();
            }
        };
        chatTimer.schedule(task, 0, 3000);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (chatTimer != null)
            chatTimer.cancel();
        startTimer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (chatTimer != null)
            chatTimer.cancel();
    }

    private void getBundle() {
        if (getIntent().getExtras() != null) {
            cleaner = (SimpleUser) getIntent().getSerializableExtra(CLEANER_USER);
        }
    }

    private void loadChats() {
        QueriesBuilder queriesBuilder = new QueriesBuilder()
                .filter("cleaner", viewModel.cleaner.get().getId())
                .order("created_datetime");
        API.sharedService().hirerGetOwnQas(API.sharedAuthorizationToken(), queriesBuilder.create()).enqueue(new Callback<List<ServiceQA>>() {
            @Override
            public void onResponse(Call<List<ServiceQA>> call, Response<List<ServiceQA>> response) {
                if (response.isSuccessful()) {
                    Logger.d(GSONUtils.toJson(response.body()));
                    viewModel.chats.clear();
                    viewModel.chats.addAll(response.body());
                    (binding.recyclerView.getAdapter()).notifyDataSetChanged();
                } else {
                    Logger.e(API.shared().parseError(response).getHudMessage());
                }
            }

            @Override
            public void onFailure(Call<List<ServiceQA>> call, Throwable t) {
                Logger.e(t.getLocalizedMessage());
            }
        });
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("問與答");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private void initViews() {

        LinearLayoutManager lm = new LinearLayoutManager(this);
        lm.setOrientation(LinearLayoutManager.VERTICAL);
        lm.setStackFromEnd(true);

        ChatAdapter adapter = new ChatAdapter(viewModel.chats);
        binding.recyclerView.getItemAnimator().setAddDuration(0);
        binding.recyclerView.addItemDecoration(new DividerItemDecoration(this));
        binding.recyclerView.setLayoutManager(lm);
        binding.recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        binding.recyclerView.scrollToPosition(viewModel.chats.size() -1);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDealSuccessEvent(OnDealSuccessEvent event) {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void showCaseListDialog(final Action action) {
        final KProgressHUD hud = KProgressHUD.create(activity).show();
        QueriesBuilder queriesBuilder = new QueriesBuilder()
                .filter("fee_payment__paid", true)
                .pagination(1, 1000)
                .order("-created_datetime");
        if (action == Action.Candidate)
            queriesBuilder.filter("service_matching__matching_state", "IV")
                    .filter("service_matching__cleaner", viewModel.cleaner.get().getId());

        API.sharedService().hirerGetOwnServices(API.sharedAuthorizationToken(), queriesBuilder.create()).enqueue(new Callback<PageNumberPagination<CleanService>>() {
            @Override
            public void onResponse(Call<PageNumberPagination<CleanService>> call, Response<PageNumberPagination<CleanService>> response) {
                hud.dismiss();
                if (response.isSuccessful()) {
                    AlertDialog.Builder builderSingle = new AlertDialog.Builder(BossChatActivity.this);
                    builderSingle.setTitle("請選擇案件");

                    final ArrayList<CleanService> services = new ArrayList<CleanService>(response.body().getResults());
                    final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(BossChatActivity.this, android.R.layout.select_dialog_singlechoice);
                    for (CleanService cleanService: services)
                        arrayAdapter.add(String.format(
                                Locale.getDefault(),
                                "%s %s",
                                TimeUtils.getTime(cleanService.getTime_range_start().getTime(), TimeUtils.DATE_FORMAT_DATE),
                                cleanService.getAddress()
                        ));

                    builderSingle.setNegativeButton("取消", null);

                    builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            CleanService service = services.get(which);
                            switch (action) {
                                case Invite:
                                    invite(service);
                                    break;
                                case Candidate:
                                    candidate(service);
                                    break;
                            }
                        }
                    });
                    builderSingle.show();
                } else {
                    Logger.e(API.shared().parseError(response).getHudMessage());
                }
            }

            @Override
            public void onFailure(Call<PageNumberPagination<CleanService>> call, Throwable t) {
                hud.dismiss();
                Logger.e(t.getLocalizedMessage());
            }
        });
    }

    private void showSuccessDialog(Action action, final CleanService service) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(BossChatActivity.this);
        if (action == Action.Invite)
            builder.setMessage("邀請提案成功");
        else
            builder.setMessage("設為候選成功");
        builder.setCancelable(false);
        builder.setPositiveButton("確定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                dialog.dismiss();
                binding.btnInvite.setEnabled(false);
                CaseInfoActivity.start(activity, service);
            }
        });
        builder.show();
    }

    private void invite(final CleanService service) {
        final KProgressHUD hud = KProgressHUD.create(activity).show();
        API.sharedService().hirerInviteService(API.sharedAuthorizationToken(), service.getId(), viewModel.cleaner.get().getId()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                hud.dismiss();
                if (response.isSuccessful()) {
                    showSuccessDialog(Action.Invite, service);
                } else {
                    Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                    Logger.e(API.shared().parseError(response).getHudMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                hud.dismiss();
                Logger.e(t.getLocalizedMessage());
            }
        });
    }

    private void candidate(final CleanService service) {

        final ArrayList<ServiceMatching> matchings = new ArrayList<>(service.getHirer_matching_set());
        CollectionUtils.filter(matchings, new Predicate<ServiceMatching>() {
            @Override
            public boolean evaluate(ServiceMatching object) {
                if (object.getCleaner() == viewModel.cleaner.get().getId() && object.getMatching_state() == ServiceMatching.MatchingState.Proposal)
                    return true;
                return false;
            }
        });

        if (matchings.isEmpty()) {
            Toast.makeText(activity, "此案件無法候選此清潔手", Toast.LENGTH_SHORT).show();
            return;
        }

        ServiceMatching serviceMatching = matchings.get(0);
        final KProgressHUD hud = KProgressHUD.create(activity).show();
        API.sharedService().hirerCandidateMatching(API.sharedAuthorizationToken(), serviceMatching.getId()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                hud.dismiss();
                if (response.isSuccessful()) {
                    showSuccessDialog(Action.Invite, service);
                } else {
                    Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                    Logger.e(API.shared().parseError(response).getHudMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                hud.dismiss();
                Logger.e(t.getLocalizedMessage());
            }
        });
    }

    public class ViewModel {
        public ObservableField<SimpleUser> cleaner = new ObservableField<>();

        public List<ServiceQA> chats = new ArrayList<>();
        public final ObservableField<String> textMsg = new ObservableField<>("");

        public ViewModel(SimpleUser cleaner) {
            this.cleaner.set(cleaner);
        }
    }

    public class Presenter implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_candidate:
                    showCaseListDialog(Action.Candidate);
                    break;
                case R.id.btn_invite:
                    showCaseListDialog(Action.Invite);
//                    showInviteSuccessDialog();
//                    DealSuccessActivity.start(activity, profile);
                    break;
                case R.id.btn_send:
                    if (!viewModel.textMsg.get().isEmpty()) {
                        Map<String, Object> queries = new HashMap<>();
                        queries.put("cleaner", viewModel.cleaner.get().getId());
                        queries.put("content", viewModel.textMsg.get());
                        API.sharedService().hirerCreateQa(API.sharedAuthorizationToken(), queries).enqueue(new Callback<ServiceQA>() {
                            @Override
                            public void onResponse(Call<ServiceQA> call, Response<ServiceQA> response) {
                                if (response.isSuccessful()) {
                                    ServiceQA newQa = response.body();
                                    viewModel.chats.add(newQa);
                                    binding.recyclerView.getAdapter().notifyDataSetChanged();

                                    binding.recyclerView.scrollToPosition(viewModel.chats.size() -1);
                                    viewModel.textMsg.set("");
                                    EventBus.getDefault().post(new RefreshNotificationEvent());
                                } else {
                                    Logger.e(API.shared().parseError(response).getHudMessage());
                                }
                            }

                            @Override
                            public void onFailure(Call<ServiceQA> call, Throwable t) {
                                Logger.e(t.getLocalizedMessage());
                            }
                        });
                    }

                    break;
            }
        }
    }
}
