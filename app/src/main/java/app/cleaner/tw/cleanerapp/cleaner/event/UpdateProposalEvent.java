package app.cleaner.tw.cleanerapp.cleaner.event;

import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;

public class UpdateProposalEvent {

    private CleanService cleanService;
    private int position;

    public UpdateProposalEvent(CleanService cleanService, int position) {
        this.cleanService = cleanService;
        this.position = position;
    }

    public CleanService getCleanService() {
        return cleanService;
    }

    public void setCleanService(CleanService cleanService) {
        this.cleanService = cleanService;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }


}
