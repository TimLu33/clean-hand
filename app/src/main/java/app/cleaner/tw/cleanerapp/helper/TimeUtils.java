package app.cleaner.tw.cleanerapp.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * TimeUtils
 *
 * @author <a href="http://www.trinea.cn" target="_blank">Trinea</a> 2013-8-24
 */
public class TimeUtils {

    public static final SimpleDateFormat DEFAULT_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final SimpleDateFormat DATE_FORMAT_MINS = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    public static final SimpleDateFormat DATE_FORMAT_DATE = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat DATE_FORMAT_DATE_SLASH = new SimpleDateFormat("yyyy/MM/dd");
    public static final SimpleDateFormat HOUR_MINUTE_FORMAT = new SimpleDateFormat("HH:mm");


    private TimeUtils() {
        throw new AssertionError();
    }

    /**
     * long time to string
     */
    public static String getTime(long timeInMillis, SimpleDateFormat dateFormat) {
        return dateFormat.format(new Date(timeInMillis));
    }

    /**
     * int time to string
     */
    public static String getTime(int timeInSeconds, SimpleDateFormat dateFormat) {
        String timestamp = timeInSeconds + "000";
        return dateFormat.format(new Date(Long.parseLong(timestamp)));
    }

    public static long getTime(String time, SimpleDateFormat dateFormat) throws ParseException {
        Date d = dateFormat.parse(time);
        return d.getTime();
    }

    /**
     * long time to string, format is {@link #DEFAULT_DATE_FORMAT}
     */
    public static String getTime(long timeInMillis) {
        return getTime(timeInMillis, DEFAULT_DATE_FORMAT);
    }

    /**
     * get current time in milliseconds
     */
    public static long getCurrentTimeInLong() {
        return System.currentTimeMillis();
    }

    /**
     * get current time in seconds
     */
    public static int getCurrentTimeInSeconds() {
        long milliseconds = getCurrentTimeInLong();
        milliseconds = milliseconds / 1000;
        return (int) milliseconds;
    }

    /**
     * get current time in milliseconds, format is {@link #DEFAULT_DATE_FORMAT}
     */
    public static String getCurrentTimeInString() {
        return getTime(getCurrentTimeInLong());
    }

    /**
     * get current time in milliseconds
     */
    public static String getCurrentTimeInString(SimpleDateFormat dateFormat) {
        return getTime(getCurrentTimeInLong(), dateFormat);
    }

    public static boolean isEarlierCurrentTime(String time) {

        int iTime = Integer.parseInt(time);
        int currentTime = TimeUtils.getCurrentTimeInSeconds();
        //Log.v("omg", time+"/"+currentTime);

        if (iTime < currentTime) {
            return true;
        } else {
            return false;
        }

    }

    public static long getDefaultTime(int hour) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE), hour, 0, 0);
        return calendar.getTimeInMillis();
    }

    public static long getDefaultTime(int hour, int minute) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE), hour, minute, 0);
        return calendar.getTimeInMillis();
    }

    /**
     * 檢查日期是否大於今日
     *
     * @param isStart true=起, false=止
     *                起 return 現在時間
     *                止 return 現在時間 + 1Day
     **/
    public static String checkDateIsValid(String currentDate, boolean isStart) throws ParseException {
        if (isStart) {
            return getTime((getTime(currentDate, DATE_FORMAT_MINS) > getCurrentTimeInLong())
                    ? getTime(currentDate, DATE_FORMAT_MINS) : getCurrentTimeInLong(), DATE_FORMAT_MINS);
        } else {
            long day = 1000 * 60 * 60 * 24L;
            return getTime((getTime(currentDate, DATE_FORMAT_MINS) > getCurrentTimeInLong())
                    ? getTime(currentDate, DATE_FORMAT_MINS) : getCurrentTimeInLong() + day, DATE_FORMAT_MINS);
        }

    }
}

