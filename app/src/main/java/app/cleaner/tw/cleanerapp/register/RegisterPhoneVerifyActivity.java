package app.cleaner.tw.cleanerapp.register;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.User;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.ApiError;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.boss.BossHomeActivity;
import app.cleaner.tw.cleanerapp.cleaner.home.CleanerHomeActivity;
import app.cleaner.tw.cleanerapp.common.Constants;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityRegPhoneVerifyBinding;
import app.cleaner.tw.cleanerapp.helper.RegexHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterPhoneVerifyActivity extends BaseActivity {

    private static final String REG_TYPE = "regType";
    private static final String IS_THIRD_PART = "isThirdPart";
    private ActivityRegPhoneVerifyBinding binding;
    private ViewModel viewModel;
    private Integer regType;
    private boolean isThirdPart;

    public static void start(Context context, int regType, boolean isThirdPart) {
        Intent starter = new Intent(context, RegisterPhoneVerifyActivity.class);
        starter.putExtra(REG_TYPE, regType);
        starter.putExtra(IS_THIRD_PART, isThirdPart);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBundle();
        EventBus.getDefault().register(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_reg_phone_verify);
        setUpToolBar();
        viewModel = new ViewModel(regType);
        binding.setViewModel(viewModel);
        binding.setPresenter(new Presenter());
    }

    private void getBundle() {
        if (getIntent().getExtras() != null) {
            regType = getIntent().getIntExtra(REG_TYPE, 0);
            isThirdPart = getIntent().getBooleanExtra(IS_THIRD_PART, false);
        }
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("手機驗證");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRegSuccessEvent(onRegisterSuccessEvent event) {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public class ViewModel {

        public final ObservableField<Integer> regType = new ObservableField<>();
        public final ObservableField<String> cellPhone = new ObservableField<>("");
        public final ObservableField<String> verifyCode = new ObservableField<>("");

        ViewModel(Integer regType) {
            this.regType.set(regType);
        }

    }

    public class Presenter implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_send:
                    //傳送驗證碼
                    if (viewModel.cellPhone.get().isEmpty()) {
                        Toast.makeText(activity, "請輸入手機號碼", Toast.LENGTH_SHORT).show();
                    } else if (!RegexHelper.isPhone(viewModel.cellPhone.get())) {
                        Toast.makeText(activity, "手機號碼格式有誤", Toast.LENGTH_SHORT).show();
                    } else {
                        // 接驗證碼API
                        final KProgressHUD hud = KProgressHUD.create(RegisterPhoneVerifyActivity.this).show();
                        API.sharedService().sendPhoneValidate(viewModel.cellPhone.get()).enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                hud.dismiss();
                                if (response.isSuccessful()) {
                                    Toast.makeText(activity, "驗證碼已發送", Toast.LENGTH_SHORT).show();
                                } else {
                                    ApiError apiError = API.shared().parseError(response);
                                    Toast.makeText(activity, apiError.getHudMessage(), Toast.LENGTH_SHORT).show();
                                    Logger.e(apiError.getHudMessage());
                                }

                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {
                                hud.dismiss();
                                Logger.e(t.getLocalizedMessage());
                            }
                        });
                    }
                    break;
                case R.id.btn_next:
                    if (viewModel.verifyCode.get().isEmpty()) {
                        Toast.makeText(activity, "請輸入驗證碼", Toast.LENGTH_SHORT).show();
                    } else {
                        // 檢查驗證碼
                        final KProgressHUD hud = KProgressHUD.create(RegisterPhoneVerifyActivity.this).show();
                        API.sharedService().validatePhone(viewModel.cellPhone.get(), viewModel.verifyCode.get()).enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                hud.dismiss();
                                if (response.isSuccessful()) {
                                    if (isThirdPart) {
                                        API.sharedService().getUser(API.sharedAuthorizationToken()).enqueue(new Callback<User>() {
                                            @Override
                                            public void onResponse(Call<User> call, Response<User> response) {
                                                if (response.isSuccessful()) {
                                                    User user = response.body();
                                                    user.setCellphone(viewModel.cellPhone.get());
                                                    API.sharedService().patchUser(API.sharedAuthorizationToken(), user).enqueue(new Callback<User>() {
                                                        @Override
                                                        public void onResponse(Call<User> call, Response<User> response) {
                                                            if (response.isSuccessful()) {
                                                                if (regType == Constants.CLEANER) {
                                                                    CleanerHomeActivity.start(activity);
                                                                } else {
                                                                    BossHomeActivity.start(activity);
                                                                }
                                                                finish();
                                                            } else {
                                                                ApiError apiError = API.shared().parseError(response);
                                                                Toast.makeText(activity, apiError.getHudMessage(), Toast.LENGTH_SHORT).show();
                                                                Logger.e(apiError.getHudMessage());
                                                            }
                                                        }

                                                        @Override
                                                        public void onFailure(Call<User> call, Throwable t) {
                                                            hud.dismiss();
                                                            Logger.e(t.getLocalizedMessage());
                                                        }
                                                    });
                                                } else {
                                                    ApiError apiError = API.shared().parseError(response);
                                                    Toast.makeText(activity, apiError.getHudMessage(), Toast.LENGTH_SHORT).show();
                                                    Logger.e(apiError.getHudMessage());
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<User> call, Throwable t) {
                                                hud.dismiss();
                                                Logger.e(t.getLocalizedMessage());
                                            }
                                        });

                                    } else {
                                        RegisterActivity.start(activity, regType, viewModel.cellPhone.get());
                                    }

                                } else {
                                    ApiError apiError = API.shared().parseError(response);
                                    Toast.makeText(activity, apiError.getHudMessage(), Toast.LENGTH_SHORT).show();
                                    Logger.e(apiError.getHudMessage());
                                }
                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {
                                hud.dismiss();
                                Logger.e(t.getLocalizedMessage());
                            }
                        });


                    }
                    //下一步
                    break;
            }

        }
    }
}
