package app.cleaner.tw.cleanerapp.boss.search;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.List;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.SimpleUser;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;
import app.cleaner.tw.cleanerapp.backend.drfapi.builder.QueriesBuilder;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.PageNumberPagination;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.cleaner.resume.ResumeAdapter;
import app.cleaner.tw.cleanerapp.cleaner.resume.ResumeDetailActivity;
import app.cleaner.tw.cleanerapp.common.Constants;
import app.cleaner.tw.cleanerapp.common.DividerItemDecoration;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityCleanerResumeBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ARTHUR on 2017/10/29.
 */

public class CleanerRatedListActivity extends BaseActivity {

    private static final String CLEANER_USER = "cleaner";
    protected ActivityCleanerResumeBinding binding;
    private SimpleUser cleaner;
    private List<CleanService> resumeList = new ArrayList<>();

    public static void start(Context context, SimpleUser cleaner) {
        Intent starter = new Intent(context, CleanerRatedListActivity.class);
        starter.putExtra(CLEANER_USER, cleaner);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cleaner_resume);
        getBundle();
        setUpToolBar();
        initViews();
        loadResumes();
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("案件列表");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private void getBundle() {
        if (getIntent().getExtras() != null) {
            cleaner = (SimpleUser) getIntent().getExtras().getSerializable(CLEANER_USER);
        }
    }

    private void initViews() {
        final LinearLayoutManager lm = new LinearLayoutManager(this);
        lm.setOrientation(LinearLayoutManager.VERTICAL);
        ResumeAdapter adapter = new ResumeAdapter(resumeList, Constants.BOSS);
        adapter.setOnItemClickListener(new ResumeAdapter.ItemClickListener() {
            @Override
            public void onItemClickListener(View view, int position) {
                ClosedResumeDetailActivity.start(activity, resumeList.get(position), cleaner);
            }
        });
        binding.recyclerView.setLayoutManager(lm);
        binding.recyclerView.addItemDecoration(new DividerItemDecoration(activity));
        binding.recyclerView.setAdapter(adapter);
    }

    protected void loadResumes() {
        if (cleaner == null)
            return;
        final KProgressHUD hud = KProgressHUD.create(activity).show();
        QueriesBuilder queriesBuilder = new QueriesBuilder()
                .filter("service_matching_set__cleaner", cleaner.getId())
                .order("-created_datetime")
                .pagination(1, 1000);
        API.sharedService().getAllRatedService(API.sharedAuthorizationToken(), queriesBuilder.create()).enqueue(new Callback<PageNumberPagination<CleanService>>() {
            @Override
            public void onResponse(Call<PageNumberPagination<CleanService>> call, Response<PageNumberPagination<CleanService>> response) {
                hud.dismiss();
                if (response.isSuccessful()) {
                    resumeList.addAll(response.body().getResults());
                    binding.recyclerView.getAdapter().notifyDataSetChanged();
                    if (resumeList.isEmpty())
                        Toast.makeText(activity, "此清潔手目前沒有任何評價", Toast.LENGTH_SHORT).show();
                } else {
                    Logger.e(API.shared().parseError(response).getHudMessage());
                }
            }

            @Override
            public void onFailure(Call<PageNumberPagination<CleanService>> call, Throwable t) {
                Logger.e(t.getLocalizedMessage());
                hud.dismiss();
            }
        });
    }
}
