package app.cleaner.tw.cleanerapp.common;

import android.content.Context;
import android.content.SharedPreferences;

public class Preference {


    private static final String PREF_NAME = "tw.com.cleanhand";
    private static final String CLEANER_TOKEN = "cleanerToken";
    private static final String HIRER_TOKEN = "hirerToken";
    private static final String TEMP_TOKEN = "tempToken";


    public static void setCleanerToken(Context context, String token) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(CLEANER_TOKEN, token).apply();
    }

    public static String getCleanerToken(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(CLEANER_TOKEN, "");
    }


    public static void setHirerToken(Context context, String token) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(HIRER_TOKEN, token).apply();
    }

    public static String getHirerToken(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(HIRER_TOKEN, "");
    }


    public static void setTempToken(Context context, String token) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(TEMP_TOKEN, token).apply();
    }

    public static String getTempToken(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(TEMP_TOKEN, "");
    }

}
