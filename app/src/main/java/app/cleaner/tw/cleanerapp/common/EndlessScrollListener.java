package app.cleaner.tw.cleanerapp.common;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public abstract class EndlessScrollListener extends RecyclerView.OnScrollListener {
    public static final int SCROLL_DOWN_LOAD_MORE = 0;
    public static final int SCROLL_UP_LOAD_MORE = 1;
    private int loadMoreState = 0;
    private int previousItemCount = 0;

    protected EndlessScrollListener(int state) {
        this.loadMoreState = state;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        int lastPosition;
        int firstPosition;

        if (layoutManager instanceof GridLayoutManager) {
            lastPosition = ((GridLayoutManager) layoutManager).findLastVisibleItemPosition();
            firstPosition = ((GridLayoutManager) layoutManager).findFirstVisibleItemPosition();
        } else {
            lastPosition = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
            firstPosition = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
        }
        int totalItemCount = layoutManager.getItemCount();
        boolean scrollDown;

        scrollDown = dy > 0;

        switch (loadMoreState) {
            case SCROLL_DOWN_LOAD_MORE:
                if (lastPosition == totalItemCount - 1 && scrollDown) {//到達底部
                    if (checkCanLoadMore(totalItemCount)) onLoadMore();
                }
                break;

            case SCROLL_UP_LOAD_MORE:
                if (firstPosition == 0 && !scrollDown) {
                    if (checkCanLoadMore(totalItemCount)) onLoadMore();
                }
                break;
        }
    }

    private boolean checkCanLoadMore(int totalItemCount) {
        boolean canLoadMode = false;
        if (totalItemCount != previousItemCount) {//如果相等代表沒資料了
            previousItemCount = totalItemCount;
            canLoadMode = true;
        }
        return canLoadMode;
    }

    public void resetPreviousCount() {
        this.previousItemCount = 0;
    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

    }

    public abstract void onLoadMore();
}
