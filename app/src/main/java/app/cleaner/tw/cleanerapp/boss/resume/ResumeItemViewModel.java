package app.cleaner.tw.cleanerapp.boss.resume;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;

import android.databinding.ObservableField;

import java.util.ArrayList;
import java.util.Locale;

import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceMatching;
import app.cleaner.tw.cleanerapp.common.Constants;
import app.cleaner.tw.cleanerapp.helper.TimeUtils;

public class ResumeItemViewModel {

    public final ObservableField<String> address = new ObservableField<>();
    public final ObservableField<String> time = new ObservableField<>();
    public final ObservableField<String> cleaner = new ObservableField<>();
    public final ObservableField<Integer> caseStatus = new ObservableField<>();

    public ResumeItemViewModel(CleanService model) {
        address.set(model.getAddress());
        time.set(String.format(
                Locale.getDefault(),
                "%s ~ %s",
                TimeUtils.getTime(model.getTime_range_start().getTime(), TimeUtils.DATE_FORMAT_MINS),
                TimeUtils.getTime(model.getTime_range_end().getTime(), TimeUtils.DATE_FORMAT_MINS)
        ));

        ArrayList<ServiceMatching> matchings = new ArrayList<>(model.getHirer_matching_set());
        CollectionUtils.filter(matchings, new Predicate<ServiceMatching>() {
            @Override
            public boolean evaluate(ServiceMatching object) {
                switch (object.getMatching_state()) {
                    case Confirm:
                    case Closed:
                        return true;
                    default:
                        return false;
                }
            }
        });
        if (!matchings.isEmpty())
            cleaner.set(matchings.get(0).getCleaner_name_read_only());

        if (model.getCleaner_matching() != null) {
            switch (model.getCleaner_matching().getMatching_state()) {
                case Invite:
                    break;
                case Proposal:
                    break;
                case Candidate:
                    break;
                case Missed:
                    break;
                case Confirm:
                    break;
                case Closed:
                    break;
                default:
                    break;
            }
        } else {
            caseStatus.set(Constants.CASE_STATUS.NOT_PAID.ordinal());
        }
    }
}
