package app.cleaner.tw.cleanerapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.boss.BossHomeActivity;
import app.cleaner.tw.cleanerapp.boss.BossLoginActivity;
import app.cleaner.tw.cleanerapp.cleaner.home.CleanerHomeActivity;
import app.cleaner.tw.cleanerapp.cleaner.login.CleanerLoginActivity;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.common.Preference;


public class LoginMainActivity extends BaseActivity implements View.OnClickListener {

    public static void start(Context context) {
        Intent starter = new Intent(context, LoginMainActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_main);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Button btnCleaner = (Button) findViewById(R.id.btn_cleaner);
        Button btnBoss = (Button) findViewById(R.id.btn_boss);
        btnCleaner.setOnClickListener(this);
        btnBoss.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_cleaner) {
            if (Preference.getCleanerToken(activity).isEmpty()) {
                CleanerLoginActivity.start(activity);
            } else {
                API.shared().setAuthorizationToken(Preference.getCleanerToken(activity));
                CleanerHomeActivity.start(activity);
            }

        } else if (view.getId() == R.id.btn_boss) {
            if (Preference.getHirerToken(activity).isEmpty()) {
                BossLoginActivity.start(activity);

            } else {
                API.shared().setAuthorizationToken(Preference.getHirerToken(activity));
                BossHomeActivity.start(activity);
            }
        }
    }
}
