package app.cleaner.tw.cleanerapp.forgot;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Toast;

import com.orhanobut.logger.Logger;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.AuthTokenBean;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.ApiError;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.boss.BossHomeActivity;
import app.cleaner.tw.cleanerapp.cleaner.home.CleanerHomeActivity;
import app.cleaner.tw.cleanerapp.common.Constants;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityForgetStep3Binding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ForgotPwdStep3Activity extends BaseActivity {

    private static final String REG_TYPE = "regType";

    private ActivityForgetStep3Binding binding;

    private ViewModel viewModel;

    private Integer regType;

    public static void start(Context context, int regType) {
        Intent starter = new Intent(context, ForgotPwdStep3Activity.class);
        starter.putExtra(REG_TYPE, regType);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBundle();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forget_step3);
        setUpToolBar();
        viewModel = new ViewModel(regType);
        binding.setViewModel(viewModel);
        binding.setPresenter(new Presenter());
    }

    private void getBundle() {
        if (getIntent().getExtras() != null) {
            regType = getIntent().getIntExtra(REG_TYPE, 0);
        }
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("忘記密碼");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    public class ViewModel {

        public final ObservableField<Integer> regType = new ObservableField<>();

        public final ObservableField<String> pwd = new ObservableField<>();

        public final ObservableField<String> confirmPwd = new ObservableField<>();

        ViewModel(Integer regType) {
            this.regType.set(regType);
            this.pwd.set("");
            this.confirmPwd.set("");
        }
    }

    public class Presenter implements View.OnClickListener {

        boolean isShowPwd = false;
        boolean isRepeatShowPwd = false;

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_confirm:
                    API.sharedService().changePassword(API.sharedAuthorizationToken(), viewModel.pwd.get(), viewModel.confirmPwd.get()).enqueue(new Callback<AuthTokenBean>() {
                        @Override
                        public void onResponse(Call<AuthTokenBean> call, Response<AuthTokenBean> response) {
                            if (response.isSuccessful()) {
                                if (regType == Constants.CLEANER) {
                                    CleanerHomeActivity.start(activity);
                                } else {
                                    BossHomeActivity.start(activity);
                                }
                            } else {
                                ApiError apiError = API.shared().parseError(response);
                                Logger.e(apiError.getHudMessage());
                                Toast.makeText(activity, apiError.getHudMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<AuthTokenBean> call, Throwable t) {
                            Logger.e(t.getLocalizedMessage());
                        }
                    });
                    break;
                case R.id.img_show_pwd:
                    isShowPwd = !isShowPwd;
                    binding.inputPwd.setTransformationMethod(isShowPwd ? HideReturnsTransformationMethod.getInstance() : PasswordTransformationMethod.getInstance());
                    break;
                case R.id.img_show_confirm_pwd:
                    isRepeatShowPwd = !isRepeatShowPwd;
                    binding.inputConfirmPwd.setTransformationMethod(isRepeatShowPwd ? HideReturnsTransformationMethod.getInstance() : PasswordTransformationMethod.getInstance());
                    break;
            }
        }
    }


}
