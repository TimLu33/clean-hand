package app.cleaner.tw.cleanerapp.backend.cleaner.models.member;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.net.URL;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import app.cleaner.tw.cleanerapp.backend.cleaner.models.geography.Town;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceType;

/**
 * Created by ARTHUR on 2017/10/17.
 */

public class CleanerProfile extends Profile implements Serializable {

    /**
     * display_name : 掃地的高先生
     * head_image : https://cleanerbucket.s3.amazonaws.com/media/image/user/head/43/03e2cad0-1df9-4a47-8ddd-d2a2a3dcdb49.jpg
     * cover_photos : [{"id":"e01b4440-31f9-4fc1-b8ca-567656675d16","image":"https://cleanerbucket.s3.amazonaws.com/media/image/user/cover/e01b4440-31f9-4fc1-b8ca-567656675d16.jpg"}]
     * allow_invite : true
     * serve_all_times : false
     * serve_all_areas : false
     * custom_service_areas : [{"id":198,"town_code":"10017020","name":"七堵區","name_eng":"Qidu District","county":{"id":38,"county_code":"10017","name":"基隆市","name_eng":"Keelung City"}}]
     * custom_service_times : [{"id":15,"start":"21:00:00","end":"23:00:00"},{"id":17,"start":"01:00:00","end":"12:00:00"}]
     * provide_service_types : [{"id":1,"name":"居家清潔"}]
     * expected_salary : 301
     * has_good_card : false
     * has_cleaner_license : false
     * allow_pet : true
     * equip_tools : false
     * recommendation :
     * introduction :
     * avg_rating : null
     * min_rating : null
     * max_rating : null
     */

    public static final int LowestSalary = 300;

    @Expose private boolean allow_invite;

    @Expose private boolean serve_all_times;

    @Expose private boolean serve_all_areas;

    @Expose private int expected_salary;

    @Expose(serialize = false) private boolean has_good_card;

    @Expose(serialize = false) private boolean has_cleaner_license;

    @Expose private boolean allow_pet;

    @Expose private boolean equip_tools;

    @Expose private String recommendation;

    @Expose private String introduction;

    @Expose(serialize = false) private List<CoverPhoto> cover_photos;

    @Expose(serialize = false) private List<Town> custom_service_areas;

    @Expose(serialize = false) private List<CustomServiceTime> custom_service_times;

    @Expose(serialize = false) private List<ServiceType> provide_service_types;

    public CleanerProfile() {
        super();
        expected_salary = LowestSalary;
    }

    public boolean isAllow_invite() {
        return allow_invite;
    }

    public void setAllow_invite(boolean allow_invite) {
        this.allow_invite = allow_invite;
    }

    public boolean isServe_all_times() {
        return serve_all_times;
    }

    public void setServe_all_times(boolean serve_all_times) {
        this.serve_all_times = serve_all_times;
    }

    public boolean isServe_all_areas() {
        return serve_all_areas;
    }

    public void setServe_all_areas(boolean serve_all_areas) {
        this.serve_all_areas = serve_all_areas;
    }

    public int getExpected_salary() {
        return expected_salary;
    }

    public void setExpected_salary(int expected_salary) {
        this.expected_salary = expected_salary;
    }

    public boolean isHas_good_card() {
        return has_good_card;
    }

    public boolean isHas_cleaner_license() {
        return has_cleaner_license;
    }

    public boolean isAllow_pet() {
        return allow_pet;
    }

    public void setAllow_pet(boolean allow_pet) {
        this.allow_pet = allow_pet;
    }

    public boolean isEquip_tools() {
        return equip_tools;
    }

    public void setEquip_tools(boolean equip_tools) {
        this.equip_tools = equip_tools;
    }

    public String getRecommendation() {
        return recommendation;
    }

    public void setRecommendation(String recommendation) {
        this.recommendation = recommendation;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public List<CoverPhoto> getCover_photos() {
        return cover_photos;
    }

    public List<Town> getCustom_service_areas() {
        return custom_service_areas;
    }

    public List<CustomServiceTime> getCustom_service_times() {
        return custom_service_times;
    }

    public List<ServiceType> getProvide_service_types() {
        return provide_service_types;
    }

//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel parcel, int i) {
//        parcel.writeString(display_name);
//        parcel.writeByte((byte) (allow_invite ? 1 : 0));
//        parcel.writeByte((byte) (serve_all_times ? 1 : 0));
//        parcel.writeByte((byte) (serve_all_areas ? 1 : 0));
//        parcel.writeInt(expected_salary);
//        parcel.writeByte((byte) (has_good_card ? 1 : 0));
//        parcel.writeByte((byte) (has_cleaner_license ? 1 : 0));
//        parcel.writeByte((byte) (allow_pet ? 1 : 0));
//        parcel.writeByte((byte) (equip_tools ? 1 : 0));
//        parcel.writeString(recommendation);
//        parcel.writeString(introduction);
//        parcel.writeFloat(avg_rating);
//        parcel.writeInt(min_rating);
//        parcel.writeInt(max_rating);
//    }

    public static class CoverPhoto implements Serializable {
        /**
         * id : e01b4440-31f9-4fc1-b8ca-567656675d16
         * image : https://cleanerbucket.s3.amazonaws.com/media/image/user/cover/e01b4440-31f9-4fc1-b8ca-567656675d16.jpg
         */

        @Expose
        private String id;
        @Expose
        private URL image;

        public String getId() {
            return id;
        }

        public URL getImage() {
            return image;
        }
    }

    public static class CustomServiceTime implements Serializable {
        /**
         * id : 15
         * start : 21:00:00
         * end : 23:00:00
         */

        @Expose(serialize = false)
        private int id;

        @Expose
        private Time start;

        @Expose
        private Time end;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public Time getStart() {
            return start;
        }

        public void setStart(Time start) {
            this.start = start;
        }

        public Time getEnd() {
            return end;
        }

        public void setEnd(Time end) {
            this.end = end;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof CustomServiceTime && getId() != 0) {
                CustomServiceTime ref = (CustomServiceTime) obj;
                return ref.getId()==getId();
            }
            return super.equals(obj);
        }

        @Override
        public int hashCode() {
            if (getId() != 0)
                return getId();
            return super.hashCode();
        }
    }

    public String getOthers() {
        ArrayList<String> os = new ArrayList<>();
        if (isAllow_pet())
            os.add("可有寵物");
        if (isEquip_tools())
            os.add("自備工具");
        if (isHas_good_card())
            os.add("俱備良民證");
        if (isHas_cleaner_license())
            os.add("俱備清潔證照");
        if (os.isEmpty())
            return null;
        else
            return TextUtils.join(", ", os);
    }

//    protected CleanerProfile(Parcel in) {
//        display_name = in.readString();
//        allow_invite = in.readByte() != 0;
//        serve_all_times = in.readByte() != 0;
//        serve_all_areas = in.readByte() != 0;
//        expected_salary = in.readInt();
//        has_good_card = in.readByte() != 0;
//        has_cleaner_license = in.readByte() != 0;
//        allow_pet = in.readByte() != 0;
//        equip_tools = in.readByte() != 0;
//        recommendation = in.readString();
//        introduction = in.readString();
//        avg_rating = in.readFloat();
//        min_rating = in.readInt();
//        max_rating = in.readInt();
//    }

//    public static final Creator<CleanerProfile> CREATOR = new Creator<CleanerProfile>() {
//        @Override
//        public CleanerProfile createFromParcel(Parcel in) {
//            return new CleanerProfile(in);
//        }
//
//        @Override
//        public CleanerProfile[] newArray(int size) {
//            return new CleanerProfile[size];
//        }
//    };


}
