package app.cleaner.tw.cleanerapp.mock;


public class MockFavoriteModel {

    private String headImageUrl;
    private String cleanerName;
    private int rating;
    private String hirerCommend;
    private String recommend;

    public String getHeadImageUrl() {
        return headImageUrl;
    }

    public void setHeadImageUrl(String headImageUrl) {
        this.headImageUrl = headImageUrl;
    }

    public String getCleanerName() {
        return cleanerName;
    }

    public void setCleanerName(String cleanerName) {
        this.cleanerName = cleanerName;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getHirerCommend() {
        return hirerCommend;
    }

    public void setHirerCommend(String hirerCommend) {
        this.hirerCommend = hirerCommend;
    }

    public String getRecommend() {
        return recommend;
    }

    public void setRecommend(String recommend) {
        this.recommend = recommend;
    }
}
