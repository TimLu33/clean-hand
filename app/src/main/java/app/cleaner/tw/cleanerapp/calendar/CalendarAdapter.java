package app.cleaner.tw.cleanerapp.calendar;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.List;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.CleanService;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.ServiceMatching;
import app.cleaner.tw.cleanerapp.common.Constants;

public class CalendarAdapter extends RecyclerView.Adapter<CalendarAdapter.ViewHolder> implements View.OnClickListener {

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    private ItemClickListener listener;

    private List<CalenderObject> calendarList = new ArrayList<>();
    private int role;

    public CalendarAdapter(List<CalenderObject> calendarList, int role) {
        this.calendarList = calendarList;
        this.role = role;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.calendar_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.itemView.setOnClickListener(this);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setTag(position);
        holder.tvDay.setText(calendarList.get(position).getDay());

        if (calendarList.get(position).getCleanServiceList() != null && !calendarList.get(position).getCleanServiceList().isEmpty()) {

            for (CleanService cleanService : calendarList.get(position).getCleanServiceList()) {
                Context context = holder.caseLayout.getContext();
                TextView tv = new TextView(context);
                setTextStyle(tv);

                if (role == Constants.CLEANER && cleanService.getCleaner_matching() != null) {
                    int status = Constants.CASE_STATUS.fromBackendState(cleanService.getCleaner_matching().getMatching_state()).ordinal();
                    if (status == Constants.CASE_STATUS.INVITE.ordinal()) {
                        tv.setText("邀請中");
                        tv.setBackgroundResource(R.drawable.ic_status_green1);
                    } else if (status == Constants.CASE_STATUS.PROPOSAL.ordinal()) {
                        tv.setText("已提案");
                        tv.setBackgroundResource(R.drawable.ic_status_green1);
                    } else if (status == Constants.CASE_STATUS.CANDIDATE.ordinal()) {
                        tv.setText("選拔中");
                        tv.setBackgroundResource(R.drawable.ic_status_green1);
                    } else if (status == Constants.CASE_STATUS.CONFIRM.ordinal()) {
                        tv.setText("已確認");
                        tv.setBackgroundResource(R.drawable.ic_status_orange);
                    } else if (status == Constants.CASE_STATUS.MISSED.ordinal()) {
                        tv.setText("落選");
                        tv.setBackgroundResource(R.drawable.ic_status_black);
                    } else if (status == Constants.CASE_STATUS.CLOSED.ordinal()) {
                        tv.setText("已結案");
                        tv.setBackgroundResource(R.drawable.ic_status_black);
                    }
                }

                if (role == Constants.BOSS && cleanService.getHirer_matching_set() != null) {
                    ServiceMatching.MatchingState state = cleanService.getCurrentServiceMatchingState(role);
                    if (state != null) {
                        int status = Constants.CASE_STATUS.fromBackendState(state).ordinal();
                        if (status == Constants.CASE_STATUS.INVITE.ordinal()) {
                            tv.setText("邀請中");
                            tv.setBackgroundResource(R.drawable.ic_status_green1);
                        } else if (status == Constants.CASE_STATUS.PROPOSAL.ordinal()) {
                            tv.setText("已提案");
                            tv.setBackgroundResource(R.drawable.ic_status_green1);
                        } else if (status == Constants.CASE_STATUS.CANDIDATE.ordinal()) {
                            tv.setText("選拔中");
                            tv.setBackgroundResource(R.drawable.ic_status_green1);
                        } else if (status == Constants.CASE_STATUS.CONFIRM.ordinal()) {
                            tv.setText("已確認");
                            tv.setBackgroundResource(R.drawable.ic_status_orange);
                        } else if (status == Constants.CASE_STATUS.CLOSED.ordinal()) {
                            tv.setText("已結案");
                            tv.setBackgroundResource(R.drawable.ic_status_black);
                        }
                    }

                }

                if (!cleanService.getFee_payment().isPaid()) {
                    tv.setText("未付款");
                    tv.setBackgroundResource(R.drawable.ic_status_green1);
                }

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.topMargin = 10;
                tv.setGravity(Gravity.CENTER);
                holder.caseLayout.addView(tv, params);
            }
            holder.caseLayout.setVisibility(View.VISIBLE);
        } else {
            holder.caseLayout.setVisibility(View.GONE);
        }

    }

    public void setOnItemClickListener(ItemClickListener listener) {
        this.listener = listener;
    }

    private void setTextStyle(TextView tv) {
        tv.setTextSize(12);
        tv.setTextColor(ContextCompat.getColor(tv.getContext(), R.color.white));
    }

    @Override
    public void onClick(View view) {
        if (listener != null) {
            listener.onItemClick(view, (int) view.getTag());
        }
    }

    @Override
    public int getItemCount() {
        return calendarList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvDay;
        LinearLayout caseLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            tvDay = itemView.findViewById(R.id.tv_day);
            caseLayout = itemView.findViewById(R.id.case_layout);
        }
    }
}
