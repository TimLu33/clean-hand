package app.cleaner.tw.cleanerapp.mock;

/**
 * Created by juntinglu on 2017/10/25.
 */

public class MockCleanerResumeDetailModel {

    private String houseUrl;

    private int houseRating;

    private String houseStyle;

    private String serviceType;

    private String others;

    private String cleanerName;


    public String getHouseUrl() {
        return houseUrl;
    }

    public void setHouseUrl(String houseUrl) {
        this.houseUrl = houseUrl;
    }

    public int getHouseRating() {
        return houseRating;
    }

    public void setHouseRating(int houseRating) {
        this.houseRating = houseRating;
    }

    public String getHouseStyle() {
        return houseStyle;
    }

    public void setHouseStyle(String houseStyle) {
        this.houseStyle = houseStyle;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getOthers() {
        return others;
    }

    public void setOthers(String others) {
        this.others = others;
    }

    public String getCleanerName() {
        return cleanerName;
    }

    public void setCleanerName(String cleanerName) {
        this.cleanerName = cleanerName;
    }

}
