package app.cleaner.tw.cleanerapp.backend.cleaner.models.member;

public class AccessTokenBean {

    public String access_token;

    public String code;

    public AccessTokenBean(String access_token, String code) {
        this.access_token = access_token;
        this.code = code;
    }

    public String getAccess_token() {
        return access_token;
    }

    public String getCode() {
        return code;
    }
}
