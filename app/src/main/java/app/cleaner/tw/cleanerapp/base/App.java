package app.cleaner.tw.cleanerapp.base;

import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.facebook.stetho.Stetho;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.FormatStrategy;
import com.orhanobut.logger.Logger;
import com.orhanobut.logger.PrettyFormatStrategy;

import io.fabric.sdk.android.Fabric;

public class App extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        Logger.addLogAdapter(new AndroidLogAdapter(getLoggerBuilder()));
        Stetho.initializeWithDefaults(this);
    }

    private FormatStrategy getLoggerBuilder() {
        return PrettyFormatStrategy.newBuilder()
                .showThreadInfo(false)
                .build();
    }

}
