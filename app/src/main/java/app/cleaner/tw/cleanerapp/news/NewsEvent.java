package app.cleaner.tw.cleanerapp.news;

/**
 * Created by juntinglu on 2018/8/19.
 */

public class NewsEvent {

    String id;

    public NewsEvent(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
