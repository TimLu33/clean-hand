package app.cleaner.tw.cleanerapp.register;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.common.Constants;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityRegAgreeTermsBinding;


public class RegisterTermActivity extends BaseActivity {

    private static final String REG_TYPE = "regType";
    private ActivityRegAgreeTermsBinding binding;
    private ViewModel viewModel;
    private Integer regType;

    public static void start(Context context, int regType) {
        Intent starter = new Intent(context, RegisterTermActivity.class);
        starter.putExtra(REG_TYPE, regType);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBundle();
        EventBus.getDefault().register(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_reg_agree_terms);
        setUpToolBar();
        viewModel = new ViewModel(regType, getString(regType == Constants.BOSS ? R.string.boss : R.string.cleaner));
        binding.setViewModel(viewModel);
        binding.setPresenter(new Presenter());
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("同意條款");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private void getBundle() {
        if (getIntent().getExtras() != null) {
            regType = getIntent().getIntExtra(REG_TYPE, 0);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRegSuccessEvent(onRegisterSuccessEvent event) {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public class ViewModel {
        public final ObservableField<Integer> userType = new ObservableField<>();
        public final ObservableField<String> userTypeString = new ObservableField<>();

        public ViewModel(Integer userType, String userTypeString) {
            this.userType.set(userType);
            this.userTypeString.set(userTypeString);
        }
    }

    public class Presenter implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if (binding.checkbox.isChecked()) {
                RegisterPhoneVerifyActivity.start(activity, regType, false);
            } else {
                Toast.makeText(activity, "請勾選同意以上條款", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
