package app.cleaner.tw.cleanerapp.message;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.List;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.SimpleUser;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.service.MessageModel;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.boss.search.BossChatActivity;
import app.cleaner.tw.cleanerapp.cleaner.cleancase.CleanerChatActivity;
import app.cleaner.tw.cleanerapp.common.Constants;
import app.cleaner.tw.cleanerapp.common.DividerItemDecoration;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.databinding.ActivityMessageBoardBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessageBoardActivity extends BaseActivity {

    private static final String ROLE = "role";
    private ActivityMessageBoardBinding binding;
    private int role;

    private List<MessageModel> messageList = new ArrayList<>();

    public static void start(Context context, int role) {
        Intent starter = new Intent(context, MessageBoardActivity.class);
        starter.putExtra(ROLE, role);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBundle();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_message_board);
        setUpToolBar();
        initViews();
        getMessage();
    }

    private void initViews() {
        LinearLayoutManager lm = new LinearLayoutManager(this);
        lm.setOrientation(LinearLayoutManager.VERTICAL);
        MessageAdapter adapter = new MessageAdapter(messageList);

        adapter.setOnItemClickListener(new MessageAdapter.ItemClickListener() {
            @Override
            public void onItemClickListener(View view, int position) {
                final KProgressHUD hud = KProgressHUD.create(activity).show();
                if (role == Constants.CLEANER) {
                    API.sharedService().cleanerFindHirer(API.sharedAuthorizationToken(), messageList.get(position).getHirer()).enqueue(new Callback<SimpleUser>() {
                        @Override
                        public void onResponse(Call<SimpleUser> call, Response<SimpleUser> response) {
                            hud.dismiss();
                            if (response.isSuccessful()) {
                                CleanerChatActivity.start(activity, null, response.body().getId(), 0);
                            } else {
                                Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<SimpleUser> call, Throwable t) {
                            hud.dismiss();
                            Logger.e(t.getLocalizedMessage());
                        }
                    });
                } else {
                    API.sharedService().hirerFindCleaner(API.sharedAuthorizationToken(), messageList.get(position).getCleaner()).enqueue(new Callback<SimpleUser>() {
                        @Override
                        public void onResponse(Call<SimpleUser> call, Response<SimpleUser> response) {
                            hud.dismiss();
                            if (response.isSuccessful()) {
                                BossChatActivity.start(activity, response.body());

                            } else {
                                Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<SimpleUser> call, Throwable t) {
                            hud.dismiss();
                            Logger.e(t.getLocalizedMessage());
                        }
                    });

                }
            }
        });

        binding.recyclerView.setLayoutManager(lm);
        binding.recyclerView.addItemDecoration(new DividerItemDecoration(activity));
        binding.recyclerView.setAdapter(adapter);
    }

    private void getBundle() {
        if (getIntent().getExtras() != null) {
            role = getIntent().getIntExtra(ROLE, 0);
        }
    }
    private void setUpToolBar() {
        binding.appBar.title.setText("留言板列表");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private void getMessage() {
        Call<List<MessageModel>> call;
        if (role == Constants.CLEANER) {
            call = API.sharedService().getCleanerMessageList(API.sharedAuthorizationToken());
        } else {
            call = API.sharedService().getHirerMessageList(API.sharedAuthorizationToken());
        }
        final KProgressHUD hud = KProgressHUD.create(activity).show();
        call.enqueue(new Callback<List<MessageModel>>() {
            @Override
            public void onResponse(Call<List<MessageModel>> call, Response<List<MessageModel>> response) {
                hud.dismiss();
                if (response.isSuccessful()) {
                    messageList.addAll(response.body());
                    binding.recyclerView.getAdapter().notifyDataSetChanged();
                } else {
                    Toast.makeText(activity, API.shared().parseError(response).getHudMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<MessageModel>> call, Throwable t) {
                hud.dismiss();
                Logger.e(t.getLocalizedMessage());
            }
        });

    }

}
