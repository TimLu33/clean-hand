package app.cleaner.tw.cleanerapp.common;

import android.graphics.Bitmap;

public class ConvenientBannerModel {

    private String imageUrl;
    private Bitmap bitmap;

    public ConvenientBannerModel(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public ConvenientBannerModel(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

}
