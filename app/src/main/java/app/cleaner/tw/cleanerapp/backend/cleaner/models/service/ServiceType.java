package app.cleaner.tw.cleanerapp.backend.cleaner.models.service;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by ARTHUR on 2017/10/17.
 */

public class ServiceType implements Serializable {
    /**
     * id : 1
     * name : 居家清潔
     */

    @Expose
    private int id;

    @Expose
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ServiceType)
            return ((ServiceType) obj).getId() == getId();
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        if (getId() != 0)
            return getId();
        return super.hashCode();
    }
}
