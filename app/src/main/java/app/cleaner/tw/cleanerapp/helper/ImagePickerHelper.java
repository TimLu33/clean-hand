package app.cleaner.tw.cleanerapp.helper;

import android.app.Activity;
import android.content.ActivityNotFoundException;


import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;

import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import app.cleaner.tw.cleanerapp.common.UCropActivity;


public class ImagePickerHelper {

    //google photos
    private static final String GOOGLE_PHOTOS_PACKAGE_NAME = "com.google.android.apps.photos";

    public static boolean launchGooglePhotosPicker(Activity callingActivity, int requestCode) {
        if (callingActivity != null) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_PICK);
            intent.setType("image/*");
            List<ResolveInfo> resolveInfoList = callingActivity.getPackageManager().queryIntentActivities(intent, 0);
            if (resolveInfoList.size() != 0) {
                if (isPackageInstalled(callingActivity, GOOGLE_PHOTOS_PACKAGE_NAME)) {
                    //Google Photo 圖片選擇優先使用
                    if (googlePicker(callingActivity, requestCode, intent, resolveInfoList)) {
                        return true;
                    } else {
                        defaultPicker(callingActivity, requestCode, intent, resolveInfoList);
                    }
                } else {
                    //沒有Google Photo 則預設使用第一個圖片選擇程式
                    return defaultPicker(callingActivity, requestCode, intent, resolveInfoList);
                }

            }

        }
        return false;
    }

    public static boolean launchGooglePhotosPicker(Activity callingActivity, Fragment fragment, int requestCode) {
        if (callingActivity != null) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_PICK);
            intent.setType("image/*");
            List<ResolveInfo> resolveInfoList = callingActivity.getPackageManager().queryIntentActivities(intent, 0);

            if (!resolveInfoList.isEmpty()) {
                if (isPackageInstalled(callingActivity, GOOGLE_PHOTOS_PACKAGE_NAME)) {
                    if (googlePicker(fragment, requestCode, intent, resolveInfoList)) {
                        return true;
                    } else {
                        defaultPicker(fragment, requestCode, intent, resolveInfoList);
                    }
                } else {
                    //沒有Google Photo 則預設使用第一個圖片選擇程式
                    defaultPicker(fragment, requestCode, intent, resolveInfoList);
                }
            }
        }
        return false;
    }

    private static boolean googlePicker(Activity callingActivity, int requestCode, Intent intent, List<ResolveInfo> resolveInfoList) {
        for (int i = 0; i < resolveInfoList.size(); i++) {
            if (resolveInfoList.get(i) != null) {
                String packageName = resolveInfoList.get(i).activityInfo.packageName;
                if (GOOGLE_PHOTOS_PACKAGE_NAME.equals(packageName)) {
                    intent.setComponent(new ComponentName(packageName, resolveInfoList.get(i).activityInfo.name));
                    callingActivity.startActivityForResult(intent, requestCode);
                    return true;
                }
            }
        }
        return false;
    }

    private static boolean googlePicker(Fragment fragment, int requestCode, Intent intent, List<ResolveInfo> resolveInfoList) {
        for (int i = 0; i < resolveInfoList.size(); i++) {
            if (resolveInfoList.get(i) != null) {
                String packageName = resolveInfoList.get(i).activityInfo.packageName;
                if (GOOGLE_PHOTOS_PACKAGE_NAME.equals(packageName)) {
                    intent.setComponent(new ComponentName(packageName, resolveInfoList.get(i).activityInfo.name));
                    fragment.startActivityForResult(intent, requestCode);
                    return true;
                }
            }
        }
        return false;
    }

    private static boolean defaultPicker(Activity callingActivity, int requestCode, Intent intent, List<ResolveInfo> resolveInfoList) {
        try {
            String packageName = resolveInfoList.get(0).activityInfo.packageName;
            intent.setComponent(new ComponentName(packageName, resolveInfoList.get(0).activityInfo.name));
            callingActivity.startActivityForResult(intent, requestCode);
            return true;
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    private static boolean defaultPicker(Fragment fragment, int requestCode, Intent intent, List<ResolveInfo> resolveInfoList) {
        try {
            String packageName = resolveInfoList.get(0).activityInfo.packageName;
            intent.setComponent(new ComponentName(packageName, resolveInfoList.get(0).activityInfo.name));
            fragment.startActivityForResult(intent, requestCode);
            return true;
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void startCropImage(Activity activity, File mFileTemp, int requestCode) {
        //UCrop setting
        Uri uri = Uri.parse("file://" + mFileTemp.getPath());
        String fileName = mFileTemp.getName();
        String path = Environment.getExternalStorageDirectory().getAbsolutePath();
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(path, fileName)));
        uCrop.withAspectRatio(1, 1);
        uCrop.withMaxResultSize(500, 500);
        Intent intent = new Intent();
        intent.setClass(activity, UCropActivity.class);
        Bundle bundle = uCrop.getIntent(activity).getExtras();
        bundle.putBoolean(UCrop.Options.EXTRA_SHOW_CROP_FRAME, true);
        bundle.putBoolean(UCrop.Options.EXTRA_SHOW_CROP_GRID, true);
        bundle.putBoolean(UCrop.Options.EXTRA_CIRCLE_DIMMED_LAYER, false);
        intent.putExtras(bundle);

        activity.startActivityForResult(intent, requestCode);
    }

    public static void startCropImage(Activity activity, File mFileTemp, int requestCode, float aspectRatio) {
        //UCrop setting
        Uri uri = Uri.parse("file://" + mFileTemp.getPath());
        String fileName = mFileTemp.getName();
        String path = Environment.getExternalStorageDirectory().getAbsolutePath();
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(path, fileName)));
        uCrop.withAspectRatio(1, 1);
        uCrop.withMaxResultSize(1080, 608);
        Intent intent = new Intent();
        intent.setClass(activity, UCropActivity.class);
        Bundle bundle = uCrop.getIntent(activity).getExtras();
        bundle.putFloat(UCropActivity.ASPECT_RATIO, aspectRatio);
        bundle.putBoolean(UCrop.Options.EXTRA_SHOW_CROP_FRAME, true);
        bundle.putBoolean(UCrop.Options.EXTRA_SHOW_CROP_GRID, true);
        bundle.putBoolean(UCrop.Options.EXTRA_CIRCLE_DIMMED_LAYER, false);
        intent.putExtras(bundle);

        activity.startActivityForResult(intent, requestCode);
    }

    public static void getImageFromUri(Activity activity, Intent data, File mFileTemp) throws IOException {
        InputStream inputStream = activity.getContentResolver().openInputStream(data.getData());
        FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
        copyStream(inputStream, fileOutputStream);
        fileOutputStream.close();
        if (inputStream != null) {
            inputStream.close();
        }
    }

    private static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    private static boolean isPackageInstalled(Context context, String packageName) {
        PackageManager packageManager = context.getPackageManager();
        try {
            return packageManager.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES) != null;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}