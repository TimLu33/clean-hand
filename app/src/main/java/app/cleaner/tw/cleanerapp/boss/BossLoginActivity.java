package app.cleaner.tw.cleanerapp.boss;


import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.util.Arrays;

import app.cleaner.tw.cleanerapp.R;
import app.cleaner.tw.cleanerapp.backend.cleaner.api.API;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.AccessTokenBean;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.AuthTokenBean;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.CleanerProfile;
import app.cleaner.tw.cleanerapp.backend.cleaner.models.member.User;
import app.cleaner.tw.cleanerapp.backend.drfapi.models.ApiError;
import app.cleaner.tw.cleanerapp.base.BaseActivity;
import app.cleaner.tw.cleanerapp.common.Constants;
import app.cleaner.tw.cleanerapp.common.Preference;
import app.cleaner.tw.cleanerapp.common.onBackPresenter;
import app.cleaner.tw.cleanerapp.common.viewmodel.LoginViewModel;
import app.cleaner.tw.cleanerapp.databinding.ActivityBossLoginBinding;
import app.cleaner.tw.cleanerapp.forgot.ForgotPwdStep1Activity;
import app.cleaner.tw.cleanerapp.helper.GSONUtils;
import app.cleaner.tw.cleanerapp.register.RegisterTermActivity;
import app.cleaner.tw.cleanerapp.register.onRegisterSuccessEvent;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BossLoginActivity extends BaseActivity {

    private final int GOOGLE_LOGIN_REQUEST_CODE = 1001;
    private final int GOOGLE_LOGIN_REQUEST_CODE_2 = 1002;

    private ActivityBossLoginBinding binding;

    private LoginViewModel viewModel;

    public CallbackManager callBack;

    public static void start(Context context) {
        Intent starter = new Intent(context, BossLoginActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_boss_login);
        EventBus.getDefault().register(this);
        setUpToolBar();
        binding.setPresenter(new Presenter());
        viewModel = new LoginViewModel();
        binding.setViewModel(viewModel);
    }

    private void setUpToolBar() {
        binding.appBar.title.setText("雇主登入");
        binding.appBar.btnBack.setVisibility(View.VISIBLE);
        binding.appBar.btnBack.setOnClickListener(new onBackPresenter(this));
    }

    private void showRegRepeatDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setMessage("此帳號已註冊為”我是清潔手”，謝謝！");
        builder.setPositiveButton("確定", null);
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == GOOGLE_LOGIN_REQUEST_CODE) {
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                GoogleSignInAccount account;
                try {
                    account = task.getResult(ApiException.class);
                    final GoogleSignInAccount finalAccount = account;
                    final String scopes = "oauth2:" + Scopes.PLUS_LOGIN + " " + Scopes.PLUS_ME + " " + Scopes.PROFILE + " " + Scopes.EMAIL;

                    AsyncTask<Void, Void, String> tasks = new AsyncTask<Void, Void, String>() {
                        @Override
                        protected String doInBackground(Void... params) {
                            String token = null;

                            try {
                                token = GoogleAuthUtil.getToken(activity, finalAccount.getAccount(), scopes);
                            } catch (IOException transientEx) {
                                transientEx.printStackTrace();
                            } catch (UserRecoverableAuthException e) {
                                startActivityForResult(e.getIntent(), GOOGLE_LOGIN_REQUEST_CODE_2);
                            } catch (GoogleAuthException authEx) {
                                authEx.printStackTrace();
                            }
                            return token;
                        }

                        @Override
                        protected void onPostExecute(final String token) {
                            if (token != null) {
                                final KProgressHUD progressHUD = KProgressHUD.create(activity).show();

                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            AccessTokenBean model = new AccessTokenBean(token, "");
                                            RequestBody body = RequestBody.create(MediaType.parse("application/json"), GSONUtils.toJson(model));
                                            API.sharedService().login_google(body).enqueue(new Callback<AuthTokenBean>() {
                                                @Override
                                                public void onResponse(Call<AuthTokenBean> call, Response<AuthTokenBean> response) {
                                                    if (response.isSuccessful()) {
                                                        thirdPartLogin(response, progressHUD);
                                                    } else {
                                                        final ApiError apiError = API.shared().parseError(response);
                                                        runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                progressHUD.dismiss();
                                                                Toast.makeText(activity, apiError.getHudMessage(), Toast.LENGTH_SHORT).show();
                                                            }
                                                        });
                                                        Logger.e(API.shared().parseError(response).getHudMessage());
                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<AuthTokenBean> call, Throwable t) {
                                                    progressHUD.dismiss();
                                                    Logger.e(t.getLocalizedMessage());
                                                }
                                            });
                                        } catch (Exception e) {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    progressHUD.dismiss();
                                                }
                                            });
                                            e.printStackTrace();
                                        }
                                    }
                                }).start();
                            }
                        }

                    };
                    tasks.execute();
                } catch (ApiException e) {
                    e.printStackTrace();
                }

            } else if (requestCode == GOOGLE_LOGIN_REQUEST_CODE_2) {

            } else {
                callBack.onActivityResult(requestCode, resultCode, data);
            }
        }

    }

    private void thirdPartLogin(Response<AuthTokenBean> response, final KProgressHUD progressHUD) {
        API.shared().setAuthorizationToken(response.body().getToken());
        Preference.setTempToken(activity, response.body().getToken());

        API.sharedService().getUser(API.sharedAuthorizationToken()).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                progressHUD.dismiss();
                if (response.isSuccessful()) {
                    final User user = response.body();
                    final CleanerProfile cleanerProfile = user.getCleaner_profile();

                    if (cleanerProfile != null) {
                        //有hireProfile 已註冊為清潔手
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressHUD.dismiss();
                                showRegRepeatDialog();
                            }
                        });
                    } else {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    progressHUD.dismiss();
                                    Logger.d("FireBaseToken:" + FirebaseInstanceId.getInstance().getToken());
                                    final String android_id = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
                                    Logger.d(android_id);
                                    final String fcm_token = FirebaseInstanceId.getInstance().getToken();
                                    API.sharedService().registerFcmDevice(API.sharedAuthorizationToken(), android_id, fcm_token, "android", true).execute();
                                    Preference.setHirerToken(activity, Preference.getTempToken(activity));
                                    BossHomeActivity.start(activity);
                                    finish();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }).start();
                    }
                } else {
                    Logger.e(API.shared().parseError(response).getHudMessage());
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                progressHUD.dismiss();
                Logger.e(t.getLocalizedMessage());
            }
        });
    }

    public class Presenter implements View.OnClickListener {

        boolean isShowPwd = false;

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.img_forgot:
                    isShowPwd = !isShowPwd;
                    binding.inputPwd.setTransformationMethod(isShowPwd ? HideReturnsTransformationMethod.getInstance() : PasswordTransformationMethod.getInstance());
                    break;
                case R.id.btn_forgot_pwd:
                    ForgotPwdStep1Activity.start(activity, Constants.BOSS);
                    break;
                case R.id.btn_fb:
                    Logger.d("btn fb");
                    FacebookSdk.sdkInitialize(activity);
                    callBack = CallbackManager.Factory.create();
                    LoginManager.getInstance().registerCallback(callBack, new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(final LoginResult loginResult) {
                            final KProgressHUD progressHUD = KProgressHUD.create(activity).show();
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Logger.d(loginResult.getAccessToken().getToken());
                                        AccessTokenBean model = new AccessTokenBean(loginResult.getAccessToken().getToken(), "");
                                        RequestBody body = RequestBody.create(MediaType.parse("application/json"), GSONUtils.toJson(model));
                                        API.sharedService().login_facebook(body).enqueue(new Callback<AuthTokenBean>() {
                                            @Override
                                            public void onResponse(Call<AuthTokenBean> call, Response<AuthTokenBean> response) {
                                                if (response.isSuccessful()) {
                                                    thirdPartLogin(response, progressHUD);
                                                } else {
                                                    final ApiError apiError = API.shared().parseError(response);
                                                    runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            progressHUD.dismiss();
                                                            Toast.makeText(activity, apiError.getHudMessage(), Toast.LENGTH_SHORT).show();
                                                        }
                                                    });
                                                    Logger.e(API.shared().parseError(response).getHudMessage());
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<AuthTokenBean> call, Throwable t) {
                                                Logger.e(t.getLocalizedMessage());
                                            }
                                        });
                                    } catch (Exception e) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                progressHUD.dismiss();
                                            }
                                        });
                                        e.printStackTrace();
                                    }
                                }
                            }).start();
                        }

                        @Override
                        public void onCancel() {
                        }

                        @Override
                        public void onError(FacebookException error) {
                        }
                    });
                    LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList("public_profile", "email"));
                    break;
                case R.id.btn_google:
                    GoogleSignInOptions gso = new GoogleSignInOptions.Builder()
                            .requestIdToken(getString(R.string.google_oauth_client_id))
                            .requestId()
                            .requestEmail()
                            .build();
                    GoogleSignInClient signInClient = GoogleSignIn.getClient(activity, gso);
                    startActivityForResult(signInClient.getSignInIntent(), GOOGLE_LOGIN_REQUEST_CODE);
                    break;
                case R.id.btn_login:
                    bossLogin();
                    break;
                case R.id.btn_reg:
                    RegisterTermActivity.start(activity, Constants.BOSS);
                    break;
            }
        }
    }

    private void bossLogin() {
        final KProgressHUD progressHUD = KProgressHUD.create(this).show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Response<AuthTokenBean> response = API.sharedService().login(viewModel.acc.get(), viewModel.pwd.get()).execute();
                    if (response.isSuccessful()) {
                        API.shared().setAuthorizationToken(response.body().getToken());
                        Preference.setTempToken(activity, response.body().getToken());

                        API.sharedService().getUser(API.sharedAuthorizationToken()).enqueue(new Callback<User>() {
                            @Override
                            public void onResponse(Call<User> call, Response<User> response) {
                                if (response.isSuccessful()) {
                                    final User user = response.body();
                                    final CleanerProfile cleanerProfile = user.getCleaner_profile();
                                    if (cleanerProfile != null) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                progressHUD.dismiss();
                                                showRegRepeatDialog();
                                            }
                                        });
                                    } else {
                                        progressHUD.dismiss();
                                        Preference.setHirerToken(activity, Preference.getTempToken(activity));
                                        BossHomeActivity.start(activity);
                                        finish();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<User> call, Throwable t) {
                                progressHUD.dismiss();
                                Logger.e(t.getLocalizedMessage());
                                Toast.makeText(activity, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });

                        Logger.d("FireBaseToken:" + FirebaseInstanceId.getInstance().getToken());
                        String android_id = Settings.Secure.getString(activity.getContentResolver(),
                                Settings.Secure.ANDROID_ID);
                        String fcm_token = FirebaseInstanceId.getInstance().getToken();
                        API.sharedService().registerFcmDevice(API.sharedAuthorizationToken(), android_id, fcm_token, "android", true).execute();

                    } else {
                        final ApiError apiError = API.shared().parseError(response);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressHUD.dismiss();
                                Toast.makeText(BossLoginActivity.this, apiError.getHudMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                        Logger.e(API.shared().parseError(response).getHudMessage());
                    }
                } catch (IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressHUD.dismiss();
                        }
                    });
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRegSuccessEvent(onRegisterSuccessEvent event) {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
